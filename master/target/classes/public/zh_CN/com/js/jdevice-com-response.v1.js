/**
 * Created by LinChunSen on 2020/7/28.
 */

/**
 * 需要重新登录
 * */
function response_needLogin(data) {
    if(data.head=="LOGIN_NEED"){
        return true;
    }
    return false;
}
/**
 * 登录成功
 * */
function response_loginSuccess(data) {
    if(data.head=="LOGIN_SUCCESS"){
        return true;
    }
    return false;
}
/**
 * 退出登录成功
 * */
function response_logoutSuccess(data) {
    if(data.head=="LOGOUT_SUCCESS"){
        return true;
    }
    return false;
}
/**
 * 登录失败
 * */
function response_loginError(data) {
    if(data.head=="LOGIN_ERROR"){
        return true;
    }
    return false;
}
/**
 * 登录验证码错误
 * */
function response_loginVCodeError(data) {
    if(data.head=="LOGIN_ERROR_VCODE"){
        return true;
    }
    return false;
}
/**
 * 登录失败：账号未注册
 * */
function response_loginErrorNotRegister(data) {
    if(data.head=="LOGIN_ERROR_NOT_REGISTER"){
        return true;
    }
    return false;
}
/**
 * 登录密码重置成功
 * */
function response_resetLoginPwdSuccess(data) {
    if(data.head=="LOGIN_RESET_PWD_SUCCESS"){
        return true;
    }
    return false;
}
/**
 * 登录密码重置失败
 * */
function response_resetLoginPwdFail(data) {
    if(data.head=="LOGIN_RESET_PWD_FAIL"){
        return true;
    }
    return false;
}
/**
 * 登录手机号重置成功
 * */
function response_resetLoginPhoneSuccess(data) {
    if(data.head=="LOGIN_RESET_PHONE_SUCCESS"){
        return true;
    }
    return false;
}
/**
 * 登录手机号重置失败
 * */
function response_resetLoginPhoneFail(data) {
    if(data.head=="LOGIN_RESET_PHONE_FAIL"){
        return true;
    }
    return false;
}
/**
 * 账号注册成功
 * */
function response_registerSuccess(data) {
    if(data.head=="REGISTER_SUCCESS"){
        return true;
    }
    return false;
}
/**
 * 账号注册失败
 * */
function response_registerFail(data) {
    if(data.head=="REGISTER_FAIL"){
        return true;
    }
    return false;
}
/**
 * 账号注册失败：账号已存在
 * */
function response_registerErrorUidExist(data) {
    if(data.head=="REGISTER_ERROR_UID_EXIST"){
        return true;
    }
    return false;
}
/**
 * 账号注册失败：账号
 * */
function response_registerErrorUid(data) {
    if(data.head=="REGISTER_ERROR_UID"){
        return true;
    }
    return false;
}
/**
 * 注册失败：验证码错误
 * */
function response_registerErrorVCode(data) {
    if(data.head=="REGISTER_ERROR_VCODE"){
        return true;
    }
    return false;
}
/**
 * 账号已禁用
 * */
function response_userDisable(data) {
    if(data.head=="USER_DISABLE"){
        return true;
    }
    return false;
}
/**
 * 操作成功
 * */
function response_successDone(data) {
    if(data.head=="SUCCESS_DONE"){
        return true;
    }
    return false;
}
/**
 * 操作失败
 * */
function response_failDone(data) {
    if(data.head=="FAIL_DONE"){
        return true;
    }
    return false;
}
/**
 * 参数错误
 * */
function response_errorParam(data) {
    if(data.head=="ERROR_PARAM"){
        return true;
    }
    return false;
}
/**
 * 功能尚未实现
 * */
function response_needImplement(data) {
    if(data.head=="NEED_IMPLEMENT"){
        return true;
    }
    return false;
}
/**
 * 需要完善用户信息
 * */
function response_needPerfectUserInfo(data) {
    if(data.head=="LOGIN_NEED_PERFECT_INFO"){
        return true;
    }
    return false;
}
/**
 * 未加入任何组织
 * */
function response_noOrganization(data) {
    if(data.head=="NO_ORGANIZATION"){
        return true;
    }
    return false;
}
/**
 * 正在组织审核中
 * */
function response_underReviewByOrganization(data) {
    if(data.head=="UNDER_REVIEW_BY_ORGANIZATION"){
        return true;
    }
    return false;
}
/**
 * 没有权限
 * */
function response_noPermission(data) {
    if(data.head=="NO_PERMISSION"){
        return true;
    }
    return false;
}
/**
 * 无数据返回
 * */
function response_notFoundData(data) {
    if(data.head=="NOT_FOUND_DATA"){
        return true;
    }
    return false;
}