/**
 * Created by LinChunSen on 2019/11/25.
 */
function getUserInfo() {
    $.post("/login/getLoginUserModel",{},function (data) {
        if(response_loginSuccess(data)){
            $("#login-li").hide();
            $("#logout-li").show();
        }else {
            alert("请先登录！！");
            window.location.href="/";
        }
    });
}
function showLoginUserInfo() {
    $.post("/login/getLoginUserModel",{},function (data) {
        if(response_loginSuccess(data)){
            $("#login-li").hide();
            $("#logout-li").show();
        }else {
            $("#login-li").show();
            $("#logout-li").hide();
        }
    });
}

function logout() {
    $.post("/login/logout",{},function (data) {
        if(response_successDone(data)||response_logoutSuccess(data)){
            alert("退出登录成功");
            window.location.href="/";
        }else {
            alert(data.Msg);
        }
    });
}

function isPhoneNumber(tel) {
    var reg =/^0?1[3|4|5|6|7|8][0-9]\d{8}$/;
    return reg.test(tel);
}
var LoginUser;
function getLoginUser() {
    //获取当前登录用户
    $.post("/login/getLoginUserModel",{},function (data) {
        if(response_loginSuccess(data)){
            LoginUser=data.body;
            showSideNavByUser(data.body);
        }else {
            window.location.href="../view/login.html";
        }
    });
}
function getLoginUserAndShowNav(callback) {
    //获取当前登录用户
    $.post("/login/getLoginUserModel",{},function (data) {
        if(response_loginSuccess(data)){
            var user=data.body;
            showSideNavByUser(user);
            callback(user);
        }else if(response_needPerfectUserInfo(data)){
        window.location.href="../view/personalSet.html";
        }else if(response_noOrganization(data)){
            window.location.href="../view/organizationCenter.html";
        }else if(response_underReviewByOrganization(data)){
            window.location.href="../view/organizationCenter.html";
        }else{
            window.location.href="../view/login.html?login_return="+window.location;
        }
    });
}
function showSideNavByUser(user) {
    if(user==undefined||user==null){
        return;
    }
    /**
     * 用户头像
     * */
    $("#sideNav-user-name").html(user.name);
    $("#sideNav-user-name").each(function (){
        if($("#sideNav-user-name").text().length>13){
            $(this).text($(this).text().substring(0,13));
            $(this).html($(this).html()+'...');
        }
    });
    var sideNavUserRoleDisc="";
    if(user.roleLevel==64){
        sideNavUserRoleDisc="系统管理员";
    }else if(user.roleLevel==32){
        sideNavUserRoleDisc="组织管理员";
    }else if(user.roleLevel==16){
        sideNavUserRoleDisc="工厂管理员";
    }else if(user.roleLevel==8){
        sideNavUserRoleDisc="车间管理员";
    }else if(user.roleLevel==4){
        sideNavUserRoleDisc="工作中心管理员";
    }else if(user.roleLevel==2){
        sideNavUserRoleDisc="普通用户";
    }else {
        sideNavUserRoleDisc="浏览访客";
    }
    if(user.status==16){
        sideNavUserRoleDisc="未加入组织";
    }else if(user.status==8){
        sideNavUserRoleDisc="组织审核中";
    }else if(user.status==4){
        sideNavUserRoleDisc="账号已禁用";
    }
    $("#sideNav-user-roleDisc").html(sideNavUserRoleDisc);
    if(user.photoUrl!=null&&user.photoUrl!=undefined&&user.photoUrl.trim().substr(0, 7)=="/upload"){
        $("#sideNav-user-photoImg").attr("src",user.photoUrl);
    }else{

    }
    /**
     * 侧边导航
     * */
    if(user.roleLevel<2){
        $(".sideNav-li-role-user").hide();
    }else {
        $(".sideNav-li-role-user").show();
        if(user.roleLevel>=64){
            $(".sideNav-li-role-system").show();
        }
    }
    /**
     * 顶部导航
     * */
}

function isSuccessResponse(data) {
    if(data.head=="SUCCESS_DONE"){
        return true;
    }else{
        return false;
    }
}

function getSuccessResponseAndAlert(data) {
    if(data.head=="SUCCESS_DONE"){
        alert("操作成功！！");
        return true;
    }else if(data.head==="LOGIN_NEED"){
        alert("请先登录！！");
        window.location.href="login.html?login_return="+window.location;
        return false;
    }else if(data.head==="ERROR_PARAM"){
        alert("参数错误！！");
        return false;
    }else if(data.head==="NO_PERMISSION"){
        alert("权限不足！！");
        return false;
    }else if(data.head==="USER_DISABLE"){
        alert("账号已被禁用！！");
        return false;
    }else {
        alert("操作失败！！");
        return false;
    }
}

function getDeviceLogoUrl(logoUrl) {
    if(logoUrl!=undefined&&logoUrl!=null&&logoUrl.trim().substr(0, 7)=="/upload"){
        return logoUrl;
    }else {
        return '../img/device_default.jpg';
    }
}

/**
 * 获取到操作成功的返回，异常弹出提示
 * */
function getSuccessResponse(data) {
    if(response_successDone(data)){
        return true;
    }else if(response_needLogin(data)){
        alert("请先登录！！");
        window.location.href="../view/login.html?login_return="+window.location;
        return false;
    }else if(response_errorParam(data)){
        alert("参数错误！！");
        return false;
    }else if(response_noPermission(data)){
        alert("权限不足！！");
        return false;
    }else {
        alert("操作失败！！");
        return false;
    }
}

function getDeviceStatusColorByStatus(status) {
    if(status==="Run"){
        return "#00d036";
    }else if(status=="Pending"){
        return "#F7B233";
    }else if(status=="Wait"){
        return "#F7B233";
    }else if(status=="Fault"){
        return "#FFEB00";
    }else if(status=="Stop"){
        return "#6F7072";
    }else if(status=="Maintain"){
        return "#009DE0";
    }else if(status=="Sealup"){
        return "#AAAAAA";
    }else if(status=="Offline"){
        return "#AAAAAA";
    }else if(status=="Repair"){
        return "#E1001A";
    }
}

function getDeviceStatusIconWithColorByStatus(status) {
    //运行
    if(status==="Run"){
        return "<i class='fa fa-chevron-circle-right fa-fw' style='color:#00d036'></i>";
    }
    //待料
    else if(status=="Pending"){
        return "<i class='fa fa-spinner fa-fw' style='color:#F7B233'></i>";
    }
    //待机
    else if(status=="Wait"){
        return "<i class='fa fa-spinner fa-fw' style='color:#F7B233'></i>";
    }
    //故障
    else if(status=="Fault"){
        return "<i class='fa fa-warning fa-fw' style='color:#FFEB00'></i>";
    }
    //停机
    else if(status=="Stop"){
        return "<i class='fa fa-power-off fa-fw' style='color:#6F7072'></i>";
    }
    //保养
    else if(status=="Maintain"){
        return "<i class='fa fa-calendar fa-fw' style='color:#009DE0'></i>";
    }
    //封存
    else if(status=="Sealup"){
        return "<i class='fa fa-database fa-fw' style='color:#AAAAAA'></i>";
    }
    //离线
    else if(status=="Offline"){
        return "<i class='fa fa-bell-slash fa-fw' style='color:#AAAAAA'></i>";
    }
    //维修
    else if(status=="Repair"){
        return "<i class='fa fa-wrench fa-fw' style='color:#E1001A'></i>";
    }
}

function getDeviceStatusIconByStatus(status) {
    if(status==="Run"){
        return "<i class='fa fa-chevron-circle-right fa-fw'></i>";
    }else if(status=="Pending"){
        return "<i class='fa fa-spinner fa-fw'></i>";
    }else if(status=="Wait"){
        return "<i class='fa fa-spinner fa-fw'></i>";
    }else if(status=="Fault"){
        return "<i class='fa fa-warning fa-fw'></i>";
    }else if(status=="Stop"){
        return "<i class='fa fa-power-off fa-fw'></i>";
    }else if(status=="Maintain"){
        return "<i class='fa fa-calendar fa-fw'></i>";
    }else if(status=="Sealup"){
        return "<i class='fa fa-database fa-fw'></i>";
    }else if(status=="Offline"){
        return "<i class='fa fa-bell-slash fa-fw'></i>";
    }else if(status=="Repair"){
        return "<i class='fa fa-wrench fa-fw'></i>";
    }
}

//调用各个浏览器提供的全屏方法
function handleFullScreen() {
    var de = document.documentElement;
    if (de.requestFullscreen) {
        de.requestFullscreen();
    } else if (de.mozRequestFullScreen) {
        de.mozRequestFullScreen();
    } else if (de.webkitRequestFullScreen) {
        de.webkitRequestFullScreen();
    } else if (de.msRequestFullscreen) {
        de.msRequestFullscreen();
    }

};
//调用各个浏览器提供的退出全屏方法
function exitFullscreen() {
    if(document.exitFullscreen) {
        document.exitFullscreen();
    } else if(document.mozCancelFullScreen) {
        document.mozCancelFullScreen();
    } else if(document.webkitExitFullscreen) {
        document.webkitExitFullscreen();
    }
}