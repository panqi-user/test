/**
 * Created by LinChunSen on 2020/3/18.
 */
function isPhoneNumber(tel) {
    var reg =/^0?1[3|4|5|6|7|8][0-9]\d{8}$/;
    return reg.test(tel);
}
function isSuccessResponse(data) {
    if(isNeedLogin(data)){
        window.location.href="/zh_CN/app/default/loginUseVCode.html?login_return="+window.location;
        return false;
    }
    if(data.head=="SUCCESS_DONE"){
        return true;
    }else if(data.head=="LOGIN_SUCCESS"){
        return true;
    }else{
        return false;
    }
}
function isClaimedAndonMessage(msg) {
    if(isNull(msg)){
        return false;
    }
    if(msg.status==4){
        return true;
    }
    return false;
}
function isNotClaimAndonMessage(msg) {
    if(isNull(msg)){
        return false;
    }
    if(msg.status==8){
        return true;
    }
    return false;
}
function isClosedAndonMessage(msg) {
    if(isNull(msg)){
        return false;
    }
    if(msg.status==0){
        return true;
    }
    return false;
}
function isNotClosedAndonMessage(msg) {
    if(isNull(msg)){
        return false;
    }
    if(msg.status==2){
        return true;
    }
    return false;
}
function needPerfectInfo(data) {
    if(data.head==="LOGIN_NEED_PERFECT_INFO"){
        return true;
    }
    return false;
}
function isNoOrganization(data) {
    if(data.head==="NO_ORGANIZATION"){
        return true;
    }
    return false;
}
function underReviewByOrganization(data) {
    if(data.head==="UNDER_REVIEW_BY_ORGANIZATION"){
        return true;
    }
    return false;
}
function isNeedLogin(data) {
    if(data.head==="LOGIN_NEED"){
        return true;
    }
    return false;
}
function noPermission(data) {
    if(data.head==="NO_PERMISSION"){
        return true;
    }
    return false;
}
function andonHadClaimed(data) {
    if(data.head==="ANDON_HAD_CLAIMED"){
        return true;
    }
    return false;
}

function getDeviceStatusColorByStatus(status) {
    if(status==="Run"){
        return "#009036";
    }else if(status=="Pending"){
        return "#F7B233";
    }else if(status=="Wait"){
        return "#F7B233";
    }else if(status=="Fault"){
        return "#FFEB00";
    }else if(status=="Stop"){
        return "#6F7072";
    }else if(status=="Maintain"){
        return "#009DE0";
    }else if(status=="Sealup"){
        return "#AAAAAA";
    }else if(status=="Offline"){
        return "#AAAAAA";
    }else if(status=="Repair"){
        return "#E1001A";
    }
}

function getDeviceStatusFontColorByStatus(status) {
    if(status==="Run"){
        return "#ffffff";
    }else if(status=="Pending"){
        return "#ffffff";
    }else if(status=="Wait"){
        return "#ffffff";
    }else if(status=="Fault"){
        return "#aaaaaa";
    }else if(status=="Stop"){
        return "#ffffff";
    }else if(status=="Maintain"){
        return "#ffffff";
    }else if(status=="Sealup"){
        return "#ffffff";
    }else if(status=="Offline"){
        return "#ffffff";
    }else if(status=="Repair"){
        return "#ffffff";
    }
}

function getDeviceStatusIconWithColorByStatus(status) {
    //运行
    if(status==="Run"){
        return "<i class='fa fa-chevron-circle-right' style='color:#009036'></i>";
    }
    //待料
    else if(status=="Pending"){
        return "<i class='fa fa-spinner' style='color:#F7B233'></i>";
    }
    //待机
    else if(status=="Wait"){
        return "<i class='fa fa-spinner' style='color:#F7B233'></i>";
    }
    //故障
    else if(status=="Fault"){
        return "<i class='fa fa-warning' style='color:#FFEB00'></i>";
    }
    //停机
    else if(status=="Stop"){
        return "<i class='fa fa-power-off' style='color:#6F7072'></i>";
    }
    //保养
    else if(status=="Maintain"){
        return "<i class='fa fa-calendar' style='color:#009DE0'></i>";
    }
    //封存
    else if(status=="Sealup"){
        return "<i class='fa fa-database' style='color:#AAAAAA'></i>";
    }
    //离线
    else if(status=="Offline"){
        return "<i class='fa fa-bell-slash' style='color:#AAAAAA'></i>";
    }
    //维修
    else if(status=="Repair"){
        return "<i class='fa fa-wrench' style='color:#E1001A'></i>";
    }
}

function getDeviceStatusIconByStatus(status) {
    if(status==="Run"){
        return "<i class='fa fa-chevron-circle-right'></i>";
    }else if(status=="Pending"){
        return "<i class='fa fa-spinner'></i>";
    }else if(status=="Wait"){
        return "<i class='fa fa-spinner'></i>";
    }else if(status=="Fault"){
        return "<i class='fa fa-warning'></i>";
    }else if(status=="Stop"){
        return "<i class='fa fa-power-off'></i>";
    }else if(status=="Maintain"){
        return "<i class='fa fa-calendar'></i>";
    }else if(status=="Sealup"){
        return "<i class='fa fa-database'></i>";
    }else if(status=="Offline"){
        return "<i class='fa fa-bell-slash'></i>";
    }else if(status=="Repair"){
        return "<i class='fa fa-wrench'></i>";
    }
}

function getLoginReturnUrl(defaultUrl) {
    var url = document.location.toString();
    var arrObj = url.split("?login_return=");
    if(arrObj.length > 1){
        if(isEmptyString(arrObj[1])){
            return defaultUrl;
        }
        return arrObj[1];
    }else {
        return defaultUrl;
    }
}

function findUrlParam(url,paraName) {
    var arrObj = url.split("?");
    if (arrObj.length > 1) {
        var arrPara = arrObj[1].split("&");
        var arr;
        for (var i = 0; i < arrPara.length; i++) {
            arr = arrPara[i].split("=");
            if (arr != null && arr[0] == paraName) {
                return arr[1];
            }
        }
        return "";
    }
    else {
        return "";
    }
}

function getDeviceLogoUrl(logoUrl) {
    if(logoUrl!=undefined&&logoUrl!=null&&logoUrl.trim().substr(0, 7)=="/upload"){
        return logoUrl;
    }else {
        return '../img/device_default.jpg';
    }
}
function getLogoUrl(logoUrl) {
    if(logoUrl!=undefined&&logoUrl!=null&&logoUrl.trim().substr(0, 7)=="/upload"){
        return logoUrl;
    }else {
        return '../img/noImage.jpg';
    }
}

function getUserRoleShortName(roleLevel) {
    var roleDisc="";
    if(roleLevel==64){
        roleDisc="系";
    }else if(roleLevel==32){
        roleDisc="总";
    }else if(roleLevel==16){
        roleDisc="厂";
    }else if(roleLevel==8){
        roleDisc="任";
    }else if(roleLevel==4){
        roleDisc="长";
    }else if(roleLevel==2){
        roleDisc="工";
    }else{
        roleDisc="审";
    }
    return roleDisc;
}
function getUserStatusColor(statusVal) {
    var statusDisc="";
    if(statusVal==2){
        statusDisc="#009036";
    }else if(statusVal==4){
        statusDisc="#AA0000";
    }else if(statusVal==8){
        statusDisc="#F7B233";
    }
    return statusDisc;
}
function logout() {
    $.post("/login/logout",{},function (data) {
        if(data.head==="LOGOUT_SUCCESS"){
            window.location.href="/zh_CN/app/default/loginUseVCode.html";
        }else {
            alert(data.Msg);
        }
    });
}

function isEmptyString(str) {
    if(str==undefined||str==null||str.trim()==""){
        return true;
    }
    return false;
}

// num传入的数字，n需要的字符长度
function prefixInteger(num, n) {
    return (Array(n).join(0) + num).slice(-n);
}

function notEmptyString(str) {
    if(str!=undefined&&str!=null&&str.trim()!=""){
        return true;
    }
    return false;
}

function isNull(obj) {
    if(obj==undefined||obj==null){
        return true;
    }
    return false;
}

function syncNoticePojo() {
    $.post("/notice/getNoticePojo",{},function (data) {
        if(data.head=="SUCCESS_DONE"){
            if(data.body.messageNum>0){
                $(".nav-msg-unread-sum").show();
                $(".nav-msg-unread-sum").html(data.body.messageNum);
            }
            if(data.body.userReviewNum>0){
                $(".nav-user-review-sum").show();
                $(".nav-user-review-sum").html(data.body.userReviewNum);
            }
        }
    });
}

function getFormatDateNoYear() {
    var date = new Date();
    var month = date.getMonth() + 1;
    var strDate = date.getDate();
    if (month >= 1 && month <= 9) {
        month = "0" + month;
    }
    if (strDate >= 0 && strDate <= 9) {
        strDate = "0" + strDate;
    }
    var currentDate = month + "-" + strDate
        + " " + date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
    return currentDate;
}