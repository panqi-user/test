/**
 * Created by LinChunSen on 2020/7/28.
 */

function dateFormatFormLongTimeMs(longTimeMs) {
    var jdatetimeType = "";
    var jdatetime = new Date();
    jdatetime.setTime(longTimeMs);
    jdatetimeType+= jdatetime.getFullYear();  //年
    jdatetimeType+= "-" + ((jdatetime.getMonth()+1)>=10?(jdatetime.getMonth()+1):"0"+(jdatetime.getMonth()+1)); //月
    jdatetimeType += "-" + (jdatetime.getDate()>=10?jdatetime.getDate():"0"+jdatetime.getDate());  //日
    jdatetimeType+= "  " + (jdatetime.getHours()>=10?jdatetime.getHours():"0"+jdatetime.getHours());  //时
    jdatetimeType+= ":" + (jdatetime.getMinutes()>=10?jdatetime.getMinutes():"0"+jdatetime.getMinutes());   //分
    jdatetimeType+= ":" + (jdatetime.getSeconds()>=10?jdatetime.getSeconds():"0"+jdatetime.getSeconds());   //秒
    jdatetimeType+="."+jdatetime.getMilliseconds();//毫秒
    return jdatetimeType;
}

function dateFormatFormLongTimeS(longTimeMs) {
    var jdatetimeType = "";
    var jdatetime = new Date();
    jdatetime.setTime(longTimeMs);
    jdatetimeType+= jdatetime.getFullYear();  //年
    jdatetimeType+= "-" + ((jdatetime.getMonth()+1)>=10?(jdatetime.getMonth()+1):"0"+(jdatetime.getMonth()+1)); //月
    jdatetimeType += "-" + (jdatetime.getDate()>=10?jdatetime.getDate():"0"+jdatetime.getDate());  //日
    jdatetimeType+= "  " + (jdatetime.getHours()>=10?jdatetime.getHours():"0"+jdatetime.getHours());  //时
    jdatetimeType+= ":" + (jdatetime.getMinutes()>=10?jdatetime.getMinutes():"0"+jdatetime.getMinutes());   //分
    jdatetimeType+= ":" + (jdatetime.getSeconds()>=10?jdatetime.getSeconds():"0"+jdatetime.getSeconds());   //秒
    return jdatetimeType;
}

function dateFormatS(jdatetime) {
    var jdatetimeType = "";
    jdatetimeType+= jdatetime.getFullYear();  //年
    jdatetimeType+= "-" + ((jdatetime.getMonth()+1)>=10?(jdatetime.getMonth()+1):"0"+(jdatetime.getMonth()+1)); //月
    jdatetimeType += "-" + (jdatetime.getDate()>=10?jdatetime.getDate():"0"+jdatetime.getDate());  //日
    jdatetimeType+= "  " + (jdatetime.getHours()>=10?jdatetime.getHours():"0"+jdatetime.getHours());  //时
    jdatetimeType+= ":" + (jdatetime.getMinutes()>=10?jdatetime.getMinutes():"0"+jdatetime.getMinutes());   //分
    jdatetimeType+= ":" + (jdatetime.getSeconds()>=10?jdatetime.getSeconds():"0"+jdatetime.getSeconds());   //秒
    return jdatetimeType;
}

function convertToTime_MinuteNoYear(longTimeMs) {
    var jdatetimeType = "";
    var jdatetime = new Date();
    jdatetime.setTime(longTimeMs);
    jdatetimeType+= ((jdatetime.getMonth()+1)>=10?(jdatetime.getMonth()+1):"0"+(jdatetime.getMonth()+1)); //月
    jdatetimeType += "-" + (jdatetime.getDate()>=10?jdatetime.getDate():"0"+jdatetime.getDate());  //日
    jdatetimeType+= "  " + (jdatetime.getHours()>=10?jdatetime.getHours():"0"+jdatetime.getHours());  //时
    jdatetimeType+= ":" + (jdatetime.getMinutes()>=10?jdatetime.getMinutes():"0"+jdatetime.getMinutes());   //分
    return jdatetimeType;
}

function convertToTime_SecondNoYear(longTimeMs) {
    var jdatetimeType = "";
    var jdatetime = new Date();
    jdatetime.setTime(longTimeMs);
    jdatetimeType+= ((jdatetime.getMonth()+1)>=10?(jdatetime.getMonth()+1):"0"+(jdatetime.getMonth()+1)); //月
    jdatetimeType += "-" + (jdatetime.getDate()>=10?jdatetime.getDate():"0"+jdatetime.getDate());  //日
    jdatetimeType+= "  " + (jdatetime.getHours()>=10?jdatetime.getHours():"0"+jdatetime.getHours());  //时
    jdatetimeType+= ":" + (jdatetime.getMinutes()>=10?jdatetime.getMinutes():"0"+jdatetime.getMinutes());   //分
    jdatetimeType+= ":" + (jdatetime.getSeconds()>=10?jdatetime.getSeconds():"0"+jdatetime.getSeconds());   //秒
    return jdatetimeType;
}

function getFormatDate() {
    var date = new Date();
    var month = date.getMonth() + 1;
    var strDate = date.getDate();
    if (month >= 1 && month <= 9) {
        month = "0" + month;
    }
    if (strDate >= 0 && strDate <= 9) {
        strDate = "0" + strDate;
    }
    var currentDate = date.getFullYear() + "-" + month + "-" + strDate
        + " " + date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
    return currentDate;
}

function getTimeLongDisc(timeMS) {
    if(timeMS<=0){
        return "0秒";
    }
    var days    = timeMS / 1000 / 60 / 60 / 24;
    var daysRound   = Math.floor(days);
    var hours    = timeMS/ 1000 / 60 / 60 - (24 * daysRound);
    var hoursRound   = Math.floor(hours);
    var minutes   = timeMS / 1000 /60 - (24 * 60 * daysRound) - (60 * hoursRound);
    var minutesRound  = Math.floor(minutes);
    var seconds   = timeMS/ 1000 - (24 * 60 * 60 * daysRound) - (60 * 60 * hoursRound) - (60 * minutesRound);
    var secondsRound = Math.floor(seconds);
    var resultDisc="";
    if(daysRound>0){
        resultDisc+=daysRound+"天";
        if(hoursRound>0){
            resultDisc+=hoursRound+"时";
        }
    }else {
        if(hoursRound>0){
            resultDisc+=hoursRound+"时";
            if(minutesRound>0){
                resultDisc+=minutesRound+"分";
            }
        }else {
            if(minutesRound>0){
                resultDisc+=minutesRound+"分";
            }
            resultDisc+=secondsRound+"秒";
        }
    }
    return resultDisc;
}

function convertToTimeLong_MS(longTimeMs) {
    var jdatetimeType = "";
    var jdatetime = new Date();
    jdatetime.setTime(longTimeMs);
    jdatetimeType+= jdatetime.getFullYear();  //年
    jdatetimeType+= "-" + ((jdatetime.getMonth()+1)>=10?(jdatetime.getMonth()+1):"0"+(jdatetime.getMonth()+1)); //月
    jdatetimeType += "-" + (jdatetime.getDate()>=10?jdatetime.getDate():"0"+jdatetime.getDate());  //日
    jdatetimeType+= "  " + (jdatetime.getHours()>=10?jdatetime.getHours():"0"+jdatetime.getHours());  //时
    jdatetimeType+= ":" + (jdatetime.getMinutes()>=10?jdatetime.getMinutes():"0"+jdatetime.getMinutes());   //分
    jdatetimeType+= ":" + (jdatetime.getSeconds()>=10?jdatetime.getSeconds():"0"+jdatetime.getSeconds());   //秒
    jdatetimeType+="."+jdatetime.getMilliseconds();//毫秒
    return jdatetimeType;
}

function convertToTimeLong_S(longTimeMs) {
    var jdatetimeType = "";
    var jdatetime = new Date();
    jdatetime.setTime(longTimeMs);
    jdatetimeType+= jdatetime.getFullYear();  //年
    jdatetimeType+= "-" + ((jdatetime.getMonth()+1)>=10?(jdatetime.getMonth()+1):"0"+(jdatetime.getMonth()+1)); //月
    jdatetimeType += "-" + (jdatetime.getDate()>=10?jdatetime.getDate():"0"+jdatetime.getDate());  //日
    jdatetimeType+= "  " + (jdatetime.getHours()>=10?jdatetime.getHours():"0"+jdatetime.getHours());  //时
    jdatetimeType+= ":" + (jdatetime.getMinutes()>=10?jdatetime.getMinutes():"0"+jdatetime.getMinutes());   //分
    jdatetimeType+= ":" + (jdatetime.getSeconds()>=10?jdatetime.getSeconds():"0"+jdatetime.getSeconds());   //秒
    return jdatetimeType;
}

function convertToTime_MinuteNoYear(longTimeMs) {
    var jdatetimeType = "";
    var jdatetime = new Date();
    jdatetime.setTime(longTimeMs);
    jdatetimeType+= ((jdatetime.getMonth()+1)>=10?(jdatetime.getMonth()+1):"0"+(jdatetime.getMonth()+1)); //月
    jdatetimeType += "-" + (jdatetime.getDate()>=10?jdatetime.getDate():"0"+jdatetime.getDate());  //日
    jdatetimeType+= "  " + (jdatetime.getHours()>=10?jdatetime.getHours():"0"+jdatetime.getHours());  //时
    jdatetimeType+= ":" + (jdatetime.getMinutes()>=10?jdatetime.getMinutes():"0"+jdatetime.getMinutes());   //分
    return jdatetimeType;
}

function convertToTime_HourAndMinute(longTimeMs) {
    var jdatetimeType = "";
    var jdatetime = new Date();
    jdatetime.setTime(longTimeMs);
    jdatetimeType+= "  " + (jdatetime.getHours()>=10?jdatetime.getHours():"0"+jdatetime.getHours());  //时
    if(jdatetime.getSeconds()<30){

    }
    jdatetimeType+= ":" + (jdatetime.getMinutes()>=10?jdatetime.getMinutes():"0"+jdatetime.getMinutes());   //分
    return jdatetimeType;
}

function convertToTime_HourAndRoundMinute(longTimeMs) {
    var jdatetimeType = "";
    var jdatetime = new Date();
    jdatetime.setTime(longTimeMs);
    jdatetimeType+= "  " + (jdatetime.getHours()>=10?jdatetime.getHours():"0"+jdatetime.getHours());  //时
    jdatetimeType+= ":" + (jdatetime.getMinutes()>=10?jdatetime.getMinutes():"0"+jdatetime.getMinutes());   //分
    // if(jdatetime.getSeconds()<30){
    //     jdatetimeType+= ":" + (jdatetime.getMinutes()>=10?jdatetime.getMinutes():"0"+jdatetime.getMinutes());   //分
    // }else {
    //     jdatetimeType+= ":" + (jdatetime.getMinutes()+1>=10?jdatetime.getMinutes()+1:"0"+(jdatetime.getMinutes()+1));   //分
    // }
    return jdatetimeType;
}

function getTimeLongFromStr(dateStr) {
    if(isEmptyString(dateStr)){
        return "";
    }
    var str = dateStr.replace(/-/g,"/");
    var date = new Date(str);
    return date.getTime();
}

function getTimeMinOptions(minInterval) {
    var optionStr="";
    for(i=0;i<1440;i+=minInterval){
        optionStr+="<option value='"+i+"'>"+getTimeMinName(i)+"</option>";
    }
    return optionStr;
}

function getTimeMinName(minNum) {
    var name="";
    var hourNum=Math.floor(minNum/60);
    var minNum=minNum%60;
    name+=hourNum>=10?hourNum:"0"+hourNum;
    name+=":";
    name+=minNum>=10?minNum:"0"+minNum;
    return name;
}
/**
 * 获取当前时间
 * */
function getNowTimeLong() {
    return new Date().getTime();
}

function get12HAgoTimeLong() {
    return getNowTimeLong()-12*60*60*1000;
}
