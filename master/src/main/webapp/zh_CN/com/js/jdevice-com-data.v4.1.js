/**
 * Created by LinChunSen on 2020/4/10.
 */
function getUserPositionArr() {
    return ["设备管理","生产管理","产品研发","产品设计","质量管理","其他岗位"];
}
function getUserTradeArr() {
    return ["电子","机械","纺织","煤炭","医药","通信","冶金","橡胶","军工","物流运输","化工","汽车","IT互联网","其他"];
}
function getWorkshopDashboardUrlOptions() {
    return "<option value='status_workshop.html'>设备状态看板</option>"+//
        "<option value='statusTempHumi_workshop.html'>状态温湿度看板</option>";
}
function getWorkshopDashboardName(urlStr) {
    if(urlStr==="status_workshop.html"){
        return "设备状态看板";
    }
    if(urlStr==="statusTempHumi_workshop.html"){
        return "状态温湿度看板";
    }
    return "";
}
function getWorkshopDashboardFaClassName(urlStr) {
    if(urlStr==="status_workshop.html"){
        return "fa-desktop";
    }
    if(urlStr==="statusTempHumi_workshop.html"){
        return "fa-thermometer-empty";
    }
    return "fa-desktop";
}
function getDeviceLevelOptions() {
    return "<option value='0'>普通设备(一般报警)</option>"+//
        "<option value='256'>关键设备(紧急报警)</option>";
}
function getDeviceLevelMsgName(val) {
    if(val<=2){
        return "一般";
    }
    if(val>=256){
        return "紧急";
    }
}
function getDeviceLevelName(val) {
    if(val<=2){
        return "普通设备(一般报警)";
    }
    if(val>=256){
        return "关键设备(紧急报警)";
    }
}
function getDeviceLevelBgColor(val) {
    if(val<=2){
        return "#666666";
    }
    if(val>=256){
        return "#ff5555";
    }
}
function getDeviceSignboardCollectIntervalOptions() {
    return "<option value='0'>不采集</option>"+//
        "<option value='1'>1分钟</option>"+//
        "<option value='3'>3分钟</option>"+//
        "<option value='5'>5分钟</option>"+//
        "<option value='10'>10分钟</option>"+//
        "<option value='15'>15分钟</option>"+//
        "<option value='20'>20分钟</option>"+//
        "<option value='30'>30分钟</option>";
}
function getDeviceSignboardUploadIntervalOptions() {
    return "<option value='30'>半小时</option>"+//
        "<option value='60'>1小时</option>"+//
        "<option value='120'>2小时</option>"+//
        "<option value='180'>3小时</option>"+//
        "<option value='240'>4小时</option>"+//
        "<option value='300'>5小时</option>";
}
function getDeviceSignboardErrStatusOptions() {
    return "<option value='0'></option>"+//
        "<option value='1'>多磁感应</option>"+//
        "<option value='2'>温湿度采集异常</option>";
}
function getDeviceSignboardErrStatusName(errStatusType) {
    if(errStatusType==0){
        return "";
    }else if(errStatusType==1){
        return "多磁感应";
    }else if(errStatusType==2){
        return "温湿度采集异常";
    }else {
        return "未知错误";
    }
}
function foundDeviceSignboardErr(errStatusType) {
    if(errStatusType==0){
        return false;
    }
    return true;
}
function getWarnLevelOptions() {
    return "<option value='0'>无需预警</option>"+//
        "<option value='1'>下属所有设备</option>"+//
        "<option value='2'>设备组设备</option>"+//
        "<option value='4'>车间所有设备</option>"+//
        "<option value='8'>工厂所有设备</option>"+//
        "<option value='16'>组织所有设备</option>";
}
function getDeviceStatusOptions() {
    return "<option value='Run'>Run (运行)</option>"+//
        "<option value='Pending'>Pending (待料)</option>"+//
        "<option value='Wait'>Wait (待机)</option>"+//
        "<option value='Fault'>Fault (故障)</option>"+//
        "<option value='Stop'>Stop (停机)</option>"+//
        "<option value='Maintain'>Maintain (保养)</option>"+//
        "<option value='Repair'>Repair (维修)</option>"+//
        "<option value='Sealup'>Sealup (封存)</option>"+//
        "<option value='Offline'>Offline (离线)</option>";
}
function getUserRoleOptions_NoSystemAdmin() {
    return "<option value='0'></option>"+//
        "<option value='2'>普通用户</option>"+//
        "<option value='4'>设备组管理员</option>"+//
        "<option value='8'>车间管理员</option>"+//
        "<option value='16'>工厂管理员</option>"+//
        "<option value='32'>组织管理员</option>";
}
function getUserRoleOptions_SystemAdmin() {
    return "<option value='64'>系统管理员</option>";
}
function getUserStatusOptions() {
    return "<option value='2'>启用中</option>"+//
        "<option value='4'>禁用中</option>"+//
        "<option value='8'>待审核</option>";
}
function role_admin(roleLevel) {
    if(roleLevel>2){
        return true;
    }
    return false;
}
function getWarnLevelName(val) {
    var warnLevel="";
    if(val<=0){
        warnLevel="无需预警";
    }else if(val==1){
        warnLevel="下属所有设备";
    }else if(val==2){
        warnLevel="设备组设备";
    }else if(val==4){
        warnLevel="车间所有设备";
    }else if(val==8){
        warnLevel="工厂所有设备";
    }else if(val==16){
        warnLevel="组织所有设备";
    }
    return warnLevel;
}
function getUserRoleName(roleLevel) {
    var roleDisc="";
    if(roleLevel==64){
        roleDisc="系统管理员";
    }else if(roleLevel==32){
        roleDisc="组织管理员";
    }else if(roleLevel==16){
        roleDisc="工厂管理员";
    }else if(roleLevel==8){
        roleDisc="车间管理员";
    }else if(roleLevel==4){
        roleDisc="设备组管理员";
    }else if(roleLevel==2){
        roleDisc="普通用户";
    }else{
        roleDisc="待审用户";
    }
    return roleDisc;
}
function getRoleLevel_user(roleLevel) {
    return 2;
}
function getRoleLevel_workcenterAdmin(roleLevel) {
    return 4;
}
function getRoleLevel_workshopAdmin(roleLevel) {
    return 8;
}
function getRoleLevel_factoryAdmin(roleLevel) {
    return 16;
}
function getRoleLevel_organizationAdmin(roleLevel) {
    return 32;
}
function getRoleLevel_systemAdmin(roleLevel) {
    return 64;
}
function checkRoleLevel_user(roleLevel) {
    return roleLevel==2;
}
function checkRoleLevel_workcenterAdmin(roleLevel) {
    return roleLevel==4;
}
function checkRoleLevel_workshopAdmin(roleLevel) {
    return roleLevel==8;
}
function checkRoleLevel_factoryAdmin(roleLevel) {
    return roleLevel==16;
}
function checkRoleLevel_organizationAdmin(roleLevel) {
    return roleLevel==32;
}
function checkRoleLevel_systemAdmin(roleLevel) {
    return roleLevel==64;
}
function isSystemAdmin(roleLevel) {
    if(roleLevel==64){
        return true;
    }
    return false;
}
function getUserStatusName(statusVal) {
    var statusDisc="";
    if(statusVal==2){
        statusDisc="启用中";
    }else if(statusVal==4){
        statusDisc="禁用中";
    }else if(statusVal==8){
        statusDisc="待审核";
    }
    return statusDisc;
}
function checkUserStatus_applying(statusVal) {
    if(statusVal==8){
        return true;
    }
    return false;
}
function getLoginReturnUrl(defaultUrl) {
    var url = document.location.toString();
    var arrObj = url.split("?login_return=");
    if(arrObj.length > 1){
        if(isEmptyString(arrObj[1])){
            return defaultUrl;
        }
        return arrObj[1];
    }else {
        return defaultUrl;
    }
}
function getAndonStatusName(status) {
    if(andonStatus_claimed(status)){
        return "处理中";
    }
    if(andonStatus_closed(status)){
        return "已关闭";
    }
    if(andonStatus_notClaimed(status)){
        return "未认领";
    }
    if(andonStatus_notClosed(status)){
        return "未关闭";
    }
    return "";
}

function getStauts_falut() {
    return 8;
}

function getAndonStatus_closed() {
    return 0;
}

function getAndonStatus_notClosed() {
    return 2;
}

function getAndonStatus_claimed() {
    return 4;
}

function getAndonStatus_unclaimed() {
    return 8;
}

function getAndonStatus_init() {
    return 16;
}

function andonStatus_notClaimed(status) {
    return status==8;
}
function andonStatus_claimed(status) {
    return status==4;
}
function andonStatus_notClosed(status) {
    return status==2;
}
function andonStatus_closed(status) {
    return status==0;
}
function getMonitorName(monitor) {
    var deviceMonitorDisc="";
    if(monitor==32){
        deviceMonitorDisc="标识牌";
    }else if(monitor==16){
        deviceMonitorDisc="Jedge";
    }else {
        deviceMonitorDisc="游离中"
    }
    return deviceMonitorDisc;
}
function getDeviceSignboardTemperature(){
    return  "<option value='-10'>-10℃</option>"+
        "<option value='-5'>-5℃</option>"+
        "<option value='0'>0℃</option>"+
        "<option value='5'>5℃</option>"+
        "<option value='10'>10℃</option>"+
        "<option value='15'>15℃</option>"+
        "<option value='20'>20℃</option>"+
        "<option value='25'>25℃</option>"+
        "<option value='30'>30℃</option>"+
        "<option value='35'>35℃</option>"+
        "<option value='40'>40℃</option>"+
        "<option value='45'>45℃</option>"+
        "<option value='50'>50℃</option>"+
        "<option value='55'>55℃</option>"+
        "<option value='60'>60℃</option>"+
        "<option value='65'>65℃</option>";
}
function getDeviceSignboardHumidity(){
    return  "<option value='10'>10%</option>"+
        "<option value='15'>15%</option>"+
        "<option value='20'>20%</option>"+
        "<option value='25'>25%</option>"+
        "<option value='30'>30%</option>"+
        "<option value='35'>35%</option>"+
        "<option value='40'>40%</option>"+
        "<option value='45'>45%</option>"+
        "<option value='50'>50%</option>"+
        "<option value='55'>55%</option>"+
        "<option value='60'>60%</option>"+
        "<option value='65'>65%</option>"+
        "<option value='70'>70%</option>"+
        "<option value='75'>75%</option>"+
        "<option value='80'>80%</option>"+
        "<option value='85'>85%</option>"+
        "<option value='90'>90%</option>";
}


function getPlanType() {
    return "<option value='2'>设备点巡检</option>"+//
        "<option value='4'>车间点巡检</option>"+//
        "<option value='8'>工厂点巡检</option>";//
}
function getConfigType() {
    return "<option value='2'>单选型</option>"+//
        "<option value='4'>数字型</option>";//
}
function getBolResult() {
    return "<option value='1'>true</option>"+//
        "<option value='0'>false</option>";//
}
function getPlanTypeStatus() {
    return "<option value='2'>待点检</option>"+//
        "<option value='4'>点检中</option>"+//
        "<option value='8'>点检完成</option>";//
}
function showPlanType(val){
    var typename = "";
    if(val==2){
        typename = "设备点巡检"
    }else if(val==4){
        typename = "车间点巡检"
    }else if(val==8){
        typename = "工厂点巡检"
    }
    return typename;
}
function showConfigType(val){
    var configname = "";
    if(val==2){
        configname = "单选型"
    }else if(val==4){
        configname = "数字型"
    }
    return configname;
}
function showPlanStatus(val){
    var statusname = "";
    if(val==2){
        statusname = "待点检"
    }else if(val==4){
        statusname = "点检中"
    }else if(val==8){
        statusname = "点检完成"
    }
    return statusname;
}
function showItemsStatus(val){
    var statusname = "";
    if(val==2){
        statusname = "未进行"
    }else if(val==4){
        statusname = "已完成"
    }
    return statusname;
}
/**
 * 设备点巡检类型
 * */
function getPlanType_Device() {
    return 2;
}
function getPlanType_Workshop() {
    return 4;
}
function getPlanType_Factory() {
    return 8;
}
/*
* 点巡检项类型
* */
function getConfigType_bool(){
    return 2;
}
function getConfigType_num(){
    return 4;
}
/**
 * 计划实例状态
 * */
function getPlanTye_Wait(){
    return 2;
}
function getPlanTye_Processing(){
    return 4;
}
function getPlanTye_Finish(){
    return 8;
}
/**
 * 点检项结果
 * */
function showConfigResult(val){
    var statusname = "";
    if(val==1){
        statusname = "正常"
    }else if(val==0){
        statusname = "异常"
    }
    return statusname;
}
function getConfig_valuebool(){
    return 1;
}
function getConfig_valueboolfalse(){
    return 0;
}
/*
* 触发间隔
* */
function showTimeInterval(){
    return "<option value='720'>12小时</option>"+//
        "<option value='1440'>一天</option>"+//
        "<option value='2880'>两天</option>"+//
        "<option value='4320'>三天</option>"+//
        "<option value='10080'>七天</option>"+//
        "<option value='21600'>十五天</option>"+//
        "<option value='43200'>三十天</option>";//

}