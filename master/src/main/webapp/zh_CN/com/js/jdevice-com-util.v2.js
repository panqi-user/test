/**
 * Created by LinChunSen on 2020/7/28.
 */
/**
 * 数字前添加0补位，num传入的数字，n需要的字符长度
 * */
function prefixInteger(num, n,char) {
    return (Array(n).join(char) + num).slice(-n);
}
function isEmptyString(str) {
    if(str==undefined||str==null||str.trim()==""){
        return true;
    }
    return false;
}
function notEmptyString(str) {
    if(str!=undefined&&str!=null&&str.trim()!=""){
        return true;
    }
    return false;
}
function isNull(obj) {
    if(obj==undefined||obj==null){
        return true;
    }
    return false;
}
function notNull(obj) {
    if(obj!=undefined&&obj!=null){
        return true;
    }
    return false;
}
//paraName是url的key
function getUrlParam(paraName) {
    var url = document.location.toString();
    var arrObj = url.split("?");
    if (arrObj.length > 1) {
        var arrPara = arrObj[1].split("&");
        var arr;
        for (var i = 0; i < arrPara.length; i++) {
            arr = arrPara[i].split("=");
            if (arr != null && arr[0] == paraName) {
                return arr[1];
            }
        }
        return "";
    }
    else {
        return "";
    }
}
