package cn.sd.zhidetec.jdevice.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.sd.zhidetec.jdevice.dao.AndonReasonModelDao;
import cn.sd.zhidetec.jdevice.dao.WorkshopModelDao;
import cn.sd.zhidetec.jdevice.model.FactoryModel;
import cn.sd.zhidetec.jdevice.model.ShiftModel;
import cn.sd.zhidetec.jdevice.model.WorkshopModel;

@Service
public class WorkshopService {

	@Autowired
	protected WorkshopModelDao workshopModelDao;
	protected Logger logger = LoggerFactory.getLogger(getClass());
	@Autowired
	protected DeviceTempHumiService deviceTempHumiService;
	@Autowired
	protected DeviceSignboardService deviceSignboardService;
	@Autowired
	protected AndonService andonService;
	@Autowired
	protected DeviceService deviceService;
	@Autowired
	protected DeviceOeeService deviceOeeService;
	@Autowired
	protected DeviceFaultService deviceFaultService;
	@Autowired
	protected ShiftService shiftService;
	@Autowired
	protected WorkcenterService workcenterService;
	@Autowired
	protected DeviceTypeService deviceTypeService;
	@Autowired
	protected WorkcenterService workcenterModelDao;
	@Autowired
	protected AndonReasonModelDao andonReasonModelDao;
	@Autowired
	protected UserService userService;
	@Autowired
	protected JedgeService jedgeService;
	
	public int getWorkshopNum(String organizationId) {
		return workshopModelDao.getWorkshopNum(organizationId);
	}

	public WorkshopModel initFactory(FactoryModel factoryModel) {
		if (factoryModel==null) {
			return null;
		}
		WorkshopModel workshopModel=new WorkshopModel();
		workshopModel.setOrganizationId(factoryModel.getOrganizationId());
		workshopModel.setFactoryId(factoryModel.getId());
		if (setWorkshopModel(workshopModel)) {
			//工作中心
			if (workcenterModelDao.initWorkshop(workshopModel)==null) {
				workshopModel=null;
			}
			//班次
			List<ShiftModel> shiftModels=shiftService.initWorkshop(workshopModel);
			if (shiftModels==null||shiftModels.size()<=0) {
				workshopModel=null;
			}
			//设备类型
			if (deviceTypeService.initWorkshop(workshopModel)==null) {
				workshopModel=null;
			}
			//报警原因
			if (andonReasonModelDao.initWorkshop(workshopModel)) {
				workshopModel=null;
			}
			//设备
			
			return workshopModel;
		}
		return null;
	}

	public WorkshopModel getWorkshopModelById(long id) {
		return workshopModelDao.getWorkshopModelById(id);
	}

	public List<WorkshopModel> getWorkshopModelsById(long workshopId) {
		return workshopModelDao.getWorkshopModelsById(workshopId);
	}


    /**
     * 根据工厂id获取车间列表
     * @param factoryId
     * @return
     */
	public List<WorkshopModel> getWorkshopModelsByFactoryId(long factoryId) {
		return workshopModelDao.getWorkshopModelsByFactoryId(factoryId);
	}

	public List<WorkshopModel> getWorkshopModelsByOrganizationId(String organizationId) {
		return workshopModelDao.getWorkshopModelsByOrganizationId(organizationId);
	}

	public boolean setWorkshopModel(WorkshopModel workshopModel) {
		//获取上次车间状态
		if (workshopModel.getId()>0) {
			List<WorkshopModel> list = workshopModelDao.getWorkshopModelsById(workshopModel.getId());
			if (list.size()>0) {
				WorkshopModel lastWorkshopModel=list.get(0);
				//更换工厂
				if (lastWorkshopModel.getFactoryId()!=workshopModel.getFactoryId()) {//两个工厂id不一样
					//设备
					deviceService.changeFactory(workshopModel.getId(), workshopModel.getFactoryId());
					//班次
					shiftService.changeFactory(workshopModel.getId(), workshopModel.getFactoryId());
					//工作中心
					workcenterService.changeFactory(workshopModel.getId(), workshopModel.getFactoryId());
					//设备类型
					deviceTypeService.changeFactory(workshopModel.getId(), workshopModel.getFactoryId());
					//人员
					userService.changeFactory(workshopModel.getId(), workshopModel.getFactoryId());


					//改变工厂id
                    andonReasonModelDao.changeFactory(workshopModel.getId(),workshopModel.getFactoryId());



					//采集系统
					jedgeService.changeFactory(workshopModel.getId(), workshopModel.getFactoryId());
				}
			}else {
				workshopModel.setId(-1);
			}
		}
		//新建
		return workshopModelDao.setWorkshopModel(workshopModel);
	}

	public boolean deleteWorkshopModel(WorkshopModel workshopModel) {
		return workshopModelDao.deleteWorkshopModel(workshopModel);
	}
}
