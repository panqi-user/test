package cn.sd.zhidetec.jdevice.service;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import cn.sd.zhidetec.jdevice.pojo.AndroidPackagePojo;

@Service
public class AndroidApkService extends Thread{
	
	@Value("${android.apkReloadIntervalMS}")
	protected int apkReloadIntervalMS=1000;
	
	protected Logger logger = LoggerFactory.getLogger(getClass());
	public AndroidApkService() {
		super();
	}

	@Override
	public void run() {
		started=true;
		if (apkReloadIntervalMS<1000) {
			apkReloadIntervalMS=1000;
		}
		File dir = new File(DIR_DOWNLOAD_ANDROID);
		//不存在则创建
		if (!dir.isDirectory() || !dir.exists()) {
			if (dir.mkdirs()==false) {
				logger.info(getClass().getSimpleName()+" 文件夹创建失败："+dir.getName());
				return;
			}
		}
		long fileLastModifiedTimeMs=0;
		long fileModifiedTimeMs=0;
		while (started) {
			try {
				//检查更新
				File[] files = dir.listFiles();
				for (File file : files) {
					if (file.isFile()&&file.exists()&&file.canRead()) {
						if (fileModifiedTimeMs<=file.lastModified()) {
							fileModifiedTimeMs=file.lastModified();
						}
					}
				}
				//重新加载
				if (fileModifiedTimeMs>fileLastModifiedTimeMs) {
					fileLastModifiedTimeMs=fileModifiedTimeMs;
					logger.info(getClass().getSimpleName()+" 开始加载Android客户端更新");
					reload();
				}
				Thread.sleep(apkReloadIntervalMS);
			} catch (Exception e) {
				// TODO: handle exception
				logger.error(getClass().getSimpleName()+" Android客户端更新检查异常",e);
			}
		}
	}
	private boolean started=false;
	public void stopService() {
		started=false;
	}
	public boolean started() {
		return started;
	}

	public boolean reload() {
		File file=new File(DIR_DOWNLOAD_ANDROID);
		//文件夹
		if (!file.exists()) {
			file.mkdirs();
		}
		//从文件中加载
		return loadFromDir(DIR_DOWNLOAD_ANDROID);
	}
	
	public boolean loadFromDir(String dirPath) {
		try {
			packagePojos.clear();
			File dir = new File(dirPath);
			if (!dir.isDirectory() || !dir.exists()) {
				return false;
			}
			File[] files = dir.listFiles();
			TypeReference<List<AndroidPackagePojo>> typeReference = new TypeReference<List<AndroidPackagePojo>>() {
			};
			for (File file : files) {
				if (file.isFile()&&file.getName().endsWith(".json")) {
					List<AndroidPackagePojo> models = jsonMapper.readValue(file, typeReference);
					if (models==null||models.size()<=0) {
						continue;
					}
					synchronized (pojosLockObject) {
						packagePojos.addAll(models);
					}	
				}
			}
			return true;
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	
	public boolean save(MultipartFile multipartFile) {
		String fileName=multipartFile.getOriginalFilename();
		File file=new File(DIR_DOWNLOAD_ANDROID+File.separator+fileName);
		try {
			multipartFile.transferTo(file);
			logger.info("Android客户端："+fileName+"上传成功！！");
			return true;
		} catch (Exception e) {
			// TODO: handle exception
			logger.error("Android客户端保存失败！！", e);
			return false;
		}
	}
	
	public List<AndroidPackagePojo> getAndroidPackagePojos() {
		List<AndroidPackagePojo> list = new ArrayList<>();
		synchronized (pojosLockObject) {
			for (AndroidPackagePojo androidModel : packagePojos) {
				list.add(androidModel);
			}
		}
		//排序
		Collections.sort(list, androidModelListComparator);
		return list;
	}
	
	public static Comparator<AndroidPackagePojo> androidModelListComparator=new Comparator<AndroidPackagePojo>() {

		@Override
		public int compare(AndroidPackagePojo o1, AndroidPackagePojo o2) {
			// TODO Auto-generated method stub
			return o1.getApkData().getVersionCode()-o2.getApkData().getVersionCode();
		}
		
	};
	
	private List<AndroidPackagePojo> packagePojos=Collections.synchronizedList(new ArrayList<>());
	private Object pojosLockObject=new Object();
	private ObjectMapper jsonMapper=new ObjectMapper();
	public final static String DIR_DOWNLOAD_ANDROID="download"+File.separator+"android"+File.separator;
}
