package cn.sd.zhidetec.jdevice.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import cn.sd.zhidetec.jdevice.Starter;
import cn.sd.zhidetec.jdevice.config.DataSourceConfiger;
import cn.sd.zhidetec.jdevice.model.FactoryModel;
import cn.sd.zhidetec.jdevice.model.OrganizationModel;
import cn.sd.zhidetec.jdevice.service.WorkshopService;
import cn.sd.zhidetec.jdevice.util.StringUtil;

@Service
public class FactoryModelDao {

	@Autowired
	@Qualifier(DataSourceConfiger.BEAN_NAME_JdeviceJdbcTemplate)
	protected JdbcTemplate jdbcTemplate;
	@Autowired
	protected WorkshopService workshopModelDao;

	public int getFactoryNum(String organizationId) {
		Object[] args = new Object[1];
		args[0] = organizationId;
		return jdbcTemplate.queryForObject(Sql_GetNum_By_OrganizationId, args, Integer.class);
	}

	private static String Sql_GetNum_By_OrganizationId = "SELECT COUNT(*) AS Num FROM jdevice_c_factory WHERE organization_id=?";

	/**
	 * 删除工厂
	 * 
	 * @param factoryModel 工厂信息
	 * @return 操作是否成功
	 */
	public boolean deleteFactoryModel(FactoryModel factoryModel) {
		int i = 0;
		Object[] args = new Object[1];
		args[i++] = factoryModel.getId();
		if (factoryModel.getId()>0) {
			i = jdbcTemplate.update(Sql_DeleteModel_By_Id, args);
		}
		if (i > 0) {
			return true;
		}
		return false;
	}

	private static String Sql_DeleteModel_By_Id = "DELETE FROM jdevice_c_factory WHERE id=?";

	/**
	 * 设置工厂信息，如果不存在则新建
	 * 
	 * @param factoryModel 工厂信息
	 * @return 操作是否成功
	 */
	public boolean setFactoryModel(FactoryModel factoryModel) {
		if (StringUtil.isEmpty(factoryModel.getOrganizationId())) {
			return false;
		}
		int i = 0;
		Object[] args = new Object[5];
		// 修改信息
		if (factoryModel.getId() > 0
				&& getFactoryModelsById(factoryModel.getId()).size() > 0) {
			args[i++] = StringUtil.getEmptyString(factoryModel.getCode());
			args[i++] = StringUtil.getEmptyString(factoryModel.getName());
			args[i++] = StringUtil.getEmptyString(factoryModel.getLogoUrl());
			args[i++] = StringUtil.getEmptyString(factoryModel.getOrganizationId());
			args[i++] = factoryModel.getId();
			i = jdbcTemplate.update(Sql_UpdateModel_By_Id, args);
		}
		// 新建信息
		else {
			factoryModel.setId(Starter.IdMaker.nextId());
			args[i++] = factoryModel.getId();
			args[i++] = StringUtil.getEmptyString(factoryModel.getCode());
			args[i++] = StringUtil.getEmptyString(factoryModel.getName());
			args[i++] = StringUtil.getEmptyString(factoryModel.getLogoUrl());
			args[i++] = StringUtil.getEmptyString(factoryModel.getOrganizationId());
			i = jdbcTemplate.update(Sql_InsertModel, args);
		}
		if (i > 0) {
			return true;
		}
		return false;
	}

	private static String Sql_UpdateModel_By_Id = "UPDATE jdevice_c_factory" + //
			" SET " + //
			"code = ?," + //
			"name = ?," + //
			"logo_url = ?," + //
			"organization_id = ?" + //
			" WHERE " + //
			"id=?";
	private static String Sql_InsertModel = "INSERT INTO jdevice_c_factory (" + //
			"id," + //
			"code," + //
			"name," + //
			"logo_url," + //
			"organization_id" + //
			")VALUES(" + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?" + //
			")";

	/**
	 * 根据工厂ID获取工厂列表
	 * 
	 * @param factoryId 工厂ID
	 * @return 数据列表
	 */
	public List<FactoryModel> getFactoryModelsById(long factoryId) {
		Object[] args = new Object[1];
		args[0] = factoryId;
		List<FactoryModel> list = jdbcTemplate.query(Sql_GetModels_By_Id, args, beanPropertyRowMapper);
		return list;
	}

	private static String Sql_GetModels_By_Id = "SELECT * FROM jdevice_c_factory WHERE id = ?";

	/**
	 * 根据工厂ID获取工厂
	 * 
	 * @param factoryId 工厂ID
	 * @return 工厂
	 */
	public FactoryModel getFactoryModelById(long factoryId) {
		List<FactoryModel> list = getFactoryModelsById(factoryId);
		if (list != null && list.size() > 0) {
			return list.get(0);
		}
		return null;
	}

	/**
	 * 根据组织ID获取工厂列表
	 * 
	 * @param organizationId 组织ID
	 * @return 数据列表
	 */
	public List<FactoryModel> getFactoryModelsByOrganizationId(String organizationId) {
		Object[] args = new Object[1];
		args[0] = organizationId;
		List<FactoryModel> list = jdbcTemplate.query(Sql_GetModels_By_OrganizationId, args, beanPropertyRowMapper);
		return list;
	}

	private static String Sql_GetModels_By_OrganizationId = "SELECT * FROM jdevice_c_factory WHERE organization_id = ?";
	

	public FactoryModel initOrganization(OrganizationModel organizationModel) {
		if (organizationModel==null) {
			return null;
		}
		FactoryModel factoryModel=new FactoryModel();
		factoryModel.setOrganizationId(organizationModel.getId());
		if (setFactoryModel(factoryModel)) {
			//车间
			if (workshopModelDao.initFactory(factoryModel)!=null) {
				return factoryModel;	
			}
		}
		return factoryModel;
	}
	
	private BeanPropertyRowMapper<FactoryModel> beanPropertyRowMapper = new BeanPropertyRowMapper<FactoryModel>(
			FactoryModel.class);
}
