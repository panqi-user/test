package cn.sd.zhidetec.jdevice.controller;

import java.io.OutputStream;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import cn.sd.zhidetec.jdevice.HttpResponseMsg;
import cn.sd.zhidetec.jdevice.model.DeviceModel;
import cn.sd.zhidetec.jdevice.model.RoleModel;
import cn.sd.zhidetec.jdevice.model.UserModel;
import cn.sd.zhidetec.jdevice.service.DeviceService;
import cn.sd.zhidetec.jdevice.service.FileUploadService;
import cn.sd.zhidetec.jdevice.service.RBuilderService;
import cn.sd.zhidetec.jdevice.util.DateUtil;
import cn.sd.zhidetec.jdevice.util.PoiExcelUtil;
import cn.sd.zhidetec.jdevice.util.StringUtil;

@Controller
@RequestMapping("/device")
public class DeviceController {

	@Autowired
	protected FileUploadService fileUploadService;
	@Autowired
	protected DeviceService deviceService;
	@Autowired
	protected RBuilderService rBuilderService;
	protected Logger logger = LoggerFactory.getLogger(getClass());

	/**
	 * 上传用户头像
	 * 
	 * @param multipartFile 文件名
	 * @return 返回给前端的操作结果
	 */
	@RequestMapping(path = "/upload/logoImg")
	@ResponseBody
	public RBuilderService.Response uploadLogoImg(HttpSession httpSession, //
			@RequestParam("deviceId") long deviceId, //
			@RequestParam("file") MultipartFile multipartFile) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_User, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		// 获取当前登录用户
		UserModel userModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		StringBuilder logoUrlBuilder = new StringBuilder();
		// 保存到文件
		if (deviceId > 0 && fileUploadService.uploadDevicePhotoImg(multipartFile, userModel.getOrganizationId(),
				logoUrlBuilder)) {
			// 保存到数据库
			String logoUrl = logoUrlBuilder.toString();
			if (deviceService.setDeviceLogoUrlById(deviceId, logoUrl)) {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, logoUrl);
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, logoUrl);
			}
		} else {
			return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
		}
	}

	/**
	 * 根据设备ID获取设备
	 * 
	 * @param httpSession Session会话
	 * @param id          设备id
	 * @return 返回给前端的操作结果
	 */
	@RequestMapping(path = "/getDeviceModelById")
	@ResponseBody
	public RBuilderService.Response getDeviceModelById(HttpSession httpSession, @RequestParam("id") long id) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_User, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		// 根据用户权限返回数据
		UserModel userModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		DeviceModel deviceModel = deviceService.getDeviceModelById(id);
		if (deviceModel != null) {
			if (userModel.getRoleLevel() <= RoleModel.ROLE_LEVEL_WorkcenterAdmin//
					&& userModel.getWorkcenterId() == deviceModel.getWorkcenterId()) {
				return rBuilderService.update(response, //
						HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, //
						deviceModel);
			}
			if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_WorkshopAdmin//
					&& userModel.getWorkshopId() == deviceModel.getWorkshopId()) {
				return rBuilderService.update(response, //
						HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, //
						deviceModel);
			}
			if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_FactoryAdmin//
					&& userModel.getFactoryId() == deviceModel.getFactoryId()) {
				return rBuilderService.update(response, //
						HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, //
						deviceModel);
			}
			if (userModel.getRoleLevel() >= RoleModel.ROLE_LEVEL_OrganizationAdmin//
					&& userModel.getOrganizationId().equals(deviceModel.getOrganizationId())) {
				return rBuilderService.update(response, //
						HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, //
						deviceModel);
			}
		}
		return rBuilderService.update(response, //
				HttpResponseMsg.HEAD_MSG_NOT_FOUND_DATA, //
				null);
	}

	/**
	 * 获取设备状态变更历史列表
	 * 
	 * @param httpSession  Session会话
	 * @param workshopCode 车间编号
	 * @return 返回给前端的操作结果
	 */
	@RequestMapping(path = "/getDeviceStatusHistory")
	@ResponseBody
	public RBuilderService.Response getDeviceStatusHistory(HttpSession httpSession, //
			@RequestParam("deviceId") long deviceId, //
			@RequestParam("beginTimeMs") long beginTimeMs, //
			@RequestParam("maxNum") int maxNum) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_User, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE,
				deviceService.getDeviceStatusHistory(deviceId, beginTimeMs, maxNum));
	}

	/**
	 * 获取设备列表
	 * 
	 * @param httpSession  Session会话
	 * @param workshopCode 车间编号
	 * @return 返回给前端的操作结果
	 */
	@RequestMapping(path = "/getDeviceModels")
	@ResponseBody
	public RBuilderService.Response getDeviceModels(HttpSession httpSession) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_User, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		// 根据用户权限返回数据
		UserModel userModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		if (userModel.getRoleLevel() <= RoleModel.ROLE_LEVEL_WorkcenterAdmin) {
			return rBuilderService.update(response, //
					HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, //
					deviceService.getDeviceModelsByWorkcenterId(userModel.getWorkcenterId()));
		}
		if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_WorkshopAdmin) {
			return rBuilderService.update(response, //
					HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, //
					deviceService.getDeviceModelsByWorkshopId(userModel.getWorkshopId()));
		}
		if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_FactoryAdmin) {
			return rBuilderService.update(response, //
					HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, //
					deviceService.getDeviceModelsByFactoryId(userModel.getFactoryId()));
		}
		if (userModel.getRoleLevel() >= RoleModel.ROLE_LEVEL_OrganizationAdmin) {
			return rBuilderService.update(response, //
					HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, //
					deviceService.getDeviceModelsByOrganizationId(userModel.getOrganizationId()));
		}
		return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_DATA, null);
	}

	/**
	 * 获取设备列表
	 * 
	 * @param httpSession  Session会话
	 * @param workshopCode 车间编号
	 * @return 返回给前端的操作结果
	 */
	@RequestMapping(path = "/getDeviceModelsByWorkshopId")
	@ResponseBody
	public RBuilderService.Response getDeviceModelsByWorkshopId(HttpSession httpSession,
			@RequestParam("workshopId") long workshopId) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_User, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		// 根据用户权限返回数据
		UserModel userModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		if (userModel.getRoleLevel() <= RoleModel.ROLE_LEVEL_WorkcenterAdmin) {
			return rBuilderService.update(response, //
					HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, //
					deviceService.getDeviceModelsByWorkcenterId(userModel.getWorkcenterId()));
		}
		if (userModel.getRoleLevel() >= RoleModel.ROLE_LEVEL_WorkshopAdmin) {
			return rBuilderService.update(response, //
					HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, //
					deviceService.getDeviceModelsByWorkshopId(workshopId));
		}
		return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_DATA, null);
	}

	/**
	 * 设置设备信息
	 * 
	 * @param httpSession Session会话
	 * @param deviceModel 设备信息
	 * @return 返回给前端的操作结果
	 */
	@RequestMapping(path = "/setDeviceModel")
	@ResponseBody
	public RBuilderService.Response setDeviceModel(HttpSession httpSession, //
			@RequestBody DeviceModel deviceModel) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_WorkcenterAdmin, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		// 根据用户权限操作数据
		UserModel userModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		// 新建
		if (deviceModel.getId() <= 0) {
			deviceModel.setOrganizationId(userModel.getOrganizationId());
		}
		if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_WorkcenterAdmin) {
			if (userModel.getWorkcenterId() == deviceModel.getWorkcenterId()//
					&& userModel.getWorkshopId() == deviceModel.getWorkshopId()//
					&& userModel.getFactoryId() == deviceModel.getFactoryId()//
					&& userModel.getOrganizationId().equals(deviceModel.getOrganizationId())) {
				if (deviceService.setDeviceModel(deviceModel)) {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, deviceModel.getId());
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
				}
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
			}
		}
		if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_WorkshopAdmin) {
			if (userModel.getWorkshopId() == deviceModel.getWorkshopId()//
					&& userModel.getFactoryId() == deviceModel.getFactoryId()//
					&& userModel.getOrganizationId().equals(deviceModel.getOrganizationId())) {
				if (deviceService.setDeviceModel(deviceModel)) {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, deviceModel.getId());
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
				}
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
			}
		}
		if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_FactoryAdmin) {
			if (userModel.getFactoryId() == deviceModel.getFactoryId()//
					&& userModel.getOrganizationId().equals(deviceModel.getOrganizationId())) {
				if (deviceService.setDeviceModel(deviceModel)) {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, deviceModel.getId());
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
				}
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
			}
		}
		if (userModel.getRoleLevel() >= RoleModel.ROLE_LEVEL_OrganizationAdmin) {
			if (userModel.getOrganizationId().equals(deviceModel.getOrganizationId())) {
				if (deviceService.setDeviceModel(deviceModel)) {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, deviceModel.getId());
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
				}
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
			}
		}
		return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
	}

	/**
	 * 从Session中获取用户信息，然后删除设备
	 * 
	 * @param httpSession Session
	 * @return 操作结果
	 */
	@RequestMapping(path = "/deleteDeviceModel")
	@ResponseBody
	public RBuilderService.Response deleteDeviceModel(HttpSession httpSession, //
			@RequestBody DeviceModel deviceModel) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_WorkshopAdmin, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		// 根据用户权限操作数据
		UserModel userModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_WorkshopAdmin) {
			if (userModel.getWorkshopId() == deviceModel.getWorkshopId()//
					&& userModel.getFactoryId() == deviceModel.getFactoryId()//
					&& userModel.getOrganizationId().equals(deviceModel.getOrganizationId())) {
				if (deviceService.deleteDeviceModel(deviceModel)) {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, null);
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
				}
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
			}
		}
		if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_FactoryAdmin) {
			if (userModel.getFactoryId() == deviceModel.getFactoryId()//
					&& userModel.getOrganizationId().equals(deviceModel.getOrganizationId())) {
				if (deviceService.deleteDeviceModel(deviceModel)) {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, null);
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
				}
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
			}
		}
		if (userModel.getRoleLevel() >= RoleModel.ROLE_LEVEL_OrganizationAdmin) {
			if (userModel.getOrganizationId().equals(deviceModel.getOrganizationId())) {
				if (deviceService.deleteDeviceModel(deviceModel)) {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, null);
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
				}
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
			}
		}
		return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
	}

	@RequestMapping(path = "/exportDeviceStatusToExcel")
	public void exportDeviceStatusToExcel(HttpSession httpSession, //
			HttpServletRequest httpServletRequest, //
			HttpServletResponse httpServletResponse, //
			@RequestParam("factoryId") long factoryId, //
			@RequestParam("workshopId") long workshopId, //
			@RequestParam("workcenterId") long workcenterId, //
			@RequestParam("deviceId") long deviceId, //
			@RequestParam("beginTimeMs") long beginTimeMs, //
			@RequestParam("endTimeMs") long endTimeMs) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_User, //
				rBuilderService, //
				response) == false) {
			return;
		}
		// 数据分页
		int minIndex = 0;
		int dataNum = 1000;
		String minIndexStr = httpServletRequest.getParameter("minIndex");
		String dataNumStr = httpServletRequest.getParameter("dataNum");
		String fileName = httpServletRequest.getParameter("fileName");
		if (StringUtil.isEmpty(fileName)) {
			fileName = "" + System.currentTimeMillis();
		}
		if (StringUtil.isNumber(minIndexStr)) {
			minIndex = Integer.parseInt(minIndexStr);
		}
		if (StringUtil.isNumber(dataNumStr)) {
			dataNum = Integer.parseInt(dataNumStr);
		}
		// 根据用户权限返回数据
		UserModel userModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		List<Map<String, Object>> list = null;
		if (userModel.getRoleLevel() <= RoleModel.ROLE_LEVEL_WorkcenterAdmin) {
			list = deviceService.getDevicesStatusMapList(deviceId, //
					userModel.getWorkcenterId(), //
					workshopId, //
					factoryId, //
					userModel.getOrganizationId(), //
					beginTimeMs, //
					endTimeMs, //
					minIndex, //
					dataNum);
		}
		if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_WorkshopAdmin) {
			list = deviceService.getDevicesStatusMapList(deviceId, //
					workcenterId, //
					userModel.getWorkshopId(), //
					factoryId, //
					userModel.getOrganizationId(), //
					beginTimeMs, //
					endTimeMs, //
					minIndex, //
					dataNum);
		}
		if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_FactoryAdmin) {
			list = deviceService.getDevicesStatusMapList(deviceId, //
					workcenterId, //
					workshopId, //
					userModel.getFactoryId(), //
					userModel.getOrganizationId(), //
					beginTimeMs, //
					endTimeMs, //
					minIndex, //
					dataNum);
		}
		if (userModel.getRoleLevel() >= RoleModel.ROLE_LEVEL_OrganizationAdmin) {
			list = deviceService.getDevicesStatusMapList(deviceId, //
					workcenterId, //
					workshopId, //
					factoryId, //
					userModel.getOrganizationId(), //
					beginTimeMs, //
					endTimeMs, //
					minIndex, //
					dataNum);
		}
		// 转成Excel
		String[] title = new String[] { "序号", //
				"设备状态", //
				"更改时间", //
				"持续时间", //
				"设备识别码", //
				"设备编号", //
				"设备名称"};
		String[][] values = new String[list.size()][7];
		int cindex = 0;
		long lastDeviceId=-1;
		long lastChangeTimeMs=-1;
		for (int i = 0; i < list.size(); i++) {
			cindex = 0;
			String keepTime="";
			long currentDeviceId=(long) list.get(i).get("device_id");
			long timeMs=(long) list.get(i).get("time_ms");
			if (lastDeviceId==currentDeviceId) {
				keepTime=DateUtil.getDateTimeLengthDisc(lastChangeTimeMs-timeMs);
				lastChangeTimeMs=timeMs;
			}else {
				lastDeviceId=currentDeviceId;
				keepTime=DateUtil.getDateTimeLengthDisc(System.currentTimeMillis()-timeMs);
				lastChangeTimeMs=timeMs;
			}
			values[i][cindex++] = StringUtil.getEmptyString("" + (i + 1));
			values[i][cindex++] = StringUtil.getEmptyString(list.get(i).get("status_name").toString());
			values[i][cindex++] = StringUtil.getEmptyString(DateUtil.getDateLong(timeMs));
			values[i][cindex++] = StringUtil.getEmptyString(keepTime);
			values[i][cindex++] = StringUtil.getEmptyString(list.get(i).get("eqp_identity").toString());
			values[i][cindex++] = StringUtil.getEmptyString(list.get(i).get("code").toString());
			values[i][cindex++] = StringUtil.getEmptyString(list.get(i).get("name").toString());
		}
		HSSFWorkbook hssfWorkbook = PoiExcelUtil.getHSSFWorkbook(fileName, title, values, null);
		// 返回
		try {
			// 设置头
			httpServletResponse.setContentType("application/octet-stream;charset=UTF-8");
			httpServletResponse.setHeader("Content-Disposition", "attachment;filename=" + fileName + ".xls");
			httpServletResponse.addHeader("Pargam", "no-cache");
			httpServletResponse.addHeader("Cache-Control", "no-cache");
			// 返回
			OutputStream outputStream = httpServletResponse.getOutputStream();
			hssfWorkbook.write(outputStream);
			outputStream.flush();
			outputStream.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}


    /**
     * 根据设备组id查询设备
     * @param workCenterId
     * @return
     */
	@RequestMapping(path = "findByWorkCenterId")
    @ResponseBody
	public RBuilderService.Response  findByWorkcenterId(long workCenterId){
        List<DeviceModel> deviceModelList = deviceService.getDeviceModelsByWorkcenterId(workCenterId);
        RBuilderService.Response response = rBuilderService.build(null,null);
        if (deviceModelList.size()!=0){
            return rBuilderService.update(response,HttpResponseMsg.HEAD_MSG_SUCCESS_DONE,deviceModelList);
        }else {
                return rBuilderService.update(response,HttpResponseMsg.HEAD_MSG_ERROR_PARAM,null);
        }


    }







}
