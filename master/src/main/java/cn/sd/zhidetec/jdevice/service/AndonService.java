package cn.sd.zhidetec.jdevice.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.sd.zhidetec.jdevice.Starter;
import cn.sd.zhidetec.jdevice.dao.AndonMessageModelDao;
import cn.sd.zhidetec.jdevice.dao.AndonRecordModelDao;
import cn.sd.zhidetec.jdevice.model.AndonMessageModel;
import cn.sd.zhidetec.jdevice.model.AndonRecordModel;
import cn.sd.zhidetec.jdevice.model.DeviceModel;
import cn.sd.zhidetec.jdevice.model.DeviceSignboardModel;
import cn.sd.zhidetec.jdevice.model.DeviceStatusModel;
import cn.sd.zhidetec.jdevice.model.DeviceTempHumiModel;
import cn.sd.zhidetec.jdevice.model.MessageModel;
import cn.sd.zhidetec.jdevice.util.DateUtil;

@Service
public class AndonService {

	@Autowired
	protected AndonRecordModelDao andonRecordModelDao;
	@Autowired
	protected AndonMessageModelDao andonMessageModelDao;
	@Autowired
	protected MsgPushService msgPushService;
	@Autowired
	protected DeviceService deviceService;
	/**
	 * 检查设备标识牌的温湿度是否异常
	 * @param model 设备标识牌对象
	 * @return 是否发现异常
	 * */
	public boolean checkDeviceSignboardTemperatureAndHumidity(DeviceSignboardModel model) {
		if (model==null||model.getDeviceId()<=0) {
			return false;
		}
		boolean foundTemperatureWarn=false;
		boolean foundHumidityWarn=false;
		//当前温湿度检查
		if (model.getTemperatureMinVal()>model.getTemperature()||model.getTemperatureMaxVal()<model.getTemperature()) {
			foundTemperatureWarn=true;
		}
		if (model.getHumidityMinVal()>model.getHumidity()||model.getHumidityMaxVal()<model.getHumidity()) {
			foundHumidityWarn=true;
		}
		//历史温湿度检查
		if (foundHumidityWarn==false&&foundTemperatureWarn==false) {
			if (model.getTemperatureMinVal()>model.getTemperature()||model.getTemperatureMaxVal()<model.getTemperature()) {
				foundTemperatureWarn=true;
			}
			if (model.getHumidityMinVal()>model.getHumidity()||model.getHumidityMaxVal()<model.getHumidity()) {
				foundHumidityWarn=true;
			}
		}
		//消息生成&推送
		if (foundHumidityWarn||foundTemperatureWarn) {
			if (model!=null) {
				//获取绑定的设备
				DeviceModel deviceModel=deviceService.getDeviceModelById(model.getDeviceId());
				//生成
				MessageModel messageModel=MessageModel.build(model);
				//
				messageModel.setId(Starter.IdMaker.nextId());
				messageModel.setTimeMs(model.getLastSyncTimeMs());
				messageModel.setPhontoUrl(deviceModel.getLogoUrl());
				messageModel.setType(MessageModel.TYPE_Tips);
				messageModel.setTitle(deviceModel.getName()+"温湿度异常");
				messageModel.setContent("当前："+model.getTemperature()+"℃、"+model.getHumidity()+"%,最大："+model.getTemperatureMaxVal()+"℃、"+model.getHumidityMaxVal()+"%,最小："+model.getTemperatureMinVal()+"℃、"+model.getHumidityMinVal()+"%");
				//
				messageModel.setWarnLevel(0);
				messageModel.setUrl("zh_CN/app/default/view/deviceInfo.html?id="+deviceModel.getId());
				//推送
				msgPushService.pushMessage(messageModel);
				return true;	
			}
		}
		return false;
	}
	/**
	 * 检查设备标识牌的温湿度是否异常
	 * @param model 设备标识牌对象
	 * @param list 设备温湿度列表
	 * @return 是否发现异常
	 * */
	public boolean checkDeviceSignboardTemperatureAndHumidity(DeviceSignboardModel model,List<DeviceTempHumiModel> list) {
		if (model==null||model.getDeviceId()<=0) {
			return false;
		}
		boolean foundTemperatureWarn=false;
		boolean foundHumidityWarn=false;
		DeviceTempHumiModel errorModel=null;
		//当前温湿度检查
		if (model.getTemperatureMinVal()>model.getTemperature()||model.getTemperatureMaxVal()<model.getTemperature()) {
			foundTemperatureWarn=true;
		}
		if (model.getHumidityMinVal()>model.getHumidity()||model.getHumidityMaxVal()<model.getHumidity()) {
			foundHumidityWarn=true;
		}
		//历史温湿度检查
		if (foundHumidityWarn==false&&foundTemperatureWarn==false) {
			if (list!=null) {
				for (DeviceTempHumiModel deviceTempHumiModel : list) {
					if (model.getTemperatureMinVal()>deviceTempHumiModel.getTemperature()||model.getTemperatureMaxVal()<deviceTempHumiModel.getTemperature()) {
						foundTemperatureWarn=true;
					}
					if (model.getHumidityMinVal()>deviceTempHumiModel.getHumidity()||model.getHumidityMaxVal()<deviceTempHumiModel.getHumidity()) {
						foundHumidityWarn=true;
					}
					if (foundHumidityWarn==true||foundTemperatureWarn==true) {
						errorModel=deviceTempHumiModel;
						break;
					}
				}
			}
		}
		//消息生成&推送
		if (foundHumidityWarn||foundTemperatureWarn) {
			if (errorModel!=null) {
				//获取绑定的设备
				DeviceModel deviceModel=deviceService.getDeviceModelById(model.getDeviceId());
				//生成
				MessageModel messageModel=MessageModel.build(errorModel);
				//
				messageModel.setId(Starter.IdMaker.nextId());
				messageModel.setTimeMs(errorModel.getTimeMs());
				messageModel.setPhontoUrl(deviceModel.getLogoUrl());
				messageModel.setType(MessageModel.TYPE_Tips);
				messageModel.setTitle(deviceModel.getName()+"温湿度异常");
				messageModel.setContent("当前："+errorModel.getTemperature()+"℃、"+errorModel.getHumidity()+"%,最大："+model.getTemperatureMaxVal()+"℃、"+model.getHumidityMaxVal()+"%,最小："+model.getTemperatureMinVal()+"℃、"+model.getHumidityMinVal()+"%");
				//
				messageModel.setWarnLevel(0);
				messageModel.setUrl("zh_CN/app/default/view/deviceInfo.html?id="+deviceModel.getId());
				//推送
				msgPushService.pushMessage(messageModel);
				return true;	
			}
		}
		return false;
	}
	
	/**
	 * 检查设备供电电压是否异常
	 * @param model 设备标识牌对象
	 * @return 是否发现异常
	 * */
	public boolean checkDeviceSignboardPowerVoltage(DeviceSignboardModel model) {
		if (model==null||model.getDeviceId()<=0) {
			return false;
		}
		//供电电压异常
		if (model.getPowerVoltageMinVal()>model.getPowerVoltage()||model.getPowerVoltageMaxVal()<model.getPowerVoltage()) {
			//获取绑定的设备
			DeviceModel deviceModel=deviceService.getDeviceModelById(model.getDeviceId());
			if (deviceModel!=null) {
				//消息生成
				MessageModel messageModel=new MessageModel();
				//
				messageModel.setId(Starter.IdMaker.nextId());
				messageModel.setTimeMs(model.getLastSyncTimeMs());
				messageModel.setPhontoUrl(deviceModel.getLogoUrl());
				messageModel.setType(MessageModel.TYPE_Tips);
				messageModel.setTitle(deviceModel.getName()+"电池电量低");
				messageModel.setContent(deviceModel.getName()+"标识牌电池电量低，请及时更换电池！！");
				//
				messageModel.setDeviceId(deviceModel.getId());
				messageModel.setWorkcenterId(deviceModel.getWorkcenterId());
				messageModel.setWorkshopId(deviceModel.getWorkshopId());
				messageModel.setFactoryId(deviceModel.getFactoryId());
				messageModel.setOrganizationId(deviceModel.getOrganizationId());
				//
				messageModel.setWarnLevel(0);
				messageModel.setUrl("zh_CN/app/default/view/deviceInfo.html?id="+deviceModel.getId());
				//消息推送
				boolean needSendMessage=false;
				synchronized (lastPowerWarnTimeMapLockObj) {
					Long lastWarnTimeMs=lastPowerWarnTimeMap.get(model.getCode());
					if (lastWarnTimeMs==null||lastWarnTimeMs>=System.currentTimeMillis()+DateUtil.MILLISECOND_ONE_DAY) {
						lastPowerWarnTimeMap.put(model.getCode(), System.currentTimeMillis());
						needSendMessage=true;
					}	
				}
				if (needSendMessage) {
					msgPushService.pushMessage(messageModel);	
				}
				return true;	
			}
		}
		return false;
	}
	private Map<String, Long> lastPowerWarnTimeMap=new HashMap<>();
	private Object lastPowerWarnTimeMapLockObj = new Object();
	
	/**
	 * 设备状态更新
	 * @param lastDeviceModel 更新前的设备
	 * @param newDeviceModel 更新后的设备
	 * */
	public void updateDeviceStatus(DeviceModel lastDeviceModel,DeviceModel newDeviceModel) {
		//设备状态差异性比较
		if (lastDeviceModel!=null) {
			//设备状态不变
			if (newDeviceModel.getStatusValue()==lastDeviceModel.getStatusValue()) {
				return;
			}
			int deviceStatusValue=newDeviceModel.getStatusValue();
			//去除离线状态
			if (deviceStatusValue==DeviceStatusModel.VALUE_Offline) {
				return;
			}
			//查找离线前的设备状态
			int statusValueBeforeOfflineInt=-1;
			if (lastDeviceModel.getStatusValue()==DeviceStatusModel.VALUE_Offline) {
				Integer statusValueBeforeOffline=deviceService.getDevicePreStatusValue(lastDeviceModel.getId(),lastDeviceModel.getStatusTimeMs());
				//离线前无状态
				if (statusValueBeforeOffline==null||statusValueBeforeOffline<0) {
				}
				else {
					statusValueBeforeOfflineInt=statusValueBeforeOffline;
					//离线前后设备状态不变
					if (deviceStatusValue==statusValueBeforeOffline) {
						return;
					}	
				}
			}
			/**
			 * Andon消息处理
			 * */
			//待料消息直接关闭
			if (lastDeviceModel.getStatusValue()==DeviceStatusModel.VALUE_Wait//
					||lastDeviceModel.getStatusValue()==DeviceStatusModel.VALUE_Pending) {
				long solveTimeMs=System.currentTimeMillis();
				andonMessageModelDao.setMsgStatusClosed(lastDeviceModel.getId(),solveTimeMs,solveTimeMs,lastDeviceModel.getStatusName());
				//生成记录
				AndonMessageModel currentAndonMessageModel=andonMessageModelDao.getAndonMessageModelBySolveTimeMs(lastDeviceModel.getId(), solveTimeMs);
				if (currentAndonMessageModel!=null) {
					AndonRecordModel andonRecordModel=new AndonRecordModel();
					andonRecordModel.setAndonId(currentAndonMessageModel.getId());
					andonRecordModel.setContent("当前状态: "+newDeviceModel.getStatusName());
					andonRecordModel.setId(Starter.IdMaker.nextId());
					andonRecordModel.setTimeMs(solveTimeMs);
					andonRecordModel.setTitle("已关闭");
					andonRecordModel.setAndonStatus(AndonMessageModel.STATUS_VALUE_NotClosed);	
					andonRecordModelDao.addRecordModel(andonRecordModel);
				}
			}
			//旧Andon消息可关闭
			else if(lastDeviceModel.getStatusValue()==DeviceStatusModel.VALUE_Fault//
					||statusValueBeforeOfflineInt==DeviceStatusModel.VALUE_Fault//
					||statusValueBeforeOfflineInt==DeviceStatusModel.VALUE_Wait//
					||statusValueBeforeOfflineInt==DeviceStatusModel.VALUE_Pending) {
				long solveTimeMs=System.currentTimeMillis();
				andonMessageModelDao.setMsgStatusNotClosed(lastDeviceModel.getId(),solveTimeMs);
				//生成记录
				AndonMessageModel currentAndonMessageModel=andonMessageModelDao.getAndonMessageModelBySolveTimeMs(lastDeviceModel.getId(), solveTimeMs);
				if (currentAndonMessageModel!=null) {
					AndonRecordModel andonRecordModel=new AndonRecordModel();
					andonRecordModel.setAndonId(currentAndonMessageModel.getId());
					andonRecordModel.setContent("当前状态: "+newDeviceModel.getStatusName());
					andonRecordModel.setId(Starter.IdMaker.nextId());
					andonRecordModel.setTimeMs(solveTimeMs);
					andonRecordModel.setTitle("已恢复");
					andonRecordModel.setAndonStatus(AndonMessageModel.STATUS_VALUE_NotClosed);	
					andonRecordModelDao.addRecordModel(andonRecordModel);
				}	
			}
			//生成新Andon消息
			if (deviceStatusValue==DeviceStatusModel.VALUE_Fault//
					||deviceStatusValue==DeviceStatusModel.VALUE_Wait//
					||deviceStatusValue==DeviceStatusModel.VALUE_Pending) {
				AndonMessageModel andonMessageModel=AndonMessageModel.buildByDeviceModel(Starter.IdMaker.nextId(),//
						newDeviceModel,//
						lastDeviceModel,//
						AndonMessageModel.STATUS_VALUE_Unclaimed,//
						AndonMessageModel.WARN_LEVEL_ANDON_MESSAGE);	
				//入库
				andonMessageModelDao.newAndonMessageModel(andonMessageModel);
				//记录
				AndonRecordModel andonRecordModel=new AndonRecordModel();
				andonRecordModel.setAndonId(andonMessageModel.getId());
				andonRecordModel.setAndonStatus(AndonMessageModel.STATUS_VALUE_Init);
				andonRecordModel.setId(Starter.IdMaker.nextId());
				andonRecordModel.setTimeMs(andonMessageModel.getCreateTimeMs());
				andonRecordModel.setTitle(andonMessageModel.getTitle());
				andonRecordModel.setContent("设备编号: "+andonMessageModel.getDeviceCode());
				andonRecordModelDao.addRecordModel(andonRecordModel);
				//推送Andon消息
				msgPushService.pushAndonMessage(andonMessageModel,andonMessageModel.getCreateTimeMs());
			}
		}
	}
	public boolean changeWorkcenter(long deviceId,long workcenterId,long workshopId,long factoryId) {
		return andonMessageModelDao.changeWorkcenter(deviceId,workcenterId,workshopId,factoryId);
	}
	public boolean changeWorkshop(long workcenterId,long workshopId,long factoryId) {
		return andonMessageModelDao.changeWorkshop(workcenterId, workshopId, factoryId);
	}
	public boolean changeFactory(long workshopId,long factoryId) {
		return andonMessageModelDao.changeFactory(workshopId, factoryId);
	}
}
