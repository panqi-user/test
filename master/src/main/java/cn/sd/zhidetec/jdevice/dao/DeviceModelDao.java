package cn.sd.zhidetec.jdevice.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import cn.sd.zhidetec.jdevice.Starter;
import cn.sd.zhidetec.jdevice.config.DataSourceConfiger;
import cn.sd.zhidetec.jdevice.model.DeviceModel;
import cn.sd.zhidetec.jdevice.model.DeviceStatusModel;
import cn.sd.zhidetec.jdevice.pojo.DeviceStatusSumPojo;
import cn.sd.zhidetec.jdevice.util.StringUtil;

@Service
public class DeviceModelDao {

	@Autowired
	@Qualifier(DataSourceConfiger.BEAN_NAME_JdeviceJdbcTemplate)
	protected JdbcTemplate jdbcTemplate;
	protected Logger logger = LoggerFactory.getLogger(getClass());

	/**
	 * 设置设备监控器的类型
	 * */
	public boolean setDeviceMonitor(long deviceId, int monitor) {
		int i = 0;
		Object[] args = new Object[2];
		args[i++] = monitor;
		args[i++] = deviceId;
		i = jdbcTemplate.update(Sql_SetDeviceMonitor, args);
		if (i > 0) {
			return true;
		}
		return false;
	}

	private static String Sql_SetDeviceMonitor = "UPDATE jdevice_c_device SET monitor=? WHERE id=?";

	/**
	 * 获取设备监控者监控器的设备数目
	 */
	public int getMonitorNum(int monitor, String organizationId) {
		Object[] args = new Object[2];
		args[0] = organizationId;
		args[1] = monitor;
		return jdbcTemplate.queryForObject(Sql_GetMonitorNum_By_OrganizationId, args, Integer.class);
	}

	private static String Sql_GetMonitorNum_By_OrganizationId = "SELECT COUNT(*) AS device_num FROM jdevice_c_device WHERE organization_id=? AND monitor=?";

	/**
	 * 获取设备状态汇总列表
	 */
	public List<DeviceStatusSumPojo> getDeviceStatusSumPojoByOrganizationId(String organizationId) {
		Object[] args = new Object[1];
		args[0] = organizationId;
		List<DeviceStatusSumPojo> list = jdbcTemplate.query(Sql_GetDeviceStatusSumPojo_By_OrganizationId, args,
				pojoPropertyRowMapper);
		return list;
	}

	private static String Sql_GetDeviceStatusSumPojo_By_OrganizationId = "SELECT COUNT(*) AS device_num,status_value,status_name,status FROM jdevice_c_device WHERE organization_id=? GROUP BY status_value,status_name,status";
	private BeanPropertyRowMapper<DeviceStatusSumPojo> pojoPropertyRowMapper = new BeanPropertyRowMapper<DeviceStatusSumPojo>(
			DeviceStatusSumPojo.class);

	/**
	 * 删除设备
	 * 
	 * @param deviceModel 设备
	 * @return 操作是否成功
	 */
	public boolean deleteDeviceModel(DeviceModel deviceModel) {
		int i = 0;
		Object[] args = new Object[1];
		args[i++] = deviceModel.getId();
		if (deviceModel.getId()>0) {
			i = jdbcTemplate.update(Sql_DeleteModel_By_Id, args);
		}
		if (i > 0) {
			return true;
		}
		return false;
	}

	private static String Sql_DeleteModel_By_Id = "DELETE FROM jdevice_c_device WHERE id=?";

	/**
	 * 修改设备状态
	 * 
	 * @param deviceModel 设备状态
	 * @return 操作是否成功
	 */
	public boolean setDeviceStatus(DeviceModel deviceModel) {
		int i = 0;
		Object[] args = new Object[7];
		// 修改信息
		if (deviceModel.getId()>0) {
			args[i++] = deviceModel.getMonitor();
			args[i++] = deviceModel.getStatus_monitor_bits();
			args[i++] = StringUtil.getEmptyString(deviceModel.getStatus());
			args[i++] = deviceModel.getStatusValue();
			args[i++] = StringUtil.getEmptyString(deviceModel.getStatusName());
			args[i++] = deviceModel.getStatusTimeMs();
			args[i++] = deviceModel.getId();
			i = jdbcTemplate.update(Sql_UpdateDeviceStatus_By_Id, args);
		}
		if (i > 0) {
			return true;
		}
		return false;
	}

	private static String Sql_UpdateDeviceStatus_By_Id = "UPDATE jdevice_c_device" + //
			" SET " + //
			"monitor = ?," + //
			"status_monitor_bits = ?," + //
			"status = ?," + //
			"status_value = ?," + //
			"status_name = ?," + //
			"status_time_ms = ?" + //
			" WHERE " + //
			"id=?";

	/**
	 * 设置设备信息，如果不存在则新建，不设置设备状态，新建设备时默认为离线状态
	 * 
	 * @param deviceModel 设备信息
	 * @return 操作是否成功
	 */
	public boolean setDeviceModel(DeviceModel deviceModel) {
		int i = 0;
		Object[] args = null;
		// 修改信息
		if (deviceModel.getId() > 0) {
			args = new Object[11];
			args[i++] = StringUtil.getEmptyString(deviceModel.getEqpIdentity());
			args[i++] = StringUtil.getEmptyString(deviceModel.getCode());
			args[i++] = StringUtil.getEmptyString(deviceModel.getName());
			args[i++] = deviceModel.getLevel();
			args[i++] = StringUtil.getEmptyString(deviceModel.getLogoUrl());
			args[i++] = deviceModel.getTypeId();
			args[i++] = deviceModel.getWorkcenterId();
			args[i++] = deviceModel.getWorkshopId();
			args[i++] = deviceModel.getFactoryId();
			args[i++] = StringUtil.getEmptyString(deviceModel.getOrganizationId());
			args[i++] = deviceModel.getId();
			i = jdbcTemplate.update(Sql_UpdateDeviceInfo_By_Id, args);
		}
		// 新建信息
		else {
			args = new Object[17];
			deviceModel.setId(Starter.IdMaker.nextId());
			deviceModel.setStatus(DeviceStatusModel.STATUS_Offline);
			deviceModel.setStatusName(DeviceStatusModel.NAME_Offline);
			deviceModel.setStatusValue(DeviceStatusModel.VALUE_Offline);
			deviceModel.setStatusTimeMs(System.currentTimeMillis());
			deviceModel.setMonitor(DeviceModel.MONITOR_VAL_NONE);
			deviceModel.setStatus_monitor_bits(-1);
			args[i++] = deviceModel.getId();
			args[i++] = StringUtil.getEmptyString(deviceModel.getEqpIdentity());
			args[i++] = StringUtil.getEmptyString(deviceModel.getCode());
			args[i++] = StringUtil.getEmptyString(deviceModel.getName());
			args[i++] = deviceModel.getLevel();
			args[i++] = StringUtil.getEmptyString(deviceModel.getStatus());
			args[i++] = deviceModel.getStatusValue();
			args[i++] = StringUtil.getEmptyString(deviceModel.getStatusName());
			args[i++] = deviceModel.getStatusTimeMs();
			args[i++] = deviceModel.getMonitor();
			args[i++] = StringUtil.getEmptyString(deviceModel.getLogoUrl());
			args[i++] = deviceModel.getTypeId();
			args[i++] = deviceModel.getWorkcenterId();
			args[i++] = deviceModel.getWorkshopId();
			args[i++] = deviceModel.getFactoryId();
			args[i++] = StringUtil.getEmptyString(deviceModel.getOrganizationId());
			args[i++] = deviceModel.getStatus_monitor_bits();
			i = jdbcTemplate.update(Sql_InsertModel, args);
		}
		if (i > 0) {
			return true;
		}
		return false;
	}

	private static String Sql_UpdateDeviceInfo_By_Id = "UPDATE jdevice_c_device" + //
			" SET " + //
			"eqp_identity = ?," + //
			"code = ?," + //
			"name = ?," + //
			"level = ?," + //
			"logo_url = ?," + //
			"type_id = ?," + //
			"workcenter_id = ?," + //
			"workshop_id = ?," + //
			"factory_id = ?," + //
			"organization_id = ?" + //
			" WHERE " + //
			"id=?";
	private static String Sql_InsertModel = "INSERT INTO jdevice_c_device (" + //
			"id," + //
			"eqp_identity," + //
			"code," + //
			"name," + //
			"level," + //
			"status," + //
			"status_value," + //
			"status_name," + //
			"status_time_ms," + //
			"monitor," + //
			"logo_url," + //
			"type_id," + //
			"workcenter_id," + //
			"workshop_id," + //
			"factory_id," + //
			"organization_id," + //
			"status_monitor_bits" + //
			")VALUES(" + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?" + //
			")";

	/**
	 * 根据设备ID获取设备列表,先从缓存中获取，如果不存在则从数据库获取并更新到缓存
	 * 
	 * @param deviceId 设备ID
	 * @return 设备
	 */
	public DeviceModel getDeviceModelById(long deviceId) {
		DeviceModel model=null;
		if (deviceId>0) {
			Object[] args = new Object[1];
			args[0] = deviceId;
			List<DeviceModel> list = jdbcTemplate.query(Sql_GetModel_By_Id, args, beanPropertyRowMapper);
			if (list == null || list.size() <= 0) {
				return null;
			}
			model = list.get(0);
		}
		return model;
	}

	private static String Sql_GetModel_By_Id = "SELECT * FROM jdevice_c_device WHERE id = ?";

	/**
	 * 根据工作中心ID获取设备列表
	 * 
	 * @param workcenterId 工作中心ID
	 * @return 数据列表
	 */
	public List<DeviceModel> getDeviceModelsByWorkcenterId(long workcenterId) {
		Object[] args = new Object[1];
		args[0] = workcenterId;
		List<DeviceModel> list = jdbcTemplate.query(Sql_GetModels_By_WorkcenterId, args, beanPropertyRowMapper);
		return list;
	}

	private static String Sql_GetModels_By_WorkcenterId = "SELECT * FROM jdevice_c_device WHERE workcenter_id = ?";

	/**
	 * 根据车间ID获取设备列表
	 * 
	 * @param workshopId 车间ID
	 * @return 数据列表
	 */
	public List<DeviceModel> getDeviceModelsByWorkshopId(long workshopId) {
		Object[] args = new Object[1];
		args[0] = workshopId;
		List<DeviceModel> list = jdbcTemplate.query(Sql_GetModels_By_WorkshopId, args, beanPropertyRowMapper);
		return list;
	}

	private static String Sql_GetModels_By_WorkshopId = "SELECT * FROM jdevice_c_device WHERE workshop_id = ? ORDER BY workcenter_id DESC";

	/**
	 * 根据工厂ID获取设备列表
	 * 
	 * @param factoryId 工厂ID
	 * @return 数据列表
	 */
	public List<DeviceModel> getDeviceModelsByFactoryId(long factoryId) {
		Object[] args = new Object[1];
		args[0] = factoryId;
		List<DeviceModel> list = jdbcTemplate.query(Sql_GetModels_By_FactoryId, args, beanPropertyRowMapper);
		return list;
	}

	private static String Sql_GetModels_By_FactoryId = "SELECT * FROM jdevice_c_device WHERE factory_id = ? ORDER BY workcenter_id DESC";

	/**
	 * 根据组织ID获取设备列表
	 * 
	 * @param organizationId 组织ID
	 * @return 数据列表
	 */
	public List<DeviceModel> getDeviceModelsByOrganizationId(String organizationId) {
		Object[] args = new Object[1];
		args[0] = organizationId;
		List<DeviceModel> list = jdbcTemplate.query(Sql_GetModels_By_OrganizationId, args, beanPropertyRowMapper);
		return list;
	}

	private static String Sql_GetModels_By_OrganizationId = "SELECT * FROM jdevice_c_device WHERE organization_id = ? ORDER BY workcenter_id DESC";
	/**
	 * 根据车间ID和设备识别码来查询设备
	 * 
	 * @param workshopId 车间ID
	 * @param eqpIdentity 设备识别码
	 * @return 目标设备
	 * */
	public DeviceModel getDeviceModelByEqpIdentity(String eqpIdentity, long workshopId) {
		Object[] args = new Object[2];
		int i=0;
		args[i++] = workshopId;
		args[i++] = eqpIdentity;
		List<DeviceModel> list = jdbcTemplate.query(Sql_GetModels_By_WorkshopId_EqpIdentity, args, beanPropertyRowMapper);
		if (list!=null&&list.size()>0) {
			return list.get(0);
		}
		return null;
	}
	private static String Sql_GetModels_By_WorkshopId_EqpIdentity = "SELECT * FROM jdevice_c_device WHERE workshop_id = ? AND eqp_identity = ? ORDER BY workcenter_id DESC";
	/**
	 * 根据车间ID计算车间各个设备从某个时间开始指定状态的数目
	 * @param workshopId 车间ID
	 * @param statusValue 状态值
	 * @param startTimeMs 开始时间
	 * @return 返回结果
	 * */
	public List<Map<String, Object>> getWorkshopDeviceStatusNum(long workshopId,//
			int statusValue,//
			long startTimeMs) {
		Object[] args = new Object[3];
		int i=0;
		args[i++] = workshopId;
		args[i++] = statusValue;
		args[i++] = startTimeMs;
		return jdbcTemplate.queryForList(Sql_GetDeviceStatusNum_By_WorkshopId,args);
	}
	private static String Sql_GetDeviceStatusNum_By_WorkshopId="SELECT "+//
			" COUNT(*) AS status_num, "+//
			"device_id "+//
			" FROM "+//
			"jdevice_h_device_status "+//
			" WHERE "+//
			"workshop_id = ? "+//
			" AND status_value = ? "+//
			" AND time_ms > ? "+//
			"GROUP BY device_id ORDER BY status_num DESC";
	/**
	 * 计算某个设备从指定时间开始各设备状态的数目
	 * @param deviceId 设备ID
	 * @param statusValue 状态值
	 * @param startTimeMs 开始时间
	 * @return 返回结果
	 * */
	public List<Map<String, Object>> getDeviceStatusNum(long deviceId,//
			int statusValue,//
			long startTimeMs) {
		Object[] args = new Object[3];
		int i=0;
		args[i++] = deviceId;
		args[i++] = statusValue;
		args[i++] = startTimeMs;
		return jdbcTemplate.queryForList(Sql_GetDeviceStatusNum_By_Id,args);
	}
	private static String Sql_GetDeviceStatusNum_By_Id="SELECT "+//
			" COUNT(*) AS status_num, "+//
			"device_id "+//
			" FROM "+//
			"jdevice_h_device_status "+//
			" WHERE "+//
			"device_id = 159367834419400 "+//
			" AND status_value = 256 "+//
			" AND time_ms > 3 "+//
			" GROUP BY device_id ORDER BY status_num DESC";
	/**
	 * 根据工作中心ID计算工作中心各个设备从某个时间开始指定状态的数目
	 * @param workcenterId 工作中心ID
	 * @param statusValue 状态值
	 * @param startTimeMs 开始时间
	 * @return 返回结果
	 * */
	public List<Map<String, Object>> getWorkcenterDeviceStatusNum(long workcenterId, int statusValue, long startTimeMs) {
		Object[] args = new Object[3];
		int i=0;
		args[i++] = workcenterId;
		args[i++] = statusValue;
		args[i++] = startTimeMs;
		return jdbcTemplate.queryForList(Sql_GetDeviceStatusNum_By_WorkcenterId,args);
	}
	private static String Sql_GetDeviceStatusNum_By_WorkcenterId="SELECT "+//
			" COUNT(*) AS device_num, "+//
			"device_id "+//
			" FROM "+//
			"jdevice_h_device_status "+//
			" WHERE "+//
			"workcenter_id = ? "+//
			" AND status_value = ? "+//
			" AND time_ms > ? "+//
			"GROUP BY device_id";
	private BeanPropertyRowMapper<DeviceModel> beanPropertyRowMapper = new BeanPropertyRowMapper<DeviceModel>(
			DeviceModel.class);

	public boolean insertDeviceStatusHistory(DeviceModel lastDeviceModel,DeviceModel deviceModel) {
		if (lastDeviceModel==null||deviceModel==null) {
			return false;
		}
		if (lastDeviceModel.getStatusValue()==deviceModel.getStatusValue()) {
			return false;
		}
		//插入
		Object[] args = new Object[14];
		int i=0;
		args[i++] = deviceModel.getId();
		args[i++] = deviceModel.getStatusTimeMs();
		args[i++] = deviceModel.getStatus();
		args[i++] = deviceModel.getStatusValue();
		args[i++] = deviceModel.getStatusName();
		args[i++] = lastDeviceModel.getStatusTimeMs();
		args[i++] = lastDeviceModel.getStatus();
		args[i++] = lastDeviceModel.getStatusValue();
		args[i++] = lastDeviceModel.getStatusName();
		args[i++] = deviceModel.getStatusTimeMs()-lastDeviceModel.getStatusTimeMs();
		args[i++] = lastDeviceModel.getWorkcenterId();
		args[i++] = lastDeviceModel.getWorkshopId();
		args[i++] = lastDeviceModel.getFactoryId();
		args[i++] = lastDeviceModel.getOrganizationId();
		i=jdbcTemplate.update(Sql_InsertDeviceStatusHistory, args);
		return i>0;
	}
	private static String Sql_InsertDeviceStatusHistory="INSERT INTO jdevice_h_device_status ("+//
			"device_id,"+//
			"time_ms,"+//
			"status,"+//
			"status_value,"+//
			"status_name,"+//
			"pre_time_ms,"+//
			"pre_status,"+//
			"pre_status_value,"+//
			"pre_status_name,"+//
			"keep_time_ms,"+//
			"workcenter_id,"+//
			"workshop_id,"+//
			"factory_id,"+//
			"organization_id"+//
			")VALUES("+//
			"?,"+//
			"?,"+//
			"?,"+//
			"?,"+//
			"?,"+//
			"?,"+//
			"?,"+//
			"?,"+//
			"?,"+//
			"?,"+//
			"?,"+//
			"?,"+//
			"?,"+//
			"?)";
	
	public Integer getDevicePreStatusValue(long deviceId,long statusTimeMs) {
		if (deviceId<=0||statusTimeMs<=0) {
			return -1;
		}
		Object[] args = new Object[2];
		int i=0;
		args[i++] = deviceId;
		args[i++] = statusTimeMs;
		List<Integer> list = jdbcTemplate.queryForList(Sql_GetDevicePreStatusValue_By_StatusTimeMs, args, Integer.class);
		if (list.size()<=0) {
			return null;
		}
		return list.get(0);
	}
	private static String Sql_GetDevicePreStatusValue_By_StatusTimeMs="SELECT pre_status_value FROM jdevice_h_device_status WHERE device_id=? AND time_ms=?";
	
	public List<DeviceStatusSumPojo> getDeviceStatusSumPojoByWorkshopId(long workshopId) {
		Object[] args = new Object[1];
		args[0] = workshopId;
		List<DeviceStatusSumPojo> list = jdbcTemplate.query(Sql_GetDeviceStatusSumPojo_By_WorkshopId, args,
				pojoPropertyRowMapper);
		return list;
	}
	
	private static String Sql_GetDeviceStatusSumPojo_By_WorkshopId = "SELECT COUNT(*) AS device_num,status_value,status_name,status FROM jdevice_c_device WHERE workshop_id=? GROUP BY status_value,status_name,status";

	public List<Map<String, Object>> getDeviceStatusHistory(long deviceId, long beginTimeMs, int maxNum) {
		if (deviceId<=0||maxNum<=0) {
			return new ArrayList<Map<String,Object>>();
		}
		Object[] args = new Object[3];
		int i=0;
		args[i++] = deviceId;
		args[i++] = beginTimeMs;
		args[i++] = maxNum;
		List<Map<String, Object>> list = jdbcTemplate.queryForList(Sql_getDeviceStatusHistory, args);
		return list;
	}
	private static String Sql_getDeviceStatusHistory = "SELECT * FROM jdevice_h_device_status WHERE device_id=? AND time_ms>=? ORDER BY time_ms DESC LIMIT 0,?;";
	

	public boolean changeStatusHistoryWorkcenter(long deviceId, long workcenterId, long workshopId, long factoryId) {
		Object[] args = new Object[4];
		int i=0;
		args[i++] = workcenterId;
		args[i++] = workshopId;
		args[i++] = factoryId;
		args[i++] = deviceId;
		return jdbcTemplate.update(Sql_ChangeStatusHistoryWorkcenter_By_DeviceId, args)>0;
	}
	
	private static String Sql_ChangeStatusHistoryWorkcenter_By_DeviceId = "UPDATE jdevice_h_device_status SET workcenter_id=?,workshop_id=?,factory_id=? WHERE device_id=?;";
	
	public boolean changeStatusHistoryWorkshop(long workcenterId,long workshopId, long factoryId) {
		Object[] args = new Object[3];
		int i=0;
		args[i++] = workshopId;
		args[i++] = factoryId;
		args[i++] = workcenterId;
		return jdbcTemplate.update(Sql_ChangeStatusHistoryWorkshop_By_WorkcenterId, args)>0;
	}
	
	private static String Sql_ChangeStatusHistoryWorkshop_By_WorkcenterId="UPDATE jdevice_h_device_status SET workshop_id=?,factory_id=? WHERE workcenter_id=?;";
	
	public boolean changeStatusHistoryFactory(long workshopId,long factoryId) {
		Object[] args = new Object[2];
		int i=0;
		args[i++] = factoryId;
		args[i++] = workshopId;
		return jdbcTemplate.update(Sql_ChangeStatusHistoryFactory_By_WorkshopId, args)>0;
	}
	
	private static String Sql_ChangeStatusHistoryFactory_By_WorkshopId="UPDATE jdevice_h_device_status SET factory_id=? WHERE workshop_id=?;";

	public boolean changeDeviceFactory(long workshopId, long factoryId) {
		Object[] args = new Object[2];
		int i=0;
		args[i++] = factoryId;
		args[i++] = workshopId;
		return jdbcTemplate.update(Sql_ChangeDeviceFactory_By_WorkshopId, args)>0;
	}
	
	private static String Sql_ChangeDeviceFactory_By_WorkshopId="UPDATE jdevice_c_device SET factory_id=? WHERE workshop_id=?;";

	public boolean changeDeviceWorkshop(long workcenterId,long workshopId, long factoryId) {
		Object[] args = new Object[3];
		int i=0;
		args[i++] = workshopId;
		args[i++] = factoryId;
		args[i++] = workcenterId;
		return jdbcTemplate.update(Sql_ChangeDeviceWorkshop_By_WorkcenterId, args)>0;
	}
	
	private static String Sql_ChangeDeviceWorkshop_By_WorkcenterId="UPDATE jdevice_c_device SET workshop_id=?,factory_id=? WHERE workcenter_id=?;";
	
	public boolean changeDeviceWorkcenter(long deviceId,long workcenterId,long workshopId, long factoryId) {
		Object[] args = new Object[4];
		int i=0;
		args[i++] = workcenterId;
		args[i++] = workshopId;
		args[i++] = factoryId;
		args[i++] = deviceId;
		return jdbcTemplate.update(Sql_ChangeDeviceWorkcenter_By_DeviceId, args)>0;
	}
	
	private static String Sql_ChangeDeviceWorkcenter_By_DeviceId = "UPDATE jdevice_c_device SET workcenter_id=?,workshop_id=?,factory_id=? WHERE id=?;";

	public boolean setDeviceLogoUrlById(long deviceId, String logoUrl) {
		Object[] args = new Object[2];
		int i=0;
		args[i++] = logoUrl;
		args[i++] = deviceId;
		return jdbcTemplate.update(Sql_UpdateDeviceLogoUrl_By_Id, args)>0;
	}
	private static String Sql_UpdateDeviceLogoUrl_By_Id="UPDATE jdevice_c_device SET logo_url=? WHERE id=?;";
	
	public List<Map<String, Object>> getDevicesStatusMapList(long deviceId,//
			long workcenterId,//
			long workshopId,//
			long factoryId,//
			String organizationId,//
			long beginTimeMs,//
			long endTimeMs,//
			int minIndex,//
			int dataNum) {
		int i=0;
		Object[] args=new Object[13];
		args[i++]=factoryId;
		args[i++]=factoryId;
		args[i++]=workshopId;
		args[i++]=workshopId;
		args[i++]=workcenterId;
		args[i++]=workcenterId;
		args[i++]=deviceId;
		args[i++]=deviceId;
		args[i++]=organizationId;
		args[i++]=beginTimeMs;
		args[i++]=endTimeMs;
		args[i++]=minIndex;
		args[i++]=dataNum;
		return jdbcTemplate.queryForList(Sql_GetDevicesStatusMap, args);
	}
	private static String Sql_GetDevicesStatusMap="SELECT "+//
			" tst.status_name as status_name, "+//
			" tst.device_id as device_id, "+//
			" tst.time_ms as time_ms, "+//
			" tde.eqp_identity as eqp_identity, "+//
			" tde.code as code, "+//
			" tde.name as name "+//
			"FROM "+//
			"	jdevice_h_device_status tst, "+//
			"	jdevice_c_device tde "+//
			"WHERE "+//
			"	tst.device_id = tde.id "+//
			"AND (? <= 0 OR tst.factory_id =?) "+//
			"AND (? <= 0 OR tst.workshop_id =?) "+//
			"AND (? <= 0 OR tst.workcenter_id =?) "+//
			"AND (? <= 0 OR tst.device_id =?) "+//
			"AND tst.organization_id = ? "+//
			"AND tst.time_ms >=? "+//
			"AND tst.time_ms <=? "+//
			"ORDER BY "+//
			"	tst.device_id, tst.time_ms DESC "+//
			"LIMIT ?,?";
}
