package cn.sd.zhidetec.jdevice.service;

import java.util.List;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingDeque;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.sd.zhidetec.jdevice.dao.DeviceSignboardModelDao;
import cn.sd.zhidetec.jdevice.dao.DeviceSignboardTypeModelDao;
import cn.sd.zhidetec.jdevice.model.DeviceModel;
import cn.sd.zhidetec.jdevice.model.DeviceSignboardModel;
import cn.sd.zhidetec.jdevice.model.DeviceSignboardStatusModel;
import cn.sd.zhidetec.jdevice.util.StringUtil;

@Service
public class DeviceSignboardService {

	@Autowired
	protected DeviceService deviceService;
	@Autowired
	protected DeviceSignboardModelDao deviceSignboardModelDao;
	@Autowired
	protected DeviceSignboardTypeModelDao deviceSignboardTypeModelDao;
	protected Logger logger = LoggerFactory.getLogger(getClass());
	
	/**
	 * 更新异常状态到数据库并返回最新的实例
	 * */
	public DeviceSignboardModel updateStatusAndReturnNew(DeviceSignboardModel deviceSignboardModel) {
		if (deviceSignboardModel == null || StringUtil.isEmpty(deviceSignboardModel.getCode())) {
			return deviceSignboardModel;
		}
		// 获取当前设备标识牌信息
		DeviceSignboardModel model = null;
		List<DeviceSignboardModel> models = deviceSignboardModelDao.getDeviceModelsByCode(deviceSignboardModel.getCode());
		if (models != null && models.size() > 0) {
			model = models.get(0);
			//获取状态配置
			List<DeviceSignboardStatusModel> statusModels = deviceSignboardTypeModelDao.getStatusModelsByTypeId(model.getTypeId());
			model.setTypeStatus(statusModels);
		}else {
			logger.error("Not Found Device Signboard Code:"+deviceSignboardModel.getCode());
		}
		// 填充状态信息
		if (model != null) {
			model.copyStatusField(deviceSignboardModel);
		}
		// 加入上传队列
		updateStatusModel(model);
		return model;
	}
	/**
	 * 更新异常状态到数据库并返回最新的实例
	 * */
	public DeviceSignboardModel updateErrStatusAndReturnNew(DeviceSignboardModel deviceSignboardModel) {
		if (deviceSignboardModel == null || StringUtil.isEmpty(deviceSignboardModel.getCode())) {
			return deviceSignboardModel;
		}
		// 获取当前设备标识牌
		DeviceSignboardModel model = null;
		List<DeviceSignboardModel> models = deviceSignboardModelDao.getDeviceModelsByCode(deviceSignboardModel.getCode());
		if (models != null && models.size() > 0) {
			model = models.get(0);
			List<DeviceSignboardStatusModel> statusModels = deviceSignboardTypeModelDao.getStatusModelsByTypeId(model.getTypeId());
			model.setTypeStatus(statusModels);
		}
		// 填充状态信息
		if (model != null) {
			model.copyErrStatusField(deviceSignboardModel);
		}
		// 加入上传队列
		updateErrStatusModel(deviceSignboardModel);
		return model;
	}
	
	public void start() {
		if (updateThreadStarted==false) {
			updateThreadStarted=true;
			updateThread.start();
		}
	}
	
	private Thread updateThread=new Thread(){

		@Override
		public void run() {
			logger.info("DeviceSignboardService UpdateThread Started!!");
			DeviceSignboardModel updateStatusModel=null;
			DeviceSignboardModel updateErrStatusModel=null;
			while (updateThreadStarted) {
				try {
					Thread.sleep(1);
					/**
					 * 推送标识牌状态
					 * */
					synchronized (updateStatusQueueObjLock) {
						updateStatusModel=updateStatusQueue.peek();
					}
					while (updateStatusModel!=null) {
						//入库
						deviceSignboardModelDao.updateDeviceModelStatus(updateStatusModel);
						//下一个
						synchronized (updateStatusQueueObjLock) {
							updateStatusQueue.poll();
							updateStatusModel=updateStatusQueue.peek();
						}
					}
					/**
					 * 推送标识牌异常状态
					 * */
					synchronized (updateErrStatusQueueObjLock) {
						updateErrStatusModel=updateErrStatusQueue.peek();
					}
					while (updateErrStatusModel!=null) {
						//入库
						deviceSignboardModelDao.updateDeviceModelErrorStatus(updateStatusModel);
						//下一个
						synchronized (updateErrStatusQueueObjLock) {
							updateErrStatusQueue.poll();
							updateErrStatusModel=updateErrStatusQueue.peek();
						}
					}
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			}
		}
		
	};
	private boolean updateThreadStarted=false;
	private Queue<DeviceSignboardModel> updateStatusQueue = new LinkedBlockingDeque<DeviceSignboardModel>();
	private Object updateStatusQueueObjLock = new Object();
	private Queue<DeviceSignboardModel> updateErrStatusQueue = new LinkedBlockingDeque<DeviceSignboardModel>();
	private Object updateErrStatusQueueObjLock = new Object();
	
	public void updateStatusModel(DeviceSignboardModel model) {
		if (model==null) {
			return;
		}
		synchronized (updateStatusQueueObjLock) {
			updateStatusQueue.offer(model);
		}
	}
	
	public void updateErrStatusModel(DeviceSignboardModel model) {
		if (model==null) {
			return;
		}
		synchronized (updateErrStatusQueueObjLock) {
			updateErrStatusQueue.offer(model);
		}
	}
	public DeviceSignboardModel getDeviceModelByCode(String code) {
		List<DeviceSignboardModel> list=deviceSignboardModelDao.getDeviceModelsByCode(code);
		if (list!=null&&list.size()>0) {
			return list.get(0);
		}
		return null;
	}
	public List<DeviceSignboardModel> getDeviceModelsByCode(String code) {
		return deviceSignboardModelDao.getDeviceModelsByCode(code);
	}
	public List<DeviceSignboardModel> getDeviceModelsByDeviceId(String deviceId) {
		return deviceSignboardModelDao.getDeviceModelsByDeviceId(deviceId);
	}
	public List<DeviceSignboardModel> getDeviceModelsByWorkcenterId(long workcenterId) {
		return deviceSignboardModelDao.getDeviceModelsByWorkcenterId(workcenterId);
	}
	public List<DeviceSignboardModel> getDeviceModelsByWorkshopId(long workshopId) {
		return deviceSignboardModelDao.getDeviceModelsByWorkshopId(workshopId);
	}
	public List<DeviceSignboardModel> getDeviceModelsByFactoryId(long factoryId) {
		return deviceSignboardModelDao.getDeviceModelsByFactoryId(factoryId);
	}
	public List<DeviceSignboardModel> getDeviceModelsByOrganizationId(String organizationId) {
		return deviceSignboardModelDao.getDeviceModelsByOrganizationId(organizationId);
	}
	public boolean bindingDeviceModel(DeviceSignboardModel deviceModel) {
		deviceService.setDeviceMonitor(deviceModel.getDeviceId(), DeviceModel.MONITOR_VAL_NONE);
		return deviceSignboardModelDao.bindingDeviceModel(deviceModel);
	}
	public boolean deleteDeviceModel(DeviceSignboardModel deviceModel) {
		return deviceSignboardModelDao.deleteDeviceModel(deviceModel);
	}
	public boolean updateDeviceModelInfo(DeviceSignboardModel deviceModel) {
		return deviceSignboardModelDao.updateDeviceModelInfoToDB(deviceModel);
	}
	public boolean changeWorkcenter(long deviceId,long workcenterId,long workshopId,long factoryId) {
		return deviceSignboardModelDao.changeWorkcenter(deviceId,workcenterId,workshopId,factoryId);
	}
	public boolean changeWorkshop(long workcenterId,long workshopId,long factoryId) {
		return deviceSignboardModelDao.changeWorkshop(workcenterId, workshopId, factoryId);
	}
	public boolean changeFactory(long workshopId,long factoryId) {
		return deviceSignboardModelDao.changeFactory(workshopId, factoryId);
	}
}
