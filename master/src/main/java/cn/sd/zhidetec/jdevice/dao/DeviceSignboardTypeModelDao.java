package cn.sd.zhidetec.jdevice.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import cn.sd.zhidetec.jdevice.Starter;
import cn.sd.zhidetec.jdevice.config.DataSourceConfiger;
import cn.sd.zhidetec.jdevice.model.DeviceSignboardTypeModel;
import cn.sd.zhidetec.jdevice.model.DeviceSignboardStatusModel;
import cn.sd.zhidetec.jdevice.model.DeviceStatusModel;
import cn.sd.zhidetec.jdevice.util.StringUtil;

@Service
public class DeviceSignboardTypeModelDao {

	@Autowired
	@Qualifier(DataSourceConfiger.BEAN_NAME_JdeviceJdbcTemplate)
	protected JdbcTemplate jdbcTemplate;
	@Autowired
	protected DeviceSignboardModelDao deviceSignboardModelDao;

	/**
	 * 获取所有设备类型
	 * 
	 * @param organizaionId 组织ID
	 * @return 设备状态规则列表
	 */
	public List<DeviceSignboardTypeModel> getTypeModels() {
		// 从数据库中查询
		List<DeviceSignboardTypeModel> list = new ArrayList<DeviceSignboardTypeModel>();
		List<Map<String, Object>> maps = jdbcTemplate.queryForList(Sql_GetModels);
		DeviceSignboardTypeModel typeModle = null;
		DeviceSignboardStatusModel statusModel = null;
		for (Map<String, Object> map : maps) {
			if (map != null) {
				long typeId = (long) map.get("type_id");
				String typeName = map.get("type_name").toString();
				String typeCode = map.get("type_code").toString();
				int typeCodeValue = (int) map.get("type_code_value");
				long id = (long) map.get("id");
				int value = (int) map.get("value");
				String name = map.get("name").toString();
				String status = map.get("status").toString();
				if (typeModle == null || typeModle.getId()!=typeId) {
					typeModle = new DeviceSignboardTypeModel();
					typeModle.setStatusModels(new ArrayList<>());
					typeModle.setId(typeId);
					typeModle.setName(typeName);
					typeModle.setCode(typeCode);
					typeModle.setCodeValue(typeCodeValue);
					statusModel = new DeviceSignboardStatusModel();
					statusModel.setId(id);
					statusModel.setValue(value);
					statusModel.setName(name);
					statusModel.setStatus(status);
					statusModel.setTypeId(typeId);
					typeModle.getStatusModels().add(statusModel);
					list.add(typeModle);
					continue;
				} else {
					statusModel = new DeviceSignboardStatusModel();
					statusModel.setId(id);
					statusModel.setValue(value);
					statusModel.setName(name);
					statusModel.setStatus(status);
					statusModel.setTypeId(typeId);
					typeModle.getStatusModels().add(statusModel);
				}
			}
		}
		return list;
	}

	private static String Sql_GetModels = "SELECT " + //
			"statusTab.id, " + //
			"statusTab.value, " + //
			"statusTab.name, " + //
			"statusTab.status, " + //
			"statusTab.type_id, " + //
			"typeTab.name AS type_name, " + //
			"typeTab.code AS type_code, " + //
			"typeTab.code_value AS type_code_value " + //
			" FROM " + //
			"jdevice_c_signboard_status statusTab, " + //
			"jdevice_c_signboard_type typeTab " + //
			" WHERE " + //
			"typeTab.id = statusTab.type_id " + //
			" ORDER BY " + //
			"statusTab.type_id";

	public boolean setStatusModel(DeviceSignboardStatusModel model) {
		int i = 0;
		Object[] args = new Object[5];
		// 修改信息
		if (model.getId() >0 
				&& getStatusModelsById(model.getId()).size() > 0) {
			args[i++] = model.getValue();
			args[i++] = StringUtil.getEmptyString(model.getName());
			args[i++] = StringUtil.getEmptyString(model.getStatus());
			args[i++] = model.getTypeId();
			args[i++] = model.getId();
			i = jdbcTemplate.update(Sql_UpdateStatusModel_By_Id, args);
		}
		// 新建信息
		else {
			model.setId(Starter.IdMaker.nextId());
			args[i++] = model.getId();
			args[i++] = model.getValue();
			args[i++] = StringUtil.getEmptyString(model.getName());
			args[i++] = StringUtil.getEmptyString(model.getStatus());
			args[i++] = model.getTypeId();
			i = jdbcTemplate.update(Sql_InsertStatusModel, args);
		}
		if (i > 0) {
			return true;
		}
		return false;
	}

	private static String Sql_UpdateStatusModel_By_Id = "UPDATE jdevice_c_signboard_status" + //
			" SET " + //
			"value = ?," + //
			"name = ?," + //
			"status = ?," + //
			"type_id = ?" + //
			" WHERE " + //
			"id=?";
	private static String Sql_InsertStatusModel = "INSERT INTO jdevice_c_signboard_status (" + //
			"id," + //
			"value," + //
			"name," + //
			"status," + //
			"type_id" + //
			")VALUES(" + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?" + //
			")";

	public List<DeviceSignboardStatusModel> getStatusModelsById(long id) {
		Object[] args = new Object[1];
		args[0] = id;
		List<DeviceSignboardStatusModel> list = jdbcTemplate.query(Sql_GetStatusModels_By_Id, args,statusBeanPropertyRowMapper);
		return list;
	}

	private static String Sql_GetStatusModels_By_Id = "SELECT * FROM jdevice_c_signboard_status WHERE id = ?";
	private BeanPropertyRowMapper<DeviceSignboardStatusModel> statusBeanPropertyRowMapper = new BeanPropertyRowMapper<DeviceSignboardStatusModel>(
			DeviceSignboardStatusModel.class);

	public boolean setTypeModel(DeviceSignboardTypeModel model) {
		int i = 0;
		Object[] args = new Object[4];
		// 修改信息
		if (model.getId()>0
				&& getStatusModelsByTypeId(model.getId()).size() > 0) {
			args[i++] = StringUtil.getEmptyString(model.getName());
			args[i++] = StringUtil.getEmptyString(model.getCode());
			args[i++] = model.getCodeValue();
			args[i++] = model.getId();
			i = jdbcTemplate.update(Sql_UpdateRuleModel_By_Id, args);
		}
		// 新建信息
		else {
			long typeId = Starter.IdMaker.nextId();
			args[i++] = typeId;
			args[i++] = StringUtil.getEmptyString(model.getName());
			args[i++] = StringUtil.getEmptyString(model.getCode());
			args[i++] = model.getCodeValue();
			i = jdbcTemplate.update(Sql_InsertTypeModel, args);
			if (i <= 0) {
				return false;
			}
			// 状态初始化
			DeviceSignboardStatusModel statusModel = new DeviceSignboardStatusModel();
			statusModel.setTypeId(typeId);
			statusModel.setStatus(DeviceStatusModel.STATUS_Run);
			statusModel.setId(-1);
			statusModel.setName(DeviceStatusModel.NAME_Run);
			statusModel.setValue(1);
			setStatusModel(statusModel);
			statusModel = new DeviceSignboardStatusModel();
			statusModel.setTypeId(typeId);
			statusModel.setStatus(DeviceStatusModel.STATUS_Maintain);
			statusModel.setId(-1);
			statusModel.setName(DeviceStatusModel.NAME_Maintain);
			statusModel.setValue(2);
			setStatusModel(statusModel);
			statusModel = new DeviceSignboardStatusModel();
			statusModel.setTypeId(typeId);
			statusModel.setStatus(DeviceStatusModel.STATUS_Stop);
			statusModel.setId(-1);
			statusModel.setName(DeviceStatusModel.NAME_Stop);
			statusModel.setValue(3);
			setStatusModel(statusModel);
			statusModel = new DeviceSignboardStatusModel();
			statusModel.setTypeId(typeId);
			statusModel.setStatus(DeviceStatusModel.STATUS_Fault);
			statusModel.setId(-1);
			statusModel.setName(DeviceStatusModel.NAME_Fault);
			statusModel.setValue(4);
			setStatusModel(statusModel);
			statusModel = new DeviceSignboardStatusModel();
			statusModel.setTypeId(typeId);
			statusModel.setStatus(DeviceStatusModel.STATUS_Repair);
			statusModel.setId(-1);
			statusModel.setName(DeviceStatusModel.NAME_Repair);
			statusModel.setValue(5);
			setStatusModel(statusModel);
			statusModel = new DeviceSignboardStatusModel();
			statusModel.setTypeId(typeId);
			statusModel.setStatus(DeviceStatusModel.STATUS_Pending);
			statusModel.setId(-1);
			statusModel.setName(DeviceStatusModel.NAME_Pending);
			statusModel.setValue(6);
			setStatusModel(statusModel);
		}
		if (i > 0) {
			return true;
		}
		return false;
	}

	private static String Sql_UpdateRuleModel_By_Id = "UPDATE jdevice_c_signboard_type" + //
			" SET " + //
			"name = ?," + //
			"code = ?," + //
			"code_value = ?" + //
			" WHERE " + //
			"id=?";
	private static String Sql_InsertTypeModel = "INSERT INTO jdevice_c_signboard_type (" + //
			"id," + //
			"name," + //
			"code," + //
			"code_value" + //
			")VALUES(" + //
			"?," + //
			"?," + //
			"?," + //
			"?" + //
			")";

	public List<DeviceSignboardStatusModel> getStatusModelsByTypeId(long id) {
		Object[] args = new Object[1];
		args[0] = id;
		List<DeviceSignboardStatusModel> list = jdbcTemplate.query(Sql_GetStatusModels_By_TypeId, args,statusBeanPropertyRowMapper);
		return list;
	}

	private static String Sql_GetStatusModels_By_TypeId = "SELECT * FROM jdevice_c_signboard_status WHERE type_id = ?";

	public boolean deleteStatusModel(DeviceSignboardStatusModel model) {
		int i = 0;
		Object[] args = new Object[1];
		if (model.getId() >0 ) {
			// 剩余状态数<=1时删除规则
			args[i++] = model.getTypeId();
			int num = jdbcTemplate.queryForObject(Sql_GetStatusModelNum_By_TypeId, args, Integer.class);
			if (num > 1) {
				i = 0;
				args[i++] = model.getId();
				// 删除状态
				i = jdbcTemplate.update(Sql_DeleteStatusModel_By_Id, args);
			} else {
				i = 0;
				args[i++] = model.getId();
				// 删除状态
				i = jdbcTemplate.update(Sql_DeleteStatusModel_By_Id, args);
				if (i <= 0) {
					return false;
				}
				// 删除规则
				i = 0;
				args[i++] = model.getTypeId();
				i = jdbcTemplate.update(Sql_DeleteTypeModel_By_Id, args);
			}
		}
		if (i > 0) {
			return true;
		}
		return false;
	}

	private static String Sql_DeleteStatusModel_By_Id = "DELETE FROM jdevice_c_signboard_status WHERE id=?";
	private static String Sql_GetStatusModelNum_By_TypeId = "SELECT COUNT(*) FROM jdevice_c_signboard_status WHERE type_id=?";

	public boolean deleteTypeModel(DeviceSignboardTypeModel model) {
		int i = 0;
		Object[] args = new Object[1];
		args[i++] = model.getId();
		if (model.getId() > 0) {
			i = jdbcTemplate.update(sql_DeleteStatusModel_By_TypeId, args);
			if (i <= 0) {
				return false;
			}
			i = jdbcTemplate.update(Sql_DeleteTypeModel_By_Id, args);
		}
		if (i > 0) {
			return true;
		}
		return false;
	}

	private static String Sql_DeleteTypeModel_By_Id = "DELETE FROM jdevice_c_signboard_type WHERE id=?";
	private static String sql_DeleteStatusModel_By_TypeId = "DELETE FROM jdevice_c_signboard_status WHERE type_id=?";

}
