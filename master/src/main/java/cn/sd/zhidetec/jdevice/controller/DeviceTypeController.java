package cn.sd.zhidetec.jdevice.controller;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.sd.zhidetec.jdevice.HttpResponseMsg;
import cn.sd.zhidetec.jdevice.model.DeviceTypeModel;
import cn.sd.zhidetec.jdevice.model.RoleModel;
import cn.sd.zhidetec.jdevice.model.UserModel;
import cn.sd.zhidetec.jdevice.service.DeviceTypeService;
import cn.sd.zhidetec.jdevice.service.RBuilderService;

import java.util.List;

@Controller
@RequestMapping("/deviceType")
public class DeviceTypeController {
	@Autowired
	protected DeviceTypeService deviceTypeService;
	@Autowired
	protected RBuilderService rBuilderService;
	protected Logger logger = LoggerFactory.getLogger(getClass());
	/**
	 * 根据设备类型ID获取设备类型
	 * 
	 * @param httpSession  Session会话
	 * @param id 设备类型id
	 * @return 返回给前端的操作结果
	 */
	@RequestMapping(path = "/getDeviceTypeModelById")
	@ResponseBody
	public RBuilderService.Response getDeviceTypeModelById(HttpSession httpSession,@RequestParam("id")long id) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_User, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		// 根据用户权限返回数据
		UserModel userModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		DeviceTypeModel deviceTypeModel = deviceTypeService.getDeviceTypeModelById(id);
		if (deviceTypeModel!=null) {
			if (userModel.getRoleLevel() <= RoleModel.ROLE_LEVEL_WorkshopAdmin//
					&&userModel.getWorkshopId()==deviceTypeModel.getWorkshopId()) {
				return rBuilderService.update(response, //
						HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, //
						deviceTypeModel);
			}
			if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_FactoryAdmin//
					&&userModel.getFactoryId()==deviceTypeModel.getFactoryId()) {
				return rBuilderService.update(response, //
						HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, //
						deviceTypeModel);
			}
			if (userModel.getRoleLevel() >= RoleModel.ROLE_LEVEL_OrganizationAdmin//
					&&userModel.getOrganizationId().equals(deviceTypeModel.getOrganizationId())) {
				return rBuilderService.update(response, //
						HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, //
						deviceTypeModel);
			}
		}
		return rBuilderService.update(response, //
				HttpResponseMsg.HEAD_MSG_NOT_FOUND_DATA, //
				null);
	}
	
	/**
	 * 获取设备类型列表
	 * 
	 * @param httpSession  Session会话
	 * @param workshopCode 车间编号
	 * @return 返回给前端的操作结果
	 */
	@RequestMapping(path = "/getDeviceTypeModels")
	@ResponseBody
	public RBuilderService.Response getDeviceTypeModels(HttpSession httpSession) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_User, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		// 根据用户权限返回数据
		UserModel userModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		if (userModel.getRoleLevel() <= RoleModel.ROLE_LEVEL_WorkshopAdmin) {
			return rBuilderService.update(response, //
					HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, //
					deviceTypeService.getDeviceTypeModelsByWorkshopId(userModel.getWorkshopId()));
		}
		if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_FactoryAdmin) {
			return rBuilderService.update(response, //
					HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, //
					deviceTypeService.getDeviceTypeModelsByFactoryId(userModel.getFactoryId()));
		}
		if (userModel.getRoleLevel() >= RoleModel.ROLE_LEVEL_OrganizationAdmin) {
			return rBuilderService.update(response, //
					HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, //
					deviceTypeService.getDeviceTypeModelsByOrganizationId(userModel.getOrganizationId()));
		}
		return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_DATA, null);
	}

	/**
	 * 设置设备类型信息
	 * 
	 * @param httpSession Session会话
	 * @param deviceTypeModel 设备类型信息
	 * @return 返回给前端的操作结果
	 */
	@RequestMapping(path = "/setDeviceTypeModel")
	@ResponseBody
	public RBuilderService.Response setDeviceTypeModel(HttpSession httpSession, //
			@RequestBody DeviceTypeModel deviceTypeModel) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_WorkcenterAdmin, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		// 根据用户权限操作数据
		UserModel userModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		//新建
		if (deviceTypeModel.getId()<=0) {
			deviceTypeModel.setOrganizationId(userModel.getOrganizationId());
		}
		if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_WorkshopAdmin) {
			if (userModel.getWorkshopId()==deviceTypeModel.getWorkshopId()//
					&& userModel.getFactoryId()==deviceTypeModel.getFactoryId()//
					&& userModel.getOrganizationId().equals(deviceTypeModel.getOrganizationId())) {
				if (deviceTypeService.setDeviceTypeModel(deviceTypeModel)) {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, null);
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
				}
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
			}
		}
		if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_FactoryAdmin) {
			if (userModel.getFactoryId()==deviceTypeModel.getFactoryId()//
					&& userModel.getOrganizationId().equals(deviceTypeModel.getOrganizationId())) {
				if (deviceTypeService.setDeviceTypeModel(deviceTypeModel)) {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, null);
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
				}
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
			}
		}
		if (userModel.getRoleLevel() >= RoleModel.ROLE_LEVEL_OrganizationAdmin) {
			if (userModel.getOrganizationId().equals(deviceTypeModel.getOrganizationId())) {
				if (deviceTypeService.setDeviceTypeModel(deviceTypeModel)) {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, null);
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
				}
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
			}
		}
		return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
	}
	/**
	 * 从Session中获取用户信息，然后删除设备类型
	 * 
	 * @param httpSession Session
	 * @return 操作结果
	 */
	@RequestMapping(path = "/deleteDeviceTypeModel")
	@ResponseBody
	public RBuilderService.Response deleteDeviceTypeModel(HttpSession httpSession, //
			@RequestBody DeviceTypeModel deviceTypeModel) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_WorkshopAdmin, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		// 根据用户权限操作数据
		UserModel userModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_WorkshopAdmin) {
			if (userModel.getWorkshopId()==deviceTypeModel.getWorkshopId()//
					&& userModel.getFactoryId()==deviceTypeModel.getFactoryId()//
					&& userModel.getOrganizationId().equals(deviceTypeModel.getOrganizationId())) {
				if (deviceTypeService.deleteDeviceTypeModel(deviceTypeModel)) {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, null);
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
				}
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
			}
		}
		if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_FactoryAdmin) {
			if (userModel.getFactoryId()==deviceTypeModel.getFactoryId()//
					&& userModel.getOrganizationId().equals(deviceTypeModel.getOrganizationId())) {
				if (deviceTypeService.deleteDeviceTypeModel(deviceTypeModel)) {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, null);
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
				}
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
			}
		}
		if (userModel.getRoleLevel() >= RoleModel.ROLE_LEVEL_OrganizationAdmin) {
			if (userModel.getOrganizationId().equals(deviceTypeModel.getOrganizationId())) {
				if (deviceTypeService.deleteDeviceTypeModel(deviceTypeModel)) {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, null);
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
				}
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
			}
		}
		return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
	}


    /**
     * 根据车间id获取设备类型
     * @param workshopId   车间id
     * @return
     */
	@RequestMapping("findDeviceTypeByWorkshopId")
    @ResponseBody
	public RBuilderService.Response getDeviceTypeModelsByWorkshopId(long workshopId){

        RBuilderService.Response response = rBuilderService.build(null, null);
        List<DeviceTypeModel> deviceTypeModelList = deviceTypeService.getDeviceTypeModelsByWorkshopId(workshopId);
        if (deviceTypeModelList.size()!=0){

            return rBuilderService.update(response,HttpResponseMsg.HEAD_MSG_SUCCESS_DONE,deviceTypeModelList);

        }else {

            return rBuilderService.update(response,HttpResponseMsg.HEAD_MSG_ERROR_PARAM,null);


        }


    }







}
