package cn.sd.zhidetec.jdevice.dao;

import java.util.List;

import cn.sd.zhidetec.jdevice.model.WorkshopModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import cn.sd.zhidetec.jdevice.Starter;
import cn.sd.zhidetec.jdevice.config.DataSourceConfiger;
import cn.sd.zhidetec.jdevice.model.AndonMessageModel;
import cn.sd.zhidetec.jdevice.util.StringUtil;

@Service
public class AndonMessageModelDao {

	@Autowired
	@Qualifier(DataSourceConfiger.BEAN_NAME_JdeviceJdbcTemplate)
	protected JdbcTemplate jdbcTemplate;

	@Autowired
	protected WorkshopModelDao workshopModelDao;

	public boolean newAndonMessageModel(AndonMessageModel andonModel) {


	    //根据factoryId查询车间表
        List<WorkshopModel> modelsById = workshopModelDao.getWorkshopModelsByFactoryId(andonModel.getFactoryId());

        //拿到车间名称
        for (WorkshopModel workshopModel : modelsById) {
            andonModel.setWorkshopName(workshopModel.getName());//将车间表的名称赋值到andonModel的车间名称
        }




        int i = 0;
		Object[] args = new Object[29];
		// 新建信息
		if (andonModel != null) {
			if (andonModel.getId()<=0) {
				andonModel.setId(Starter.IdMaker.nextId());
			}
			args[i++] = andonModel.getId();
			args[i++] = andonModel.getStatus();
			args[i++] = StringUtil.getEmptyString(andonModel.getTitle());
			args[i++] = StringUtil.getEmptyString(andonModel.getContent());
			args[i++] = StringUtil.getEmptyString(andonModel.getPhotoUrl());
			args[i++] = StringUtil.getEmptyString(andonModel.getEqpIdentity());
			args[i++] = StringUtil.getEmptyString(andonModel.getDeviceCode());

			args[i++] = StringUtil.getEmptyString(andonModel.getDeviceName());
			args[i++] = StringUtil.getEmptyString(andonModel.getDeviceStatus());
			args[i++] = StringUtil.getEmptyString(andonModel.getDeviceStatusName());
			args[i++] = andonModel.getDeviceId();
			args[i++] = andonModel.getWorkcenterId();

			args[i++] = andonModel.getWorkshopId();
			args[i++] = andonModel.getFactoryId();
			args[i++] = StringUtil.getEmptyString(andonModel.getOrganizationId());
			args[i++] = andonModel.getDeviceLevel();
			args[i++] = andonModel.getHandlerId();

			args[i++] = StringUtil.getEmptyString(andonModel.getHandlerName());
			args[i++] = andonModel.getHandlerTimeMs();
			args[i++] = andonModel.getWarnLevel();
			args[i++] = StringUtil.getEmptyString(andonModel.getCreateUser());

			args[i++] = andonModel.getCreateTimeMs();
			args[i++] = andonModel.getCloseTimeMs();
			args[i++] = andonModel.getSolveTimeMs();
			
			args[i++] = andonModel.getAssignorId();
			args[i++] = StringUtil.getEmptyString(andonModel.getAssignorName());
			args[i++] = andonModel.getAssignorTimeMs();
			
			args[i++] = StringUtil.getEmptyString(andonModel.getReason());

			args[i++] = andonModel.getWorkshopName();
			i = jdbcTemplate.update(Sql_InsertModel, args);
		}
		if (i > 0) {
			return true;
		}
		return false;
	}

	private static String Sql_InsertModel = "INSERT INTO jdevice_h_andon (" + //
			"id," + //
			"status," + //
			"title," + //
			"content," + //
			"photo_url," + //
			"eqp_identity," + //
			"device_code," + //
			"device_name," + //
			"device_status," + //
			"device_status_name," + //
			"device_id," + //
			"workcenter_id," + //
			"workshop_id," + //
			"factory_id," + //
			"organization_id," + //
			"device_level," + //
			"handler_id," + //
			"handler_name," + //
			"handler_time_ms," + //
			"warn_level," + //
			"create_user," + //
			"create_time_ms," + //
			"close_time_ms," + //
			"solve_time_ms," + //
			"assignor_id," + //
			"assignor_name," + //
			"assignor_time_ms," + //
			"reason," +
            "workshop_name" + ")VALUES(" + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?" + //
			")";

	public boolean updateAndonModel(AndonMessageModel andonModel) {
		int i = 0;
		Object[] args = new Object[28];
		// 新建信息
		if (andonModel != null) {
			if (andonModel.getId()<=0) {
				return false;
			}
			args[i++] = andonModel.getStatus();
			args[i++] = StringUtil.getEmptyString(andonModel.getTitle());
			args[i++] = StringUtil.getEmptyString(andonModel.getContent());
			args[i++] = StringUtil.getEmptyString(andonModel.getPhotoUrl());
			args[i++] = StringUtil.getEmptyString(andonModel.getEqpIdentity());
			args[i++] = StringUtil.getEmptyString(andonModel.getDeviceCode());

			args[i++] = StringUtil.getEmptyString(andonModel.getDeviceName());
			args[i++] = StringUtil.getEmptyString(andonModel.getDeviceStatus());
			args[i++] = StringUtil.getEmptyString(andonModel.getDeviceStatusName());
			args[i++] = andonModel.getDeviceId();
			args[i++] = andonModel.getWorkcenterId();

			args[i++] = andonModel.getWorkshopId();
			args[i++] = andonModel.getFactoryId();
			args[i++] = StringUtil.getEmptyString(andonModel.getOrganizationId());
			args[i++] = andonModel.getDeviceLevel();
			args[i++] = andonModel.getHandlerId();

			args[i++] = StringUtil.getEmptyString(andonModel.getHandlerName());
			args[i++] = andonModel.getHandlerTimeMs();
			args[i++] = andonModel.getWarnLevel();
			args[i++] = StringUtil.getEmptyString(andonModel.getCreateUser());

			args[i++] = andonModel.getCreateTimeMs();
			args[i++] = andonModel.getCloseTimeMs();
			args[i++] = andonModel.getSolveTimeMs();
			
			args[i++] = andonModel.getAssignorId();
			args[i++] = StringUtil.getEmptyString(andonModel.getAssignorName());
			args[i++] = andonModel.getAssignorTimeMs();
			
			args[i++] = StringUtil.getEmptyString(andonModel.getReason());
			args[i++] = andonModel.getId();
			i = jdbcTemplate.update(Sql_UpdateModel, args);
		}
		if (i > 0) {
			return true;
		}
		return false;
	}

	private static String Sql_UpdateModel = "UPDATE jdevice_h_andon" + //
			" SET " + //
			"status = ?," + //
			"title = ?," + //
			"content = ?," + //
			"photo_url = ?," + //
			"eqp_identity = ?," + //
			"device_code = ?," + //
			"device_name = ?," + //
			"device_status = ?," + //
			"device_status_name = ?," + //
			"device_id = ?," + //
			"workcenter_id = ?," + //
			"workshop_id = ?," + //
			"factory_id = ?," + //
			"organization_id = ?," + //
			"device_level = ?," + //
			"handler_id = ?," + //
			"handler_name = ?," + //
			"handler_time_ms = ?," + //
			"warn_level = ?," + //
			"create_user = ?," + //
			"create_time_ms = ?," + //
			"close_time_ms = ?," + //
			"solve_time_ms = ?," + //
			"assignor_id = ?," + //
			"assignor_name = ?," + //
			"assignor_time_ms = ?," + //
			"reason = ?" + " WHERE " + //
			"id=?";

	public List<AndonMessageModel> getModels(int status,//
			long beginTimeMs,//
			long endTimeMs,//
			long factoryId,//
			long workshopId,//
			long workcenterId,//
			long deviceId,//
			long handlerId,//
			int minIndex,//
			int maxNum,//
			String organizationId) {
		Object[] args = new Object[17];
		int i=0;
		args[i++] = handlerId;
		args[i++] = handlerId;
		args[i++] = status;
		args[i++] = status;
		args[i++] = factoryId;
		args[i++] = factoryId;
		args[i++] = workshopId;
		args[i++] = workshopId;
		args[i++] = workcenterId;
		args[i++] = workcenterId;
		args[i++] = deviceId;
		args[i++] = deviceId;
		args[i++] = beginTimeMs;
		args[i++] = endTimeMs;
		args[i++] = organizationId;
		args[i++] = minIndex;
		args[i++] = maxNum;
		List<AndonMessageModel> list = jdbcTemplate.query(Sql_GetModels, args, beanPropertyRowMapper);
		return list;
	}
	private static String Sql_GetModels="SELECT "+//
			"	* "+//
			"FROM "+//
			"	jdevice_h_andon "+//
			"WHERE "+//
			"	(handler_id = ? OR ?<=0) "+//
			"AND (status = ? OR ?<0) "+//
			"AND (factory_id=? OR ?<=0) "+//
			"AND (workshop_id=? OR ?<=0) "+//
			"AND (workcenter_id=? OR ?<=0) "+//
			"AND (device_id=? OR ?<=0) "+//
			"AND create_time_ms >=? "+//
			"AND create_time_ms <=? "+//
			"AND organization_id=? "+//
			"ORDER BY "+//
			"	create_time_ms DESC "+//
			"LIMIT ?,?";
	
	public List<AndonMessageModel> getModelsByHandlerId(long userId, int status, int minIndex, int dataNum,long beginTimeMs,long endTimeMs) {
		Object[] args = new Object[6];
		int i=0;
		args[i++] = userId;
		args[i++] = status;
		args[i++] = beginTimeMs;
		args[i++] = endTimeMs;
		args[i++] = minIndex;
		args[i++] = dataNum;
		List<AndonMessageModel> list = jdbcTemplate.query(Sql_GetModels_By_UserId, args, beanPropertyRowMapper);
		return list;
	}

	private static String Sql_GetModels_By_UserId = "SELECT * FROM jdevice_h_andon WHERE handler_id = ? AND status = ? AND create_time_ms >=? AND create_time_ms<=? ORDER BY create_time_ms DESC LIMIT ?,?";

	public int getClosedNumByHandlerId(long handlerId, int warnLevel) {
		Object[] args = new Object[2];
		args[0] = handlerId;
		args[1] = warnLevel;
		return jdbcTemplate.queryForObject(Sql_GetClosedNum_By_HandlerId, args, Integer.class);
	}

	private static String Sql_GetClosedNum_By_HandlerId = "SELECT COUNT(*) FROM jdevice_h_andon WHERE status="
			+ AndonMessageModel.STATUS_VALUE_Closed
			+ " AND handler_id=? AND warn_level<=?";
	
	public int getNotClosedNumByHandlerId(long handlerId, long workcenterId, int warnLevel) {
		Object[] args = new Object[3];
		args[0] = handlerId;
		args[1] = workcenterId;
		args[2] = warnLevel;
		return jdbcTemplate.queryForObject(Sql_GetNotClosedNum_By_HandlerId, args, Integer.class);
	}

	private static String Sql_GetNotClosedNum_By_HandlerId = "SELECT COUNT(*) FROM jdevice_h_andon WHERE status!="
			+ AndonMessageModel.STATUS_VALUE_Closed
			+ " AND (handler_id=? OR handler_id<0) AND workcenter_id=? AND warn_level<=?";

	public int getNotClosedNumByWorkcenterId(long id, int warnLevel) {
		Object[] args = new Object[2];
		args[0] = id;
		args[1] = warnLevel;
		return jdbcTemplate.queryForObject(Sql_GetNotClosedNum_By_WorkcenterId, args, Integer.class);
	}

	private static String Sql_GetNotClosedNum_By_WorkcenterId = "SELECT COUNT(*) FROM jdevice_h_andon WHERE status!="
			+ AndonMessageModel.STATUS_VALUE_Closed + " AND workcenter_id=? AND warn_level<=?";

	public int getNotClosedNumByWorkshopId(long id, int warnLevel) {
		Object[] args = new Object[2];
		args[0] = id;
		args[1] = warnLevel;
		return jdbcTemplate.queryForObject(Sql_GetNotClosedNum_By_WorkshopId, args, Integer.class);
	}

	private static String Sql_GetNotClosedNum_By_WorkshopId = "SELECT COUNT(*) FROM jdevice_h_andon WHERE status!="
			+ AndonMessageModel.STATUS_VALUE_Closed + " AND workshop_id=? AND warn_level<=?";

	public int getNotClosedNumByFactoryId(long id, int warnLevel) {
		Object[] args = new Object[2];
		args[0] = id;
		args[1] = warnLevel;
		return jdbcTemplate.queryForObject(Sql_GetNotClosedNum_By_FactoryId, args, Integer.class);
	}

	private static String Sql_GetNotClosedNum_By_FactoryId = "SELECT COUNT(*) FROM jdevice_h_andon WHERE status!="
			+ AndonMessageModel.STATUS_VALUE_Closed + " AND factory_id=? AND warn_level<=?";

	public int getNotClosedNumByOrganizationId(String id, int warnLevel) {
		Object[] args = new Object[2];
		args[0] = id;
		args[1] = warnLevel;
		return jdbcTemplate.queryForObject(Sql_GetNotClosedNum_By_OrganizationId, args, Integer.class);
	}

	private static String Sql_GetNotClosedNum_By_OrganizationId = "SELECT COUNT(*) FROM jdevice_h_andon WHERE status!="
			+ AndonMessageModel.STATUS_VALUE_Closed + " AND organization_id=? AND warn_level<=?";

	public int getUnclaimedNumByHandlerId(long handlerId, long workcenterId, int warnLevel) {
		Object[] args = new Object[3];
		args[0] = handlerId;
		args[1] = workcenterId;
		args[2] = warnLevel;
		return jdbcTemplate.queryForObject(Sql_GetUnclaimedNum_By_HandlerId, args, Integer.class);
	}

	private static String Sql_GetUnclaimedNum_By_HandlerId = "SELECT COUNT(*) FROM jdevice_h_andon WHERE status="
			+ AndonMessageModel.STATUS_VALUE_Unclaimed
			+ " AND (handler_id=? OR handler_id<=0) AND workcenter_id=? AND warn_level<=?";

	public int getUnclaimedNumByWorkcenterId(long id, int warnLevel) {
		Object[] args = new Object[2];
		args[0] = id;
		args[1] = warnLevel;
		return jdbcTemplate.queryForObject(Sql_GetUnclaimedNum_By_WorkcenterId, args, Integer.class);
	}

	private static String Sql_GetUnclaimedNum_By_WorkcenterId = "SELECT COUNT(*) FROM jdevice_h_andon WHERE status="
			+ AndonMessageModel.STATUS_VALUE_Unclaimed + " AND workcenter_id=? AND warn_level<=?";

	public int getUnclaimedNumByWorkshopId(long id, int warnLevel) {
		Object[] args = new Object[2];
		args[0] = id;
		args[1] = warnLevel;
		return jdbcTemplate.queryForObject(Sql_GetUnclaimedNum_By_WorkshopId, args, Integer.class);
	}

	private static String Sql_GetUnclaimedNum_By_WorkshopId = "SELECT COUNT(*) FROM jdevice_h_andon WHERE status="
			+ AndonMessageModel.STATUS_VALUE_Unclaimed + " AND workshop_id=? AND warn_level<=?";

	public int getUnclaimedNumByFactoryId(long id, int warnLevel) {
		Object[] args = new Object[2];
		args[0] = id;
		args[1] = warnLevel;
		return jdbcTemplate.queryForObject(Sql_GetUnclaimedNum_By_FactoryId, args, Integer.class);
	}

	private static String Sql_GetUnclaimedNum_By_FactoryId = "SELECT COUNT(*) FROM jdevice_h_andon WHERE status="
			+ AndonMessageModel.STATUS_VALUE_Unclaimed + " AND factory_id=? AND warn_level<=?";

	public int getUnclaimedNumByOrganizationId(String id, int warnLevel) {
		Object[] args = new Object[2];
		args[0] = id;
		args[1] = warnLevel;
		return jdbcTemplate.queryForObject(Sql_GetUnclaimedNum_By_OrganizationId, args, Integer.class);
	}

	private static String Sql_GetUnclaimedNum_By_OrganizationId = "SELECT COUNT(*) FROM jdevice_h_andon WHERE status="
			+ AndonMessageModel.STATUS_VALUE_Unclaimed + " AND organization_id=? AND warn_level<=?";
	
	public List<AndonMessageModel> getModelsByWorkcenterId(long workcenterId, int status, int minIndex, int dataNum,long beginTimeMs,long endTimeMs) {
		Object[] args = new Object[6];
		int i=0;
		args[i++] = workcenterId;
		args[i++] = status;
		args[i++] = beginTimeMs;
		args[i++] = endTimeMs;
		args[i++] = minIndex;
		args[i++] = dataNum;
		List<AndonMessageModel> list = jdbcTemplate.query(Sql_GetModels_By_WorkcenterId, args, beanPropertyRowMapper);
		return list;
	}

	private static String Sql_GetModels_By_WorkcenterId = "SELECT * FROM jdevice_h_andon WHERE workcenter_id = ? AND status = ? AND create_time_ms >=? AND create_time_ms<=? ORDER BY create_time_ms DESC LIMIT ?,?";

	public List<AndonMessageModel> getModelsByWorkshopId(long workshopId, int status, int minIndex, int dataNum,long beginTimeMs,long endTimeMs) {
		Object[] args = new Object[6];
		int i=0;
		args[i++] = workshopId;
		args[i++] = status;
		args[i++] = beginTimeMs;
		args[i++] = endTimeMs;
		args[i++] = minIndex;
		args[i++] = dataNum;
		List<AndonMessageModel> list = jdbcTemplate.query(Sql_GetModels_By_ByWorkshopId, args, beanPropertyRowMapper);
		return list;
	}

	private static String Sql_GetModels_By_ByWorkshopId = "SELECT * FROM jdevice_h_andon WHERE workshop_id = ? AND status = ? AND create_time_ms >=? AND create_time_ms<=? ORDER BY create_time_ms DESC LIMIT ?,?";

	public List<AndonMessageModel> getModelsByFactoryId(long factoryId, int status, int minIndex, int dataNum,long beginTimeMs,long endTimeMs) {
		Object[] args = new Object[6];
		int i=0;
		args[i++] = factoryId;
		args[i++] = status;
		args[i++] = beginTimeMs;
		args[i++] = endTimeMs;
		args[i++] = minIndex;
		args[i++] = dataNum;
		List<AndonMessageModel> list = jdbcTemplate.query(Sql_GetModels_By_FactoryId, args, beanPropertyRowMapper);
		return list;
	}

	private static String Sql_GetModels_By_FactoryId = "SELECT * FROM jdevice_h_andon WHERE factory_id = ? AND status = ? AND create_time_ms >=? AND create_time_ms<=? ORDER BY create_time_ms DESC LIMIT ?,?";

	public List<AndonMessageModel> getModelsByOrganizationId(String organizationId, int status, int minIndex,int dataNum,long beginTimeMs,long endTimeMs) {
		Object[] args = new Object[6];
		int i=0;
		args[i++] = organizationId;
		args[i++] = status;
		args[i++] = beginTimeMs;
		args[i++] = endTimeMs;
		args[i++] = minIndex;
		args[i++] = dataNum;
		List<AndonMessageModel> list = jdbcTemplate.query(Sql_GetModels_By_OrganizationId, args, beanPropertyRowMapper);
		return list;
	}

	private static String Sql_GetModels_By_OrganizationId = "SELECT * FROM jdevice_h_andon WHERE organization_id = ? AND status = ? AND create_time_ms >=? AND create_time_ms<=? ORDER BY create_time_ms DESC LIMIT ?,?";

	public List<AndonMessageModel> getModelsByDeviceId(long deviceId, int status, int minIndex,int dataNum,long beginTimeMs,long endTimeMs) {
		Object[] args = new Object[7];
		int i=0;
		args[i++] = deviceId;
		args[i++] = status;
		args[i++] = status;
		args[i++] = beginTimeMs;
		args[i++] = endTimeMs;
		args[i++] = minIndex;
		args[i++] = dataNum;
		List<AndonMessageModel> list = jdbcTemplate.query(Sql_GetModels_By_DeviceId, args, beanPropertyRowMapper);
		return list;
	}

	private static String Sql_GetModels_By_DeviceId = "SELECT * FROM jdevice_h_andon WHERE device_id = ? AND (?<0 OR status = ?) AND create_time_ms >=? AND create_time_ms<=? ORDER BY create_time_ms DESC LIMIT ?,?";
	
	public AndonMessageModel getAndonMessageModelById(long id) {
		Object[] args = new Object[1];
		args[0] = id;
		List<AndonMessageModel> list = jdbcTemplate.query(Sql_GetModel_By_Id, args, beanPropertyRowMapper);
		if (list == null || list.size() <= 0) {
			return null;
		}
		AndonMessageModel model = list.get(0);
		return model;
	}

	private static String Sql_GetModel_By_Id = "SELECT * FROM jdevice_h_andon WHERE id = ?";
	private BeanPropertyRowMapper<AndonMessageModel> beanPropertyRowMapper = new BeanPropertyRowMapper<AndonMessageModel>(
			AndonMessageModel.class);

	public AndonMessageModel getAndonMessageModelBySolveTimeMs(long deviceId, long solveTimeMs) {
		Object[] args = new Object[2];
		args[0] = deviceId;
		args[1] = solveTimeMs;
		List<AndonMessageModel> list = jdbcTemplate.query(Sql_GetAndonMessage_By_DeviceId_SolveTimeMs, args, beanPropertyRowMapper);
		if (list == null || list.size() <= 0) {
			return null;
		}
		AndonMessageModel model = list.get(0);
		return model;
	}
	private static String Sql_GetAndonMessage_By_DeviceId_SolveTimeMs="SELECT * FROM jdevice_h_andon WHERE device_id=? AND solve_time_ms=?";
	
	public boolean setMsgStatusNotClosed(long deviceId, long solveTimeMs) {
		// TODO Auto-generated method stub
		Object[] args = new Object[2];
		if (deviceId<=0) {
			return false;
		}
		args[0] = solveTimeMs;
		args[1] = deviceId;
		return jdbcTemplate.update(Sql_SetMsgStatusNotClosed_By_DeviceId, args) > 0;
	}

	private static String Sql_SetMsgStatusNotClosed_By_DeviceId = "UPDATE jdevice_h_andon" + //
			" SET " + //
			"solve_time_ms=?," + //
			"status=" + AndonMessageModel.STATUS_VALUE_NotClosed + //
			" WHERE " + //
			"device_id=? " + //
			" AND " + //
			"status>=" + AndonMessageModel.STATUS_VALUE_Claimed;
	
	public boolean setMsgStatusClosed(long deviceId, long solveTimeMs,long closeTimeMs,String reason) {
		// TODO Auto-generated method stub
		Object[] args = new Object[4];
		if (deviceId<=0) {
			return false;
		}
		args[0] = solveTimeMs;
		args[1] = closeTimeMs;
		args[2] = reason;
		args[3] = deviceId;
		return jdbcTemplate.update(Sql_SetMsgStatusClosed_By_DeviceId, args) > 0;
	}

	private static String Sql_SetMsgStatusClosed_By_DeviceId = "UPDATE jdevice_h_andon" + //
			" SET " + //
			"solve_time_ms=?," + //
			"close_time_ms=?," + //
			"reason=?," + //
			"status=" + AndonMessageModel.STATUS_VALUE_Closed + //
			" WHERE " + //
			"device_id=? " + //
			" AND " + //
			"status>=" + AndonMessageModel.STATUS_VALUE_Claimed;

	public List<AndonMessageModel> getNotClosedModelsByWorkshopId(long workshopId, int minIndex, int dataNum) {
		Object[] args = new Object[3];
		args[0] = workshopId;
		args[1] = minIndex;
		args[2] = dataNum;
		List<AndonMessageModel> list = jdbcTemplate.query(Sql_GetNotClosedModels_By_WorkshopId, args, beanPropertyRowMapper);
		return list;
	}
	
	private static String Sql_GetNotClosedModels_By_WorkshopId = "SELECT * FROM jdevice_h_andon"+//
			" WHERE "+//
			"workshop_id=?"+//
			" AND "+//
			"status>="+AndonMessageModel.STATUS_VALUE_NotClosed+//
			" ORDER BY status DESC ,create_time_ms DESC "+//
			" LIMIT ?,?";
	
	public boolean changeWorkcenter(long deviceId, long workcenterId, long workshopId, long factoryId) {
		Object[] args = new Object[4];
		int i=0;
		args[i++] = workcenterId;
		args[i++] = workshopId;
		args[i++] = factoryId;
		args[i++] = deviceId;
		return jdbcTemplate.update(Sql_ChangeWorkcenter_By_DeviceId, args)>0;
	}
	
	private static String Sql_ChangeWorkcenter_By_DeviceId = "UPDATE jdevice_h_andon SET workcenter_id=?,workshop_id=?,factory_id=? WHERE device_id=?;";
	
	public boolean changeWorkshop(long workcenterId,long workshopId, long factoryId) {
		Object[] args = new Object[3];
		int i=0;
		args[i++] = workshopId;
		args[i++] = factoryId;
		args[i++] = workcenterId;
		return jdbcTemplate.update(Sql_ChangeWorkshop_By_WorkcenterId, args)>0;
	}
	
	private static String Sql_ChangeWorkshop_By_WorkcenterId="UPDATE jdevice_h_andon SET workshop_id=?,factory_id=? WHERE workcenter_id=?;";
	
	public boolean changeFactory(long workshopId,long factoryId) {
		Object[] args = new Object[2];
		int i=0;
		args[i++] = factoryId;
		args[i++] = workshopId;
		return jdbcTemplate.update(Sql_ChangeFactory_By_WorkshopId, args)>0;
	}
	
	private static String Sql_ChangeFactory_By_WorkshopId="UPDATE jdevice_h_andon SET factory_id=? WHERE workshop_id=?;";
}
