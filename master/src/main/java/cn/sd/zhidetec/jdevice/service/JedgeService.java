package cn.sd.zhidetec.jdevice.service;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import cn.sd.zhidetec.jdevice.server.JedgeClientTask;
import cn.sd.zhidetec.jdevice.server.JedgeServer;
import cn.sd.zhidetec.jdevice.client.JedgeClientInfo;
import cn.sd.zhidetec.jdevice.model.WarnDataModel;

@Service
public class JedgeService {

	@Value("${jedges.port}")
	private int listernPort;
	
	private JedgeServer jedgeServer;
	
	public void start() throws IOException {
		jedgeServer=new JedgeServer();
		jedgeServer.setListernPort(listernPort);
		jedgeServer.start();
	}

	public JedgeServer getJedgeServer() {
		return jedgeServer;
	}

	public void setJedgeServer(JedgeServer jedgeServer) {
		this.jedgeServer = jedgeServer;
	}
	
	public void syncWarnDataModelToJedge(WarnDataModel warnDataModel) {
		List<JedgeClientTask> list = jedgeServer.getJedgeModels();
		for (JedgeClientTask jedgeModel : list) {
			if (jedgeModel.getJedgeInfoModel()!=null//
					&&jedgeModel.getJedgeInfoModel().getWorkshopId()>0
					&&warnDataModel.getWorkshopId()>0
					&&jedgeModel.getJedgeInfoModel().getWorkshopId()==warnDataModel.getWorkshopId()) {
				jedgeModel.updateWarnDataModel(warnDataModel);
			}
		}
	}
	
	public boolean syncJedgeInfoModel(JedgeClientInfo jedgeInfoModel) {
		if (jedgeInfoModel==null) {
			return false;
		}
		List<JedgeClientTask> list = jedgeServer.getJedgeModels();
		for (JedgeClientTask jedgeClientTask : list) {
			if (jedgeClientTask.getJedgeInfoModel()!=null//
					&&jedgeClientTask.getJedgeInfoModel().getAppId()!=null//
					&&jedgeInfoModel.getAppId()!=null//
					&&jedgeClientTask.getJedgeInfoModel().getAppId().equals(jedgeInfoModel.getAppId())) {
				jedgeClientTask.syncJedgeInfoModel(jedgeInfoModel);
				return true;
			}
		}
		return false;
	}
	
	public boolean changeFactory(long workshopId,long factoryId) {
		if (workshopId<0||factoryId<0) {
			return false;
		}
		List<JedgeClientTask> list = jedgeServer.getJedgeModels();
		for (JedgeClientTask jedgeClientTask : list) {
			if (jedgeClientTask.getJedgeInfoModel().getWorkshopId()==workshopId//
					&&jedgeClientTask.getJedgeInfoModel().getFactoryId()!=factoryId) {
				jedgeClientTask.changeFactory(workshopId,factoryId);
			}
		}
		return true;
	}
	
}
