package cn.sd.zhidetec.jdevice.server;

import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.sd.zhidetec.jdevice.client.JdeviceBaseClient;
import cn.sd.zhidetec.jdevice.client.JdeviceClientInfo;
import cn.sd.zhidetec.jdevice.model.DeviceModel;
import cn.sd.zhidetec.jdevice.pojo.DeviceStatusDataPojo;
import cn.sd.zhidetec.jdevice.service.JedgeTelegramService;
import cn.sd.zhidetec.jdevice.util.ByteUtil;

public class JdeviceClientTask implements Runnable{
	private JdeviceClientInfo jdeviceClientInfo;
	private Object socketObjLock = new Object();
	private Socket socket=null;
	private Thread thread;
	private boolean started=false;
	private boolean stoped=true;
	private long noDataTimeMsToOffline;
	private ObjectMapper jsonMapper=new ObjectMapper();
	protected Logger logger = LoggerFactory.getLogger(getClass());
	private String tag=this.getClass().getSimpleName();
	@Override
	public void run() {
		if (jdeviceClientInfo==null) {
			started=false;
			stoped=true;
			return;
		}
		started=true;
		stoped=false;
		noDataTimeMsToOffline=System.currentTimeMillis()+jdeviceClientInfo.getNoDataTimeMsToOffline();    	
		List<Integer> telegramIndexList = new ArrayList<>();
		List<DeviceStatusDataPojo> sendingStatusDataPojoes = new ArrayList<>();
		while (started) {
			try {
				Thread.sleep(1);
				if (socket==null) {
					sendingStatusDataPojoes.clear();
					break;
				}
				if (noDataTimeMsToOffline<System.currentTimeMillis()) {
					socket.close();
					socket=null;
					sendingStatusDataPojoes.clear();
					logger.info(tag+" "+jdeviceClientInfo.getPhone()+"数据接收超时");
					break;
				}
				//下发设备状态数据
				if (sendStatusDataPojoes.size()>0//
						&&sendingStatusDataPojoes.size()<=0) {
					synchronized (sendStatusDataPojoesLockObject) {
						sendingStatusDataPojoes.addAll(sendStatusDataPojoes);
					}
					//报文组合
					byte[] jsonBytes = jsonMapper.writeValueAsBytes(sendingStatusDataPojoes);
					byte[] telegram=new byte[jsonBytes.length+10];
					int i=0;
					telegram[i++]=JedgeTelegramService.TELEGRAM_BYTE_Begin;
					telegram[i++]=0x00;
					telegram[i++]=JedgeTelegramService.TELEGRAM_BYTE_FUNC_SendStatusData;
					telegram[i++]=(byte)(jsonBytes.length>>24);
					telegram[i++]=(byte)(jsonBytes.length>>16);
					telegram[i++]=(byte)(jsonBytes.length>>8);
					telegram[i++]=(byte)(jsonBytes.length);
					for (int j=0; j < jsonBytes.length; j++) {
						telegram[i++]=jsonBytes[j];
					}
					telegram[i++]=0x00;
					telegram[i++]=0x00;
					telegram[i++]=JedgeTelegramService.TELEGRAM_BYTE_End;
					//下发
					this.socket.getOutputStream().write(telegram);
					this.socket.getOutputStream().flush();
					logger.info(tag+" 向"+jdeviceClientInfo.getPhone()+"下发设备状态数据："+sendingStatusDataPojoes.size());
				}
				//报文获取
				synchronized (socketObjLock) {
	    			if (this.socket.getInputStream().available() > 0) {
						// 保证数据完整性
						int num = this.socket.getInputStream().available();
						Thread.sleep(100);
						if (this.socket.getInputStream().available() == num&&num>=8) {
							// 读取数据
							noDataTimeMsToOffline=System.currentTimeMillis()+jdeviceClientInfo.getNoDataTimeMsToOffline();
							byte[] response = new byte[num];
							this.socket.getInputStream().read(response);
							telegramIndexList.clear();
							// 报文分割
							if (response[0]==JedgeTelegramService.TELEGRAM_BYTE_Begin&&response[response.length-1]==JedgeTelegramService.TELEGRAM_BYTE_End) {
								for (int i = 0; i < response.length; ) {
									telegramIndexList.add(i);
									int index=telegramIndexList.get(telegramIndexList.size()-1);
									int length = ByteUtil.getInt32(response, index+3);
									i+=length+10;
								}
							}
							// 解析数据
							for (int i = 0; i < telegramIndexList.size(); i++) {
								int index=telegramIndexList.get(telegramIndexList.size()-1);
								int length = ByteUtil.getInt32(response, index+3);
								// 合法性校验
								// 回复心跳请求
								if (response[index+1]==0x00&&response[index+2]==JedgeTelegramService.TELEGRAM_BYTE_FUNC_SendHeartRequest) {
									socket.getOutputStream().write(JedgeTelegramService.getRecvHeartResponse());
								}
								// 修改终端配置
								else if (response[index+1]==0x00&&response[index+2]==JedgeTelegramService.TELEGRAM_BYTE_FUNC_UpdateClientInfo) {
									String jsonStr = new String(response, 7, length);
									if (jsonStr!=null&&!jsonStr.trim().equals("")) {
										JdeviceClientInfo andonMessageClientInfo = jsonMapper.readValue(jsonStr, JdeviceClientInfo.class);
										this.setJdeviceClientInfo(andonMessageClientInfo);
										// 操作成功的回复
										byte[] telegram=JedgeTelegramService.getDoneSuccessResponse();
										telegram[7]=0x00;
										telegram[8]=JedgeTelegramService.TELEGRAM_BYTE_FUNC_UpdateClientInfo;
										//CRC校验位
										//发送
										this.socket.getOutputStream().write(telegram);
										this.socket.getOutputStream().flush();
									}else {
										//操作失败的回复
										byte[] telegram=JedgeTelegramService.getDoneFailResponse();
										telegram[7]=0x00;
										telegram[8]=JedgeTelegramService.TELEGRAM_BYTE_FUNC_UpdateClientInfo;
										//CRC校验位
										//发送
										this.socket.getOutputStream().write(telegram);
										this.socket.getOutputStream().flush();
									}
								}
								// 操作成功的提示
								else if (response[index+1]==0x00&&response[index+2]==JedgeTelegramService.TELEGRAM_BYTE_FUNC_DoneSuccess) {
									//设备状态下发成功
									if (response[index+7]==0x00&&response[index+8]==JedgeTelegramService.TELEGRAM_BYTE_FUNC_SendStatusData) {
										synchronized (sendStatusDataPojoesLockObject) {
											for (int j = sendingStatusDataPojoes.size()-1; j >= 0; j--) {
												sendingStatusDataPojoes.remove(j);
												sendStatusDataPojoes.remove(j);
											}	
										}
										logger.info(tag+" 从"+jdeviceClientInfo.getPhone()+"接收设备状态回复："+sendingStatusDataPojoes.size());
									}
								}
								// 操作失败的提示
								else if (response[index+1]==0x00&&response[index+2]==JedgeTelegramService.TELEGRAM_BYTE_FUNC_DoneFail) {
									if (response[index+7]==0x00&&response[index+8]==JedgeTelegramService.TELEGRAM_BYTE_FUNC_SendStatusData) {
										sendingStatusDataPojoes.clear();
									}
								}
							}
						}
					}
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
		stoped=true;
		started=false;
	}
	/**
	 * 发送设备状态数据
	 * @param newStatus 设备状态数据
	 * */
	public void sendDeviceStatus(DeviceModel newStatus,DeviceModel info) {
		synchronized (sendStatusDataPojoesLockObject) {
			sendStatusDataPojoes.add(DeviceStatusDataPojo.build(newStatus,info));
		}
	}
	private List<DeviceStatusDataPojo> sendStatusDataPojoes=Collections.synchronizedList(new ArrayList<>());
	private Object sendStatusDataPojoesLockObject=new Object();
	public void startThread() {
		if (started==false&&stoped==true) {
			thread=new Thread(this);
			thread.start();
		}
	}
	public void stopThread() {
		started=false;
	}
	public boolean hadStarted() {
		return started;
	}
	public boolean hadStoped() {
		return stoped;
	}
	public Socket getSocket() {
		return socket;
	}
	public void setSocket(Socket socket) {
		this.socket = socket;
	}
	public JdeviceClientInfo getJdeviceClientInfo() {
		return jdeviceClientInfo;
	}
	public void setJdeviceClientInfo(JdeviceClientInfo clientInfo) {
		this.jdeviceClientInfo = clientInfo;
	}
	public Object getSocketObjLock() {
		return socketObjLock;
	}
	public void setSocketObjLock(Object socketObjLock) {
		this.socketObjLock = socketObjLock;
	}
	public boolean equals(JdeviceBaseClient client) {
		// TODO Auto-generated method stub
		if (client.getClientInfo()==null||jdeviceClientInfo==null) {
			return false;
		}
		return false;
	}
}
