package cn.sd.zhidetec.jdevice.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.sd.zhidetec.jdevice.dao.ShiftModelDao;
import cn.sd.zhidetec.jdevice.model.ShiftModel;
import cn.sd.zhidetec.jdevice.model.WorkshopModel;
import cn.sd.zhidetec.jdevice.util.DateUtil;

@Service
public class ShiftService {

	@Autowired
	protected ShiftModelDao shiftModelDao;
	@Autowired
	protected DeviceOeeService deviceOeeService;
	protected Logger logger = LoggerFactory.getLogger(getClass());
	
	public List<ShiftModel> initWorkshop(WorkshopModel workshopModel) {
		List<ShiftModel> shiftModels=shiftModelDao.initWorkshop(workshopModel);
		//更新缓存
		synchronized (cachedShiftModelsLockObj) {
			if (cachedShiftModels!=null) {
				for (ShiftModel shiftModel : shiftModels) {
					cachedShiftModels.add(shiftModel);	
				}
			}
		}
		return shiftModels;
	}
	
	public boolean setShiftModel(ShiftModel shiftModel) {
		boolean isNew=false;
		if (shiftModel.getId()<=0) {
			isNew=true;
		}
		if (shiftModelDao.setShiftModel(shiftModel)) {
			synchronized (cachedShiftModelsLockObj) {
				if (cachedShiftModels!=null) {
					if (isNew) {
						cachedShiftModels.add(shiftModel);	
					}else {
						for (int i = 0; i < cachedShiftModels.size(); i++) {
							if (cachedShiftModels.get(i).getId()==shiftModel.getId()) {
								cachedShiftModels.get(i).copyField(shiftModel);
								break;
							}
						}	
					}
				}
			}
			return true;	
		}
		return false;
	}
	
	public List<ShiftModel> getShiftModelsByWorkshopId(long workshopId) {
		List<ShiftModel> list=new ArrayList<ShiftModel>();
		//加载班次
		loadAllShiftModelsToCache();
		//抽取数据
		synchronized (cachedShiftModelsLockObj) {
			if (cachedShiftModels!=null) {
				for (ShiftModel shiftModel : cachedShiftModels) {
					if (shiftModel.getWorkshopId()==workshopId) {
						list.add(shiftModel);
					}
				}	
			}
		}
		return list;
	}
	
	public List<ShiftModel> getShiftModelsByFactoryId(long factoryId) {
		List<ShiftModel> list=new ArrayList<ShiftModel>();
		//加载班次
		loadAllShiftModelsToCache();
		//抽取数据
		synchronized (cachedShiftModelsLockObj) {
			if (cachedShiftModels!=null) {
				for (ShiftModel shiftModel : cachedShiftModels) {
					if (shiftModel.getFactoryId()==factoryId) {
						list.add(shiftModel);
					}
				}	
			}
		}
		return list;
	}
	
	public List<ShiftModel> getShiftModelsByOrganizationId(String organizationId) {
		List<ShiftModel> list=new ArrayList<ShiftModel>();
		//加载班次
		loadAllShiftModelsToCache();
		//抽取数据
		synchronized (cachedShiftModelsLockObj) {
			if (cachedShiftModels!=null) {
				for (ShiftModel shiftModel : cachedShiftModels) {
					if (shiftModel.getOrganizationId().equals(organizationId)) {
						list.add(shiftModel);
					}
				}	
			}
		}
		return list;
	}

	public boolean deleteShiftModel(ShiftModel model) {
		if (shiftModelDao.deleteShiftModel(model)) {
			synchronized (cachedShiftModelsLockObj) {
				if (cachedShiftModels!=null) {
					for (int i = 0; i < cachedShiftModels.size(); i++) {
						if (cachedShiftModels.get(i).getId()==model.getId()) {
							cachedShiftModels.remove(i);
							break;
						}
					}
				}	
			}	
			return true;
		}
		return false;
	}
	
	public ShiftModel getShiftModelByWorkshopId(long workshopId,long timeMs) {
		long todayTimeMs=DateUtil.getTodayBeginTimeMs();
		List<ShiftModel> shiftModels=getShiftModelsByWorkshopId(workshopId);
		for (ShiftModel shiftModel : shiftModels) {
			//不跨天
			long shiftBeginTimeMs=todayTimeMs+shiftModel.getBeginTimeMin()*DateUtil.MILLISECOND_ONE_MINUTE;
			long shiftEndTimeMs;
			if (!shiftModel.daySpan()) {
				shiftEndTimeMs=todayTimeMs+shiftModel.getEndTimeMin()*DateUtil.MILLISECOND_ONE_MINUTE;
			}
			//跨天
			else {
				shiftEndTimeMs=todayTimeMs+shiftModel.getEndTimeMin()*DateUtil.MILLISECOND_ONE_MINUTE+DateUtil.MILLISECOND_ONE_DAY;
			}
			//命中
			if (timeMs>=shiftBeginTimeMs&&timeMs<shiftEndTimeMs) {
				return shiftModel;
			}
		}
		return null;
	}
	
	public List<ShiftModel> loadAllShiftModelsToCache() {
		synchronized (cachedShiftModelsLockObj) {
			if (cachedShiftModels==null||cachedShiftModels.size()<=0) {
				cachedShiftModels=shiftModelDao.getAllShiftModels();
				logger.info("Loaded All ShiftModel From DB");
			}	
		}
		return cachedShiftModels;
	}
	private List<ShiftModel> cachedShiftModels=Collections.synchronizedList(new ArrayList<ShiftModel>());
	private Object cachedShiftModelsLockObj=new Object();
	private Thread shiftListernThread;
	private boolean started=false;
	public void start() {
		if (started) {
			return;
		}
		started=true;
		shiftListernThread=new Thread() {
			@Override
			public void run() {
				logger.info("ShiftService Shift Change Listerner Started");
				while (started) {
					try {
						Thread.sleep(500);
						//加载全部班次到缓存
						loadAllShiftModelsToCache();
						//当天时间
						long todayTimeMs=DateUtil.getTodayBeginTimeMs();
						long timeMs=System.currentTimeMillis();
						//计算
						synchronized (cachedShiftModelsLockObj) {
							for (ShiftModel shiftModel : cachedShiftModels) {
								//不跨天
								long shiftBeginTimeMs=todayTimeMs+shiftModel.getBeginTimeMin()*DateUtil.MILLISECOND_ONE_MINUTE;
								long shiftEndTimeMs;
								if (!shiftModel.daySpan()) {
									shiftEndTimeMs=todayTimeMs+shiftModel.getEndTimeMin()*DateUtil.MILLISECOND_ONE_MINUTE;
								}
								//跨天
								else {
									shiftEndTimeMs=todayTimeMs+shiftModel.getEndTimeMin()*DateUtil.MILLISECOND_ONE_MINUTE+DateUtil.MILLISECOND_ONE_DAY;
								}
								//命中
								if (timeMs>=shiftBeginTimeMs&&timeMs<shiftEndTimeMs) {
									//刚进入班次
									if (shiftModel.isInShift()==false) {
										deviceOeeService.changeShift(shiftModel);
									}
									shiftModel.setInShift(true);
								}else {
									shiftModel.setInShift(false);
								}
							}
						}
					} catch (Exception e) {
						logger.error("班次服务异常", e);
					}
				}
				logger.info("ShiftService Shift Change Listerner Stoped");
			}
		};
		shiftListernThread.start();
	}

	public ShiftModel getShiftModelById(long id) {
		return shiftModelDao.getShiftModelById(id);
	}
	
	public boolean changeFactory(long workshopId,long factoryId) {
		return shiftModelDao.changeFactory(workshopId, factoryId);
	}


    /**
     * 根据车间id查询车间班次
     * @param workshopId
     * @return
     */
    public List<ShiftModel> findShiftModelsByWorkshopId(long workshopId) {

	    return shiftModelDao.getShiftModelsByWorkshopId(workshopId);
    }
}
