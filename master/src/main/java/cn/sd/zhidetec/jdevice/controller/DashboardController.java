package cn.sd.zhidetec.jdevice.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.sd.zhidetec.jdevice.HttpResponseMsg;
import cn.sd.zhidetec.jdevice.dao.FactoryModelDao;
import cn.sd.zhidetec.jdevice.dao.UserModelDao;
import cn.sd.zhidetec.jdevice.model.DeviceModel;
import cn.sd.zhidetec.jdevice.model.DeviceOeeModel;
import cn.sd.zhidetec.jdevice.model.DeviceStatusModel;
import cn.sd.zhidetec.jdevice.model.RoleModel;
import cn.sd.zhidetec.jdevice.model.UserModel;
import cn.sd.zhidetec.jdevice.pojo.DeviceStatusSumPojo;
import cn.sd.zhidetec.jdevice.service.DeviceFaultService;
import cn.sd.zhidetec.jdevice.service.DeviceOeeService;
import cn.sd.zhidetec.jdevice.service.DeviceService;
import cn.sd.zhidetec.jdevice.service.RBuilderService;
import cn.sd.zhidetec.jdevice.service.WorkcenterService;
import cn.sd.zhidetec.jdevice.service.WorkshopService;
import cn.sd.zhidetec.jdevice.util.DateUtil;
import cn.sd.zhidetec.jdevice.util.StringUtil;

@Controller
@RequestMapping("/dashboard")
public class DashboardController {

	@Autowired
	protected DeviceService deviceService;
	@Autowired
	protected DeviceOeeService deviceOeeService;
	@Autowired
	protected DeviceFaultService deviceFaultService;
	@Autowired
	protected FactoryModelDao factoryModelDao;
	@Autowired
	protected WorkshopService workshopService;
	@Autowired
	protected WorkcenterService workcenterService;
	@Autowired
	protected UserModelDao userModelDao;
	@Autowired
	protected RBuilderService rBuilderService;
	protected Logger logger = LoggerFactory.getLogger(getClass());
	
	/**
	 * 获取当前各个设备状态的设备数目
	 * @param httpSession Session
	 * @return 信息
	 * */
	@RequestMapping(path="/getDeviceStatusSumPojoes")
	@ResponseBody
	public RBuilderService.Response getDeviceStatusSumPojoes(HttpSession httpSession) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_User, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		UserModel userModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		//获取
		List<DeviceStatusSumPojo> list = deviceService.getDeviceStatusSumPojoByOrganizationId(userModel.getOrganizationId());
		//组合
		List<DeviceStatusSumPojo> result = new ArrayList<DeviceStatusSumPojo>();
		DeviceStatusSumPojo model=new DeviceStatusSumPojo();
		model.setDeviceNum(0);
		model.setStatus(DeviceStatusModel.STATUS_Run);
		model.setStatusName(DeviceStatusModel.NAME_Run);
		model.setStatusValue(DeviceStatusModel.VALUE_Run);
		result.add(model);
		model=new DeviceStatusSumPojo();
		model.setDeviceNum(0);
		model.setStatus(DeviceStatusModel.STATUS_Fault);
		model.setStatusName(DeviceStatusModel.NAME_Fault);
		model.setStatusValue(DeviceStatusModel.VALUE_Fault);
		result.add(model);
		model=new DeviceStatusSumPojo();
		model.setDeviceNum(0);
		model.setStatus(DeviceStatusModel.STATUS_Pending);
		model.setStatusName(DeviceStatusModel.NAME_Pending);
		model.setStatusValue(DeviceStatusModel.VALUE_Pending);
		result.add(model);
		model=new DeviceStatusSumPojo();
		model.setDeviceNum(0);
		model.setStatus(DeviceStatusModel.STATUS_Repair);
		model.setStatusName(DeviceStatusModel.NAME_Repair);
		model.setStatusValue(DeviceStatusModel.VALUE_Repair);
		result.add(model);
		model=new DeviceStatusSumPojo();
		model.setDeviceNum(0);
		model.setStatus(DeviceStatusModel.STATUS_Maintain);
		model.setStatusName(DeviceStatusModel.NAME_Maintain);
		model.setStatusValue(DeviceStatusModel.VALUE_Maintain);
		result.add(model);
		model=new DeviceStatusSumPojo();
		model.setDeviceNum(0);
		model.setStatus(DeviceStatusModel.STATUS_Stop);
		model.setStatusName(DeviceStatusModel.NAME_Stop);
		model.setStatusValue(DeviceStatusModel.VALUE_Stop);
		result.add(model);
		model=new DeviceStatusSumPojo();
		model.setDeviceNum(0);
		model.setStatus(DeviceStatusModel.STATUS_Offline);
		model.setStatusName(DeviceStatusModel.NAME_Offline);
		model.setStatusValue(DeviceStatusModel.VALUE_Offline);
		result.add(model);
		//设置
		for (int i = 0; i < list.size(); i++) {
			for (DeviceStatusSumPojo deviceStatusSumPojo : result) {
				if (deviceStatusSumPojo.getStatusValue()==list.get(i).getStatusValue()) {
					deviceStatusSumPojo.copyFields(list.get(i));
					break;
				}
			}
		}
		//返回
		return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, result);
	}
	
	/**
	 * 获取车间当前各个设备状态的设备数目
	 * @param httpSession Session
	 * @param workshopId 车间ID
	 * @return 信息
	 * */
	@RequestMapping(path="/getWorkshopDeviceStatusSumPojoes")
	@ResponseBody
	public RBuilderService.Response getWorkshopDeviceStatusSumPojoes(HttpSession httpSession,//
			@RequestParam("workshopId")long workshopId) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_User, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		List<DeviceStatusSumPojo> list = deviceService.getDeviceStatusSumPojoByWorkshopId(workshopId);
		Map<String, Object> map=new HashMap<String, Object>();
		map.put("pojoes", list);
		map.put("workshopId", workshopId);
		return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, map);
	}
	
	/**
	 * 获取当前待监控的设备数目
	 * @param httpSession Session
	 * @return 组织信息
	 * */
	@RequestMapping(path="/getNoneMonitorDeviceNum")
	@ResponseBody
	public RBuilderService.Response getNoneMonitorDeviceNum(HttpSession httpSession) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_User, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		UserModel userModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, deviceService.getMonitorNum(DeviceModel.MONITOR_VAL_NONE, userModel.getOrganizationId()));
	}
	
	@RequestMapping(path="/getUserNum")
	@ResponseBody
	public RBuilderService.Response getUserNum(HttpSession httpSession, //
			HttpServletRequest httpServletRequest) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_User, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		//获取用户角色
		int roleLevel = -1;
		String roleLevelStr = httpServletRequest.getParameter("roleLevel");
		if (StringUtil.isNumber(roleLevelStr)) {
			roleLevel = Integer.parseInt(roleLevelStr);
		}
		//返回
		UserModel userModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, userModelDao.getUserNum(userModel.getOrganizationId(),roleLevel));
	}
	
	@RequestMapping(path="/getFactoryNum")
	@ResponseBody
	public RBuilderService.Response getFactoryNum(HttpSession httpSession) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_User, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		UserModel userModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, factoryModelDao.getFactoryNum(userModel.getOrganizationId()));
	}
	
	@RequestMapping(path="/getWorkshopNum")
	@ResponseBody
	public RBuilderService.Response getWorkshopNum(HttpSession httpSession) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_User, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		UserModel userModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, workshopService.getWorkshopNum(userModel.getOrganizationId()));
	}
	
	@RequestMapping(path="/getWorkcenterNum")
	@ResponseBody
	public RBuilderService.Response getWorkcenterNum(HttpSession httpSession) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_User, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		UserModel userModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, workcenterService.getWorkcenterNum(userModel.getOrganizationId()));
	}
	@RequestMapping(path="/getWorkshopDeviceStatusNum")
	@ResponseBody
	public RBuilderService.Response getWorkshopDeviceStatusNum(HttpSession httpSession,//
			@RequestParam("workshopId")long workshopId,//
			@RequestParam("statusValue")int statusValue,//
			@RequestParam("startTimeMs")long startTimeMs) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_User, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		UserModel userModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		if (userModel.getRoleLevel()>=RoleModel.ROLE_LEVEL_WorkshopAdmin) {
			return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, deviceService.getWorkshopDeviceStatusNum(workshopId,statusValue,startTimeMs));
		}else {
			return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, deviceService.getWorkcenterDeviceStatusNum(userModel.getWorkcenterId(),statusValue,startTimeMs));
		}
	}
	@RequestMapping(path="/getDeviceStatusNum")
	@ResponseBody
	public RBuilderService.Response getDeviceStatusNum(HttpSession httpSession,//
			@RequestParam("deviceId")long deviceId,//
			@RequestParam("statusValue")int statusValue,//
			@RequestParam("startTimeMs")long startTimeMs) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_User, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, deviceService.getDeviceStatusNum(deviceId,statusValue,startTimeMs));
	}
	@RequestMapping(path="/getWorkshopAShiftDeviceOeeModels")
	@ResponseBody
	public RBuilderService.Response getWorkshopAShiftDeviceOeeModels(HttpSession httpSession,//
			@RequestParam("workshopId")long workshopId,//
			@RequestParam("shiftTimeMs")long shiftTimeMs) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_User, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		// 班次时间<=0时返回当前班次
		if (shiftTimeMs<=0) {
			shiftTimeMs=System.currentTimeMillis();
		}
		UserModel userModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		if (userModel.getRoleLevel()>=RoleModel.ROLE_LEVEL_WorkshopAdmin) {
			return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, deviceOeeService.getWorkshopAShiftDeviceOeeModels(workshopId,shiftTimeMs));
		}else {
			return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, deviceOeeService.getWorkcenterAShiftDeviceOeeModels(userModel.getWorkcenterId(),shiftTimeMs));
		}
	}
	@RequestMapping(path="/getDeviceOeeModels")
	@ResponseBody
	public RBuilderService.Response getDeviceOeeModels(HttpSession httpSession,//
			@RequestParam("deviceId")long deviceId,//
			@RequestParam("shiftBeginTimeMs")long shiftBeginTimeMs) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_User, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, deviceOeeService.getDeviceOeeModels(deviceId,shiftBeginTimeMs));
	}
	@RequestMapping(path="/getLastDeviceOeeModel")
	@ResponseBody
	public RBuilderService.Response getLastDeviceOeeModel(HttpSession httpSession,//
			@RequestParam("deviceId")long deviceId,//
			@RequestParam("shiftTimeMs")long shiftTimeMs) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_User, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		if (shiftTimeMs<0) {
			shiftTimeMs=System.currentTimeMillis();
		}
		DeviceOeeModel model=deviceOeeService.getNoOeeModelByDeviceIdAndShiftTimeMs(deviceId, shiftTimeMs);
		return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE,deviceOeeService.computeDeviceOeeAndRunTime(model));
	}
	
	@RequestMapping(path="/getWorkshopDeviceMTBF")
	@ResponseBody
	public RBuilderService.Response getWorkshopDeviceMTBF(HttpSession httpSession,//
			@RequestParam("workshopId")long workshopId,//
			@RequestParam("beginTimeMs")long beginTimeMs) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_User, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		UserModel userModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		if (userModel.getRoleLevel()>=RoleModel.ROLE_LEVEL_WorkshopAdmin) {
			return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, deviceFaultService.getWorkshopMTBF(workshopId,beginTimeMs));
		}else {
			return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, deviceFaultService.getWorkcenterMTBF(userModel.getWorkcenterId(),beginTimeMs));
		}
	}
	@RequestMapping(path="/getDeviceMTBF")
	@ResponseBody
	public RBuilderService.Response getDeviceMTBF(HttpSession httpSession,//
			@RequestParam("deviceId")long deviceId,//
			@RequestParam("beginTimeMs")long beginTimeMs) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_User, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, deviceFaultService.getDeviceMTBF(deviceId, beginTimeMs));
	}
	@RequestMapping(path="/getWorkshopDeviceMTTR")
	@ResponseBody
	public RBuilderService.Response getWorkshopDeviceMTTR(HttpSession httpSession,//
			@RequestParam("workshopId")long workshopId,//
			@RequestParam("beginTimeMs")long beginTimeMs) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_User, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		UserModel userModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		if (userModel.getRoleLevel()>=RoleModel.ROLE_LEVEL_WorkshopAdmin) {
			return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, deviceFaultService.getWorkshopMTTR(workshopId,beginTimeMs));
		}else {
			return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, deviceFaultService.getWorkcenterMTTR(userModel.getWorkcenterId(),beginTimeMs));
		}
	}
	@RequestMapping(path="/getDeviceMTTR")
	@ResponseBody
	public RBuilderService.Response getDeviceMTTR(HttpSession httpSession,//
			@RequestParam("deviceId")long deviceId,//
			@RequestParam("beginTimeMs")long beginTimeMs) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_User, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, deviceFaultService.getDeviceMTTR(deviceId, beginTimeMs));
	}
	@RequestMapping(path="/getServerTime")
	@ResponseBody
	public RBuilderService.Response getServerTime(HttpSession httpSession) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_User, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		//构建结果
		long timeLong=System.currentTimeMillis();
		String timeStr=DateUtil.getCurrentDateShort();
		Map<String, Object> result=new HashMap<String, Object>();
		result.put("timeLong", timeLong);
		result.put("time", timeStr);
		//返回
		return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, result);
	}
}
