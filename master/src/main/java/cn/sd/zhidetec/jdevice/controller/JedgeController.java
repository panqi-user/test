package cn.sd.zhidetec.jdevice.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.sd.zhidetec.jdevice.HttpResponseMsg;
import cn.sd.zhidetec.jdevice.model.RoleModel;
import cn.sd.zhidetec.jdevice.model.UserModel;
import cn.sd.zhidetec.jdevice.service.DeviceService;
import cn.sd.zhidetec.jdevice.service.JedgeService;
import cn.sd.zhidetec.jdevice.service.RBuilderService;
import cn.sd.zhidetec.jdevice.client.JedgeClientInfo;
import cn.sd.zhidetec.jdevice.model.JedgeDeviceModel;

@Controller
@RequestMapping("/jedge")
public class JedgeController {
	
	@Autowired
	protected DeviceService deviceService;
	@Autowired
	protected JedgeService jedgeService;
	@Autowired
	protected RBuilderService rBuilderService;
	protected Logger logger = LoggerFactory.getLogger(getClass());

	private ObjectMapper jsonMapper=new ObjectMapper();
	@RequestMapping(path="/setJedgeDeviceModels")
	@ResponseBody
	public RBuilderService.Response setJedgeDeviceModels(HttpSession httpSession,//
			@RequestBody List<JedgeDeviceModel> models) {
		RBuilderService.Response response = rBuilderService.build(null,null);
		// 提交到设备代理服务
		try {
			logger.debug("SetJedgeDeviceModels : "+jsonMapper.writeValueAsString(models).toString());
			for (JedgeDeviceModel jedgeDeviceModel : models) {
				deviceService.setJedgeDeviceModelStatus(jedgeDeviceModel);	
			}
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return rBuilderService.update(response,HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, null);
	}
	
	/**
	 * 获取所有Jedge客户端信息
	 * 
	 * @param httpSession Session会话
	 * @return 返回给前端的操作结果
	 */
	@RequestMapping(path = "/getJedgeInfoModels")
	@ResponseBody
	public RBuilderService.Response getJedgeInfoModels(HttpSession httpSession) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_User, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		// 根据用户权限返回数据
		UserModel userModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		if (userModel.getRoleLevel() <= RoleModel.ROLE_LEVEL_WorkshopAdmin) {
			return rBuilderService.update(response, //
					HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, //
					jedgeService.getJedgeServer().getJedgeInfoModelsByWorkshopId(userModel.getWorkshopId()));
		}
		if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_FactoryAdmin) {
			return rBuilderService.update(response, //
					HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, //
					jedgeService.getJedgeServer().getJedgeInfoModelsByFactoryId(userModel.getFactoryId()));
		}
		if (userModel.getRoleLevel() >= RoleModel.ROLE_LEVEL_OrganizationAdmin) {
			return rBuilderService.update(response, //
					HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, //
					jedgeService.getJedgeServer().getJedgeInfoModelsByOrganizationId(userModel.getOrganizationId()));
		}
		return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_DATA, null);
	}

	/**
	 * 设置Jedge客户端信息
	 * 
	 * @param httpSession    Session会话
	 * @param jedgeInfoModel Jedge客户端信息
	 * @return 返回给前端的操作结果
	 */
	@RequestMapping(path = "/setJedgeInfoModel")
	@ResponseBody
	public RBuilderService.Response setJedgeInfoModel(HttpSession httpSession, //
			@RequestBody JedgeClientInfo jedgeInfoModel) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_FactoryAdmin, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		// 根据用户权限操作数据
		UserModel userModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_FactoryAdmin) {
			if (userModel.getFactoryId()==jedgeInfoModel.getFactoryId()//
					&& userModel.getOrganizationId().equals(jedgeInfoModel.getOrganizationId())) {
				if (jedgeService.syncJedgeInfoModel(jedgeInfoModel)) {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, null);
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
				}
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
			}
		}
		if (userModel.getRoleLevel() >= RoleModel.ROLE_LEVEL_OrganizationAdmin) {
			if (userModel.getOrganizationId().equals(jedgeInfoModel.getOrganizationId())) {
				if (jedgeService.syncJedgeInfoModel(jedgeInfoModel)) {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, null);
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
				}
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
			}
		}
		return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
	}
	
	/**
	 * 设置Jedge客户端信息
	 * 
	 * @param httpSession    Session会话
	 * @param jedgeInfoModel Jedge客户端信息
	 * @return 返回给前端的操作结果
	 */
	@RequestMapping(path = "/delJedgeInfoModel")
	@ResponseBody
	public RBuilderService.Response delJedgeInfoModel(HttpSession httpSession, //
			@RequestBody JedgeClientInfo jedgeInfoModel) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_FactoryAdmin, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		// 根据用户权限操作数据
		UserModel userModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_FactoryAdmin) {
			if (userModel.getFactoryId()==jedgeInfoModel.getFactoryId()//
					&& userModel.getOrganizationId().equals(jedgeInfoModel.getOrganizationId())) {
				if (jedgeService.getJedgeServer().delJedgeInfoModel(jedgeInfoModel)) {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, null);
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
				}
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
			}
		}
		if (userModel.getRoleLevel() >= RoleModel.ROLE_LEVEL_OrganizationAdmin) {
			if (userModel.getOrganizationId().equals(jedgeInfoModel.getOrganizationId())) {
				if (jedgeService.getJedgeServer().delJedgeInfoModel(jedgeInfoModel)) {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, null);
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
				}
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
			}
		}
		return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
	}

}
