package cn.sd.zhidetec.jdevice.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.sd.zhidetec.jdevice.dao.WorkcenterModelDao;
import cn.sd.zhidetec.jdevice.model.WorkcenterModel;
import cn.sd.zhidetec.jdevice.model.WorkshopModel;

@Service
public class WorkcenterService {

	@Autowired
	protected UserService userService;
	@Autowired
	protected DeviceService deviceService;
	@Autowired
	protected WorkcenterModelDao workcenterModelDao;
	protected Logger logger = LoggerFactory.getLogger(getClass());
	
	public int getWorkcenterNum(String organizationId) {
		return workcenterModelDao.getWorkcenterNum(organizationId);
	}

	public WorkcenterModel getWorkcenterModelById(long id) {
		return workcenterModelDao.getWorkcenterModelById(id);
	}

	public List<WorkcenterModel> getWorkcenterModelsById(long workcenterId) {
		return workcenterModelDao.getWorkcenterModelsById(workcenterId);
	}

	public List<WorkcenterModel> getWorkcenterModelsByWorkshopId(long workshopId) {
		return workcenterModelDao.getWorkcenterModelsByWorkshopId(workshopId);
	}

	public List<WorkcenterModel> getWorkcenterModelsByFactoryId(long factoryId) {
		return workcenterModelDao.getWorkcenterModelsByFactoryId(factoryId);
	}

	public List<WorkcenterModel> getWorkcenterModelsByOrganizationId(String organizationId) {
		return workcenterModelDao.getWorkcenterModelsByOrganizationId(organizationId);
	}

	public boolean setWorkcenterModel(WorkcenterModel workcenterModel) {
		//获取上一工作中心
		if (workcenterModel.getId()>0) {
			List<WorkcenterModel> list = workcenterModelDao.getWorkcenterModelsById(workcenterModel.getId());
			if (list.size()>0) {
				WorkcenterModel lastWorkcenterModel=list.get(0);
				//更换车间
				if (workcenterModel.getWorkshopId()!=lastWorkcenterModel.getWorkshopId()//
						||workcenterModel.getFactoryId()!=lastWorkcenterModel.getFactoryId()) {
					//设备
					deviceService.changeWorkshop(workcenterModel.getId(), workcenterModel.getWorkshopId(), workcenterModel.getFactoryId());
					//人员
					userService.changeWorkshop(workcenterModel.getId(), workcenterModel.getWorkshopId(), workcenterModel.getFactoryId());
				}
			}else {
				workcenterModel.setId(-1);
			}
		}
		//新建
		return workcenterModelDao.setWorkcenterModel(workcenterModel);
	}

	public boolean deleteWorkcenterModel(WorkcenterModel workcenterModel) {
		return workcenterModelDao.deleteWorkcenterModel(workcenterModel);
	}

	public WorkcenterModel initWorkshop(WorkshopModel workshopModel) {
		WorkcenterModel workcenterModel=workcenterModelDao.initWorkshop(workshopModel);
		if (workcenterModel!=null) {
			deviceService.initWorkcenter(workcenterModel);
		}
		return workcenterModel;
	}
	
	public boolean changeFactory(long workshopId,long factoryId) {
		return workcenterModelDao.changeFactory(workshopId, factoryId);
	}
	
}
