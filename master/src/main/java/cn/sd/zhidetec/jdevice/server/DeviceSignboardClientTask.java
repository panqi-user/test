package cn.sd.zhidetec.jdevice.server;

import java.io.IOException;
import java.net.Socket;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.sd.zhidetec.jdevice.model.DeviceModel;
import cn.sd.zhidetec.jdevice.model.DeviceSignboardModel;
import cn.sd.zhidetec.jdevice.model.DeviceStatusModel;
import cn.sd.zhidetec.jdevice.model.DeviceTempHumiModel;
import cn.sd.zhidetec.jdevice.util.ByteUtil;
import cn.sd.zhidetec.jdevice.util.DateUtil;
import cn.sd.zhidetec.jdevice.util.SocketUtil;
import cn.sd.zhidetec.jdevice.util.StringUtil;

/**
 * 设备标识牌的网络任务
 * */
public class DeviceSignboardClientTask implements Runnable{
	
	protected Logger logger = LoggerFactory.getLogger(getClass());
	private Socket socket;
	private boolean started=false;
	private DeviceSignboardServer deviceSignboardServer;
	private int recvTimeoutMS=10000;
	
	public DeviceSignboardClientTask(DeviceSignboardServer deviceSignboardServer,//
			Socket socket,//
			int recvTimeoutMS) {
		super();
		this.socket = socket;
		this.deviceSignboardServer = deviceSignboardServer;
		this.recvTimeoutMS = recvTimeoutMS;
	}
	
	@Override
	public void run() {
		started=true;
		DeviceSignboardModel deviceSignboardModel=null;
		while (started&&socket!=null) {
			try {
				Thread.sleep(1);
				if (socket.isClosed()//
						||!socket.isConnected()//
						||socket.isInputShutdown()//
						||socket.isOutputShutdown()) {
					return ;
				}
				// 处理数据
				byte[] cache = SocketUtil.recvResponseFromSocket(socket, 11, recvTimeoutMS);
				if (cache==null) {
					socket.close();
					return;
				}
				deviceSignboardServer.writeRunLog("\n"+DateUtil.getCurrentDateLong()+" [接收到原始数据] "+StringUtil.toHexArrString(cache));
				//数据检查
				if (checkTelegram(cache,0,cache.length)==false) {
					logger.info("DeviceSignboard [获取到异常报文] "+StringUtil.toHexArrString(cache));
					deviceSignboardServer.writeRunLog("\n"+DateUtil.getCurrentDateLong()+" [接收到异常数据] "+StringUtil.toHexArrString(cache));
					continue;
				}
				List<Integer> beginIndexes=getTelegramBeginIndexList(cache, 0, cache.length);
				if (beginIndexes.size()>1) {
					logger.info("DeviceSignboard [获取到"+beginIndexes.size()+"组连续报文] "+StringUtil.toHexArrString(cache));
				}
				for (Integer index : beginIndexes) {
					int dataLengthB1 = ByteUtil.getInt8(cache, index + 11);
					int dataLengthB2 = ByteUtil.getInt8(cache, index + 12);
					int dataLength = (dataLengthB1 << 8) + (dataLengthB2);
					// 上传状态信息
					if (cache[index + 10] == DeviceSignboardClientTask.TELEGRAM_BYTE_CONTROL_STATUS) {
						deviceSignboardModel=parseDeviceSignboardStatus(socket,//
								cache,//
								index,//
								dataLength);
						// 下发更新配置
						if (deviceSignboardModel!=null&&deviceSignboardServer.needUpdateConfig(deviceSignboardModel)) {
							Thread.sleep(1);
							setDeviceSignboardConfig(socket,//
									cache,//
									index,//
									deviceSignboardModel);	
						}
					} 
					// 上传异常信息
					else if (cache[index + 10] == DeviceSignboardClientTask.TELEGRAM_BYTE_CONTROL_ERROR) {
						getDeviceSignboardErrorStatus(socket,//
								cache,//
								index,//
								dataLength);
					}
					// 上传温湿度数据
					else if (cache[index + 10] == DeviceSignboardClientTask.TELEGRAM_BYTE_CONTROL_TEMP_HUMI) {
						parseDeviceSignboardTempAndHumi(socket,//
								cache,//
								index,//
								dataLength);
					}
					// 下发配置返回
					else if (cache[index + 10] == DeviceSignboardClientTask.TELEGRAM_BYTE_CONTROL_CONFIG) {
						//更新缓存
						if (deviceSignboardModel!=null) {
							deviceSignboardServer.writeRunLog(DateUtil.getCurrentDateLong()+" [配置指令回复] ["+deviceSignboardModel.getCode()+"] "+StringUtil.toHexArrString(cache));
							deviceSignboardServer.updatedConfig(deviceSignboardModel);
						}
					}	
				}
			} catch (Exception e) {
				logger.error("DeviceSignboardNetTask Throw Exception", e);
				return;
			}
		}
	}

	public void stopTask() {
		started=false;
	}
	
	public boolean isRunning() {
		return started;
	}
	
	/**
	 * 检查报文合法性
	 * 
	 * @param cache      通信报文缓存
	 * @param beginIndex 起始位置
	 * @param length     本帧通信报文长度
	 */
	public boolean checkTelegram(byte[] cache, int beginIndex, int length) {
		if (cache == null//
				|| beginIndex >= length//
				|| length > cache.length//
				|| cache[0] != TELEGRAM_BYTE_BEGIN_N1//
				|| cache[1] != TELEGRAM_BYTE_BEGIN_N2) {
			return false;
		}
		int dataLength= ByteUtil.getInt16(cache, beginIndex+11);
		byte sumByte = ByteUtil.getCheckSumByte(cache, beginIndex, dataLength+15 - 2);
		return sumByte == cache[beginIndex + length - 2];
	}

	/**
	 * 分割报文并返回起始坐标列表
	 * 
	 * @param cache      缓存
	 * @param beginIndex 起始坐标
	 * @param length     长度
	 * @return 坐标列表
	 */
	public List<Integer> getTelegramBeginIndexList(byte[] cache, int beginIndex, int length) {
		List<Integer> list = new ArrayList<Integer>();
		if (length > cache.length) {
			return list;
		}
		// 报文分割
		for (int i = 0; i < length;) {
			if (cache[i] == TELEGRAM_BYTE_BEGIN_N1 && cache[i+1] == TELEGRAM_BYTE_BEGIN_N2) {
				list.add(i);
				int index = list.get(list.size() - 1);
				int dataLength= ByteUtil.getInt16(cache, index+11);
				i += dataLength + 15;
			}
		}
		return list;
	}

	/**
	 * 解析设备标识牌的温湿度数据
	 * @throws ParseException 
	 * @throws IOException 
	 * */
	public DeviceSignboardModel parseDeviceSignboardTempAndHumi(Socket socket, //
			byte[] cache, //
			int index, //
			int dataLength) throws ParseException, IOException {
		// 报文校验
		boolean checkOk = checkTelegram(cache, index, dataLength + 15);
		DeviceSignboardModel signboardModel=null;
		// 对象封装
		if (checkOk) {
			//起始,index+0
			signboardModel=deviceSignboardServer.deviceSignboardService.getDeviceModelByCode(StringUtil.toHexArrString(cache, index + 2, 8, "-"));
			if (signboardModel==null) {
				return signboardModel;
			}
			/**
			 * 解析温度数据
			 * */
			if (signboardModel.getDeviceId()>0) {
				//控制码,index+10
				//数据长度,index+11
				String tempHumiStartTimeStr=StringUtil.toDateTimeStr(cache, index + 13);//起始温湿度时间
				int tempHumiIntervalTimeM=ByteUtil.getInt16(cache, index+19);//温湿度数据采集间隔
				int tempHumiDataNum=ByteUtil.getInt16(cache, index+21);//温湿度数据长度
				if (tempHumiDataNum>0) {
					long tempHumiStartTimeMs=DateUtil.getDateTimeLong(tempHumiStartTimeStr+".000");
					if (tempHumiStartTimeMs>System.currentTimeMillis()) {
						deviceSignboardServer.writeRunLog(DateUtil.getCurrentDateLong()+" [温湿度数据异常]\n编号:"+signboardModel.getCode()+"\n采集开始时间："+tempHumiStartTimeStr);
					}else {
						if (tempHumiStartTimeMs>DeviceSignboardServer.getBeOnLineBeginTimeMs()) {
							String logStr="";
							List<DeviceTempHumiModel> list=new ArrayList<DeviceTempHumiModel>();
							for (int i = 0; i < tempHumiDataNum; i++) {
								DeviceTempHumiModel deviceTempHumiModel=new DeviceTempHumiModel();
								list.add(deviceTempHumiModel);
								deviceTempHumiModel.setOrganizationId(signboardModel.getOrganizationId());
								deviceTempHumiModel.setFactoryId(signboardModel.getFactoryId());
								deviceTempHumiModel.setWorkshopId(signboardModel.getWorkshopId());
								deviceTempHumiModel.setFactoryId(signboardModel.getFactoryId());
								deviceTempHumiModel.setDeviceId(signboardModel.getDeviceId());
								deviceTempHumiModel.setTimeMs(tempHumiStartTimeMs+tempHumiIntervalTimeM*60000L*i);
								deviceTempHumiModel.setTemperature(ByteUtil.getFloat_1234(cache, index + 23+i*5));
								deviceTempHumiModel.setHumidity(ByteUtil.getInt8(cache, index + 27+i*5));
								logStr+="\n第"+(i+1)+"组:"+deviceTempHumiModel.getTemperature()+"℃,"+deviceTempHumiModel.getHumidity()+"%";
							}
							//日志记录
							deviceSignboardServer.writeRunLog(DateUtil.getCurrentDateLong()+" [标识牌温湿度数据]\n编号:"+signboardModel.getCode()+"\n采集开始时间："+tempHumiStartTimeStr+"\n数据数目:"+tempHumiDataNum+logStr);
							//上传
							deviceSignboardServer.deviceTempHumiService.uploadHistoryModels(list);
							//温湿度预警检查
							deviceSignboardServer.andonService.checkDeviceSignboardTemperatureAndHumidity(signboardModel, list);
							//更新最新温湿度
							if (list.size()>0) {
								deviceSignboardServer.deviceTempHumiService.updateLastModel(list.get(list.size()-1));
							}
						}else {
							deviceSignboardServer.writeRunLog(DateUtil.getCurrentDateLong()+" [标识牌温湿度数据]\n编号:"+signboardModel.getCode()+"\n采集开始时间："+tempHumiStartTimeStr+"\n数据数目:"+tempHumiDataNum);
						}	
					}
				}	
			}
			/**
			 * 回复报文
			 * */
			int j = 0;
			byte[] telegram = new byte[22];
			telegram[j++] = TELEGRAM_BYTE_BEGIN_N1;
			telegram[j++] = TELEGRAM_BYTE_BEGIN_N2;
			// 编号
			telegram[j++] = cache[index + 2];
			telegram[j++] = cache[index + 3];
			telegram[j++] = cache[index + 4];
			telegram[j++] = cache[index + 5];
			telegram[j++] = cache[index + 6];
			telegram[j++] = cache[index + 7];
			telegram[j++] = cache[index + 8];
			telegram[j++] = cache[index + 9];
			// 控制码
			telegram[j++] = TELEGRAM_BYTE_CONTROL_TEMP_HUMI;
			// 数据长度
			telegram[j++] = (byte) 0x00;
			telegram[j++] = (byte) 0x07;
			// 数据域
			if (checkOk) {
				telegram[j++] = (byte) 0x00;
				// 设备编号是否存在
			} else {
				telegram[j++] = (byte) 0x01;
			}
			telegram[j++] = deviceSignboardServer.paramByte_CalendarYear;
			telegram[j++] = deviceSignboardServer.paramByte_CalendarMonth;
			telegram[j++] = deviceSignboardServer.paramByte_CalendarDayOfMonth;
			telegram[j++] = deviceSignboardServer.paramByte_CalendarHourOfDay;
			telegram[j++] = deviceSignboardServer.paramByte_CalendarMinute;
			telegram[j++] = deviceSignboardServer.paramByte_CalendarSecond;
			// 校验位
			telegram[j++] = ByteUtil.getCheckSumByte(telegram, 0, telegram.length - 2);
			// 停止位
			telegram[j++] = TELEGRAM_BYTE_END;
			// 回复
			socket.getOutputStream().write(telegram);
			deviceSignboardServer.writeRunLog(DateUtil.getCurrentDateLong()+" [发送温湿度数据回复] ["+signboardModel.getCode()+"] "+StringUtil.toHexArrString(telegram));
		}
		return signboardModel;
	}
	
	/**
	 * 解析设备标识牌状态信息
	 * @throws ParseException 
	 */
	public DeviceSignboardModel parseDeviceSignboardStatus(Socket socket, //
			byte[] cache, //
			int index, //
			int dataLength) throws IOException, ParseException {
		// 报文校验
		boolean checkOk = checkTelegram(cache, index, dataLength + 15);
		DeviceSignboardModel signboardModel = null;
		// 对象封装
		if (checkOk) {
			signboardModel = new DeviceSignboardModel();
			//起始,index+0
			signboardModel.setCode(StringUtil.toHexArrString(cache, index + 2, 8, "-").toLowerCase());//标识牌编号
			//控制码,index+10
			//数据长度,index+11
			signboardModel.setPowerVoltage(ByteUtil.getInt16(cache, index + 13) / 1000.0f);//供电电压
			signboardModel.setLocalTime(StringUtil.toDateTimeStr(cache, index + 15));//本地时间
			signboardModel.setStatus(ByteUtil.getInt8(cache, index + 21));//状态
			signboardModel.setSoftVersion(ByteUtil.getInt16(cache, index + 22));//软件版本
			signboardModel.setHardVersion(ByteUtil.getInt16(cache, index + 24));//硬件版本
			ByteUtil.getInt8(cache, index + 26);//最新记录标示
			//上报间隔,index+27
			signboardModel.setImei(StringUtil.toAsciiString(cache, index + 29, 15, ""));//imei
			signboardModel.setImsi(StringUtil.toAsciiString(cache, index + 44, 15, ""));//imsi
			signboardModel.setSigalIntensity(ByteUtil.getInt8(cache, index + 59));//信号强度
			signboardModel.setTemperature(ByteUtil.getFloat_1234(cache, index + 60));//温度
			signboardModel.setHumidity(ByteUtil.getInt8(cache, index + 64));//湿度
			signboardModel.setLastSyncTimeMs(System.currentTimeMillis());
			//更新状态并返回当前数据
			signboardModel=deviceSignboardServer.deviceSignboardService.updateStatusAndReturnNew(signboardModel);
			//日志记录
			deviceSignboardServer.writeRunLog(DateUtil.getCurrentDateLong()+" [标识牌状态变更]\n编号:"+signboardModel.getCode()+"\n状态:"+signboardModel.getStatus()+"\n温湿度:"+signboardModel.getTemperature()+"℃、"+signboardModel.getHumidity()+"%");
			/**
			 * 设备已经绑定
			 * */
			if (signboardModel.getDeviceId()>0) {
				//更新设备状态
				if (signboardModel.getStatusModel()!=null) {
					DeviceModel newStatusModel=new DeviceModel();
					newStatusModel.setId(signboardModel.getDeviceId());
					newStatusModel.setWorkcenterId(signboardModel.getWorkcenterId());
					newStatusModel.setWorkshopId(signboardModel.getWorkshopId());
					newStatusModel.setFactoryId(signboardModel.getFactoryId());
					newStatusModel.setOrganizationId(signboardModel.getOrganizationId());
					newStatusModel.setStatus(signboardModel.getStatusModel().getStatus());
					newStatusModel.setStatusName(signboardModel.getStatusModel().getName());
					newStatusModel.setStatusValue(DeviceStatusModel.getValueByStatus(signboardModel.getStatusModel().getStatus()));
					newStatusModel.setStatusTimeMs(System.currentTimeMillis());
					deviceSignboardServer.deviceService.setSignboardDeviceStatus(newStatusModel);	
				}
				//检查供电电压
				deviceSignboardServer.andonService.checkDeviceSignboardPowerVoltage(signboardModel);
				//检查温湿度
				//deviceSignboardServer.andonService.checkDeviceSignboardTemperatureAndHumidity(signboardModel);
				//更新当前温湿度
				DeviceTempHumiModel tempHumiModel=new DeviceTempHumiModel();
				tempHumiModel.setDeviceId(signboardModel.getDeviceId());
				tempHumiModel.setWorkcenterId(signboardModel.getWorkcenterId());
				tempHumiModel.setWorkshopId(signboardModel.getWorkshopId());
				tempHumiModel.setOrganizationId(signboardModel.getOrganizationId());
				tempHumiModel.setTimeMs(System.currentTimeMillis());
				tempHumiModel.setTemperature(signboardModel.getTemperature());
				tempHumiModel.setHumidity(signboardModel.getHumidity());
				deviceSignboardServer.deviceTempHumiService.updateLastModel(tempHumiModel);
			}
		}
		/**
		 * 回复报文
		 * */
		int j = 0;
		byte[] telegram = new byte[22];
		telegram[j++] = TELEGRAM_BYTE_BEGIN_N1;
		telegram[j++] = TELEGRAM_BYTE_BEGIN_N2;
		// 编号
		telegram[j++] = cache[index + 2];
		telegram[j++] = cache[index + 3];
		telegram[j++] = cache[index + 4];
		telegram[j++] = cache[index + 5];
		telegram[j++] = cache[index + 6];
		telegram[j++] = cache[index + 7];
		telegram[j++] = cache[index + 8];
		telegram[j++] = cache[index + 9];
		// 控制码
		telegram[j++] = TELEGRAM_BYTE_CONTROL_STATUS;
		// 数据长度
		telegram[j++] = (byte) 0x00;
		telegram[j++] = (byte) 0x07;
		// 数据域
		if (checkOk) {
			telegram[j++] = (byte) 0x00;
			// 设备编号是否存在
		} else {
			telegram[j++] = (byte) 0x01;
		}
		telegram[j++] = deviceSignboardServer.paramByte_CalendarYear;
		telegram[j++] = deviceSignboardServer.paramByte_CalendarMonth;
		telegram[j++] = deviceSignboardServer.paramByte_CalendarDayOfMonth;
		telegram[j++] = deviceSignboardServer.paramByte_CalendarHourOfDay;
		telegram[j++] = deviceSignboardServer.paramByte_CalendarMinute;
		telegram[j++] = deviceSignboardServer.paramByte_CalendarSecond;
		// 校验位
		telegram[j++] = ByteUtil.getCheckSumByte(telegram, 0, telegram.length - 2);
		// 停止位
		telegram[j++] = TELEGRAM_BYTE_END;
		// 回复
		socket.getOutputStream().write(telegram);
		deviceSignboardServer.writeRunLog(DateUtil.getCurrentDateLong()+" [发送状态变更回复] ["+signboardModel.getCode()+"] "+StringUtil.toHexArrString(telegram));
		return signboardModel;
	}

	/**
	 * 获取设备标识牌异常状态信息
	 */
	public void getDeviceSignboardErrorStatus(Socket socket, //
			byte[] cache, //
			int index, //
			int dataLength) throws IOException {
		// 报文校验
		boolean checkOk = checkTelegram(cache, index, dataLength + 13);
		// 对象封装
		DeviceSignboardModel signboardModel;
		if (checkOk) {
			signboardModel = new DeviceSignboardModel();
			signboardModel.setCode(StringUtil.toHexArrString(cache, index + 2, 6, "-").toLowerCase());
			signboardModel.setErrorType(ByteUtil.getInt8(cache, index + 12));
			//日志记录
			deviceSignboardServer.writeRunLog(DateUtil.getCurrentDateLong()+" [标识牌异常状态变更]\n编号:"+signboardModel.getCode()+"\n异常:"+signboardModel.getErrorType());
			//更新
			signboardModel=deviceSignboardServer.deviceSignboardService.updateErrStatusAndReturnNew(signboardModel);
		}else {
			return;
		}
		/**
		 * 报文
		 * */
		int j = 0;
		byte[] telegram = new byte[16];
		telegram[j++] = TELEGRAM_BYTE_BEGIN_N1;
		telegram[j++] = TELEGRAM_BYTE_BEGIN_N2;
		// 编号
		telegram[j++] = cache[index + 2];
		telegram[j++] = cache[index + 3];
		telegram[j++] = cache[index + 4];
		telegram[j++] = cache[index + 5];
		telegram[j++] = cache[index + 6];
		telegram[j++] = cache[index + 7];
		telegram[j++] = cache[index + 8];
		telegram[j++] = cache[index + 9];
		// 控制码
		telegram[j++] = TELEGRAM_BYTE_CONTROL_ERROR;
		// 数据长度
		telegram[j++] = (byte) 0x00;
		telegram[j++] = (byte) 0x01;
		// 数据域
		if (checkOk) {
			telegram[j++] = (byte) 0x00;
			// 设备编号是否存在
		} else {
			telegram[j++] = (byte) 0x01;
		}
		// 校验位
		telegram[j++] = ByteUtil.getCheckSumByte(telegram, 0, telegram.length - 2);
		// 停止位
		telegram[j++] = TELEGRAM_BYTE_END;
		// 回复
		socket.getOutputStream().write(telegram);
		deviceSignboardServer.writeRunLog(DateUtil.getCurrentDateLong()+" [发送异常状态回复] ["+signboardModel.getCode()+"] "+StringUtil.toHexArrString(telegram));
	}
	/**
	 * 更改设备标识牌配置
	 * */
	public void setDeviceSignboardConfig(Socket socket, //
			byte[] cache, //
			int index,//
			DeviceSignboardModel deviceModel) throws IOException {
		/**
		 * 报文
		 * */
		int j = 0;
		byte[] telegram = new byte[51];
		telegram[j++] = TELEGRAM_BYTE_BEGIN_N1;
		telegram[j++] = TELEGRAM_BYTE_BEGIN_N2;
		// 编号
		telegram[j++] = cache[index + 2];
		telegram[j++] = cache[index + 3];
		telegram[j++] = cache[index + 4];
		telegram[j++] = cache[index + 5];
		telegram[j++] = cache[index + 6];
		telegram[j++] = cache[index + 7];
		telegram[j++] = cache[index + 8];
		telegram[j++] = cache[index + 9];
		// 控制码
		telegram[j++] = TELEGRAM_BYTE_CONTROL_CONFIG;
		// 数据长度
		telegram[j++] = (byte) 0x00;
		telegram[j++] = (byte) 0x24;
		// 数据域
		// 数据上传间隔
		telegram[j++] = (byte) (deviceModel.getUploadIntervalTime()>>8);
		telegram[j++] = (byte) deviceModel.getUploadIntervalTime();
		// 数据采集间隔
		telegram[j++] = (byte) (deviceModel.getCollectIntervalTime()>>8);
		telegram[j++] = (byte) deviceModel.getCollectIntervalTime();
		// 安全温度上限
		byte[] floatBytes=ByteUtil.getFloatBytesArr_1234(deviceModel.getTemperatureMaxVal());
		telegram[j++]=floatBytes[0];
		telegram[j++]=floatBytes[1];
		telegram[j++]=floatBytes[2];
		telegram[j++]=floatBytes[3];
		// 安全温度下限
		floatBytes=ByteUtil.getFloatBytesArr_1234(deviceModel.getTemperatureMinVal());
		telegram[j++]=floatBytes[0];
		telegram[j++]=floatBytes[1];
		telegram[j++]=floatBytes[2];
		telegram[j++]=floatBytes[3];
		// 安全湿度上限
		telegram[j++] = (byte) deviceModel.getHumidityMaxVal();
		// 安全湿度下限
		telegram[j++] = (byte) deviceModel.getHumidityMinVal();
		// 网络时间
		telegram[j++] = deviceSignboardServer.paramByte_CalendarYear;
		telegram[j++] = deviceSignboardServer.paramByte_CalendarMonth;
		telegram[j++] = deviceSignboardServer.paramByte_CalendarDayOfMonth;
		telegram[j++] = deviceSignboardServer.paramByte_CalendarHourOfDay;
		telegram[j++] = deviceSignboardServer.paramByte_CalendarMinute;
		telegram[j++] = deviceSignboardServer.paramByte_CalendarSecond;
		// 服务器IP
		telegram[j++] = deviceSignboardServer.paramByte_ServerIP_N1;
		telegram[j++] = deviceSignboardServer.paramByte_ServerIP_N2;
		telegram[j++] = deviceSignboardServer.paramByte_ServerIP_N3;
		telegram[j++] = deviceSignboardServer.paramByte_ServerIP_N4;
		// 服务器端口号
		telegram[j++] = deviceSignboardServer.paramByte_ServerPort_N1;
		telegram[j++] = deviceSignboardServer.paramByte_ServerPort_N2;
		for (int i = 0; i < 10; i++) {
			telegram[j++] = (byte) 0xff;
		}
		// 校验位
		telegram[j++] = ByteUtil.getCheckSumByte(telegram, 0, telegram.length - 2);
		// 停止位
		telegram[j++] = TELEGRAM_BYTE_END;
		// 回复
		socket.getOutputStream().write(telegram);
		deviceSignboardServer.writeRunLog(DateUtil.getCurrentDateLong()+" [更新标识牌配置] "+StringUtil.toHexArrString(telegram)+"\n编号:"+deviceModel.getCode()+"\n采集间隔:"+deviceModel.getCollectIntervalTime()+"分钟\n上报间隔:"+deviceModel.getUploadIntervalTime()+"分钟\n温湿度阈值:"+deviceModel.getHumidityMinVal()+"℃,"+deviceModel.getHumidityMaxVal()+"℃");
	}
	
	public final static byte TELEGRAM_BYTE_BEGIN_N1=(byte) 0x5A;
	public final static byte TELEGRAM_BYTE_BEGIN_N2=(byte) 0xA5;
	public final static byte TELEGRAM_BYTE_CONTROL_STATUS=(byte) 0x10;
	public final static byte TELEGRAM_BYTE_CONTROL_ERROR=(byte) 0x11;
	public final static byte TELEGRAM_BYTE_CONTROL_CONFIG=(byte) 0x12;
	public final static byte TELEGRAM_BYTE_CONTROL_TEMP_HUMI=(byte) 0x13;
	public final static byte TELEGRAM_BYTE_END=(byte) 0x16;
	
}
