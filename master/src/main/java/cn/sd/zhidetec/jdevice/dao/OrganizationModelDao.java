package cn.sd.zhidetec.jdevice.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import cn.sd.zhidetec.jdevice.Starter;
import cn.sd.zhidetec.jdevice.config.DataSourceConfiger;
import cn.sd.zhidetec.jdevice.model.OrganizationModel;
import cn.sd.zhidetec.jdevice.util.StringUtil;

@Service
public class OrganizationModelDao {

	@Autowired
	@Qualifier(DataSourceConfiger.BEAN_NAME_JdeviceJdbcTemplate)
	protected JdbcTemplate jdbcTemplate;

	/**
	 * 设置组织LOGO
	 * 
	 * @param organizationModel 组织信息
	 * @return 操作是否成功
	 */
	public boolean setLogoUrlById(String id, String logoUrl) {
		int i = 0;
		Object[] args = new Object[2];
		// 修改信息
		if (id != null//
				&& !id.trim().equals("")//
				&& getOrganizationModel(id) != null) {
			args[i++] = StringUtil.getEmptyString(logoUrl);
			args[i++] = id;
			i = jdbcTemplate.update(Sql_UpdateLogo_By_Id, args);
		} else {
			return false;
		}
		if (i > 0) {
			return true;
		}
		return false;
	}

	private static String Sql_UpdateLogo_By_Id = "UPDATE jdevice_c_organization" + //
			" SET " + //
			"logo_url = ?" + //
			" WHERE " + //
			"id=?";

	/**
	 * 设置组织信息，如果不存在则新建
	 * 
	 * @param organizationModel 组织信息
	 * @return 操作是否成功
	 */
	public boolean setOrganizationModel(OrganizationModel organizationModel) {
		int i = 0;
		Object[] args = new Object[6];
		// 修改信息
		if (organizationModel.getId() != null//
				&& !organizationModel.getId().trim().equals("")//
				&& getOrganizationModel(organizationModel.getId()) != null) {
			args[i++] = StringUtil.getEmptyString(organizationModel.getName());
			args[i++] = StringUtil.getEmptyString(organizationModel.getPhone());
			args[i++] = StringUtil.getEmptyString(organizationModel.getLogoUrl());
			args[i++] = StringUtil.getEmptyString(organizationModel.getCity());
			args[i++] = StringUtil.getEmptyString(organizationModel.getTrade());
			args[i++] = StringUtil.getEmptyString(organizationModel.getId());
			i = jdbcTemplate.update(Sql_UpdateModel_By_Id, args);
		}
		// 新建信息
		else {
			organizationModel.setId(StringUtil.getUIdByTime(Starter.ServerCode));
			args[i++] = organizationModel.getId();
			args[i++] = StringUtil.getEmptyString(organizationModel.getName());
			args[i++] = StringUtil.getEmptyString(organizationModel.getPhone());
			args[i++] = StringUtil.getEmptyString(organizationModel.getLogoUrl());
			args[i++] = StringUtil.getEmptyString(organizationModel.getCity());
			args[i++] = StringUtil.getEmptyString(organizationModel.getTrade());
			i = jdbcTemplate.update(Sql_InsertModel, args);
		}
		if (i > 0) {
			return true;
		}
		return false;
	}

	private static String Sql_UpdateModel_By_Id = "UPDATE jdevice_c_organization" + //
			" SET " + //
			"name = ?," + //
			"phone = ?," + //
			"logo_url = ?," + //
			"city = ?," + //
			"trade = ?" + //
			" WHERE " + //
			"id=?";
	private static String Sql_InsertModel = "INSERT INTO jdevice_c_organization (" + //
			"id," + //
			"name," + //
			"phone," + //
			"logo_url," + //
			"city," + //
			"trade" + //
			")VALUES(" + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?" + //
			")";

	/**
	 * 根据组织ID获取组织信息
	 * 
	 * @param id 组织ID
	 * @return 组织信息
	 */
	public OrganizationModel getOrganizationModel(String id) {
		if (StringUtil.isEmpty(id)) {
			return null;
		}
		Object[] args = new Object[1];
		args[0] = id;
		List<OrganizationModel> list = jdbcTemplate.query(Sql_GetModels_By_Id, args, beanPropertyRowMapper);
		if (list.size() > 0) {
			return list.get(0);
		}
		return null;
	}

	private static String Sql_GetModels_By_Id = "SELECT * FROM jdevice_c_organization WHERE id = ?";
	private BeanPropertyRowMapper<OrganizationModel> beanPropertyRowMapper = new BeanPropertyRowMapper<OrganizationModel>(
			OrganizationModel.class);

}
