package cn.sd.zhidetec.jdevice.config;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
@Configuration
public class UploadConfiger extends WebMvcConfigurerAdapter {
	
	protected Logger logger = LoggerFactory.getLogger(getClass());
	
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
    	String fileUri = getAbsoluteAppDir();
    	logger.info(getClass().getSimpleName()+" Get FileUrl : "+fileUri);
    	RESOURCE_LOCATION_UPLOAD = "file:";
    	//获取操作系统类型
    	String osName = System.getProperty("os.name").toLowerCase();
    	//Windows环境下
    	if (osName.startsWith("windows")) {
    		RESOURCE_LOCATION_UPLOAD+="//";
        	if (fileUri.trim().startsWith("file:")) {
    			fileUri=fileUri.trim().replaceFirst("file:", "");
    		}
        	RESOURCE_LOCATION_UPLOAD+=fileUri+"/"+DIR_NAME_UPLOAD+"/";
        	PATH_DIR_UPLOAD=fileUri.replaceFirst("/", "");
        	PATH_DIR_UPLOAD=fileUri.replaceAll("/", "\\\\");
            PATH_DIR_UPLOAD+=File.separator+DIR_NAME_UPLOAD+File.separator;
            RESOURCE_HANDLER_UPLOAD="/"+DIR_NAME_UPLOAD+"/";
		}
    	//Linux环境下
    	else {
	    	if (fileUri.trim().startsWith("file:")) {
				fileUri=fileUri.trim().replaceFirst("file:", "");
			}
	    	RESOURCE_LOCATION_UPLOAD+=fileUri+"/"+DIR_NAME_UPLOAD+"/";
	    	PATH_DIR_UPLOAD=fileUri;
	        PATH_DIR_UPLOAD+=File.separator+DIR_NAME_UPLOAD+File.separator;
	        RESOURCE_HANDLER_UPLOAD="/"+DIR_NAME_UPLOAD+"/";
		}
    	//添加静态资源映射
    	logger.info(getClass().getSimpleName()+" Add Resource Handlers : "+RESOURCE_HANDLER_UPLOAD);
    	logger.info(getClass().getSimpleName()+" Add Resource Handlers File Path : "+RESOURCE_LOCATION_UPLOAD);
    	logger.info(getClass().getSimpleName()+" Upload File Path : "+PATH_DIR_UPLOAD);
        registry.addResourceHandler(RESOURCE_HANDLER_UPLOAD+"**").addResourceLocations(RESOURCE_LOCATION_UPLOAD);
    }
    
    public final static String DIR_NAME_UPLOAD="upload";    
    public static String PATH_DIR_UPLOAD="";
    public static String RESOURCE_HANDLER_UPLOAD="";
    public static String RESOURCE_LOCATION_UPLOAD="";
    
    public String getAbsoluteAppDir()
	{
		String path = this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
		if(System.getProperty("os.name").contains("dows"))
		{
			path = path.substring(0,path.length());
		}
		if(path.contains("jar"))
		{
			path = path.substring(0,path.lastIndexOf("."));
			return path.substring(0,path.lastIndexOf("/"));
		}
		return path.replace("target/classes/", "");
	}
}