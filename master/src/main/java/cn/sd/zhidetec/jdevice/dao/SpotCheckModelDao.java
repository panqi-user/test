package cn.sd.zhidetec.jdevice.dao;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import cn.sd.zhidetec.jdevice.Starter;
import cn.sd.zhidetec.jdevice.config.DataSourceConfiger;
import cn.sd.zhidetec.jdevice.model.SpotCheckItemModel;
import cn.sd.zhidetec.jdevice.model.SpotCheckPlanConfigModel;
import cn.sd.zhidetec.jdevice.model.SpotCheckPlanModel;
import cn.sd.zhidetec.jdevice.util.StringUtil;

@Service
public class SpotCheckModelDao {

	@Autowired
	@Qualifier(DataSourceConfiger.BEAN_NAME_JdeviceJdbcTemplate)
	protected JdbcTemplate jdbcTemplate;
	protected Logger logger = LoggerFactory.getLogger(getClass());
	private BeanPropertyRowMapper<SpotCheckPlanModel> planBeanRowMapper = new BeanPropertyRowMapper<SpotCheckPlanModel>(
			SpotCheckPlanModel.class);
	private BeanPropertyRowMapper<SpotCheckItemModel> itemBeanRowMapper = new BeanPropertyRowMapper<SpotCheckItemModel>(
			SpotCheckItemModel.class);
	
	public List<SpotCheckPlanModel> getWorkcenterCheckPlans(int status,//
			long workcenterId,//
			long workshopId,//
			long factoryId,//
			String organizationId,//
			int beginIndex,//
			int dataNum,//
			long beginTimeMs,//
			long endTimeMs,//
			long filterFactoryId,//
			long filterWorkshopId,//
			long filterWorkcenterId,//
			long filterDeviceId,//
			long filterExecutorId) {
		Object[] args=new Object[26];
		int i=0;
		args[i++]=beginTimeMs;
		args[i++]=endTimeMs;
		args[i++]=filterFactoryId;
		args[i++]=filterFactoryId;
		args[i++]=filterWorkshopId;
		args[i++]=filterWorkshopId;
		args[i++]=filterWorkcenterId;
		args[i++]=filterWorkcenterId;
		args[i++]=filterDeviceId;
		args[i++]=filterDeviceId;
		args[i++]=filterExecutorId;
		args[i++]=filterExecutorId;
		args[i++]=status;
		args[i++]=workcenterId;
		args[i++]=workshopId;
		args[i++]=factoryId;
		args[i++]=organizationId;
		args[i++]=status;
		args[i++]=workshopId;
		args[i++]=factoryId;
		args[i++]=organizationId;
		args[i++]=status;
		args[i++]=factoryId;
		args[i++]=organizationId;
		args[i++]=beginIndex;
		args[i++]=dataNum;
		return jdbcTemplate.query(SQL_GetCheckPlans_By_Workcenter, args, planBeanRowMapper);
	}
	public final static String SQL_GetCheckPlans_By_Workcenter="SELECT "+//
			" * "+//
			"FROM jdevice_h_check_plan "+//
			"WHERE "+//
			" trigger_time_ms>=? "+//
			" AND trigger_time_ms<=? "+//
			" AND ((?<=0 OR factory_id=?) AND (?<=0 OR workshop_id=?) AND (?<=0 OR workcenter_id=?) AND (?<=0 OR device_id=?) AND (?<=0 OR executor_id=? ))"+//
			" AND (( "+//
			"  type = "+SpotCheckPlanConfigModel.TYPE_Device+//
			"  AND status=? "+//
			"  AND workcenter_id =? "+//
			"  AND workshop_id =? "+//
			"  AND factory_id =? "+//
			"  AND organization_id =? "+//
			" ) "+//
			"OR ( "+//
			"  type = "+SpotCheckPlanConfigModel.TYPE_Workshop+//
			"  AND status=? "+//
			"  AND workshop_id =? "+//
			"  AND factory_id =? "+//
			"  AND organization_id =? "+//
			") "+//
			"OR ( "+//
			"  type = "+SpotCheckPlanConfigModel.TYPE_Factory+//
			"  AND status=? "+//
			"  AND factory_id =? "+//
			"  AND organization_id =? "+//
			")) ORDER BY trigger_time_ms DESC LIMIT ?,?;";
	
	public List<SpotCheckPlanModel> getWorkshopCheckPlans(int status,//
			long workshopId,//
			long factoryId,//
			String organizationId,//
			int beginIndex,//
			int dataNum,//
			long beginTimeMs,//
			long endTimeMs,//
			long filterFactoryId,//
			long filterWorkshopId,//
			long filterWorkcenterId,//
			long filterDeviceId,//
			long filterExecutorId) {
		Object[] args=new Object[25];
		int i=0;
		args[i++]=beginTimeMs;
		args[i++]=endTimeMs;
		args[i++]=filterFactoryId;
		args[i++]=filterFactoryId;
		args[i++]=filterWorkshopId;
		args[i++]=filterWorkshopId;
		args[i++]=filterWorkcenterId;
		args[i++]=filterWorkcenterId;
		args[i++]=filterDeviceId;
		args[i++]=filterDeviceId;
		args[i++]=filterExecutorId;
		args[i++]=filterExecutorId;
		args[i++]=status;
		args[i++]=workshopId;
		args[i++]=factoryId;
		args[i++]=organizationId;
		args[i++]=status;
		args[i++]=workshopId;
		args[i++]=factoryId;
		args[i++]=organizationId;
		args[i++]=status;
		args[i++]=factoryId;
		args[i++]=organizationId;
		args[i++]=beginIndex;
		args[i++]=dataNum;
		return jdbcTemplate.query(SQL_GetCheckPlans_By_Workshop, args, planBeanRowMapper);
	}
	public final static String SQL_GetCheckPlans_By_Workshop="SELECT "+//
			" * "+//
			"FROM jdevice_h_check_plan "+//
			"WHERE "+//
			" trigger_time_ms>=? "+//
			" AND trigger_time_ms<=? "+//
			" AND ((?<=0 OR factory_id=?) AND (?<=0 OR workshop_id=?) AND (?<=0 OR workcenter_id=?) AND (?<=0 OR device_id=?) AND (?<=0 OR executor_id=? ))"+//
			" AND (( "+//
			"  type = "+SpotCheckPlanConfigModel.TYPE_Device+//
			"  AND status=? "+//
			"  AND workshop_id =? "+//
			"  AND factory_id =? "+//
			"  AND organization_id =? "+//
			" ) "+//
			"OR ( "+//
			"  type = "+SpotCheckPlanConfigModel.TYPE_Workshop+//
			"  AND status=? "+//
			"  AND workshop_id =? "+//
			"  AND factory_id =? "+//
			"  AND organization_id =? "+//
			") "+//
			"OR ( "+//
			"  type = "+SpotCheckPlanConfigModel.TYPE_Factory+//
			"  AND status=? "+//
			"  AND factory_id =? "+//
			"  AND organization_id =? "+//
			")) ORDER BY trigger_time_ms DESC LIMIT ?,?;";
	
	public List<SpotCheckPlanModel> getFactoryCheckPlans(int status,//
			long factoryId,//
			String organizationId,//
			int beginIndex,//
			int dataNum,//
			long beginTimeMs,//
			long endTimeMs,//
			long filterFactoryId,//
			long filterWorkshopId,//
			long filterWorkcenterId,//
			long filterDeviceId,//
			long filterExecutorId) {
		Object[] args=new Object[17];
		int i=0;
		args[i++]=beginTimeMs;
		args[i++]=endTimeMs;
		args[i++]=filterFactoryId;
		args[i++]=filterFactoryId;
		args[i++]=filterWorkshopId;
		args[i++]=filterWorkshopId;
		args[i++]=filterWorkcenterId;
		args[i++]=filterWorkcenterId;
		args[i++]=filterDeviceId;
		args[i++]=filterDeviceId;
		args[i++]=filterExecutorId;
		args[i++]=filterExecutorId;
		args[i++]=factoryId;
		args[i++]=status;
		args[i++]=organizationId;
		args[i++]=beginIndex;
		args[i++]=dataNum;
		return jdbcTemplate.query(SQL_GetCheckPlans_By_Factory, args, planBeanRowMapper);
	}
	public final static String SQL_GetCheckPlans_By_Factory="SELECT "+//
			" * "+//
			"FROM jdevice_h_check_plan "+//
			"WHERE "+//
			" trigger_time_ms>=? "+//
			"  AND trigger_time_ms<=? "+//
			"  AND ((?<=0 OR factory_id=?) AND (?<=0 OR workshop_id=?) AND (?<=0 OR workcenter_id=?) AND (?<=0 OR device_id=?) AND (?<=0 OR executor_id=? ))"+//
			"  AND factory_id=? "+//
			"  AND status=? "+//
			"  AND organization_id =? ORDER BY trigger_time_ms DESC LIMIT ?,?;";
	
	public List<SpotCheckPlanModel> getOrganizationCheckPlans(int status,//
			String organizationId,//
			int beginIndex,//
			int dataNum,//
			long beginTimeMs,//
			long endTimeMs,//
			long filterFactoryId,//
			long filterWorkshopId,//
			long filterWorkcenterId,//
			long filterDeviceId,//
			long filterExecutorId) {
		Object[] args=new Object[16];
		int i=0;
		args[i++]=beginTimeMs;
		args[i++]=endTimeMs;
		args[i++]=filterFactoryId;
		args[i++]=filterFactoryId;
		args[i++]=filterWorkshopId;
		args[i++]=filterWorkshopId;
		args[i++]=filterWorkcenterId;
		args[i++]=filterWorkcenterId;
		args[i++]=filterDeviceId;
		args[i++]=filterDeviceId;
		args[i++]=filterExecutorId;
		args[i++]=filterExecutorId;
		args[i++]=status;
		args[i++]=organizationId;
		args[i++]=beginIndex;
		args[i++]=dataNum;
		return jdbcTemplate.query(SQL_GetCheckPlans_By_Organization, args, planBeanRowMapper);
	}
	public final static String SQL_GetCheckPlans_By_Organization="SELECT "+//
			" * "+//
			"FROM jdevice_h_check_plan "+//
			"WHERE "+//
			" trigger_time_ms>=? "+//
			"  AND trigger_time_ms<=? "+//
			"  AND ((?<=0 OR factory_id=?) AND (?<=0 OR workshop_id=?) AND (?<=0 OR workcenter_id=?) AND (?<=0 OR device_id=?) AND (?<=0 OR executor_id=? ))"+//
			"  AND status=? "+//
			"  AND organization_id =? ORDER BY trigger_time_ms DESC LIMIT ?,?;";
	
	public boolean setCheckPlan(SpotCheckPlanModel model) {
		if (model==null) {
			return false;
		}
		//新增
		if (model.getId()<=0) {
			Object[] args = new Object[22];
			int i=0;
			model.setId(Starter.IdMaker.nextId());
			args[i++]=model.getId();
			args[i++]=StringUtil.getEmptyString(model.getName());
			args[i++]=model.getType();
			args[i++]=model.getStatus();
			args[i++]=model.getTriggerIntervalTimeM();
			args[i++]=model.getTriggerLastTimeMs();
			args[i++]=model.getTriggerTimeMs();
			args[i++]=model.getPlanId();
			args[i++]=model.getDeviceId();
			args[i++]=model.getWorkcenterId();
			args[i++]=model.getWorkshopId();
			args[i++]=model.getFactoryId();
			args[i++]=StringUtil.getEmptyString(model.getOrganizationId());
			args[i++]=model.getExecutorId();
			args[i++]=StringUtil.getEmptyString(model.getExecutorName());
			args[i++]=model.getStartTimeMs();
			args[i++]=model.getResultSuccess();
			args[i++]=model.getFinishTimeMs();
			args[i++]=StringUtil.getEmptyString(model.getResultDisc());
			args[i++]=model.getCreateTimeMs();
			args[i++]=model.getCreaterId();
			args[i++]=StringUtil.getEmptyString(model.getCreaterName());
			return jdbcTemplate.update(SQL_InsertCheckPlan, args)>0;
		}
		//修改
		else {
			Object[] args = new Object[22];
			int i=0;
			args[i++]=StringUtil.getEmptyString(model.getName());
			args[i++]=model.getType();
			args[i++]=model.getStatus();
			args[i++]=model.getTriggerIntervalTimeM();
			args[i++]=model.getTriggerLastTimeMs();
			args[i++]=model.getTriggerTimeMs();
			args[i++]=model.getPlanId();
			args[i++]=model.getDeviceId();
			args[i++]=model.getWorkcenterId();
			args[i++]=model.getWorkshopId();
			args[i++]=model.getFactoryId();
			args[i++]=StringUtil.getEmptyString(model.getOrganizationId());
			args[i++]=model.getExecutorId();
			args[i++]=StringUtil.getEmptyString(model.getExecutorName());
			args[i++]=model.getStartTimeMs();
			args[i++]=model.getResultSuccess();
			args[i++]=model.getFinishTimeMs();
			args[i++]=StringUtil.getEmptyString(model.getResultDisc());
			args[i++]=model.getCreateTimeMs();
			args[i++]=model.getCreaterId();
			args[i++]=StringUtil.getEmptyString(model.getCreaterName());
			args[i++]=model.getId();
			return jdbcTemplate.update(SQL_UpdateCheckPlan, args)>0;
		}
	}
	public final static String SQL_InsertCheckPlan="INSERT INTO jdevice_h_check_plan ( "+//
			" id, "+//
			" name, "+//
			" type, "+//
			" status, "+//
			" trigger_interval_time_m, "+//
			" trigger_last_time_ms, "+//
			" trigger_time_ms, "+//
			" plan_id, "+//
			" device_id, "+//
			" workcenter_id, "+//
			" workshop_id, "+//
			" factory_id, "+//
			" organization_id, "+//
			" executor_id, "+//
			" executor_name, "+//
			" start_time_ms, "+//
			" result_success, "+//
			" finish_time_ms, "+//
			" result_disc, "+//
			" create_time_ms, "+//
			" creater_id, "+//
			" creater_name "+//
			") "+//
			"VALUES "+//
			" ( "+//
			" ?, "+//
			" ?, "+//
			" ?, "+//
			" ?, "+//
			" ?, "+//
			" ?, "+//
			" ?, "+//
			" ?, "+//
			" ?, "+//
			" ?, "+//
			" ?, "+//
			" ?, "+//
			" ?, "+//
			" ?, "+//
			" ?, "+//
			" ?, "+//
			" ?, "+//
			" ?, "+//
			" ?, "+//
			" ?, "+//
			" ?, "+//
			" ? "+//
			" );";
	public final static String SQL_UpdateCheckPlan="UPDATE jdevice_h_check_plan SET "+//
			" name = ?, "+//
			" type = ?, "+//
			" status = ?, "+//
			" trigger_interval_time_m = ?, "+//
			" trigger_last_time_ms = ?, "+//
			" trigger_time_ms = ?, "+//
			" plan_id = ?, "+//
			" device_id = ?, "+//
			" workcenter_id = ?, "+//
			" workshop_id = ?, "+//
			" factory_id = ?, "+//
			" organization_id = ?, "+//
			" executor_id = ?, "+//
			" executor_name = ?, "+//
			" start_time_ms = ?, "+//
			" result_success = ?, "+//
			" finish_time_ms = ?, "+//
			" result_disc = ?, "+//
			" create_time_ms = ?, "+//
			" creater_id = ?, "+//
			" creater_name = ? "+//
			"WHERE "+//
			" id=?";
	
	public SpotCheckPlanModel getCheckPlanById(long id,String organizationId) {
		if (id<=0||StringUtil.isEmpty(organizationId)) {
			return null;
		}
		Object[] args=new Object[2];
		int i=0;
		args[i++]=id;
		args[i++]=organizationId;
		List<SpotCheckPlanModel> models = jdbcTemplate.query(SQL_GetCheckPlan_By_Id, args, planBeanRowMapper);
		if (models.size()>0) {
			return models.get(0);
		}
		return null;
	}
	public final static String SQL_GetCheckPlan_By_Id="SELECT * FROM jdevice_h_check_plan WHERE id=? AND organization_id=?";
	
	public List<SpotCheckItemModel> getCheckItemsByPlanId(long id) {
		if (id<=0) {
			return null;
		}
		Object[] args=new Object[1];
		int i=0;
		args[i++]=id;
		return jdbcTemplate.query(SQL_GetCheckItems_By_PlanId, args, itemBeanRowMapper);
	}
	public final static String SQL_GetCheckItems_By_PlanId="SELECT * FROM jdevice_h_check_item WHERE h_plan_id=?";

	public boolean newCheckItemIntances(List<SpotCheckItemModel> models) {
		if (models==null||models.size()<=0) {
			return true;
		}
		List<Object[]> batchArgs=new ArrayList<Object[]>();
		for (SpotCheckItemModel model : models) {
			Object[] args = new Object[16];
			int i=0;
			model.setId(Starter.IdMaker.nextId());
			args[i++]=model.getId();
			args[i++]=StringUtil.getEmptyString(model.getName());
			args[i++]=model.getStatus();
			args[i++]=model.getcPlanId();
			args[i++]=model.getcItemId();
			args[i++]=model.gethPlanId();
			args[i++]=model.getType();
			args[i++]=model.getValueBool();
			args[i++]=StringUtil.getEmptyString(model.getValueBoolTrueName());
			args[i++]=StringUtil.getEmptyString(model.getValueBoolFalseName());
			args[i++]=model.getValueNumber();
			args[i++]=model.getValueNumberMin();
			args[i++]=model.getValueNumberMax();
			args[i++]=StringUtil.getEmptyString(model.getResultDisc());
			args[i++]=model.getResultSuccess();
			args[i++]=model.getResultTimeMs();
			batchArgs.add(args);
		}
		return jdbcTemplate.batchUpdate(SQL_InsertCheckItem, batchArgs)!=null;
	}
	
	public boolean setCheckItem(SpotCheckItemModel model) {
		if (model==null) {
			return false;
		}
		//新增
		if (model.getId()<=0) {
			Object[] args = new Object[16];
			int i=0;
			model.setId(Starter.IdMaker.nextId());
			args[i++]=model.getId();
			args[i++]=StringUtil.getEmptyString(model.getName());
			args[i++]=model.getStatus();
			args[i++]=model.getcPlanId();
			args[i++]=model.getcItemId();
			args[i++]=model.gethPlanId();
			args[i++]=model.getType();
			args[i++]=model.getValueBool();
			args[i++]=StringUtil.getEmptyString(model.getValueBoolTrueName());
			args[i++]=StringUtil.getEmptyString(model.getValueBoolFalseName());
			args[i++]=model.getValueNumber();
			args[i++]=model.getValueNumberMin();
			args[i++]=model.getValueNumberMax();
			args[i++]=model.getResultDisc();
			args[i++]=model.getResultSuccess();
			args[i++]=model.getResultTimeMs();
			return jdbcTemplate.update(SQL_InsertCheckItem, args)>0;
		}
		//修改
		else {
			Object[] args = new Object[16];
			int i=0;
			args[i++]=StringUtil.getEmptyString(model.getName());
			args[i++]=model.getStatus();
			args[i++]=model.getcPlanId();
			args[i++]=model.getcItemId();
			args[i++]=model.gethPlanId();
			args[i++]=model.getType();
			args[i++]=model.getValueBool();
			args[i++]=StringUtil.getEmptyString(model.getValueBoolTrueName());
			args[i++]=StringUtil.getEmptyString(model.getValueBoolFalseName());
			args[i++]=model.getValueNumber();
			args[i++]=model.getValueNumberMin();
			args[i++]=model.getValueNumberMax();
			args[i++]=model.getResultDisc();
			args[i++]=model.getResultSuccess();
			args[i++]=model.getResultTimeMs();
			args[i++]=model.getId();
			return jdbcTemplate.update(SQL_UpdateCheckItem_By_Id, args)>0;
		}
	}
	
	public final static String SQL_InsertCheckItem="INSERT INTO jdevice_h_check_item ( "+//
			" id, "+//
			" name, "+//
			" status, "+//
			" c_plan_id, "+//
			" c_item_id, "+//
			" h_plan_id, "+//
			" type, "+//
			" value_bool, "+//
			" value_bool_true_name, "+//
			" value_bool_false_name, "+//
			" value_number, "+//
			" value_number_min, "+//
			" value_number_max, "+//
			" result_disc, "+//
			" result_success, "+//
			" result_time_ms "+//
			" )VALUES( "+//
			" ?, "+//
			" ?, "+//
			" ?, "+//
			" ?, "+//
			" ?, "+//
			" ?, "+//
			" ?, "+//
			" ?, "+//
			" ?, "+//
			" ?, "+//
			" ?, "+//
			" ?, "+//
			" ?, "+//
			" ?, "+//
			" ?, "+//
			" ? "+//
			" );";
	public final static String SQL_UpdateCheckItem_By_Id="UPDATE jdevice_h_check_item SET "+//
			" name = ?, "+//
			" status = ?, "+//
			" c_plan_id = ?, "+//
			" c_item_id = ?, "+//
			" h_plan_id = ?, "+//
			" type = ?, "+//
			" value_bool = ?, "+//
			" value_bool_true_name = ?, "+//
			" value_bool_false_name = ?, "+//
			" value_number = ?, "+//
			" value_number_min = ?, "+//
			" value_number_max = ?, "+//
			" result_disc = ?, "+//
			" result_success = ?, "+//
			" result_time_ms = ? "+//
			"WHERE "+//
			" id=?;";

	public int getWorkcenterCheckPlansNum(int status,//
			long workcenterId,//
			long workshopId,//
			long factoryId,//
			String organizationId,//
			long beginTimeMs,//
			long endTimeMs,//
			long filterFactoryId,//
			long filterWorkshopId,//
			long filterWorkcenterId,//
			long filterDeviceId,//
			long filterExecutorId) {
		Object[] args=new Object[24];
		int i=0;
		args[i++]=beginTimeMs;
		args[i++]=endTimeMs;
		args[i++]=filterFactoryId;
		args[i++]=filterFactoryId;
		args[i++]=filterWorkshopId;
		args[i++]=filterWorkshopId;
		args[i++]=filterWorkcenterId;
		args[i++]=filterWorkcenterId;
		args[i++]=filterDeviceId;
		args[i++]=filterDeviceId;
		args[i++]=filterExecutorId;
		args[i++]=filterExecutorId;
		args[i++]=status;
		args[i++]=workcenterId;
		args[i++]=workshopId;
		args[i++]=factoryId;
		args[i++]=organizationId;
		args[i++]=status;
		args[i++]=workshopId;
		args[i++]=factoryId;
		args[i++]=organizationId;
		args[i++]=status;
		args[i++]=factoryId;
		args[i++]=organizationId;
		return jdbcTemplate.queryForObject(SQL_GetCheckPlans_By_Workcenter, args, Integer.class);
	}
	public final static String SQL_GetCheckPlansNum_By_Workcenter="SELECT "+//
			" COUNT(id) "+//
			"FROM jdevice_h_check_plan "+//
			"WHERE "+//
			" trigger_time_ms>=? "+//
			" AND trigger_time_ms<=? "+//
			" AND ((?<=0 OR factory_id=?) AND (?<=0 OR workshop_id=?) AND (?<=0 OR workcenter_id=?) AND (?<=0 OR device_id=?) AND (?<=0 OR executor_id=? ))"+//
			" AND (( "+//
			"  type = "+SpotCheckPlanConfigModel.TYPE_Device+//
			"  AND status=? "+//
			"  AND workcenter_id =? "+//
			"  AND workshop_id =? "+//
			"  AND factory_id =? "+//
			"  AND organization_id =? "+//
			" ) "+//
			"OR ( "+//
			"  type = "+SpotCheckPlanConfigModel.TYPE_Workshop+//
			"  AND status=? "+//
			"  AND workshop_id =? "+//
			"  AND factory_id =? "+//
			"  AND organization_id =? "+//
			") "+//
			"OR ( "+//
			"  type = "+SpotCheckPlanConfigModel.TYPE_Factory+//
			"  AND status=? "+//
			"  AND factory_id =? "+//
			"  AND organization_id =? "+//
			"));";
	
	public int getWorkshopCheckPlansNum(int status,//
			long workshopId,//
			long factoryId,//
			String organizationId,//
			long beginTimeMs,//
			long endTimeMs,//
			long filterFactoryId,//
			long filterWorkshopId,//
			long filterWorkcenterId,//
			long filterDeviceId,//
			long filterExecutorId) {
		Object[] args=new Object[23];
		int i=0;
		args[i++]=beginTimeMs;
		args[i++]=endTimeMs;
		args[i++]=filterFactoryId;
		args[i++]=filterFactoryId;
		args[i++]=filterWorkshopId;
		args[i++]=filterWorkshopId;
		args[i++]=filterWorkcenterId;
		args[i++]=filterWorkcenterId;
		args[i++]=filterDeviceId;
		args[i++]=filterDeviceId;
		args[i++]=filterExecutorId;
		args[i++]=filterExecutorId;
		args[i++]=status;
		args[i++]=workshopId;
		args[i++]=factoryId;
		args[i++]=organizationId;
		args[i++]=status;
		args[i++]=workshopId;
		args[i++]=factoryId;
		args[i++]=organizationId;
		args[i++]=status;
		args[i++]=factoryId;
		args[i++]=organizationId;
		return jdbcTemplate.queryForObject(SQL_GetCheckPlansNum_By_Workshop, args, Integer.class);
	}
	public final static String SQL_GetCheckPlansNum_By_Workshop="SELECT "+//
			" COUNT(id) "+//
			"FROM jdevice_h_check_plan "+//
			"WHERE "+//
			" trigger_time_ms>=? "+//
			" AND trigger_time_ms<=? "+//
			" AND ((?<=0 OR factory_id=?) AND (?<=0 OR workshop_id=?) AND (?<=0 OR workcenter_id=?) AND (?<=0 OR device_id=?) AND (?<=0 OR executor_id=? ))"+//
			" AND (( "+//
			"  type = "+SpotCheckPlanConfigModel.TYPE_Device+//
			"  AND status=? "+//
			"  AND workshop_id =? "+//
			"  AND factory_id =? "+//
			"  AND organization_id =? "+//
			" ) "+//
			"OR ( "+//
			"  type = "+SpotCheckPlanConfigModel.TYPE_Workshop+//
			"  AND status=? "+//
			"  AND workshop_id =? "+//
			"  AND factory_id =? "+//
			"  AND organization_id =? "+//
			") "+//
			"OR ( "+//
			"  type = "+SpotCheckPlanConfigModel.TYPE_Factory+//
			"  AND status=? "+//
			"  AND factory_id =? "+//
			"  AND organization_id =? "+//
			"));";
	
	public int getFactoryCheckPlansNum(int status,//
			long factoryId,//
			String organizationId,//
			long beginTimeMs,//
			long endTimeMs,//
			long filterFactoryId,//
			long filterWorkshopId,//
			long filterWorkcenterId,//
			long filterDeviceId,//
			long filterExecutorId) {
		Object[] args=new Object[15];
		int i=0;
		args[i++]=beginTimeMs;
		args[i++]=endTimeMs;
		args[i++]=filterFactoryId;
		args[i++]=filterFactoryId;
		args[i++]=filterWorkshopId;
		args[i++]=filterWorkshopId;
		args[i++]=filterWorkcenterId;
		args[i++]=filterWorkcenterId;
		args[i++]=filterDeviceId;
		args[i++]=filterDeviceId;
		args[i++]=filterExecutorId;
		args[i++]=filterExecutorId;
		args[i++]=factoryId;
		args[i++]=status;
		args[i++]=organizationId;
		return jdbcTemplate.queryForObject(SQL_GetCheckPlansNum_By_Factory, args, Integer.class);
	}
	public final static String SQL_GetCheckPlansNum_By_Factory="SELECT "+//
			" COUNT(id) "+//
			"FROM jdevice_h_check_plan "+//
			"WHERE "+//
			" trigger_time_ms>=? "+//
			"  AND trigger_time_ms<=? "+//
			"  AND ((?<=0 OR factory_id=?) AND (?<=0 OR workshop_id=?) AND (?<=0 OR workcenter_id=?) AND (?<=0 OR device_id=?) AND (?<=0 OR executor_id=? ))"+//
			"  AND factory_id=? "+//
			"  AND status=? "+//
			"  AND organization_id =?;";
	
	public int getOrganizationCheckPlansNum(int status,//
			String organizationId,//
			long beginTimeMs,//
			long endTimeMs,//
			long filterFactoryId,//
			long filterWorkshopId,//
			long filterWorkcenterId,//
			long filterDeviceId,//
			long filterExecutorId) {
		Object[] args=new Object[14];
		int i=0;
		args[i++]=beginTimeMs;
		args[i++]=endTimeMs;
		args[i++]=filterFactoryId;
		args[i++]=filterFactoryId;
		args[i++]=filterWorkshopId;
		args[i++]=filterWorkshopId;
		args[i++]=filterWorkcenterId;
		args[i++]=filterWorkcenterId;
		args[i++]=filterDeviceId;
		args[i++]=filterDeviceId;
		args[i++]=filterExecutorId;
		args[i++]=filterExecutorId;
		args[i++]=status;
		args[i++]=organizationId;
		return jdbcTemplate.queryForObject(SQL_GetCheckPlansNum_By_Organization, args, Integer.class);
	}
	public final static String SQL_GetCheckPlansNum_By_Organization="SELECT "+//
			" COUNT(id) "+//
			"FROM jdevice_h_check_plan "+//
			"WHERE "+//
			" trigger_time_ms>=? "+//
			"  AND trigger_time_ms<=? "+//
			"  AND ((?<=0 OR factory_id=?) AND (?<=0 OR workshop_id=?) AND (?<=0 OR workcenter_id=?) AND (?<=0 OR device_id=?) AND (?<=0 OR executor_id=? ))"+//
			"  AND status=? "+//
			"  AND organization_id =?;";
}
