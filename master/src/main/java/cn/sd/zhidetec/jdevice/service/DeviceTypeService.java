package cn.sd.zhidetec.jdevice.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.sd.zhidetec.jdevice.dao.DeviceTypeModelDao;
import cn.sd.zhidetec.jdevice.model.DeviceTypeModel;
import cn.sd.zhidetec.jdevice.model.WorkshopModel;

@Service
public class DeviceTypeService {
	@Autowired
	protected DeviceTypeModelDao deviceTypeModelDao;
	@Autowired
	protected RBuilderService rBuilderService;
	protected Logger logger = LoggerFactory.getLogger(getClass());

	public DeviceTypeModel getDeviceTypeModelById(long id) {
		return deviceTypeModelDao.getDeviceTypeModelById(id);
	}

	public List<DeviceTypeModel> getDeviceTypeModelsByWorkshopId(long workshopId) {
		return deviceTypeModelDao.getDeviceTypeModelsByWorkshopId(workshopId);
	}

	public List<DeviceTypeModel> getDeviceTypeModelsByFactoryId(long factoryId) {
		return deviceTypeModelDao.getDeviceTypeModelsByFactoryId(factoryId);
	}

	public List<DeviceTypeModel> getDeviceTypeModelsByOrganizationId(String organizationId) {
		return deviceTypeModelDao.getDeviceTypeModelsByOrganizationId(organizationId);
	}

	public boolean setDeviceTypeModel(DeviceTypeModel deviceTypeModel) {
		return deviceTypeModelDao.setDeviceTypeModel(deviceTypeModel);
	}

	public boolean deleteDeviceTypeModel(DeviceTypeModel deviceTypeModel) {
		return deviceTypeModelDao.deleteDeviceTypeModel(deviceTypeModel);
	}

	public boolean changeWorkshop(long typeId,long workshopId,long factoryId) {
		return deviceTypeModelDao.changeWorkshop(typeId, workshopId, factoryId);
	}
	
	public boolean changeFactory(long workshopId,long factoryId) {
		return deviceTypeModelDao.changeFactory(workshopId,factoryId);
	}

	public DeviceTypeModel initWorkshop(WorkshopModel workshopModel) {
		if (workshopModel==null) {
			return null;
		}
		DeviceTypeModel deviceTypeModel=new DeviceTypeModel();
		deviceTypeModel.setOrganizationId(workshopModel.getOrganizationId());
		deviceTypeModel.setFactoryId(workshopModel.getFactoryId());
		deviceTypeModel.setWorkshopId(workshopModel.getId());
		if (setDeviceTypeModel(deviceTypeModel)) {
			return deviceTypeModel;
		}
		return null;
	}
}
