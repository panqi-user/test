package cn.sd.zhidetec.jdevice.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.sd.zhidetec.jdevice.HttpResponseMsg;
import cn.sd.zhidetec.jdevice.model.RoleModel;
import cn.sd.zhidetec.jdevice.model.SpotCheckItemConfigModel;
import cn.sd.zhidetec.jdevice.model.SpotCheckPlanConfigModel;
import cn.sd.zhidetec.jdevice.model.UserModel;
import cn.sd.zhidetec.jdevice.service.RBuilderService;
import cn.sd.zhidetec.jdevice.service.SpotCheckConfigService;
import cn.sd.zhidetec.jdevice.util.StringUtil;

@Controller
@RequestMapping("/spotCheckConfig")
public class SpotCheckConfigController {

	@Autowired
	protected SpotCheckConfigService spotCheckConfigService;
	@Autowired
	protected RBuilderService rBuilderService;
	protected Logger logger = LoggerFactory.getLogger(getClass());

	/**
	 * 获取点巡检计划配置列表
	 */
	@RequestMapping("/getCheckPlanConfiges")
	@ResponseBody
	public RBuilderService.Response getCheckPlanConfiges(HttpSession httpSession, //
			HttpServletRequest httpServletRequest) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_User, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		// 登录用户
		UserModel userModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		// 数据分页
		int minIndex = 0;
		int dataNum = 100;
		String minIndexStr = httpServletRequest.getParameter("minIndex");
		String dataNumStr = httpServletRequest.getParameter("dataNum");
		if (StringUtil.isNumber(minIndexStr)) {
			minIndex = Integer.parseInt(minIndexStr);
		}
		if (StringUtil.isNumber(dataNumStr)) {
			dataNum = Integer.parseInt(dataNumStr);
		}
		// 根据用户权限返回
		if (userModel.getRoleLevel() <= RoleModel.ROLE_LEVEL_WorkcenterAdmin) {
			return rBuilderService.update(response, //
					HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, //
					spotCheckConfigService.getWorkcenterCheckPlanConfiges(userModel.getWorkcenterId(), //
							userModel.getWorkshopId(), //
							userModel.getFactoryId(), //
							userModel.getOrganizationId(), //
							minIndex, dataNum));
		}
		if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_WorkshopAdmin) {
			return rBuilderService.update(response, //
					HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, //
					spotCheckConfigService.getWorkshopCheckPlanConfiges(userModel.getWorkshopId(), //
							userModel.getFactoryId(), //
							userModel.getOrganizationId(), //
							minIndex, dataNum));
		}
		if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_FactoryAdmin) {
			return rBuilderService.update(response, //
					HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, //
					spotCheckConfigService.getFactoryCheckPlanConfiges(userModel.getFactoryId(), //
							userModel.getOrganizationId(), //
							minIndex, dataNum));
		}
		if (userModel.getRoleLevel() >= RoleModel.ROLE_LEVEL_OrganizationAdmin) {
			return rBuilderService.update(response, //
					HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, //
					spotCheckConfigService.getOrganizationCheckPlanConfiges(userModel.getOrganizationId(), //
							minIndex, dataNum));
		}
		return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
	}

	/**
	 * 设置点巡检计划配置
	 */
	@RequestMapping("/setCheckPlanConfig")
	@ResponseBody
	public RBuilderService.Response setCheckPlanConfig(HttpSession httpSession, //
			@RequestBody SpotCheckPlanConfigModel model) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_WorkcenterAdmin, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		// 获取登录用户
		UserModel userModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		model.setOrganizationId(userModel.getOrganizationId());
		// 填充新建信息
		if (model.getId() <= 0) {
			model.setCreateTimeMs(System.currentTimeMillis());
			model.setCreaterName(model.getName());
			model.setCreaterId(userModel.getId());
		}
		// 根据权限操作
		boolean hadPermission = false;
		if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_WorkcenterAdmin) {
			if (model.getType() == SpotCheckPlanConfigModel.TYPE_Device) {
				hadPermission = true;
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
			}
		}
		if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_WorkshopAdmin) {
			if (model.getType() == SpotCheckPlanConfigModel.TYPE_Device//
					|| model.getType() == SpotCheckPlanConfigModel.TYPE_Workshop) {
				hadPermission = true;
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
			}
		}
		if (userModel.getRoleLevel() >= RoleModel.ROLE_LEVEL_FactoryAdmin) {
			if (model.getType() == SpotCheckPlanConfigModel.TYPE_Device//
					|| model.getType() == SpotCheckPlanConfigModel.TYPE_Workshop//
					|| model.getType() == SpotCheckPlanConfigModel.TYPE_Factory) {
				hadPermission = true;
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
			}
		}
		// 操作
		if (hadPermission) {
			if (spotCheckConfigService.setCheckPlanConfig(model)) {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, model);
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, model);
			}
		}
		return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
	}

	/**
	 * 删除点巡检配置计划
	 */
	@RequestMapping("/deleteCheckPlanConfig")
	@ResponseBody
	public RBuilderService.Response deleteCheckPlanConfig(HttpSession httpSession, //
			@RequestParam("id") long id) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_WorkcenterAdmin, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		// 获取当前登录用户
		UserModel userModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		// 获取当前计划
		SpotCheckPlanConfigModel planConfigModel = spotCheckConfigService.getCheckPlanConfigById(id,
				userModel.getOrganizationId());
		if (planConfigModel == null) {
			return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NOT_FOUND_DATA, null);
		} else {
			// 根据权限操作
			boolean hadPermission = false;
			if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_WorkcenterAdmin) {
				if (planConfigModel.getType() == SpotCheckPlanConfigModel.TYPE_Device) {
					hadPermission = true;
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
				}
			}
			if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_WorkshopAdmin) {
				if (planConfigModel.getType() == SpotCheckPlanConfigModel.TYPE_Device//
						|| planConfigModel.getType() == SpotCheckPlanConfigModel.TYPE_Workshop) {
					hadPermission = true;
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
				}
			}
			if (userModel.getRoleLevel() >= RoleModel.ROLE_LEVEL_FactoryAdmin) {
				if (planConfigModel.getType() == SpotCheckPlanConfigModel.TYPE_Device//
						|| planConfigModel.getType() == SpotCheckPlanConfigModel.TYPE_Workshop//
						|| planConfigModel.getType() == SpotCheckPlanConfigModel.TYPE_Factory) {
					hadPermission = true;
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
				}
			}
			// 操作
			if (hadPermission) {
				if (spotCheckConfigService.deleteCheckPlanConfigById(id, userModel.getOrganizationId())) {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, planConfigModel);
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, id);
				}
			}
			return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
		}
	}

	/**
	 * 根据计划ID获取点巡检配置项列表
	 */
	@RequestMapping("/getCheckItemConfigesByPlanId")
	@ResponseBody
	public RBuilderService.Response getCheckItemConfigesByPlanId(HttpSession httpSession, //
			@RequestParam("planId") long planId) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_User, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		// 获取当前登录用户
		UserModel userModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		// 获取当前计划
		SpotCheckPlanConfigModel planConfigModel = spotCheckConfigService.getCheckPlanConfigById(planId,
				userModel.getOrganizationId());
		if (planConfigModel == null) {
			return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NOT_FOUND_DATA, null);
		} else {
			// 根据权限操作
			boolean hadPermission = false;
			if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_WorkcenterAdmin) {
				if (planConfigModel.getType() == SpotCheckPlanConfigModel.TYPE_Device) {
					hadPermission = true;
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
				}
			}
			if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_WorkshopAdmin) {
				if (planConfigModel.getType() == SpotCheckPlanConfigModel.TYPE_Device//
						|| planConfigModel.getType() == SpotCheckPlanConfigModel.TYPE_Workshop) {
					hadPermission = true;
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
				}
			}
			if (userModel.getRoleLevel() >= RoleModel.ROLE_LEVEL_FactoryAdmin) {
				if (planConfigModel.getType() == SpotCheckPlanConfigModel.TYPE_Device//
						|| planConfigModel.getType() == SpotCheckPlanConfigModel.TYPE_Workshop//
						|| planConfigModel.getType() == SpotCheckPlanConfigModel.TYPE_Factory) {
					hadPermission = true;
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
				}
			}
			// 操作
			if (hadPermission) {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE,
						spotCheckConfigService.getCheckItemConfigesByPlanId(planId, userModel.getOrganizationId()));
			}
			return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
		}
	}

	/**
	 * 设置点巡检配置项
	 */
	@RequestMapping("/setCheckItemConfig")
	@ResponseBody
	public RBuilderService.Response setCheckItemConfig(HttpSession httpSession, //
			@RequestBody SpotCheckItemConfigModel model) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_WorkcenterAdmin, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		// 获取当前登录用户
		UserModel userModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		model.setOrganizationId(userModel.getOrganizationId());
		// 获取当前计划
		SpotCheckPlanConfigModel planConfigModel = spotCheckConfigService.getCheckPlanConfigById(model.getPlanId(),
				userModel.getOrganizationId());
		if (planConfigModel == null) {
			return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NOT_FOUND_DATA, null);
		} else {
			// 根据权限操作
			boolean hadPermission = false;
			if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_WorkcenterAdmin) {
				if (planConfigModel.getType() == SpotCheckPlanConfigModel.TYPE_Device) {
					hadPermission = true;
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
				}
			}
			if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_WorkshopAdmin) {
				if (planConfigModel.getType() == SpotCheckPlanConfigModel.TYPE_Device//
						|| planConfigModel.getType() == SpotCheckPlanConfigModel.TYPE_Workshop) {
					hadPermission = true;
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
				}
			}
			if (userModel.getRoleLevel() >= RoleModel.ROLE_LEVEL_FactoryAdmin) {
				if (planConfigModel.getType() == SpotCheckPlanConfigModel.TYPE_Device//
						|| planConfigModel.getType() == SpotCheckPlanConfigModel.TYPE_Workshop//
						|| planConfigModel.getType() == SpotCheckPlanConfigModel.TYPE_Factory) {
					hadPermission = true;
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
				}
			}
			// 操作
			if (hadPermission) {
				if (spotCheckConfigService.setCheckItemConfig(model)) {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, model);
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, model);
				}
			}
			return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
		}
	}

	/**
	 * 删除点巡检配置项
	 */
	@RequestMapping("/deleteCheckItemConfig")
	@ResponseBody
	public RBuilderService.Response deleteCheckItemConfig(HttpSession httpSession, //
			@RequestParam("id") long id,
			@RequestParam("planId") long planId) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_User, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		// 获取当前登录用户
		UserModel userModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		// 获取当前计划
		SpotCheckPlanConfigModel planConfigModel = spotCheckConfigService.getCheckPlanConfigById(planId,
				userModel.getOrganizationId());
		if (planConfigModel == null) {
			return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NOT_FOUND_DATA, null);
		} else {
			// 根据权限操作
			boolean hadPermission = false;
			if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_WorkcenterAdmin) {
				if (planConfigModel.getType() == SpotCheckPlanConfigModel.TYPE_Device) {
					hadPermission = true;
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
				}
			}
			if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_WorkshopAdmin) {
				if (planConfigModel.getType() == SpotCheckPlanConfigModel.TYPE_Device//
						|| planConfigModel.getType() == SpotCheckPlanConfigModel.TYPE_Workshop) {
					hadPermission = true;
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
				}
			}
			if (userModel.getRoleLevel() >= RoleModel.ROLE_LEVEL_FactoryAdmin) {
				if (planConfigModel.getType() == SpotCheckPlanConfigModel.TYPE_Device//
						|| planConfigModel.getType() == SpotCheckPlanConfigModel.TYPE_Workshop//
						|| planConfigModel.getType() == SpotCheckPlanConfigModel.TYPE_Factory) {
					hadPermission = true;
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
				}
			}
			// 操作
			if (hadPermission) {
				if (spotCheckConfigService.deleteCheckItemConfigById(id, userModel.getOrganizationId())) {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, planConfigModel);
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, id);
				}
			}
			return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
		}
	}

	/**
	 * 根据ID获取点检计划配置
	 */
	@RequestMapping("/getCheckPlanConfigById")
	@ResponseBody
	public RBuilderService.Response getCheckPlanConfigById(HttpSession httpSession, //
			@RequestParam("id") long id) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_User, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		// 获取当前登录用户
		UserModel userModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		// 获取当前计划
		SpotCheckPlanConfigModel planConfigModel = spotCheckConfigService.getCheckPlanConfigById(id,
				userModel.getOrganizationId());
		if (planConfigModel == null) {
			return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NOT_FOUND_DATA, null);
		} else {
			// 根据权限操作
			boolean hadPermission = false;
			if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_WorkcenterAdmin) {
				if (planConfigModel.getType() == SpotCheckPlanConfigModel.TYPE_Device) {
					hadPermission = true;
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
				}
			}
			if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_WorkshopAdmin) {
				if (planConfigModel.getType() == SpotCheckPlanConfigModel.TYPE_Device//
						|| planConfigModel.getType() == SpotCheckPlanConfigModel.TYPE_Workshop) {
					hadPermission = true;
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
				}
			}
			if (userModel.getRoleLevel() >= RoleModel.ROLE_LEVEL_FactoryAdmin) {
				if (planConfigModel.getType() == SpotCheckPlanConfigModel.TYPE_Device//
						|| planConfigModel.getType() == SpotCheckPlanConfigModel.TYPE_Workshop//
						|| planConfigModel.getType() == SpotCheckPlanConfigModel.TYPE_Factory) {
					hadPermission = true;
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
				}
			}
			// 操作
			if (hadPermission) {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, planConfigModel);
			}
			return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
		}
	}
}
