package cn.sd.zhidetec.jdevice.dao;

import cn.sd.zhidetec.jdevice.Starter;
import cn.sd.zhidetec.jdevice.config.DataSourceConfiger;
import cn.sd.zhidetec.jdevice.model.UserModel;
import cn.sd.zhidetec.jdevice.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 用户小程序的信息dao层
 */

@Service
public class WechatModeDao {

    @Autowired
    @Qualifier(DataSourceConfiger.BEAN_NAME_JdeviceJdbcTemplate)
    protected JdbcTemplate jdbcTemplate;

    /**
     * 根据手机号修改用户的微信信息
     *
     * @param phone       用户手机号
     * @param openId      微信小程序的唯一标志
     * @param wxToken     微信小程序的登录标志
     * @return
     */

    private BeanPropertyRowMapper<UserModel> beanPropertyRowMapper = new BeanPropertyRowMapper<UserModel>(
            UserModel.class);

    /**
     * 根据用户的token，查询该用户的信息
     *
     * @param wxToken 用户的手机号
     * @return
     */
    public List<UserModel> getUserModelsByToken(String wxToken) {
        Object[] args = new Object[1];
        args[0] = wxToken;
        List<UserModel> list = jdbcTemplate.query(Sql_User_By_Token, args, beanPropertyRowMapper);
        return list;
    }

    private static String Sql_User_By_Token = "SELECT * FROM jdevice_c_user WHERE wx_token = ?";

    /**
     * 更换用户的微信用户信息
     *
     * @return 操作是否成功
     */
    public boolean setUserWechatByPhone(String phone, String openid, String token) {
        int i = 0;
        Object[] args = new Object[3];

        if (!StringUtil.isEmpty(phone)) {
            args[i++] = openid;
            args[i++] = token;
            args[i++] = phone;
            i = jdbcTemplate.update(Sql_GetWecaht_By_Phone, args);
        }
        else {
            return false;
        }
        if (i > 0) {
            return true;
        }
        return false;
    }

    private static String Sql_GetWecaht_By_Phone = "UPDATE jdevice_c_user" + //
            " SET " + //
            "wx_open_id = ?," + //
            "wx_token = ?" + //
            " WHERE " + //
            "phone=?";


    /**
     * 插入用户微信消息
     *
     * @param userModel
     * @return
     */
    public boolean insertWechatUserModel(UserModel userModel) {
        int i = 0;
        Object[] args = new Object[20];
        userModel.setId(Starter.IdMaker.nextId());
        args[i++] = userModel.getId();
        args[i++] = StringUtil.getEmptyString(userModel.getPhone());//手机号为code
        args[i++] = StringUtil.getEmptyString(userModel.getName());
        args[i++] = StringUtil.getEmptyString(userModel.getPassword());
        args[i++] = userModel.getRoleLevel();

        args[i++] = userModel.getWarnLevel();
        args[i++] = userModel.getPhotoUrl();
        args[i++] = StringUtil.getEmptyString(userModel.getPhone());
        args[i++] = userModel.getStatus();

        args[i++] = StringUtil.getEmptyString(userModel.getPosition());
        args[i++] = userModel.getSex();
        args[i++] = userModel.getAge();

        args[i++] = StringUtil.getEmptyString(userModel.getDeviceBrand());
        args[i++] = StringUtil.getEmptyString(userModel.getDeviceToken());
        args[i++] = userModel.getWorkcenterId();
        args[i++] = userModel.getWorkshopId();
        args[i++] = userModel.getFactoryId();
        args[i++] = StringUtil.getEmptyString(userModel.getOrganizationId());
        args[i++] = userModel.getWxOpenId();
        args[i++] = userModel.getWxToken();
        i = jdbcTemplate.update(Sql_Insert_WX_Model, args);
        if (i > 0) {
            return true;
        }
        return false;
    }

    private static String Sql_Insert_WX_Model = "INSERT INTO jdevice_c_user (" + //
            "id," + //
            "code," + //
            "name," + //
            "password," + //
            "role_level," + //
            "warn_level," + //
            "photo_url," + //
            "phone," + //
            "status," + //
            "position," + //
            "sex," + //
            "age," + //
            "device_brand," + //
            "device_token," + //
            "workcenter_id," + //
            "workshop_id," + //
            "factory_id," + //
            "organization_id," + //
            "wx_open_id," + //
            "wx_token" + //
            ")VALUES(" + //
            "?," + //
            "?," + //
            "?," + //
            "?," + //
            "?," + //
            "?," + //
            "?," + //
            "?," + //
            "?," + //
            "?," + //
            "?," + //
            "?," + //
            "?," + //
            "?," + //
            "?," + //
            "?," + //
            "?," + //
            "?," + //
            "?," + //
            "?" + //
            ")";
}
