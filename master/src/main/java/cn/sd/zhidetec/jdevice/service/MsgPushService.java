package cn.sd.zhidetec.jdevice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.sd.zhidetec.jdevice.client.MsgPushClient_Hms;
import cn.sd.zhidetec.jdevice.client.MsgPushClient_UMeng;
import cn.sd.zhidetec.jdevice.client.MsgPushClient_Xiaomi;
import cn.sd.zhidetec.jdevice.model.AndonMessageModel;
import cn.sd.zhidetec.jdevice.model.MessageModel;

@Service
public class MsgPushService {

	@Autowired
	protected MsgPushClient_Hms msgPushClient_Hms;
	@Autowired
	protected MsgPushClient_UMeng msgPushClient_UMeng;
	@Autowired
	protected MsgPushClient_Xiaomi msgPushClient_Xiaomi;
	@Autowired
	protected JedgeService jedgeServer;
	
	public boolean pushAndonMessage(AndonMessageModel andonMessageModel,long timeMs) {
		MessageModel messageModel=MessageModel.build(andonMessageModel, timeMs);
		//推送
		jedgeServer.getJedgeServer().sendAndonMessage(andonMessageModel);
		msgPushClient_Hms.sendMessageModel(messageModel);
		msgPushClient_UMeng.sendMessageModel(messageModel);
		msgPushClient_Xiaomi.sendMessageModel(messageModel);
		return true;
	}
	
	public boolean pushMessage(MessageModel messageModel) {
		//推送
		jedgeServer.getJedgeServer().sendMessageModel(messageModel);
		msgPushClient_Hms.sendMessageModel(messageModel);
		msgPushClient_UMeng.sendMessageModel(messageModel);
		msgPushClient_Xiaomi.sendMessageModel(messageModel);
		return true;
	}
}
