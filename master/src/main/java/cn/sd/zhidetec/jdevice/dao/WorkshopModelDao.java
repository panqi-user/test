package cn.sd.zhidetec.jdevice.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import cn.sd.zhidetec.jdevice.Starter;
import cn.sd.zhidetec.jdevice.config.DataSourceConfiger;
import cn.sd.zhidetec.jdevice.model.WorkshopModel;
import cn.sd.zhidetec.jdevice.util.StringUtil;

@Service
public class WorkshopModelDao {

	@Autowired
	@Qualifier(DataSourceConfiger.BEAN_NAME_JdeviceJdbcTemplate)
	protected JdbcTemplate jdbcTemplate;
	
	public int getWorkshopNum(String organizationId) {
		Object[] args = new Object[1];
		args[0] = organizationId;
		return jdbcTemplate.queryForObject(Sql_GetNum_By_OrganizationId, args, Integer.class);
	}

	private static String Sql_GetNum_By_OrganizationId = "SELECT COUNT(*) AS Num FROM jdevice_c_workshop WHERE organization_id=?";

	/**
	 * 删除车间
	 * 
	 * @param workshopModel 车间信息
	 * @return 操作是否成功
	 */
	public boolean deleteWorkshopModel(WorkshopModel workshopModel) {
		int i = 0;
		Object[] args = new Object[1];
		args[i++] = workshopModel.getId();
		if (workshopModel.getId() > 0) {
			i = jdbcTemplate.update(Sql_DeleteModel_By_Id, args);
		}
		if (i > 0) {
			return true;
		}
		return false;
	}

	private static String Sql_DeleteModel_By_Id = "DELETE FROM jdevice_c_workshop WHERE id=?";

	/**
	 * 设置车间信息，如果不存在则新建
	 * 
	 * @param workshopModel 车间信息
	 * @return 操作是否成功
	 */
	public boolean setWorkshopModel(WorkshopModel workshopModel) {
		if (StringUtil.isEmpty(workshopModel.getOrganizationId())) {
			return false;
		}
		//
		int i = 0;
		Object[] args = new Object[7];
		// 修改信息
		if (workshopModel.getId() > 0) {
			args[i++] = StringUtil.getEmptyString(workshopModel.getCode());
			args[i++] = StringUtil.getEmptyString(workshopModel.getName());
			args[i++] = StringUtil.getEmptyString(workshopModel.getLogoUrl());
			args[i++] = StringUtil.getEmptyString(workshopModel.getDashboardUrl());
			args[i++] = workshopModel.getFactoryId();
			args[i++] = StringUtil.getEmptyString(workshopModel.getOrganizationId());
			args[i++] = workshopModel.getId();
			i = jdbcTemplate.update(Sql_UpdateModel_By_Id, args);
		}
		// 新建信息
		else {
			workshopModel.setId(Starter.IdMaker.nextId());
			args[i++] = workshopModel.getId();
			args[i++] = StringUtil.getEmptyString(workshopModel.getCode());
			args[i++] = StringUtil.getEmptyString(workshopModel.getName());
			args[i++] = StringUtil.getEmptyString(workshopModel.getLogoUrl());
			args[i++] = StringUtil.getEmptyString(workshopModel.getDashboardUrl());
			args[i++] = workshopModel.getFactoryId();
			args[i++] = StringUtil.getEmptyString(workshopModel.getOrganizationId());
			i = jdbcTemplate.update(Sql_InsertModel, args);
			// 预警级别初始化

		}
		if (i > 0) {
			return true;
		}
		return false;
	}

	private static String Sql_UpdateModel_By_Id = "UPDATE jdevice_c_workshop" + //
			" SET " + //
			"code = ?," + //
			"name = ?," + //
			"logo_url = ?," + //
			"dashboard_url = ?," + //
			"factory_id = ?," + //
			"organization_id = ?" + //
			" WHERE " + //
			"id=?";
	private static String Sql_InsertModel = "INSERT INTO jdevice_c_workshop (" + //
			"id," + //
			"code," + //
			"name," + //
			"logo_url," + //
			"dashboard_url, "+//
			"factory_id," + //
			"organization_id" + //
			")VALUES(" + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?" + //
			")";

	/**
	 * 根据车间ID获取车间列表
	 * 
	 * @param workshopId 车间ID
	 * @return 数据列表
	 */
	public List<WorkshopModel> getWorkshopModelsById(long workshopId) {
		Object[] args = new Object[1];
		args[0] = workshopId;
		List<WorkshopModel> list = jdbcTemplate.query(Sql_GetModels_By_Id, args, beanPropertyRowMapper);
		return list;
	}

	private static String Sql_GetModels_By_Id = "SELECT * FROM jdevice_c_workshop WHERE id = ?";

	/**
	 * 根据车间ID获取车间
	 * 
	 * @param id 车间ID
	 * @return 车间
	 */
	public WorkshopModel getWorkshopModelById(long id) {
		List<WorkshopModel> list = getWorkshopModelsById(id);
		if (list != null && list.size() > 0) {
			return list.get(0);
		}
		return null;
	}

	/**
	 * 根据工厂ID获取车间列表
	 * 
	 * @param factoryId 工厂ID
	 * @return 数据列表
	 */
	public List<WorkshopModel> getWorkshopModelsByFactoryId(long factoryId) {
		Object[] args = new Object[1];
		args[0] = factoryId;
		List<WorkshopModel> list = jdbcTemplate.query(Sql_GetModels_By_FactoryId, args, beanPropertyRowMapper);
		return list;
	}

	private static String Sql_GetModels_By_FactoryId = "SELECT * FROM jdevice_c_workshop WHERE factory_id = ?";

	/**
	 * 根据组织ID获取车间列表
	 * 
	 * @param organizationId 组织ID
	 * @return 数据列表
	 */
	public List<WorkshopModel> getWorkshopModelsByOrganizationId(String organizationId) {
		Object[] args = new Object[1];
		args[0] = organizationId;
		List<WorkshopModel> list = jdbcTemplate.query(Sql_GetModels_By_OrganizationId, args, beanPropertyRowMapper);
		return list;
	}

	private static String Sql_GetModels_By_OrganizationId = "SELECT * FROM jdevice_c_workshop WHERE organization_id = ? ORDER BY factory_id DESC";
	
	private BeanPropertyRowMapper<WorkshopModel> beanPropertyRowMapper = new BeanPropertyRowMapper<WorkshopModel>(
			WorkshopModel.class);
}
