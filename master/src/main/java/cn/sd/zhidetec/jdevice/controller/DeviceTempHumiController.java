package cn.sd.zhidetec.jdevice.controller;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.sd.zhidetec.jdevice.HttpResponseMsg;
import cn.sd.zhidetec.jdevice.model.RoleModel;
import cn.sd.zhidetec.jdevice.model.UserModel;
import cn.sd.zhidetec.jdevice.service.DeviceTempHumiService;
import cn.sd.zhidetec.jdevice.service.RBuilderService;

@Controller
@RequestMapping("/deviceTempHumi")
public class DeviceTempHumiController {

	@Autowired
	protected RBuilderService rBuilderService;
	@Autowired
	protected DeviceTempHumiService deviceTempHumiService;
	protected Logger logger = LoggerFactory.getLogger(getClass());
	/**
	 * 获取车间设备最新的温湿度值列表
	 * */
	@RequestMapping(path="/getWorkshopLastModels")
	@ResponseBody
	public RBuilderService.Response getWorkshopLastModels(HttpSession httpSession,//
			@RequestParam("workshopId")long workshopId) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_User, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		UserModel userModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		if (userModel.getRoleLevel()>=RoleModel.ROLE_LEVEL_WorkshopAdmin) {
			return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, deviceTempHumiService.getWorkshopLastModels(workshopId));
		}else {
			return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, deviceTempHumiService.getWorkcenterLastModels(userModel.getWorkcenterId()));
		}
	}
	/**
	 * 获取当前设备最新的温湿度值
	 * */
	@RequestMapping(path="/getDeviceLastModel")
	@ResponseBody
	public RBuilderService.Response getDeviceLastModel(HttpSession httpSession,//
			@RequestParam("deviceId")long deviceId) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_User, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, deviceTempHumiService.getDeviceLastModel(deviceId));
	}
	/**
	 * 获取指定设备从指定时间开始的温湿度列表
	 * */
	@RequestMapping(path="/getDeviceModels")
	@ResponseBody
	public RBuilderService.Response getDeviceModels(HttpSession httpSession,//
			@RequestParam("deviceId")long deviceId,//
			@RequestParam("beginTimeMs")long beginTimeMs) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_User, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, deviceTempHumiService.getDeviceModels(deviceId, beginTimeMs));
	}
}
