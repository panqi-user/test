package cn.sd.zhidetec.jdevice.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import cn.sd.zhidetec.jdevice.Starter;
import cn.sd.zhidetec.jdevice.client.MsgPushClient_Hms;
import cn.sd.zhidetec.jdevice.client.MsgPushClient_UMeng;
import cn.sd.zhidetec.jdevice.config.DataSourceConfiger;
import cn.sd.zhidetec.jdevice.model.RoleModel;
import cn.sd.zhidetec.jdevice.model.UserModel;
import cn.sd.zhidetec.jdevice.util.StringUtil;

/**
 * 用户信息DAO服务
 */
@Service
public class UserModelDao {

	@Autowired
	@Qualifier(DataSourceConfiger.BEAN_NAME_JdeviceJdbcTemplate)
	protected JdbcTemplate jdbcTemplate;

	public int getUserNum(String organizationId,int roleLevel) {
		Object[] args = new Object[3];
		int i=0;
		args[i++] = organizationId;
		args[i++] = roleLevel;
		args[i++] = roleLevel;
		return jdbcTemplate.queryForObject(Sql_GetNum_By_OrganizationId, args, Integer.class);
	}

	private static String Sql_GetNum_By_OrganizationId = "SELECT COUNT(*) AS Num FROM jdevice_c_user WHERE organization_id=? AND (?<0 OR role_level=?)";

	/**
	 * 新建用户信息
	 * 
	 * @param userModel 用户信息
	 * @return 操作是否成功
	 */
	public boolean newUserModel(UserModel userModel) {
		int i = 0;
		Object[] args = new Object[18];
		userModel.setId(Starter.IdMaker.nextId());
		args[i++] = userModel.getId();
		args[i++] = StringUtil.getEmptyString(userModel.getCode());
		args[i++] = StringUtil.getEmptyString(userModel.getName());
		args[i++] = StringUtil.getEmptyString(userModel.getPassword());
		args[i++] = userModel.getRoleLevel();

		args[i++] = userModel.getWarnLevel();
		args[i++] = userModel.getPhotoUrl();
		args[i++] = StringUtil.getEmptyString(userModel.getPhone());
		args[i++] = userModel.getStatus();

		args[i++] = StringUtil.getEmptyString(userModel.getPosition());
		args[i++] = userModel.getSex();
		args[i++] = userModel.getAge();

		args[i++] = StringUtil.getEmptyString(userModel.getDeviceBrand());
		args[i++] = StringUtil.getEmptyString(userModel.getDeviceToken());
		args[i++] = userModel.getWorkcenterId();
		args[i++] = userModel.getWorkshopId();
		args[i++] = userModel.getFactoryId();
		args[i++] = StringUtil.getEmptyString(userModel.getOrganizationId());
		i = jdbcTemplate.update(Sql_InsertModel, args);
		if (i > 0) {
			return true;
		}
		return false;
	}

	private static String Sql_InsertModel = "INSERT INTO jdevice_c_user (" + //
			"id," + //
			"code," + //
			"name," + //
			"password," + //
			"role_level," + //
			"warn_level," + //
			"photo_url," + //
			"phone," + //
			"status," + //
			"position," + //
			"sex," + //
			"age," + //
			"device_brand," + //
			"device_token," + //
			"workcenter_id," + //
			"workshop_id," + //
			"factory_id," + //
			"organization_id" + //
			")VALUES(" + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?" + //
			")";

	/**
	 * 根据手机号修改用户头像
	 * 
	 * @param userModel 用户信息
	 * @return 操作是否成功
	 */
	public boolean setUserPhotoUrlByPhone(String phone, String photoUrl) {
		int i = 0;
		Object[] args = new Object[2];
		if (phone != null//
				&& !phone.trim().equals("")//
				&& photoUrl != null//
				&& !photoUrl.trim().equals("")) {
			args[i++] = photoUrl;
			args[i++] = phone;
			i = jdbcTemplate.update(Sql_UpdatePhotoUrl_By_Phone, args);
		} else {
			return false;
		}
		if (i > 0) {
			return true;
		}
		return false;
	}

	private static String Sql_UpdatePhotoUrl_By_Phone = "UPDATE jdevice_c_user" + //
			" SET " + //
			"photo_url = ?" + //
			" WHERE " + //
			"phone=?";

	/**
	 * 更换客户端设备
	 * 
	 * @return 操作是否成功
	 */
	public boolean setAppDeviceInfoByPhone(String phone, String deviceBrand, String deviceToken) {
		int i = 0;
		Object[] args = new Object[3];
		if (MsgPushClient_Hms.isHuaweiDevice(deviceBrand)) {
			deviceBrand = MsgPushClient_Hms.DEVICE_BRAND;
		}
		if (MsgPushClient_UMeng.useUMengPush(deviceBrand)) {
			deviceBrand = MsgPushClient_UMeng.DEVICE_BRAND;
		}
		if (!StringUtil.isEmpty(phone)) {
			args[i++] = deviceBrand;
			args[i++] = deviceToken;
			args[i++] = phone;
			i = jdbcTemplate.update(Sql_UpdateAppDevice_By_Phone, args);
		} else {
			return false;
		}
		if (i > 0) {
			return true;
		}
		return false;
	}

	private static String Sql_UpdateAppDevice_By_Phone = "UPDATE jdevice_c_user" + //
			" SET " + //
			"device_brand = ?," + //
			"device_token = ?" + //
			" WHERE " + //
			"phone=?";

	/**
	 * 更换手机号
	 * 
	 * @param userModel 用户信息
	 * @return 操作是否成功
	 */
	public boolean setUserPhoneByPhone(String phone, String phoneNew) {
		int i = 0;
		Object[] args = new Object[2];
		if (phone != null//
				&& !phone.trim().equals("")//
				&& phoneNew != null//
				&& !phoneNew.trim().equals("")) {
			args[i++] = phoneNew;
			args[i++] = phone;
			i = jdbcTemplate.update(Sql_UpdatePhone_By_Phone, args);
		} else {
			return false;
		}
		if (i > 0) {
			return true;
		}
		return false;
	}

	private static String Sql_UpdatePhone_By_Phone = "UPDATE jdevice_c_user" + //
			" SET " + //
			"phone = ?" + //
			" WHERE " + //
			"phone=?";

	/**
	 * 根据手机号修改用户密码
	 * 
	 * @param userModel 用户信息
	 * @return 操作是否成功
	 */
	public boolean setUserPasswordByPhone(String phone, String password) {
		int i = 0;
		Object[] args = new Object[2];
		if (phone != null//
				&& !phone.trim().equals("")//
				&& password != null//
				&& !password.trim().equals("")) {
			args[i++] = password;
			args[i++] = phone;
			i = jdbcTemplate.update(Sql_UpdatePassword_By_Phone, args);
		} else {
			return false;
		}
		if (i > 0) {
			return true;
		}
		return false;
	}

	private static String Sql_UpdatePassword_By_Phone = "UPDATE jdevice_c_user" + //
			" SET " + //
			"password = ? " + //
			" WHERE " + //
			"phone=?";

	/**
	 * 设置用户信息如：名称、编号
	 * 
	 * @param userModel 用户信息
	 * @return 操作是否成功
	 */
	public boolean setUserInfoById(UserModel userModel) {
		int i = 0;
		Object[] args = new Object[6];
		if (userModel.getId() > 0
				&& getUserModelsById(userModel.getId()).size() > 0) {
			args[i++] = StringUtil.getEmptyString(userModel.getName());
			args[i++] = StringUtil.getEmptyString(userModel.getCode());
			args[i++] = StringUtil.getEmptyString(userModel.getPosition());
			args[i++] = userModel.getSex();
			args[i++] = userModel.getAge();
			args[i++] = userModel.getId();
			i = jdbcTemplate.update(Sql_UpdateUserInfo_By_Id, args);
		} else {
			return false;
		}
		if (i > 0) {
			return true;
		}
		return false;
	}

	private static String Sql_UpdateUserInfo_By_Id = "UPDATE jdevice_c_user" + //
			" SET " + //
			"name = ?," + //
			"code = ?," + //
			"position = ?," + //
			"sex = ?," + //
			"age = ?" + //
			" WHERE " + //
			" id=?";

	/**
	 * 移除用户组织
	 * 
	 * @param userModel 用户信息
	 * @return 操作是否成功
	 */
	public boolean removeOrganization(UserModel userModel) {
		int i = 0;
		Object[] args = new Object[8];
		userModel.setRoleLevel(RoleModel.ROLE_LEVEL_Browser);
		userModel.setWarnLevel(0);
		userModel.setStatus(UserModel.STATUS_VAL_NoOrganization);
		userModel.setFactoryId(-1);
		userModel.setWorkcenterId(-1);
		userModel.setWorkshopId(-1);
		userModel.setOrganizationId("");
		// 修改用户信息
		if (userModel.getId()>0
				&& getUserModelsById(userModel.getId()).size() > 0) {
			args[i++] = userModel.getRoleLevel();
			args[i++] = userModel.getWarnLevel();
			args[i++] = userModel.getStatus();
			args[i++] = -1;
			args[i++] = -1;
			args[i++] = -1;
			args[i++] = "";
			args[i++] = userModel.getId();
			i = jdbcTemplate.update(Sql_RemoveOrganization_By_Id, args);
		} else {
			return false;
		}
		if (i > 0) {
			return true;
		}
		return false;
	}

	private static String Sql_RemoveOrganization_By_Id = "UPDATE jdevice_c_user" + //
			" SET " + //
			"role_level = ?," + //
			"warn_level = ?," + //
			"status = ?," + //
			"workcenter_id = ?," + //
			"workshop_id = ?," + //
			"factory_id = ?," + //
			"organization_id = ?" + //
			" WHERE " + //
			"id=?";

	/**
	 * 设置用户信息
	 * 
	 * @param userModel 用户信息
	 * @return 操作是否成功
	 */
	public boolean setUserModel(UserModel userModel) {
		int i = 0;
		Object[] args = new Object[14];
		// 修改用户信息
		if (userModel.getId()>0
				&& getUserModelsById(userModel.getId()).size() > 0) {
			args[i++] = StringUtil.getEmptyString(userModel.getCode());
			args[i++] = StringUtil.getEmptyString(userModel.getName());
			args[i++] = userModel.getRoleLevel();
			args[i++] = userModel.getWarnLevel();
			args[i++] = StringUtil.getEmptyString(userModel.getPhone());
			args[i++] = userModel.getStatus();
			args[i++] = StringUtil.getEmptyString(userModel.getPosition());
			args[i++] = userModel.getSex();
			args[i++] = userModel.getAge();
			args[i++] = userModel.getWorkcenterId();
			args[i++] = userModel.getWorkshopId();
			args[i++] = userModel.getFactoryId();
			args[i++] = StringUtil.getEmptyString(userModel.getOrganizationId());
			args[i++] = userModel.getId();
			i = jdbcTemplate.update(Sql_UpdateModel_By_Id, args);
		} else {
			return false;
		}
		if (i > 0) {
			return true;
		}
		return false;
	}

	private static String Sql_UpdateModel_By_Id = "UPDATE jdevice_c_user" + //
			" SET " + //
			"code = ?," + //
			"name = ?," + //
			"role_level = ?," + //
			"warn_level = ?," + //
			"phone = ?," + //
			"status = ?," + //
			"position = ?," + //
			"sex = ?," + //
			"age = ?," + //
			"workcenter_id = ?," + //
			"workshop_id = ?," + //
			"factory_id = ?," + //
			"organization_id = ?" + //
			" WHERE " + //
			"id=?";

	/**
	 * 根据手机号和密码查找用户信息
	 * 
	 * @param phone    用户名
	 * @param password 密码
	 * @return 用户信息
	 */
	public UserModel getUserModelByPhoneAndPassword(String phone, String password) {
		Object[] args = new Object[2];
		args[0] = phone;
		args[1] = password;
		List<UserModel> list = jdbcTemplate.query(Sql_Find_By_CodeAndPassword, args, beanPropertyRowMapper);
		if (list.size() > 0) {
			return list.get(0);
		}
		return null;
	}

	private static String Sql_Find_By_CodeAndPassword = "Select * From jdevice_c_user Where phone = ? AND password = ?";

	/**
	 * 根据工作中心ID查找用户信息列表
	 * 
	 * @param workcenterId 工作中心ID
	 * @return 用户信息列表
	 */
	public List<UserModel> getUserModelsByWorkcenterId(long workcenterId) {
		Object[] args = new Object[1];
		args[0] = workcenterId;
		List<UserModel> list = jdbcTemplate.query(Sql_GetModels_By_WorkcenterId, args, beanPropertyRowMapper);
		return setUserPassword(list, null);
	}

	private static String Sql_GetModels_By_WorkcenterId = "SELECT * FROM jdevice_c_user WHERE workcenter_id = ? order by status desc";

	/**
	 * 根据车间ID查找用户信息列表
	 * 
	 * @param workshopId 车间ID
	 * @return 用户信息列表
	 */
	public List<UserModel> getUserModelsByWorkshopId(long workshopId) {
		Object[] args = new Object[1];
		args[0] = workshopId;
		List<UserModel> list = jdbcTemplate.query(Sql_GetModels_By_WorkshopId, args, beanPropertyRowMapper);
		return setUserPassword(list, null);
	}

	private static String Sql_GetModels_By_WorkshopId = "SELECT * FROM jdevice_c_user WHERE workshop_id = ? order by status desc";

	/**
	 * 根据工厂ID查找用户信息列表
	 * 
	 * @param factoryId 工厂ID
	 * @return 用户信息列表
	 */
	public List<UserModel> getUserModelsByFactoryId(long factoryId) {
		Object[] args = new Object[1];
		args[0] = factoryId;
		List<UserModel> list = jdbcTemplate.query(Sql_GetModels_By_FactoryId, args, beanPropertyRowMapper);
		return setUserPassword(list, null);
	}

	private static String Sql_GetModels_By_FactoryId = "SELECT * FROM jdevice_c_user WHERE factory_id = ? order by status desc";

	/**
	 * 根据组织ID查找用户信息列表
	 * 
	 * @param organizationId 组织ID
	 * @return 用户信息列表
	 */
	public List<UserModel> getUserModelsByOrganizationId(String organizationId) {
		Object[] args = new Object[1];
		args[0] = organizationId;
		List<UserModel> list = jdbcTemplate.query(Sql_GetModels_By_OrganizationId, args, beanPropertyRowMapper);
		return setUserPassword(list, null);
	}

	private static String Sql_GetModels_By_OrganizationId = "SELECT * FROM jdevice_c_user WHERE organization_id = ? order by status desc";

	/**
	 * 根据用户ID查找用户信息列表
	 * 
	 * @param id 用户ID
	 * @return 用户信息列表
	 */
	public List<UserModel> getUserModelsById(long id) {
		Object[] args = new Object[1];
		args[0] = id;
		List<UserModel> list = jdbcTemplate.query(Sql_GetModels_By_Id, args, beanPropertyRowMapper);
		return setUserPassword(list, null);
	}

	private static String Sql_GetModels_By_Id = "SELECT * FROM jdevice_c_user WHERE id = ?";

	/**
	 * 根据用户ID查找用户
	 * 
	 * @param id 用户ID
	 * @return 用户
	 */
	public UserModel getUserModelById(long id) {
		List<UserModel> list = getUserModelsById(id);
		if (list != null && list.size() > 0) {
			return list.get(0);
		}
		return null;
	}

	public List<String> getAllDeviceTokenList() {
		List<String> list = jdbcTemplate.queryForList(Sql_GetAllDeviceTokenList, String.class);
		return list;
	}
	private static String Sql_GetAllDeviceTokenList = "SELECT " + //
			"DISTINCT(device_token) " + //
			" FROM " + //
			"jdevice_c_user";
	
	/**
	 * 根据用户所属与权限查询指定设备类型的设备Token
	 * 
	 * @return 设备Token列表
	 */
	public List<String> getDeviceTokenList(String organizationId,//
			long factoryId,//
			long workshopId,//
			long workcenterId,//
			String deviceType,//
			int minWarnLevel) {
		Object[] args = new Object[7];
		int i = 0;
		args[i++] = organizationId;
		args[i++] = minWarnLevel;
		args[i++] = deviceType;
		args[i++] = factoryId;
		args[i++] = workshopId;
		args[i++] = workcenterId;
		args[i++] = workcenterId;
		List<String> list = jdbcTemplate.queryForList(Sql_GetDeviceTokenList, args, String.class);
		return list;
	}

	private static String Sql_GetDeviceTokenList = "SELECT " + //
			"DISTINCT(device_token) " + //
			" FROM " + //
			"jdevice_c_user" + //
			" WHERE " + //
			"organization_id = ? " + //
			" AND " + //
			"warn_level >= ? " + //
			" AND " + //
			"device_brand = ? " + //
			" AND ( " + //
			"role_level >= "+RoleModel.ROLE_LEVEL_OrganizationAdmin+" " + //
			" OR " + //
			"(role_level = "+RoleModel.ROLE_LEVEL_FactoryAdmin+" AND factory_id = ?)" + //
			" OR " + //
			"(role_level = "+RoleModel.ROLE_LEVEL_WorkshopAdmin+" AND workshop_id = ?)" + //
			" OR " + //
			"(role_level = "+RoleModel.ROLE_LEVEL_WorkcenterAdmin+" AND workcenter_id = ?)" + //
			" OR " + //
			"(role_level = "+RoleModel.ROLE_LEVEL_User+" AND workcenter_id = ?)" + //
			")";

	/**
	 * 根据用户手机号查找用户信息列表
	 * 
	 * @param phoneNum 手机号
	 * @return 用户信息列表
	 */
	public List<UserModel> getUserModelsByPhone(String phoneNum) {
		Object[] args = new Object[1];
		args[0] = phoneNum;
		List<UserModel> list = jdbcTemplate.query(Sql_GetModels_By_Phone, args, beanPropertyRowMapper);
		return list;
	}

	private static String Sql_GetModels_By_Phone = "SELECT * FROM jdevice_c_user WHERE phone = ?";

	public UserModel getUserModelByDeviceToken(String deviceToken) {
		Object[] args = new Object[1];
		args[0] = deviceToken;
		List<UserModel> list = jdbcTemplate.query(Sql_GetModels_By_DeviceToken, args, beanPropertyRowMapper);
		if (list!=null&&list.size()>0) {
			return list.get(0);
		}
		return null;
	}

	private static String Sql_GetModels_By_DeviceToken = "SELECT * FROM jdevice_c_user WHERE device_token = ?";
	
	public int getReviewUserNumByOrganizationId(String id) {
		Object[] args = new Object[1];
		args[0] = id;
		return jdbcTemplate.queryForObject(Sql_ReviewUserNum_By_OrganizationId, args, Integer.class);
	}

	private static String Sql_ReviewUserNum_By_OrganizationId = "SELECT COUNT(*) FROM jdevice_c_user WHERE (role_level<="
			+ RoleModel.ROLE_LEVEL_Browser + " Or status=" + UserModel.STATUS_VAL_Applying + ") AND organization_id=?";


	public List<UserModel> setUserPassword(List<UserModel> list, String value) {
		for (UserModel userModel : list) {
			userModel.setPassword(value);
		}
		return list;
	}
	
	private BeanPropertyRowMapper<UserModel> beanPropertyRowMapper = new BeanPropertyRowMapper<UserModel>(
			UserModel.class);
	
	public boolean changeFactory(long workshopId, long factoryId) {
		Object[] args = new Object[2];
		int i=0;
		args[i++] = factoryId;
		args[i++] = workshopId;
		return jdbcTemplate.update(Sql_ChangeUserFactory_By_WorkshopId, args)>0;
	}
	
	private static String Sql_ChangeUserFactory_By_WorkshopId="UPDATE jdevice_c_user SET factory_id=? WHERE workshop_id=?;";

	public boolean changeWorkshop(long workcenterId,long workshopId, long factoryId) {
		Object[] args = new Object[3];
		int i=0;
		args[i++] = workshopId;
		args[i++] = factoryId;
		args[i++] = workcenterId;
		return jdbcTemplate.update(Sql_ChangeUserWorkshop_By_WorkcenterId, args)>0;
	}
	
	private static String Sql_ChangeUserWorkshop_By_WorkcenterId="UPDATE jdevice_c_user SET workshop_id=?,factory_id=? WHERE workcenter_id=?;";
	
	public boolean changeWorkcenter(long userId,long workcenterId,long workshopId, long factoryId) {
		Object[] args = new Object[4];
		int i=0;
		args[i++] = workcenterId;
		args[i++] = workshopId;
		args[i++] = factoryId;
		args[i++] = userId;
		return jdbcTemplate.update(Sql_ChangeUserWorkcenter_By_DeviceId, args)>0;
	}
	
	private static String Sql_ChangeUserWorkcenter_By_DeviceId = "UPDATE jdevice_c_user SET workcenter_id=?,workshop_id=?,factory_id=? WHERE id=?;";
}
