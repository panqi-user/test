package cn.sd.zhidetec.jdevice.dao;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import cn.sd.zhidetec.jdevice.Starter;
import cn.sd.zhidetec.jdevice.config.DataSourceConfiger;
import cn.sd.zhidetec.jdevice.model.DeviceExtendModel;

@Service
public class DeviceExtendModelDao {
	
	@Autowired
	@Qualifier(DataSourceConfiger.BEAN_NAME_JdeviceJdbcTemplate)
	protected JdbcTemplate jdbcTemplate;
	protected Logger logger = LoggerFactory.getLogger(getClass());
	private BeanPropertyRowMapper<DeviceExtendModel> beanPropertyRowMapper = new BeanPropertyRowMapper<DeviceExtendModel>(
			DeviceExtendModel.class);
	
	public List<DeviceExtendModel> getFieldModelsByDeviceId(long deviceId,String organizationId) {
		Object[] args=new Object[2];
		int i=0;
		args[i++]=deviceId;
		args[i++]=organizationId;
		return jdbcTemplate.query(Sql_GetModels_By_DeviceId, args, beanPropertyRowMapper);
	}
	private static String Sql_GetModels_By_DeviceId="SELECT * FROM  jdevice_c_device_extend WHERE device_id=? AND organization_id=?";

	public boolean deleteFieldModelById(long id,String organizationId) {
		Object[] args=new Object[2];
		int i=0;
		args[i++]=id;
		args[i++]=organizationId;
		return jdbcTemplate.update(Sql_DeleteModel_By_Id, args)>0;
	}
	private static String Sql_DeleteModel_By_Id="DELETE FROM jdevice_c_device_extend WHERE id=? AND organization_id=?";
	
	public boolean deleteFieldModelByDeviceId(long deviceId,String organizationId) {
		Object[] args=new Object[2];
		int i=0;
		args[i++]=deviceId;
		args[i++]=organizationId;
		return jdbcTemplate.update(Sql_DeleteModel_By_DeviceId, args)>0;
	}
	private static String Sql_DeleteModel_By_DeviceId="DELETE FROM jdevice_c_device_extend WHERE device_id=? AND organization_id=?";

	public boolean setFieldModel(DeviceExtendModel deviceFieldModel) {
		if (deviceFieldModel==null) {
			return false;
		}
		//新建
		if (deviceFieldModel.getId()<=0) {
			Object[] args=new Object[6];
			int i=0;
			deviceFieldModel.setId(Starter.IdMaker.nextId());
			args[i++]=deviceFieldModel.getId();
			args[i++]=deviceFieldModel.getCode();
			args[i++]=deviceFieldModel.getName();
			args[i++]=deviceFieldModel.getValue();
			args[i++]=deviceFieldModel.getDeviceId();
			args[i++]=deviceFieldModel.getOrganizationId();
			return jdbcTemplate.update(Sql_InsertModel, args)>0;
		}
		//修改
		else {
			Object[] args=new Object[6];
			int i=0;
			args[i++]=deviceFieldModel.getCode();
			args[i++]=deviceFieldModel.getName();
			args[i++]=deviceFieldModel.getValue();
			args[i++]=deviceFieldModel.getDeviceId();
			args[i++]=deviceFieldModel.getOrganizationId();
			args[i++]=deviceFieldModel.getId();
			return jdbcTemplate.update(Sql_UpdateModel, args)>0;
		}
	}
	private static String Sql_InsertModel="INSERT INTO jdevice_c_device_extend (id,code,name,value,device_id,organization_id) VALUES (?,?,?,?,?,?)";
	private static String Sql_UpdateModel="UPDATE jdevice_c_device_extend SET code=?,name=?,value=?,device_id=?,organization_id=? WHERE id=?";

}
