package cn.sd.zhidetec.jdevice.dao;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import cn.sd.zhidetec.jdevice.config.DataSourceConfiger;
import cn.sd.zhidetec.jdevice.model.DeviceTempHumiModel;

@Service
public class DeviceTempHumiModelDao {

	@Autowired
	@Qualifier(DataSourceConfiger.BEAN_NAME_JdeviceJdbcTemplate)
	protected JdbcTemplate jdbcTemplate;
	protected Logger logger = LoggerFactory.getLogger(getClass());
	private BeanPropertyRowMapper<DeviceTempHumiModel> beanPropertyRowMapper = new BeanPropertyRowMapper<DeviceTempHumiModel>(
			DeviceTempHumiModel.class);
	
	public boolean addModels(List<DeviceTempHumiModel> list) {
		if (list==null||list.size()<=0) {
			return false;
		}
		//插入
		int i = 0;
		List<Object[]> batchArgs=new ArrayList<Object[]>();
		for (DeviceTempHumiModel model : list) {
			i = 0;
			Object[] args = new Object[8];
			args[i++] = model.getTimeMs();
			args[i++] = model.getTemperature();
			args[i++] = model.getHumidity();
			args[i++] = model.getDeviceId();
			args[i++] = model.getWorkcenterId();
			args[i++] = model.getWorkshopId();
			args[i++] = model.getFactoryId();
			args[i++] = model.getOrganizationId();
			batchArgs.add(args);
		}
		jdbcTemplate.batchUpdate(Sql_InsertHistoryModels, batchArgs);
		return true;
	}
	private static String Sql_InsertHistoryModels="INSERT INTO jdevice_h_device_temp_humi(time_ms,temperature,humidity,device_id,workcenter_id,workshop_id,factory_id,organization_id) VALUES(?,?,?,?,?,?,?,?)";
	/**
	 * 更新设备最新的温湿度记录，如果不存在则新建
	 * */
	public boolean setDeviceLastModel(DeviceTempHumiModel model) {
		if (model==null) {
			return false;
		}
		//获取最新的实例
		DeviceTempHumiModel lastModel=getDeviceLastModel(model.getDeviceId());
		//新建
		if (lastModel==null) {
			int i = 0;
			Object[] args = new Object[8];
			args[i++] = model.getTimeMs();
			args[i++] = model.getTemperature();
			args[i++] = model.getHumidity();
			args[i++] = model.getDeviceId();
			args[i++] = model.getWorkcenterId();
			args[i++] = model.getWorkshopId();
			args[i++] = model.getFactoryId();
			args[i++] = model.getOrganizationId();
			return jdbcTemplate.update(Sql_InsertLastModel, args)>0;
		}
		//修改
		else if(lastModel.getTimeMs()<model.getTimeMs()){
			int i = 0;
			Object[] args = new Object[4];
			args[i++] = model.getTimeMs();
			args[i++] = model.getTemperature();
			args[i++] = model.getHumidity();
			args[i++] = model.getDeviceId();
			return jdbcTemplate.update(Sql_UpdateLastModel_By_DeviceId, args)>0;
		}
		return false;
	}
	private static String Sql_InsertLastModel="INSERT INTO jdevice_c_device_temp_humi(time_ms,temperature,humidity,device_id,workcenter_id,workshop_id,factory_id,organization_id) VALUES(?,?,?,?,?,?,?,?);";
	private static String Sql_UpdateLastModel_By_DeviceId="UPDATE jdevice_c_device_temp_humi SET time_ms=?,temperature=?,humidity=? WHERE device_id=?;";
	/**
	 * 获取设备最新的温湿度
	 * */
	public DeviceTempHumiModel getDeviceLastModel(long deviceId) {
		if (deviceId<=0) {
			return null;
		}
		Object[] args = new Object[1];
		args[0] = deviceId;
		List<DeviceTempHumiModel> list = jdbcTemplate.query(Sql_GetLastModel_By_DeviceId, args,beanPropertyRowMapper);
		if (list!=null&&list.size()>0) {
			return list.get(0);
		}
		return null;
	}
	private static String Sql_GetLastModel_By_DeviceId="SELECT * FROM jdevice_c_device_temp_humi WHERE device_id=?";
	
	public List<DeviceTempHumiModel> getDeviceModels(long deviceId,long beginTimeMs) {
		List<DeviceTempHumiModel> list = null;
		if (deviceId<=0) {
			return list;
		}
		Object[] args = new Object[2];
		args[0] = deviceId;
		args[1] = beginTimeMs;
		list = jdbcTemplate.query(Sql_GetModels_By_DeviceId_BeginTimeMs, args,beanPropertyRowMapper);
		return list;
	}
	private static String Sql_GetModels_By_DeviceId_BeginTimeMs="SELECT * FROM jdevice_h_device_temp_humi WHERE device_id=? AND time_ms>=?";
	
	public List<DeviceTempHumiModel> getWorkshopLastModels(long workshopId) {
		if (workshopId<=0) {
			return null;
		}
		Object[] args = new Object[1];
		args[0] = workshopId;
		return jdbcTemplate.query(Sql_GetLastModels_By_WorkshopId, args,beanPropertyRowMapper);
	}
	private static String Sql_GetLastModels_By_WorkshopId="SELECT * FROM jdevice_c_device_temp_humi WHERE workshop_id=?";
	
	public List<DeviceTempHumiModel> getWorkcenterLastModels(long workcenterId) {
		if (workcenterId<=0) {
			return null;
		}
		Object[] args = new Object[1];
		args[0] = workcenterId;
		return jdbcTemplate.query(Sql_GetLastModels_By_WorkcenterId, args,beanPropertyRowMapper);
	}
	private static String Sql_GetLastModels_By_WorkcenterId="SELECT * FROM jdevice_c_device_temp_humi WHERE workcenter_id=?";
	
	public boolean changeWorkcenter(long deviceId, long workcenterId, long workshopId, long factoryId) {
		Object[] args = new Object[4];
		int i=0;
		args[i++] = workcenterId;
		args[i++] = workshopId;
		args[i++] = factoryId;
		args[i++] = deviceId;
		return jdbcTemplate.update(Sql_ChangeWorkcenter_By_DeviceId, args)>0;
	}
	
	private static String Sql_ChangeWorkcenter_By_DeviceId = "UPDATE jdevice_c_device_temp_humi SET workcenter_id=?,workshop_id=?,factory_id=? WHERE device_id=?;";
	
	public boolean changeWorkshop(long workcenterId,long workshopId, long factoryId) {
		Object[] args = new Object[3];
		int i=0;
		args[i++] = workshopId;
		args[i++] = factoryId;
		args[i++] = workcenterId;
		return jdbcTemplate.update(Sql_ChangeWorkshop_By_WorkcenterId, args)>0;
	}
	
	private static String Sql_ChangeWorkshop_By_WorkcenterId="UPDATE jdevice_c_device_temp_humi SET workshop_id=?,factory_id=? WHERE workcenter_id=?;";
	
	public boolean changeFactory(long workshopId,long factoryId) {
		Object[] args = new Object[2];
		int i=0;
		args[i++] = factoryId;
		args[i++] = workshopId;
		return jdbcTemplate.update(Sql_ChangeFactory_By_WorkshopId, args)>0;
	}
	
	private static String Sql_ChangeFactory_By_WorkshopId="UPDATE jdevice_c_device_temp_humi SET factory_id=? WHERE workshop_id=?;";
}
