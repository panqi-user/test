package cn.sd.zhidetec.jdevice.controller;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import cn.sd.zhidetec.jdevice.HttpResponseMsg;
import cn.sd.zhidetec.jdevice.dao.UserModelDao;
import cn.sd.zhidetec.jdevice.model.RoleModel;
import cn.sd.zhidetec.jdevice.model.UserModel;
import cn.sd.zhidetec.jdevice.model.WarnLevelModel;
import cn.sd.zhidetec.jdevice.service.RBuilderService;
import cn.sd.zhidetec.jdevice.util.StringUtil;
import cn.sd.zhidetec.jdevice.service.FileUploadService;

/**
 * 人员管理Controller
 */
@Controller
@RequestMapping("/user")
public class UserController {

	@Autowired
	protected FileUploadService fileUploadService;
	@Autowired
	protected UserModelDao userModelDao;
	@Autowired
	protected RBuilderService rBuilderService;
	protected Logger logger = LoggerFactory.getLogger(getClass());

	/**
	 * 上传用户头像
	 * @param multipartFile 文件名
	 * @return 返回给前端的操作结果
	 * */
	@RequestMapping(path="/upload/photoImg")
	@ResponseBody
	public RBuilderService.Response upload(HttpSession httpSession,//
			@RequestParam("file")MultipartFile multipartFile) {
		RBuilderService.Response response = rBuilderService.build(null,null);
		//用户权限判断
		if (LoginController.hadPermission(httpSession,//
				RoleModel.ROLE_LEVEL_Browser,//
				rBuilderService,//
				response)==false) {
			return response;
		}
		//获取当前登录用户
		UserModel userModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		//保存到文件
		if (fileUploadService.uploadUserPhotoImg(multipartFile,userModel)) {
			//保存到数据库
			if (userModelDao.setUserPhotoUrlByPhone(userModel.getPhone(), userModel.getPhotoUrl())) {
				return rBuilderService.update(response,HttpResponseMsg.HEAD_MSG_SUCCESS_DONE,userModel.getPhotoUrl());		
			}else {
				return rBuilderService.update(response,HttpResponseMsg.HEAD_MSG_FAIL_DONE,null);
			}		
		}else {
			return rBuilderService.update(response,HttpResponseMsg.HEAD_MSG_FAIL_DONE,null);
		}
	}
	
	/**
	 * 根据用户ID获取用户对象
	 * 
	 * @param id 用户Id
	 * @return 返回给前端的操作结果
	 */
	@RequestMapping("/getUserModelById")
	@ResponseBody
	public RBuilderService.Response getUserModelById(HttpSession httpSession,@RequestParam("id")long id) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_User, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		UserModel sessionUserModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		UserModel userModel = userModelDao.getUserModelById(id);
		if (userModel!=null) {
			if (sessionUserModel.getRoleLevel() <= RoleModel.ROLE_LEVEL_WorkcenterAdmin//
					&&sessionUserModel.getWorkcenterId()==userModel.getWorkcenterId()) {
				return rBuilderService.update(response, //
						HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, //
						userModel);
			}
			if (sessionUserModel.getRoleLevel() == RoleModel.ROLE_LEVEL_WorkshopAdmin//
					&&sessionUserModel.getWorkshopId()==userModel.getWorkshopId()) {
				return rBuilderService.update(response, //
						HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, //
						userModel);
			}
			if (sessionUserModel.getRoleLevel() == RoleModel.ROLE_LEVEL_FactoryAdmin//
					&&sessionUserModel.getFactoryId()==userModel.getFactoryId()) {
				return rBuilderService.update(response, //
						HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, //
						userModel);
			}
			if (sessionUserModel.getRoleLevel() >= RoleModel.ROLE_LEVEL_OrganizationAdmin//
					&&sessionUserModel.getOrganizationId().equals(userModel.getOrganizationId())) {
				return rBuilderService.update(response, //
						HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, //
						userModel);
			}
		}
		return rBuilderService.update(response, //
				HttpResponseMsg.HEAD_MSG_NOT_FOUND_DATA, //
				null);
	}
	
	/**
	 * 获取用户信息列表，当用户编号为空时返回全部
	 * 
	 * @param userCode 用户编号
	 * @return 返回给前端的操作结果
	 */
	@RequestMapping("/getUserModels")
	@ResponseBody
	public RBuilderService.Response getUserModels(HttpSession httpSession) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_User, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		// 根据用户权限返回数据
		UserModel userModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		if (userModel.getRoleLevel() <= RoleModel.ROLE_LEVEL_User) {
			return rBuilderService.update(response, //
					HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, //
					userModelDao.getUserModelsById(userModel.getId()));
		}
		if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_WorkcenterAdmin) {
			return rBuilderService.update(response, //
					HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, //
					userModelDao.getUserModelsByWorkcenterId(userModel.getWorkcenterId()));
		}
		if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_WorkshopAdmin) {
			return rBuilderService.update(response, //
					HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, //
					userModelDao.getUserModelsByWorkshopId(userModel.getWorkshopId()));
		}
		if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_FactoryAdmin) {
			return rBuilderService.update(response, //
					HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, //
					userModelDao.getUserModelsByFactoryId(userModel.getFactoryId()));
		}
		if (userModel.getRoleLevel() >= RoleModel.ROLE_LEVEL_OrganizationAdmin) {
			return rBuilderService.update(response, //
					HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, //
					userModelDao.getUserModelsByOrganizationId(userModel.getOrganizationId()));
		}
		return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_DATA, null);
	}
	
	/**
	 * 加入组织
	 * 
	 * @param httpSession Session会话
	 * @return 返回给前端的操作结果
	 */
	@RequestMapping(path = "/addOrganization")
	@ResponseBody
	public RBuilderService.Response addOrganization(HttpSession httpSession, //
			@RequestParam("organizationId") String organizationId) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_Browser, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		//自动转成大写
		if (!StringUtil.isEmpty(organizationId)) {
			organizationId=organizationId.toUpperCase();
		}
		// 进行操作
		UserModel sessionUserModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		// 加入组织，职权降低
		sessionUserModel.setOrganizationId(organizationId);
		sessionUserModel.setRoleLevel(RoleModel.ROLE_LEVEL_Browser);
		sessionUserModel.setWarnLevel(WarnLevelModel.WarnLevel_Role);
		sessionUserModel.setStatus(UserModel.STATUS_VAL_Applying);
		sessionUserModel.setFactoryId(-1);
		sessionUserModel.setWorkcenterId(-1);
		sessionUserModel.setWorkshopId(-1);
		// 清缓存中的组织
		httpSession.setAttribute(OrganizationController.SESSION_KEY_ORGANIZATION, null);
		// 保存
		if (userModelDao.setUserModel(sessionUserModel)) {
			return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, null);
		} else {
			return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
		}
	}
	/**
	 * 自己退出组织
	 * 
	 * @param httpSession Session会话
	 * @return 返回给前端的操作结果
	 */
	@RequestMapping(path = "/quitOrganizationBySelf")
	@ResponseBody
	public RBuilderService.Response quitOrganizationBySelf(HttpSession httpSession) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_Browser, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		// 进行操作
		UserModel sessionUserModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		// 自己更换组织，权限降低
		if (userModelDao.removeOrganization(sessionUserModel)) {
			return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, null);
		} else {
			return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
		}
	}
	/**
	 * 从组织中移除指定的用户
	 * 
	 * @param httpSession Session会话
	 * @param userModel   用户信息
	 * @return 返回给前端的操作结果
	 */
	@RequestMapping(path = "/removeOrganization")
	@ResponseBody
	public RBuilderService.Response removeOrganization(HttpSession httpSession, //
			@RequestBody UserModel userModel) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_Browser, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		// 进行操作
		UserModel sessionUserModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		// 自己更换组织，权限降低
		if (userModel.getOrganizationId().equals(sessionUserModel.getOrganizationId())//
				&& userModel.getId()==sessionUserModel.getId()) {
			if (userModelDao.removeOrganization(sessionUserModel)) {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, null);
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
			}
		}
		// 由组织管理员进行移除
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_OrganizationAdmin, //
				rBuilderService, //
				response) == false) {
			return response;
		} else {
			if (sessionUserModel.getOrganizationId().equals(userModel.getOrganizationId())) {
				if (userModelDao.removeOrganization(userModel)) {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, null);
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
				}
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
			}
		}
	}
	/**
	 * 用户中心内设置用户信息
	 * 
	 * @param httpSession Session会话
	 * @param userModel   用户信息
	 * @return 返回给前端的操作结果
	 */
	@RequestMapping(path = "/setUserInfoSelf")
	@ResponseBody
	public RBuilderService.Response setUserInfoSelf(HttpSession httpSession, //
			@RequestBody UserModel userModel) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_Browser, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		// 进行操作
		UserModel sessionUserModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		if (sessionUserModel.getId()==userModel.getId()) {
			if (userModelDao.setUserInfoById(userModel)) {
				//更改session中的User信息
				sessionUserModel.copyInfoSelf(userModel);
				//需要完善个人信息
				if (sessionUserModel.needPerfectInfo()) {
					return rBuilderService.update(response,HttpResponseMsg.HEAD_MSG_LOGIN_NEED_PERFECT_INFO, sessionUserModel);
				}
				//需要加入组织
				if (sessionUserModel.noOrganization()) {
					return rBuilderService.update(response,HttpResponseMsg.HEAD_MSG_NO_ORGANIZATION, sessionUserModel);
				}
				//组织审核中
				if (sessionUserModel.underReviewByOrganization()) {
					return rBuilderService.update(response,HttpResponseMsg.HEAD_MSG_UNDER_REVIEW_BY_ORGANIZATION, sessionUserModel);
				}
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, null);
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
			}
		}else {
			return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
		}
	}
	
	/**
	 * 人员管理中设置用户信息
	 * 
	 * @param httpSession Session会话
	 * @param userModel   用户信息
	 * @return 返回给前端的操作结果
	 */
	@RequestMapping(path = "/setUserModel")
	@ResponseBody
	public RBuilderService.Response setUserModel(HttpSession httpSession, //
			@RequestBody UserModel userModel) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_Browser, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		// 进行操作
		UserModel sessionUserModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		// 自己更换组织，权限降低
		if (!userModel.getOrganizationId().equals(sessionUserModel.getOrganizationId())//
				&& userModel.getId()==sessionUserModel.getId()) {
			userModel.setRoleLevel(RoleModel.ROLE_LEVEL_Browser);
			userModel.setStatus(UserModel.STATUS_VAL_Applying);
			if (userModelDao.setUserModel(userModel)) {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, null);
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
			}
		}
		// 无法赋予高于操作用户的权限和手机号
		if (userModel.getRoleLevel()>=sessionUserModel.getRoleLevel()) {
			userModel.setRoleLevel(sessionUserModel.getRoleLevel());
		}
		// 根据用户权限进行修改操作
		boolean successed=false;
		if (sessionUserModel.getRoleLevel() <= RoleModel.ROLE_LEVEL_User) {
			if (sessionUserModel.getId()==userModel.getId()//
					&& sessionUserModel.getWorkcenterId()==userModel.getWorkcenterId()//
					&& sessionUserModel.getWorkshopId()==userModel.getWorkshopId()//
					&& sessionUserModel.getFactoryId()==userModel.getFactoryId()//
					&& sessionUserModel.getOrganizationId().equals(userModel.getOrganizationId())) {
				if (userModelDao.setUserModel(userModel)) {
					successed=true;
				} else {
					successed=false;
				}
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
			}
		}
		if (sessionUserModel.getRoleLevel() == RoleModel.ROLE_LEVEL_WorkcenterAdmin) {
			if (sessionUserModel.getWorkcenterId()==userModel.getWorkcenterId()//
					&& sessionUserModel.getWorkshopId()==userModel.getWorkshopId()//
					&& sessionUserModel.getFactoryId()==userModel.getFactoryId()//
					&& sessionUserModel.getOrganizationId().equals(userModel.getOrganizationId())) {
				if (userModelDao.setUserModel(userModel)) {
					successed=true;
				} else {
					successed=false;
				}
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
			}
		}
		if (sessionUserModel.getRoleLevel() == RoleModel.ROLE_LEVEL_WorkshopAdmin) {
			if (sessionUserModel.getWorkshopId()==userModel.getWorkshopId()//
					&& sessionUserModel.getFactoryId()==userModel.getFactoryId()//
					&& sessionUserModel.getOrganizationId().equals(userModel.getOrganizationId())) {
				if (userModelDao.setUserModel(userModel)) {
					successed=true;
				} else {
					successed=false;
				}
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
			}
		}
		if (sessionUserModel.getRoleLevel() == RoleModel.ROLE_LEVEL_FactoryAdmin) {
			if (sessionUserModel.getFactoryId()==userModel.getFactoryId()//
					&& sessionUserModel.getOrganizationId().equals(userModel.getOrganizationId())) {
				if (userModelDao.setUserModel(userModel)) {
					successed=true;
				} else {
					successed=false;
				}
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
			}
		}
		if (sessionUserModel.getRoleLevel() >= RoleModel.ROLE_LEVEL_OrganizationAdmin) {
			if (sessionUserModel.getOrganizationId().equals(userModel.getOrganizationId())) {
				if (userModelDao.setUserModel(userModel)) {
					successed=true;
				} else {
					successed=false;
				}
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
			}
		}
		if (successed) {
			//session更改
			if (sessionUserModel.getId()==userModel.getId()) {
				sessionUserModel.setOrganizationId(userModel.getOrganizationId());
				sessionUserModel.setFactoryId(userModel.getFactoryId());
				sessionUserModel.setWorkshopId(userModel.getWorkshopId());
				sessionUserModel.setWorkcenterId(userModel.getWorkcenterId());
				sessionUserModel.setWarnLevel(userModel.getWarnLevel());
				sessionUserModel.setRoleLevel(userModel.getRoleLevel());
				sessionUserModel.setStatus(userModel.getStatus());
				sessionUserModel.setPhone(userModel.getPhone());
				sessionUserModel.setCode(userModel.getCode());
				sessionUserModel.setName(userModel.getName());	
			}
			return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, null);
		} else {
			return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
		}
	}
}
