package cn.sd.zhidetec.jdevice.client;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingDeque;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;

import cn.sd.zhidetec.jdevice.dao.UserModelDao;
import cn.sd.zhidetec.jdevice.model.MessageModel;
import cn.sd.zhidetec.jdevice.util.StringUtil;

/**
 * 友盟消息推送服务
 * */
@Service
public class MsgPushClient_UMeng extends Thread{

	@Value("${umeng.appkey}")
	private String appkey = "5e8b2573167edd878e000035";
	@Value("${umeng.appMasterSecret}")
	private String appMasterSecret = "lcxlymfnt1wnoxzxecs0esy4bjy6kwxs";
	@Value("${umeng.url}")
	private String url = "https://msgapi.umeng.com/api/send";
	@Autowired
	protected UserModelDao userModelDao;
	protected Logger logger = LoggerFactory.getLogger(getClass());

	private boolean started;
	private Queue<MessageModel> messageModels = new LinkedBlockingDeque<>();
	private Object messageModelsLockObj = new Object();
	public void stopService() {
		started=false;
	}
	public boolean started() {
		return started;
	}
	public void sendMessageModel(MessageModel messageModel) {
		synchronized (messageModelsLockObj) {
			messageModels.add(messageModel);
		}
	}
	@Override
	public void run() {
		started=true;
		logger.info(getClass().getSimpleName()+" Started");
		while (started) {
			try {
				Thread.sleep(1);
				if (messageModels.size()<=0) {
					Thread.sleep(10);
					continue;
				}
				MessageModel messageModel=messageModels.peek();
				if (messageModel==null) {
					messageModels.poll();
					Thread.sleep(10);
					continue;
				}
				//获取所有设备Token
				List<String> list =null;
				if (messageModel.getType()==MessageModel.TYPE_System_Notice) {
					list = userModelDao.getAllDeviceTokenList();
				}else {
					list = userModelDao.getDeviceTokenList(messageModel.getOrganizationId(),//
							messageModel.getFactoryId(),//
							messageModel.getWorkshopId(),//
							messageModel.getWorkcenterId(),//
							DEVICE_BRAND,//
							messageModel.getWarnLevel());
				}
				if (list==null||list.size()<=0) {
				}else {
					//开始推送
					syncSendPushMessage(list, messageModel);	
				}
				messageModels.poll();
			}catch (Exception e) {
				logger.error(getClass().getSimpleName()+" 消息推送服务异常", e);
			}
		}
	}
	/**
	 * 以线程同步的方式推送消息
	 * @param deviceTokenList 目标设备Token
	 * @param messageModel Andon消息
	 * */
	public void syncSendPushMessage(List<String> deviceTokenList,MessageModel messageModel) throws IOException {  
        //目标设备Token
        StringBuffer deviceTokens = new StringBuffer("");
        for (String deviceToken : deviceTokenList) {
            deviceTokens.append(deviceToken+",");	
		}
        //
        JSONObject payloadExtra = new JSONObject();
        payloadExtra.put("WEBVIEW_LOAD_URL",""+messageModel.getUrl());
        //
        JSONObject payloadBody = new JSONObject();
        payloadBody.put("ticker", ""+messageModel.getTitle());
        payloadBody.put("title", ""+messageModel.getTitle());
        payloadBody.put("text", ""+messageModel.getContent());
        payloadBody.put("after_open", "go_activity");
        payloadBody.put("activity", "cn.sd.zhidetec.jdevice.app.MainActivity");
        //
        JSONObject payload = new JSONObject();
        payload.put("display_type", "notification");
        payload.put("body", payloadBody);
        payload.put("extra", payloadExtra);
        //
        JSONObject body = new JSONObject();
        body.put("appkey",appkey);
        body.put("timestamp",""+messageModel.getTimeMs());
        body.put("type","listcast");
        body.put("device_tokens",deviceTokens.toString());
        body.put("payload", payload);
        //
        String postBody = body.toString();
        logger.debug(getClass().getSimpleName()+" Send Request : "+postBody);
        //签名
		String sign = DigestUtils.md5Hex(("POST" + url + postBody + appMasterSecret).getBytes("utf8"));
		//推送
        httpPost(url+"?sign=" + sign, postBody, 5000, 5000);
    }
	/**
	 * 向指定URL发送POST请求
	 * @param httpUrl Url地址
	 * @param data 数据域
	 * @param connectTimeout 连接超时
	 * @param readTimeout 读取反馈超时
	 * */
	private String httpPost(String httpUrl,String data, int connectTimeout, int readTimeout) throws IOException
    {
        OutputStream outPut = null;
        HttpURLConnection urlConnection = null;
        InputStream in = null;
        try
        {
            URL url = new URL(httpUrl);
            urlConnection = (HttpURLConnection)url.openConnection();          
            urlConnection.setRequestMethod("POST");
            urlConnection.setDoOutput(true);
            urlConnection.setDoInput(true);
            urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
            urlConnection.setConnectTimeout(connectTimeout);
            urlConnection.setReadTimeout(readTimeout);
            urlConnection.connect();
            
            // POST data
            outPut = urlConnection.getOutputStream();
            outPut.write(data.getBytes("UTF-8"));
            outPut.flush();
            
            // read response
            if (urlConnection.getResponseCode() < 400)
            {
                in = urlConnection.getInputStream();
            }
            else
            {
                in = urlConnection.getErrorStream();
            }
            
            List<String> lines = IOUtils.readLines(in, urlConnection.getContentEncoding());
            StringBuffer strBuf = new StringBuffer();
            for (String line : lines)
            {
                strBuf.append(line);
            }
            logger.debug(getClass().getSimpleName()+" Get Response : "+strBuf.toString());
            return strBuf.toString();
        }
        finally
        {
            IOUtils.closeQuietly(outPut);
            IOUtils.closeQuietly(in);
            if (urlConnection != null)
            {
                urlConnection.disconnect();
            }
        }
    }
	
	public final static String DEVICE_BRAND="Umeng";
	public static boolean useUMengPush(String deviceBrand) {
		if (StringUtil.isEmpty(deviceBrand)) {
			return false;
		}
		if (deviceBrand.equalsIgnoreCase("umeng")) {
			return true;
		}
		return false;
	}
	
	public static void main(String[] args) {
		MsgPushClient_UMeng uMengPushService=new MsgPushClient_UMeng();
		List<String> tokenList=new ArrayList<String>();
		tokenList.add("AqYToBtx22VWBlZYBaWjgi9jfhWG4bIsGIsMtxAxkuDD");
		MessageModel messageModel=new MessageModel();
		messageModel.setTitle("title");
		messageModel.setContent("content");
		messageModel.setTimeMs(System.currentTimeMillis());
		try {
			uMengPushService.syncSendPushMessage(tokenList, messageModel);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
