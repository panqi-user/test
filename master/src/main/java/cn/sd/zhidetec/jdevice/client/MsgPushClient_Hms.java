package cn.sd.zhidetec.jdevice.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.MessageFormat;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingDeque;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import cn.sd.zhidetec.jdevice.dao.UserModelDao;
import cn.sd.zhidetec.jdevice.model.MessageModel;
import cn.sd.zhidetec.jdevice.util.StringUtil;

/**
 * 华为HMS推送服务
 * */
@Service
public class MsgPushClient_Hms extends Thread{

	@Value("${hms.appSecret}")
	private String appSecret = "326a07171d7de0af1dd7376a2c403430d663b59b4515d2b18191bdec6079b3fa";
	@Value("${hms.appId}")
	private String appId = "101981743";
	@Value("${hms.tokenUrl}")
	private String tokenUrl = "https://oauth-login.cloud.huawei.com/oauth2/v2/token";
	@Value("${hms.apiBaseUrl}")
	private String apiBaseUrl = "https://push-api.cloud.huawei.com/v1";
	@Autowired
	protected UserModelDao userModelDao;
	protected Logger logger = LoggerFactory.getLogger(getClass());

	private String accessToken;
	private String tokenType;
	private String authorizationHead;
	private String apiUrl;
	private long tokenExpiredTime;
	private boolean started;
	private Queue<MessageModel> messageModels = new LinkedBlockingDeque<>();
	private Object messageModelsLockObj = new Object();
	public void stopService() {
		started=false;
	}
	public boolean started() {
		return started;
	}
	public void sendMessageModel(MessageModel messageModel) {
		synchronized (messageModelsLockObj) {
			messageModels.add(messageModel);
		}
	}
	@Override
	public void run() {
		started=true;
		logger.info(getClass().getSimpleName()+" Started");
		while (started) {
			try {
				Thread.sleep(1);
				if (messageModels.size()<=0) {
					Thread.sleep(10);
					continue;
				}
				MessageModel messageModel=messageModels.peek();
				if (messageModel==null) {
					messageModels.poll();
					Thread.sleep(10);
					continue;
				}
				//获取所有设备Token
				List<String> list=null;
				if (messageModel.getType()==MessageModel.TYPE_System_Notice) {
					list = userModelDao.getAllDeviceTokenList();
				}else {
					list = userModelDao.getDeviceTokenList(messageModel.getOrganizationId(),//
							messageModel.getFactoryId(),//
							messageModel.getWorkshopId(),//
							messageModel.getWorkcenterId(),//
							DEVICE_BRAND,//
							messageModel.getWarnLevel());
				}
				if (list==null||list.size()<=0) {
				}else {
					//开始推送
					syncSendPushMessage(list, messageModel);	
				}
				messageModels.poll();
			}catch (Exception e) {
				logger.error(getClass().getSimpleName()+" 消息推送服务异常", e);
			}
		}
	}
	/**
	 * 刷新身份认证信息
	 * */
	private void refreshToken() throws IOException{
		String msgBody = MessageFormat.format("grant_type=client_credentials&client_secret={0}&client_id={1}",URLEncoder.encode(appSecret, "UTF-8"), appId);
        String response = httpPost(tokenUrl,null, msgBody, 5000, 5000);
        JSONObject obj = JSONObject.parseObject(response);
        accessToken = obj.getString("access_token");
        tokenType = obj.getString("token_type");
        authorizationHead = ""+tokenType+" "+accessToken;
        tokenExpiredTime = System.currentTimeMillis() + obj.getLong("expires_in")*1000 - 5*60*1000;
        apiUrl = apiBaseUrl+"/"+appId+"/messages:send";
	}
	/**
	 * 以线程同步的方式向HMS推送消息
	 * @param deviceTokenList 目标设备Token
	 * @param messageModel Andon消息
	 * */
	public void syncSendPushMessage(List<String> deviceTokenList,MessageModel messageModel) throws IOException {
        if (tokenExpiredTime <= System.currentTimeMillis())
        {
            refreshToken();
        }      
        //目标设备Token
        JSONArray deviceTokens = new JSONArray();
        for (String deviceToken : deviceTokenList) {
            deviceTokens.add(deviceToken);	
		}
        //
        JSONObject clickAction = new JSONObject();
        clickAction.put("type", 1);
        clickAction.put("intent", "intent://cn.sd.zhidetec.jdevice.app:7080/app?#Intent;"//
        		+ "scheme=mainscheme;"//
        		+ "launchFlags=0x4000000;"//
        		+ "S.WEBVIEW_LOAD_URL="+messageModel.getUrl()+";"//
        		+ "end");
        //
        JSONObject androidNotification = new JSONObject();
        androidNotification.put("title", messageModel.getTitle());
        androidNotification.put("body", messageModel.getContent());
        androidNotification.put("click_action", clickAction);
        androidNotification.put("use_default_vibrate", true);
        androidNotification.put("use_default_light", true);
        androidNotification.put("ticker", messageModel.getTitle());
        androidNotification.put("importance", "HIGH");
        androidNotification.put("visibility", "PUBLIC");
        androidNotification.put("tag", ""+messageModel.getId());
        //
        JSONObject android = new JSONObject();
        android.put("notification", androidNotification);
        android.put("collapse_key", -1);
        //
        JSONObject notification = new JSONObject();
        notification.put("title", messageModel.getTitle());
        notification.put("body", messageModel.getContent());
        //
        JSONObject message = new JSONObject();
        message.put("notification", notification);
        message.put("android",android);
        message.put("token",deviceTokens);
        //
        JSONObject body = new JSONObject();
        body.put("validate_only",false);
        body.put("message",message);
        //
        String postBody = body.toString();
        logger.debug(getClass().getName()+" Send Request : "+postBody);
        httpPost(apiUrl,authorizationHead, postBody, 5000, 5000);
    }
	/**
	 * 向指定URL发送POST请求
	 * @param httpUrl Url地址
	 * @param authorizationHead Http身份认证头信息
	 * @param data 数据域
	 * @param connectTimeout 连接超时
	 * @param readTimeout 读取反馈超时
	 * */
	private String httpPost(String httpUrl,String authorizationHead, String data, int connectTimeout, int readTimeout) throws IOException
    {
        OutputStream outPut = null;
        HttpURLConnection urlConnection = null;
        InputStream in = null;
        try
        {
            URL url = new URL(httpUrl);
            urlConnection = (HttpURLConnection)url.openConnection();          
            urlConnection.setRequestMethod("POST");
            urlConnection.setDoOutput(true);
            urlConnection.setDoInput(true);
            urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
            if (!StringUtil.isEmpty(authorizationHead)) {
            	urlConnection.setRequestProperty("Authorization", authorizationHead);
			}
            urlConnection.setConnectTimeout(connectTimeout);
            urlConnection.setReadTimeout(readTimeout);
            urlConnection.connect();
            
            // POST data
            outPut = urlConnection.getOutputStream();
            outPut.write(data.getBytes("UTF-8"));
            outPut.flush();
            
            // read response
            if (urlConnection.getResponseCode() < 400)
            {
                in = urlConnection.getInputStream();
            }
            else
            {
                in = urlConnection.getErrorStream();
            }
            
            List<String> lines = IOUtils.readLines(in, urlConnection.getContentEncoding());
            StringBuffer strBuf = new StringBuffer();
            for (String line : lines)
            {
                strBuf.append(line);
            }
            logger.debug(getClass().getName()+" Get Response : "+strBuf.toString());
            return strBuf.toString();
        }
        finally
        {
            IOUtils.closeQuietly(outPut);
            IOUtils.closeQuietly(in);
            if (urlConnection != null)
            {
                urlConnection.disconnect();
            }
        }
    }
	
	public final static String DEVICE_BRAND="Huawei";
	public static boolean isHuaweiDevice(String deviceBrand) {
		if (StringUtil.isEmpty(deviceBrand)) {
			return false;
		}
		if (deviceBrand.equalsIgnoreCase("huawei")||deviceBrand.equalsIgnoreCase("honor")) {
			return true;
		}
		return false;
	}
}
