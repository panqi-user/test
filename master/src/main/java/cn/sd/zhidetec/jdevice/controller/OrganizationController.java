package cn.sd.zhidetec.jdevice.controller;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import cn.sd.zhidetec.jdevice.HttpResponseMsg;
import cn.sd.zhidetec.jdevice.dao.DeviceSignboardTypeModelDao;
import cn.sd.zhidetec.jdevice.dao.FactoryModelDao;
import cn.sd.zhidetec.jdevice.dao.OrganizationModelDao;
import cn.sd.zhidetec.jdevice.dao.UserModelDao;
import cn.sd.zhidetec.jdevice.model.FactoryModel;
import cn.sd.zhidetec.jdevice.model.OrganizationModel;
import cn.sd.zhidetec.jdevice.model.RoleModel;
import cn.sd.zhidetec.jdevice.model.UserModel;
import cn.sd.zhidetec.jdevice.model.WarnLevelModel;
import cn.sd.zhidetec.jdevice.service.FileUploadService;
import cn.sd.zhidetec.jdevice.service.RBuilderService;
import cn.sd.zhidetec.jdevice.util.StringUtil;

/**
 * 组织管理Controller
 * */
@Controller
@RequestMapping("/organization")
public class OrganizationController {
	
	@Autowired
	protected FileUploadService fileUploadService;
	@Autowired
	protected OrganizationModelDao organizationModelDao;
	@Autowired
	protected UserModelDao userModelDao;
	@Autowired
	protected FactoryModelDao factoryModelDao;
	@Autowired
	protected DeviceSignboardTypeModelDao deviceSignboardRuleModelDao;
	@Autowired
	protected RBuilderService rBuilderService;
	protected Logger logger = LoggerFactory.getLogger(getClass());
	
	/**
	 * 上传组织logo
	 * @param multipartFile 文件名
	 * @return 返回给前端的操作结果
	 * */
	@RequestMapping(path="/upload/logoImg")
	@ResponseBody
	public RBuilderService.Response upload(HttpSession httpSession,//
			@RequestParam("file")MultipartFile multipartFile) {
		RBuilderService.Response response = rBuilderService.build(null,null);
		//用户权限判断
		if (LoginController.hadPermission(httpSession,//
				RoleModel.ROLE_LEVEL_Browser,//
				rBuilderService,//
				response)==false) {
			return response;
		}
		//获取当前组织
		OrganizationModel organizationModel = (OrganizationModel) httpSession.getAttribute(OrganizationController.SESSION_KEY_ORGANIZATION);
		if (organizationModel==null) {
			UserModel sessionUserModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
			organizationModel = organizationModelDao.getOrganizationModel(sessionUserModel.getOrganizationId());
			httpSession.setAttribute(OrganizationController.SESSION_KEY_ORGANIZATION,organizationModel);
			if (organizationModel==null) {
				return rBuilderService.update(response,HttpResponseMsg.HEAD_MSG_FAIL_DONE,null);
			}
		}
		//保存到文件
		if (fileUploadService.uploadOrganizationLogoImg(multipartFile,organizationModel)) {
			//保存到数据库
			if (organizationModelDao.setLogoUrlById(organizationModel.getId(), organizationModel.getLogoUrl())) {
				return rBuilderService.update(response,HttpResponseMsg.HEAD_MSG_SUCCESS_DONE,organizationModel.getLogoUrl());		
			}else {
				return rBuilderService.update(response,HttpResponseMsg.HEAD_MSG_FAIL_DONE,null);
			}		
		}else {
			return rBuilderService.update(response,HttpResponseMsg.HEAD_MSG_FAIL_DONE,null);
		}
	}
	
	/**
	 * 从Session中获取组织ID,然后获取组织信息
	 * @param httpSession Session
	 * @return 组织信息
	 * */
	@RequestMapping(path="/getOrganizationModel")
	@ResponseBody
	public RBuilderService.Response getOrganizationModel(HttpSession httpSession) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_Browser, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		UserModel sessionUserModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		// 是否已经退出组织
		if (StringUtil.isEmpty(sessionUserModel.getOrganizationId())) {
			httpSession.setAttribute(OrganizationController.SESSION_KEY_ORGANIZATION, null);
			return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NOT_FOUND_DATA, null);
		}
		// 进行操作
		OrganizationModel organizationModel = (OrganizationModel) httpSession
				.getAttribute(OrganizationController.SESSION_KEY_ORGANIZATION);
		if (organizationModel == null) {
			organizationModel = organizationModelDao.getOrganizationModel(sessionUserModel.getOrganizationId());
			httpSession.setAttribute(OrganizationController.SESSION_KEY_ORGANIZATION, organizationModel);
			if (organizationModel == null) {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NOT_FOUND_DATA, null);
			}
		}
		return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, organizationModel);
	}

	/**
	 * 创建新组织
	 * @param httpSession Session
	 * @return 操作结果
	 * */
	@RequestMapping(path="/newOrganizationModel")
	@ResponseBody
	public RBuilderService.Response newOrganizationModel(HttpSession httpSession,//
			@RequestBody OrganizationModel organizationModel) {
		RBuilderService.Response response = rBuilderService.build(null,null);
		UserModel sessionUserModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		//用户权限判断	
		if (sessionUserModel==null) {
			return rBuilderService.update(response,HttpResponseMsg.HEAD_MSG_LOGIN_NEED, null);
		}
		if (sessionUserModel.getRoleLevel()!=RoleModel.ROLE_LEVEL_Browser) {
			return rBuilderService.update(response,HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
		}
		sessionUserModel.setStatus(UserModel.STATUS_VAL_Enable);
		//组织创建
		organizationModel.setId(null);
		if (organizationModelDao.setOrganizationModel(organizationModel)) {
			httpSession.setAttribute(OrganizationController.SESSION_KEY_ORGANIZATION,organizationModel);
			//默认值
			FactoryModel factoryModel=factoryModelDao.initOrganization(organizationModel);
			if (factoryModel==null) {
				return rBuilderService.update(response,HttpResponseMsg.HEAD_MSG_FAIL_DONE, factoryModel);
			}
			//提升用户权限
			sessionUserModel.setOrganizationId(organizationModel.getId());
			sessionUserModel.setRoleLevel(RoleModel.ROLE_LEVEL_OrganizationAdmin);
			sessionUserModel.setWarnLevel(WarnLevelModel.WarnLevel_Role);
			if (userModelDao.setUserModel(sessionUserModel)) {
				return rBuilderService.update(response,HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, null);					
			}else {
				return rBuilderService.update(response,HttpResponseMsg.HEAD_MSG_FAIL_DONE, sessionUserModel);
			}
		}else {
			return rBuilderService.update(response,HttpResponseMsg.HEAD_MSG_FAIL_DONE, organizationModel);
		}
	}
	
	/**
	 * 从Session中获取组织ID,然后修改组织信息
	 * @param httpSession Session
	 * @return 操作结果
	 * */
	@RequestMapping(path="/setOrganizationModel")
	@ResponseBody
	public RBuilderService.Response setOrganizationModel(HttpSession httpSession,//
			@RequestBody OrganizationModel organizationModel) {
		RBuilderService.Response response = rBuilderService.build(null,null);
		//用户权限判断
		if (LoginController.hadPermission(httpSession,//
				RoleModel.ROLE_LEVEL_OrganizationAdmin,//
				rBuilderService,//
				response)==false) {
			return response;
		}
		//进行操作
		UserModel sessionUserModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		if (organizationModel!=null) {
			organizationModel.setId(sessionUserModel.getOrganizationId());
			if (organizationModelDao.setOrganizationModel(organizationModel)) {
				httpSession.setAttribute(OrganizationController.SESSION_KEY_ORGANIZATION,organizationModel);
				return rBuilderService.update(response,HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, organizationModel);	
			}else {
				return rBuilderService.update(response,HttpResponseMsg.HEAD_MSG_FAIL_DONE, organizationModel);
			}
		}
		return rBuilderService.update(response,HttpResponseMsg.HEAD_MSG_FAIL_DONE, organizationModel);
	}
	
	public static final String SESSION_KEY_ORGANIZATION = "SESSION_ORGANIZATION";
}
