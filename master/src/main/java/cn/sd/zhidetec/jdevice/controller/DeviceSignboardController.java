package cn.sd.zhidetec.jdevice.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.sd.zhidetec.jdevice.HttpResponseMsg;
import cn.sd.zhidetec.jdevice.model.DeviceSignboardModel;
import cn.sd.zhidetec.jdevice.model.DeviceSignboardTypeModel;
import cn.sd.zhidetec.jdevice.model.DeviceSignboardStatusModel;
import cn.sd.zhidetec.jdevice.model.RoleModel;
import cn.sd.zhidetec.jdevice.model.UserModel;
import cn.sd.zhidetec.jdevice.server.DeviceSignboardServer;
import cn.sd.zhidetec.jdevice.service.DeviceService;
import cn.sd.zhidetec.jdevice.service.DeviceSignboardService;
import cn.sd.zhidetec.jdevice.service.DeviceSignboardTypeService;
import cn.sd.zhidetec.jdevice.service.RBuilderService;
import cn.sd.zhidetec.jdevice.util.StringUtil;

@Controller
@RequestMapping("/deviceSignboard")
public class DeviceSignboardController {

	@Autowired
	protected DeviceSignboardTypeService deviceSignboardTypeService;
	@Autowired
	protected DeviceSignboardService deviceSignboardService;
	@Autowired
	protected DeviceSignboardServer deviceSignboardServer;
	@Autowired
	protected DeviceService deviceService;
	@Autowired
	protected RBuilderService rBuilderService;
	protected Logger logger = LoggerFactory.getLogger(getClass());

	@RequestMapping(path = "/getServerLog")
	@ResponseBody
	public RBuilderService.Response getDeviceSignboardServerLog(HttpSession httpSession) {
		return rBuilderService.build(HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, deviceSignboardServer.getRunLogList());
	}
	
	@RequestMapping(path = "/clearServerLog")
	@ResponseBody
	public RBuilderService.Response clearDeviceSignboardServerLog(HttpSession httpSession) {
		deviceSignboardServer.clearRunLog();
		return rBuilderService.build(HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, deviceSignboardServer.getRunLogList());
	}
	
	/**
	 * 删除设备类型
	 * 
	 * @param httpSession Session会话
	 * @return 返回给前端的操作结果
	 */
	@RequestMapping(path = "/deleteDeviceStatusTypeModel")
	@ResponseBody
	public RBuilderService.Response deleteDeviceStatusTypeModel(HttpSession httpSession, //
			@RequestBody DeviceSignboardTypeModel model) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_SystemAdmin, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		// 根据用户权限操作数据
		UserModel userModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_SystemAdmin) {
			if (deviceSignboardTypeService.deleteTypeModel(model)) {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, null);
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
			}
		}
		return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
	}

	/**
	 * 删除设备状态
	 * 
	 * @param httpSession Session会话
	 * @return 返回给前端的操作结果
	 */
	@RequestMapping(path = "/deleteDeviceStatusModel")
	@ResponseBody
	public RBuilderService.Response deleteDeviceStatusModel(HttpSession httpSession, //
			@RequestBody DeviceSignboardStatusModel model) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_SystemAdmin, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		// 根据用户权限操作数据
		UserModel userModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_SystemAdmin) {
			if (deviceSignboardTypeService.deleteStatusModel(model)) {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, null);
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
			}
		}
		return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
	}

	/**
	 * 设置设备类型
	 * 
	 * @param httpSession Session会话
	 * @return 返回给前端的操作结果
	 */
	@RequestMapping(path = "/setDeviceStatusTypeModel")
	@ResponseBody
	public RBuilderService.Response setDeviceStatusTypeModel(HttpSession httpSession, //
			@RequestBody DeviceSignboardTypeModel model) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_SystemAdmin, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		// 根据用户权限操作数据
		UserModel userModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		// 新建
		if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_SystemAdmin) {
			if (deviceSignboardTypeService.setTypeModel(model)) {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, null);
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
			}
		}
		return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
	}

	/**
	 * 设置设备状态
	 * 
	 * @param httpSession Session会话
	 * @return 返回给前端的操作结果
	 */
	@RequestMapping(path = "/setDeviceStatusModel")
	@ResponseBody
	public RBuilderService.Response setDeviceStatusModel(HttpSession httpSession, //
			@RequestBody DeviceSignboardStatusModel model) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_SystemAdmin, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		// 根据用户权限操作数据
		UserModel userModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_SystemAdmin) {
			if (deviceSignboardTypeService.setStatusModel(model)) {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, null);
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
			}
		}
		return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
	}

	/**
	 * 获取设备类型
	 * 
	 * @param httpSession Session会话
	 * @return 返回给前端的操作结果
	 */
	@RequestMapping(path = "/getDeviceStatusTypeModels")
	@ResponseBody
	public RBuilderService.Response getDeviceStatusTypeModels(HttpSession httpSession) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_User, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		return rBuilderService.update(response, //
				HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, //
				deviceSignboardTypeService.getTypeModels());
	}
	
	/**
	 * 根据code获取设备标识牌
	 * 
	 * @param httpSession Session会话
	 * @return 返回给前端的操作结果
	 */
	@RequestMapping(path = "/getDeviceModelByCode")
	@ResponseBody
	public RBuilderService.Response getDeviceModelByCode(HttpSession httpSession,//
			@RequestParam("code")String code) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_User, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		return rBuilderService.update(response, //
				HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, //
				deviceSignboardService.getDeviceModelsByCode(code));
	}

	/**
	 * 根据设备ID获取设备标识牌列表
	 * 
	 * @param httpSession Session会话
	 * @return 返回给前端的操作结果
	 */
	@RequestMapping(path = "/getDeviceModelsByDeviceId")
	@ResponseBody
	public RBuilderService.Response getDeviceModelsByDeviceId(HttpSession httpSession,@RequestParam("deviceId")String deviceId) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_User, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		List<DeviceSignboardModel> deviceSignboardModels = deviceSignboardService.getDeviceModelsByDeviceId(deviceId);
		return rBuilderService.update(response, //
				HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, //
				deviceSignboardModels);
	}
	
	/**
	 * 获取设备列表
	 * 
	 * @param httpSession Session会话
	 * @return 返回给前端的操作结果
	 */
	@RequestMapping(path = "/getDeviceModels")
	@ResponseBody
	public RBuilderService.Response getDeviceModels(HttpSession httpSession) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_User, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		// 根据用户权限返回数据
		UserModel userModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		if (userModel.getRoleLevel() <= RoleModel.ROLE_LEVEL_WorkcenterAdmin) {
			return rBuilderService.update(response, //
					HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, //
					deviceSignboardService.getDeviceModelsByWorkcenterId(userModel.getWorkcenterId()));
		}
		if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_WorkshopAdmin) {
			return rBuilderService.update(response, //
					HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, //
					deviceSignboardService.getDeviceModelsByWorkshopId(userModel.getWorkshopId()));
		}
		if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_FactoryAdmin) {
			return rBuilderService.update(response, //
					HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, //
					deviceSignboardService.getDeviceModelsByFactoryId(userModel.getFactoryId()));
		}
		if (userModel.getRoleLevel() >= RoleModel.ROLE_LEVEL_OrganizationAdmin) {
			return rBuilderService.update(response, //
					HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, //
					deviceSignboardService.getDeviceModelsByOrganizationId(userModel.getOrganizationId()));
		}
		return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_DATA, null);
	}

	/**
	 * 设备绑定
	 * 
	 * @param httpSession Session会话
	 * @param deviceModel 设备信息
	 * @return 返回给前端的操作结果
	 */
	@RequestMapping(path = "/bindingDeviceModel")
	@ResponseBody
	public RBuilderService.Response bindingDeviceModel(HttpSession httpSession, //
			@RequestBody DeviceSignboardModel deviceModel) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_WorkcenterAdmin, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		// 根据用户权限操作数据
		UserModel userModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		deviceModel.setOrganizationId(userModel.getOrganizationId());
		if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_WorkcenterAdmin) {
			if (userModel.getWorkcenterId()==deviceModel.getWorkcenterId()//
					&& userModel.getWorkshopId()==deviceModel.getWorkshopId()//
					&& userModel.getFactoryId()==deviceModel.getFactoryId()) {
				if (deviceSignboardService.bindingDeviceModel(deviceModel)) {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, null);
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
				}
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
			}
		}
		if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_WorkshopAdmin) {
			if (userModel.getWorkshopId()==deviceModel.getWorkshopId()//
					&& userModel.getFactoryId()==deviceModel.getFactoryId()) {
				if (deviceSignboardService.bindingDeviceModel(deviceModel)) {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, null);
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
				}
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
			}
		}
		if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_FactoryAdmin) {
			if (userModel.getFactoryId()==deviceModel.getFactoryId()) {
				if (deviceSignboardService.bindingDeviceModel(deviceModel)) {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, null);
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
				}
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
			}
		}
		if (userModel.getRoleLevel() >= RoleModel.ROLE_LEVEL_OrganizationAdmin) {
			if (deviceSignboardService.bindingDeviceModel(deviceModel)) {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, null);
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
			}
		}
		return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
	}
	/**
	 * 更新已经绑定的设备标识牌信息
	 * @param deviceModel 设备标识牌
	 * @return 返回给前端的操作结果
	 * */
	@RequestMapping(path = "/updateDeviceModelInfo")
	@ResponseBody
	public RBuilderService.Response updateDeviceModelInfo(HttpSession httpSession,//
			@RequestBody DeviceSignboardModel deviceModel) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_WorkcenterAdmin, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		// 根据用户权限操作数据
		UserModel userModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		if (StringUtil.isEmpty(deviceModel.getOrganizationId())) {
			deviceModel.setOrganizationId(userModel.getOrganizationId());
		}
		if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_WorkcenterAdmin) {
			if (userModel.getWorkcenterId()==deviceModel.getWorkcenterId()//
					&& userModel.getWorkshopId()==deviceModel.getWorkshopId()//
					&& userModel.getFactoryId()==deviceModel.getFactoryId()//
					&& userModel.getOrganizationId().equals(deviceModel.getOrganizationId())) {
				if (deviceSignboardService.updateDeviceModelInfo(deviceModel)) {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, null);
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
				}
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
			}
		}
		if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_WorkshopAdmin) {
			if (userModel.getWorkshopId()==deviceModel.getWorkshopId()//
					&& userModel.getFactoryId()==deviceModel.getFactoryId()//
					&& userModel.getOrganizationId().equals(deviceModel.getOrganizationId())) {
				if (deviceSignboardService.updateDeviceModelInfo(deviceModel)) {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, null);
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
				}
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
			}
		}
		if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_FactoryAdmin) {
			if (userModel.getFactoryId()==deviceModel.getFactoryId()//
					&& userModel.getOrganizationId().equals(deviceModel.getOrganizationId())) {
				if (deviceSignboardService.updateDeviceModelInfo(deviceModel)) {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, null);
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
				}
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
			}
		}
		if (userModel.getRoleLevel() >= RoleModel.ROLE_LEVEL_OrganizationAdmin) {
			if (userModel.getOrganizationId().equals(deviceModel.getOrganizationId())) {
				if (deviceSignboardService.updateDeviceModelInfo(deviceModel)) {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, null);
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
				}
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
			}
		}
		return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
	}
	/**
	 * 根据设备标识牌编号绑定设备
	 * 
	 * @param httpSession Session会话
	 * @return 返回给前端的操作结果
	 */
	@RequestMapping(path = "/bindingDeviceModelByCode")
	@ResponseBody
	public RBuilderService.Response bindingDeviceModelByCode(HttpSession httpSession,//
			@RequestParam("signboardCode")String signboardCode,//
			@RequestParam("typeId")long typeId,//
			@RequestParam("factoryId")long factoryId,//
			@RequestParam("workshopId")long workshopId,//
			@RequestParam("workcenterId")long workcenterId,//
			@RequestParam("deviceId")long deviceId){
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_WorkcenterAdmin, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		// 获取登录用户
		UserModel userModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		String organizationId = userModel.getOrganizationId();
		// 数据封装
		DeviceSignboardModel signboardModel=new DeviceSignboardModel();
		signboardModel.setDeviceId(deviceId);
		signboardModel.setWorkcenterId(workcenterId);
		signboardModel.setWorkshopId(workshopId);
		signboardModel.setFactoryId(factoryId);
		signboardModel.setOrganizationId(organizationId);
		signboardModel.setCode(signboardCode);
		signboardModel.setTypeId(typeId);
		//判断标识牌是否以及被绑定
		List<DeviceSignboardModel> signboardModels = deviceSignboardService.getDeviceModelsByCode(signboardCode);
		if (signboardModels.size()<=0) {
			return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NOT_FOUND_DATA, null);
		}else {
			if (signboardModels.get(0).getDeviceId()<=0) {
				
			}else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_RELEASE_FIRST, null);
			}
		}
		// 根据用户权限操作数据
		if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_WorkcenterAdmin) {
			if (userModel.getWorkcenterId()==workcenterId//
					&& userModel.getWorkshopId()==workshopId//
					&& userModel.getFactoryId()==factoryId//
					&& userModel.getOrganizationId().equals(organizationId)) {	
				//绑定
				if (deviceSignboardService.bindingDeviceModel(signboardModel)) {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, null);
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
				}
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
			}
		}
		if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_WorkshopAdmin) {
			if (userModel.getWorkshopId()==workshopId//
					&& userModel.getFactoryId()==factoryId//
					&& userModel.getOrganizationId().equals(organizationId)) {
				//绑定
				if (deviceSignboardService.bindingDeviceModel(signboardModel)) {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, null);
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
				}
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
			}
		}
		if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_FactoryAdmin) {
			if (userModel.getFactoryId()==factoryId//
					&& userModel.getOrganizationId().equals(organizationId)) {
				//绑定
				if (deviceSignboardService.bindingDeviceModel(signboardModel)) {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, null);
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
				}
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
			}
		}
		if (userModel.getRoleLevel() >= RoleModel.ROLE_LEVEL_OrganizationAdmin) {
			if (userModel.getOrganizationId().equals(organizationId)) {
				//绑定
				if (deviceSignboardService.bindingDeviceModel(signboardModel)) {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, null);
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
				}
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
			}
		}
		return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
	}
	
	/**
	 * 设备解绑
	 * 
	 * @param httpSession Session会话
	 * @param deviceModel 设备信息
	 * @return 返回给前端的操作结果
	 */
	@RequestMapping(path = "/untyingDeviceModel")
	@ResponseBody
	public RBuilderService.Response untyingDeviceModel(HttpSession httpSession, //
			@RequestBody DeviceSignboardModel deviceModel) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_WorkcenterAdmin, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		//检查
		long deviceId=deviceModel.getDeviceId();
		if (deviceId<=0) {
			return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
		}
		// 根据用户权限操作数据
		UserModel userModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_WorkcenterAdmin) {
			if (userModel.getWorkcenterId()==deviceModel.getWorkcenterId()//
					&& userModel.getWorkshopId()==deviceModel.getWorkshopId()//
					&& userModel.getFactoryId()==deviceModel.getFactoryId()//
					&& userModel.getOrganizationId().equals(deviceModel.getOrganizationId())) {
				// 解绑
				deviceModel.setOrganizationId("");
				deviceModel.setFactoryId(-1);
				deviceModel.setWorkshopId(-1);
				deviceModel.setWorkcenterId(-1);
				deviceModel.setDeviceId(-1);
				// DB
				if (deviceSignboardService.bindingDeviceModel(deviceModel)) {
					//更新设备服务
					deviceService.clearSignboardMonitor(deviceId);
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, null);
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
				}
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
			}
		}
		if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_WorkshopAdmin) {
			if (userModel.getWorkshopId()==deviceModel.getWorkshopId()//
					&& userModel.getFactoryId()==deviceModel.getFactoryId()//
					&& userModel.getOrganizationId().equals(deviceModel.getOrganizationId())) {
				// 解绑
				deviceModel.setOrganizationId("");
				deviceModel.setFactoryId(-1);
				deviceModel.setWorkshopId(-1);
				deviceModel.setWorkcenterId(-1);
				deviceModel.setDeviceId(-1);
				// DB
				if (deviceSignboardService.bindingDeviceModel(deviceModel)) {
					//更新设备服务
					deviceService.clearSignboardMonitor(deviceId);
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, null);
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
				}
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
			}
		}
		if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_FactoryAdmin) {
			if (userModel.getFactoryId()==deviceModel.getFactoryId()//
					&& userModel.getOrganizationId().equals(deviceModel.getOrganizationId())) {
				// 解绑
				deviceModel.setOrganizationId("");
				deviceModel.setFactoryId(-1);
				deviceModel.setWorkshopId(-1);
				deviceModel.setWorkcenterId(-1);
				deviceModel.setDeviceId(-1);
				// DB
				if (deviceSignboardService.bindingDeviceModel(deviceModel)) {
					//更新设备服务
					deviceService.clearSignboardMonitor(deviceId);
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, null);
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
				}
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
			}
		}
		if (userModel.getRoleLevel() >= RoleModel.ROLE_LEVEL_OrganizationAdmin) {
			if (userModel.getOrganizationId().equals(deviceModel.getOrganizationId())) {
				// 解绑
				deviceModel.setOrganizationId("");
				deviceModel.setFactoryId(-1);
				deviceModel.setWorkshopId(-1);
				deviceModel.setWorkcenterId(-1);
				deviceModel.setDeviceId(-1);
				// DB
				if (deviceSignboardService.bindingDeviceModel(deviceModel)) {
					//更新设备服务
					deviceService.clearSignboardMonitor(deviceId);
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, null);
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
				}
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
			}
		}
		return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
	}

	/**
	 * 从Session中获取用户信息，然后删除设备
	 * 
	 * @param httpSession Session
	 * @return 操作结果
	 */
	@RequestMapping(path = "/deleteDeviceModel")
	@ResponseBody
	public RBuilderService.Response deleteDeviceModel(HttpSession httpSession, //
			@RequestBody DeviceSignboardModel deviceModel) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_WorkshopAdmin, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		// 根据用户权限操作数据
		UserModel userModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_WorkshopAdmin) {
			if (userModel.getWorkshopId()==deviceModel.getWorkshopId()//
					&& userModel.getFactoryId()==deviceModel.getFactoryId()//
					&& userModel.getOrganizationId().equals(deviceModel.getOrganizationId())) {
				if (deviceSignboardService.deleteDeviceModel(deviceModel)) {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, null);
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
				}
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
			}
		}
		if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_FactoryAdmin) {
			if (userModel.getFactoryId()==deviceModel.getFactoryId()//
					&& userModel.getOrganizationId().equals(deviceModel.getOrganizationId())) {
				if (deviceSignboardService.deleteDeviceModel(deviceModel)) {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, null);
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
				}
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
			}
		}
		if (userModel.getRoleLevel() >= RoleModel.ROLE_LEVEL_OrganizationAdmin) {
			if (userModel.getOrganizationId().equals(deviceModel.getOrganizationId())) {
				if (deviceSignboardService.deleteDeviceModel(deviceModel)) {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, null);
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
				}
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
			}
		}
		return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
	}

}
