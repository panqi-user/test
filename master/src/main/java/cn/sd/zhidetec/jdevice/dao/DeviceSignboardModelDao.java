package cn.sd.zhidetec.jdevice.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import cn.sd.zhidetec.jdevice.Starter;
import cn.sd.zhidetec.jdevice.config.DataSourceConfiger;
import cn.sd.zhidetec.jdevice.model.DeviceSignboardModel;
import cn.sd.zhidetec.jdevice.util.StringUtil;

@Service
public class DeviceSignboardModelDao {

	@Autowired
	@Qualifier(DataSourceConfiger.BEAN_NAME_JdeviceJdbcTemplate)
	protected JdbcTemplate jdbcTemplate;

	/**
	 * 根据ID删除设备
	 * 
	 * @param deviceModel 设备
	 * @return 操作是否成功
	 */
	public boolean deleteDeviceModel(DeviceSignboardModel deviceModel) {
		int i = 0;
		Object[] args = new Object[1];
		args[i++] = deviceModel.getId();
		if (deviceModel.getId()>0) {
			i = jdbcTemplate.update(Sql_DeleteDeviceModel_By_Id, args);
		}
		if (i > 0) {
			return true;
		}
		return false;
	}

	private static String Sql_DeleteDeviceModel_By_Id = "DELETE FROM jdevice_c_signboard WHERE id=?";

	/**
	 * 设备绑定
	 * 
	 * @param deviceModel 要绑定的设备
	 * @return 操作是否成功
	 */
	public boolean bindingDeviceModel(DeviceSignboardModel deviceModel) {
		int i = 0;
		Object[] args = new Object[7];
		// 修改信息
		if (deviceModel.getCode() != null//
				&& !deviceModel.getCode().trim().equals("")) {
			args[i++] = deviceModel.getTypeId();
			args[i++] = deviceModel.getDeviceId();
			args[i++] = deviceModel.getWorkcenterId();
			args[i++] = deviceModel.getWorkshopId();
			args[i++] = deviceModel.getFactoryId();
			args[i++] = StringUtil.getEmptyString(deviceModel.getOrganizationId());
			args[i++] = deviceModel.getCode();
			i = jdbcTemplate.update(Sql_BindingDeviceModel_By_Code, args);
		} else {
			return false;
		}
		if (i > 0) {
			return true;
		}
		//
		return false;
	}

	private static String Sql_BindingDeviceModel_By_Code = "UPDATE jdevice_c_signboard" + //
			" SET " + //
			"type_id = ?," + //
			"device_id = ?," + //
			"workcenter_id = ?," + //
			"workshop_id = ?," + //
			"factory_id = ?," + //
			"organization_id = ?" + //
			" WHERE " + //
			"code=?";

	/**
	 * 更改设备状态信息
	 * 
	 * @param deviceModel 设备
	 * @return 操作是否成功
	 */
	public boolean updateDeviceModelStatus(DeviceSignboardModel deviceModel) {
		int i = 0;
		Object[] args = new Object[12];
		// 修改信息
		if (deviceModel.getCode() != null//
				&& !deviceModel.getCode().trim().equals("")) {
			args[i++] = StringUtil.getEmptyString(deviceModel.getLocalTime());
			args[i++] = deviceModel.getPowerVoltage();
			args[i++] = deviceModel.getStatus();
			args[i++] = deviceModel.getSoftVersion();
			args[i++] = deviceModel.getHardVersion();
			args[i++] = StringUtil.getEmptyString(deviceModel.getImei());
			args[i++] = StringUtil.getEmptyString(deviceModel.getImsi());
			args[i++] = deviceModel.getSigalIntensity();
			args[i++] = deviceModel.getSafeTemperature();
			args[i++] = deviceModel.getSafeHumidity();
			args[i++] = deviceModel.getLastSyncTimeMs();
			args[i++] = StringUtil.getEmptyString(deviceModel.getCode());
			i = jdbcTemplate.update(Sql_UpdateDeviceStatus_By_Code, args);
			if (i > 0) {
				return true;
			}
		}
		return false;
	}

	private static String Sql_UpdateDeviceStatus_By_Code = "UPDATE jdevice_c_signboard" + //
			" SET " + //
			"local_time = ?," + //
			"power_voltage = ?," + //
			"status = ?," + //
			"soft_version = ?," + //
			"hard_version = ?," + //
			"imei = ?," + //
			"imsi = ?," + //
			"sigal_intensity = ?," + //
			"temperature = ?," + //
			"humidity = ?," + //
			"last_sync_time_ms = ?"+//
			" WHERE " + //
			"code=?";

	/**
	 * 更改设备异常状态信息
	 * 
	 * @param deviceModel 设备
	 * @return 操作是否成功
	 */
	public boolean updateDeviceModelErrorStatus(DeviceSignboardModel deviceModel) {
		int i = 0;
		Object[] args = new Object[2];
		// 修改信息
		if (deviceModel.getCode() != null//
				&& !deviceModel.getCode().trim().equals("")) {
			args[i++] = deviceModel.getErrorType();
			args[i++] = StringUtil.getEmptyString(deviceModel.getCode());
			i = jdbcTemplate.update(Sql_UpdateDeviceErrorStatus_By_Code, args);
			if (i > 0) {
				return true;
			}
		}
		return false;
	}

	private static String Sql_UpdateDeviceErrorStatus_By_Code = "UPDATE jdevice_c_signboard" + //
			" SET " + //
			"error_type = ?" + //
			" WHERE " + //
			"code=?";

	/**
	 * 更新标识牌信息
	 * @param deviceModel 设备标识牌对象
	 * @return 操作是否成功
	 * */
	public boolean updateDeviceModelInfoToDB(DeviceSignboardModel deviceModel) {
		int i = 0;
		Object[] args = new Object[15];
		// 修改信息
		if (deviceModel.getCode() != null//
				&& !deviceModel.getCode().trim().equals("")) {
			//
			args[i++] = deviceModel.getUploadIntervalTime();
			args[i++] = deviceModel.getCollectIntervalTime();
//			args[i++] = deviceModel.getServerIp();
//			args[i++] = deviceModel.getServerPort();
			args[i++] = deviceModel.getHumidityMinVal();
			args[i++] = deviceModel.getHumidityMaxVal();
			args[i++] = deviceModel.getTemperatureMinVal();
			args[i++] = deviceModel.getTemperatureMaxVal();
			args[i++] = deviceModel.getPowerVoltageMinVal();
			args[i++] = deviceModel.getPowerVoltageMaxVal();
			//
			args[i++] = deviceModel.getTypeId();
			args[i++] = deviceModel.getDeviceId();
			args[i++] = deviceModel.getWorkcenterId();
			args[i++] = deviceModel.getWorkshopId();
			args[i++] = deviceModel.getFactoryId();
			args[i++] = StringUtil.getEmptyString(deviceModel.getOrganizationId());
			//
			args[i++] = StringUtil.getEmptyString(deviceModel.getCode());
			i = jdbcTemplate.update(Sql_UpdateDeviceInfo_By_Code, args);
			if (i > 0) {
				return true;
			}
		}
		return false;
	}

	private static String Sql_UpdateDeviceInfo_By_Code = "UPDATE jdevice_c_signboard" + //
			" SET " + //
			"upload_interval_time = ?," + //
			"collect_interval_time = ?," + //
			//"server_ip = ?," + //
			//"server_port = ?," + //
			"humidity_min_val = ?," + //
			"humidity_max_val = ?," + //
			"temperature_min_val = ?," + //
			"temperature_max_val = ?," + //
			"power_voltage_min_val = ?," + //
			"power_voltage_max_val = ?," + //
			"type_id = ?," + //
			"device_id = ?," + //
			"workcenter_id = ?," + //
			"workshop_id = ?," + //
			"factory_id = ?," + //
			"organization_id = ?" + //
			" WHERE " + //
			"code=?";
	/**
	 * 设置设备信息，如果不存在则新建
	 * 
	 * @param deviceModel 设备信息
	 * @return 操作是否成功
	 */
	public boolean setDeviceModel(DeviceSignboardModel deviceModel) {
		int i = 0;
		Object[] args = new Object[25];
		// 修改信息
		if (deviceModel.getId() >0
				&& getDeviceModelsById(deviceModel.getId()).size() > 0) {
			args[i++] = StringUtil.getEmptyString(deviceModel.getCode());
			args[i++] = StringUtil.getEmptyString(deviceModel.getLocalTime());
			args[i++] = deviceModel.getPowerVoltage();
			args[i++] = deviceModel.getStatus();
			args[i++] = deviceModel.getSoftVersion();
			args[i++] = deviceModel.getHardVersion();
			//
			args[i++] = deviceModel.getUploadIntervalTime();
			args[i++] = deviceModel.getCollectIntervalTime();
			args[i++] = deviceModel.getServerIp();
			args[i++] = deviceModel.getServerPort();
			args[i++] = deviceModel.getHumidityMinVal();
			args[i++] = deviceModel.getHumidityMaxVal();
			args[i++] = deviceModel.getTemperatureMinVal();
			args[i++] = deviceModel.getTemperatureMaxVal();
			args[i++] = deviceModel.getPowerVoltageMinVal();
			args[i++] = deviceModel.getPowerVoltageMaxVal();
			//
			args[i++] = deviceModel.getErrorType();
			args[i++] = deviceModel.getLastSyncTimeMs();
			args[i++] = deviceModel.getTypeId();
			args[i++] = deviceModel.getDeviceId();
			args[i++] = deviceModel.getWorkcenterId();
			args[i++] = deviceModel.getWorkshopId();
			args[i++] = deviceModel.getFactoryId();
			args[i++] = StringUtil.getEmptyString(deviceModel.getOrganizationId());
			args[i++] = deviceModel.getId();
			i = jdbcTemplate.update(Sql_UpdateModel_By_Id, args);
		}
		// 新建信息
		else {
			deviceModel.setId(Starter.IdMaker.nextId());
			args[i++] = deviceModel.getId();
			args[i++] = StringUtil.getEmptyString(deviceModel.getCode());
			args[i++] = StringUtil.getEmptyString(deviceModel.getLocalTime());
			args[i++] = deviceModel.getPowerVoltage();
			args[i++] = deviceModel.getStatus();
			args[i++] = deviceModel.getSoftVersion();
			args[i++] = deviceModel.getHardVersion();
			//
			args[i++] = deviceModel.getUploadIntervalTime();
			args[i++] = deviceModel.getCollectIntervalTime();
			args[i++] = deviceModel.getServerIp();
			args[i++] = deviceModel.getServerPort();
			args[i++] = deviceModel.getHumidityMinVal();
			args[i++] = deviceModel.getHumidityMaxVal();
			args[i++] = deviceModel.getTemperatureMinVal();
			args[i++] = deviceModel.getTemperatureMaxVal();
			args[i++] = deviceModel.getPowerVoltageMinVal();
			args[i++] = deviceModel.getPowerVoltageMaxVal();
			//
			args[i++] = deviceModel.getErrorType();
			args[i++] = deviceModel.getLastSyncTimeMs();
			args[i++] = deviceModel.getTypeId();
			args[i++] = deviceModel.getDeviceId();
			args[i++] = deviceModel.getWorkcenterId();
			args[i++] = deviceModel.getWorkshopId();
			args[i++] = deviceModel.getFactoryId();
			args[i++] = StringUtil.getEmptyString(deviceModel.getOrganizationId());
			i = jdbcTemplate.update(Sql_InsertModel, args);
		}
		if (i > 0) {
			return true;
		}
		return false;
	}

	private static String Sql_UpdateModel_By_Id = "UPDATE jdevice_c_signboard" + //
			" SET " + //
			"code = ?," + //
			"local_time = ?," + //
			"power_voltage = ?," + //
			"status = ?," + //
			"soft_version = ?," + //
			"hard_version = ?," + //
			"upload_interval_time = ?," + //
			"collect_interval_time = ?," + //
			"server_ip = ?," + //
			"server_port = ?," + //
			"humidity_min_val = ?," + //
			"humidity_max_val = ?," + //
			"temperature_min_val = ?," + //
			"temperature_max_val = ?," + //
			"power_voltage_min_val = ?," + //
			"power_voltage_max_val = ?," + //
			"error_type = ?," + //
			"last_sync_time_ms = ?,"+//
			"type_id," + //
			"device_id = ?," + //
			"workcenter_id = ?," + //
			"workshop_id = ?," + //
			"factory_id = ?," + //
			"organization_id = ?" + //
			" WHERE " + //
			"id=?";
	private static String Sql_InsertModel = "INSERT INTO jdevice_c_signboard (" + //
			"id," + //
			"code," + //
			"local_time," + //
			"power_voltage," + //
			"status," + //
			"soft_version," + //
			"hard_version," + //
			"upload_interval_time," + //
			"collect_interval_time," + //
			"server_ip," + //
			"server_port," + //
			"humidity_min_val," + //
			"humidity_max_val," + //
			"temperature_min_val," + //
			"temperature_max_val," + //
			"power_voltage_min_val," + //
			"power_voltage_max_val," + //
			"error_type," + //
			"last_sync_time_ms = ?,"+//
			"type_id," + //
			"device_id," + //
			"workcenter_id," + //
			"workshop_id," + //
			"factory_id," + //
			"organization_id" + //
			")VALUES(" + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?" + //
			")";

	/**
	 * 根据设备Code获取设备列表
	 * 
	 * @param deviceId 设备ID
	 * @return 数据列表
	 */
	public List<DeviceSignboardModel> getDeviceModelsByCode(String deviceCode) {
		Object[] args = new Object[1];
		args[0] = deviceCode;
		List<DeviceSignboardModel> list = jdbcTemplate.query(Sql_GetModels_By_Code, args, beanPropertyRowMapper);
		return list;
	}

	private static String Sql_GetModels_By_Code = "SELECT * FROM jdevice_c_signboard WHERE code = ?";

	/**
	 * 根据设备ID获取设备列表
	 * 
	 * @param deviceId 设备ID
	 * @return 数据列表
	 */
	public List<DeviceSignboardModel> getDeviceModelsById(long deviceId) {
		Object[] args = new Object[1];
		args[0] = deviceId;
		List<DeviceSignboardModel> list = jdbcTemplate.query(Sql_GetModels_By_Id, args, beanPropertyRowMapper);
		return list;
	}

	private static String Sql_GetModels_By_Id = "SELECT * FROM jdevice_c_signboard WHERE id = ?";

	/**
	 * 根据设备ID获取设备列表
	 * 
	 * @param workcenterId 工作中心ID
	 * @return 数据列表
	 */
	public List<DeviceSignboardModel> getDeviceModelsByDeviceId(String deviceId) {
		Object[] args = new Object[1];
		args[0] = deviceId;
		List<DeviceSignboardModel> list = jdbcTemplate.query(Sql_GetModels_By_DeviceId, args, beanPropertyRowMapper);
		return list;
	}

	private static String Sql_GetModels_By_DeviceId = "SELECT * FROM jdevice_c_signboard WHERE device_id = ?";

	/**
	 * 根据工作中心ID获取设备列表
	 * 
	 * @param workcenterId 工作中心ID
	 * @return 数据列表
	 */
	public List<DeviceSignboardModel> getDeviceModelsByWorkcenterId(long workcenterId) {
		Object[] args = new Object[1];
		args[0] = workcenterId;
		List<DeviceSignboardModel> list = jdbcTemplate.query(Sql_GetModels_By_WorkcenterId, args,
				beanPropertyRowMapper);
		return list;
	}

	private static String Sql_GetModels_By_WorkcenterId = "SELECT * FROM jdevice_c_signboard WHERE workcenter_id = ?";

	/**
	 * 根据车间ID获取设备列表
	 * 
	 * @param workshopId 车间ID
	 * @return 数据列表
	 */
	public List<DeviceSignboardModel> getDeviceModelsByWorkshopId(long workshopId) {
		Object[] args = new Object[1];
		args[0] = workshopId;
		List<DeviceSignboardModel> list = jdbcTemplate.query(Sql_GetModels_By_WorkshopId, args, beanPropertyRowMapper);
		return list;
	}

	private static String Sql_GetModels_By_WorkshopId = "SELECT * FROM jdevice_c_signboard WHERE workshop_id = ? ORDER BY workcenter_id DESC";

	/**
	 * 根据工厂ID获取设备列表
	 * 
	 * @param factoryId 工厂ID
	 * @return 数据列表
	 */
	public List<DeviceSignboardModel> getDeviceModelsByFactoryId(long factoryId) {
		Object[] args = new Object[1];
		args[0] = factoryId;
		List<DeviceSignboardModel> list = jdbcTemplate.query(Sql_GetModels_By_FactoryId, args, beanPropertyRowMapper);
		return list;
	}

	private static String Sql_GetModels_By_FactoryId = "SELECT * FROM jdevice_c_signboard WHERE factory_id = ? ORDER BY workcenter_id DESC";

	/**
	 * 根据组织ID获取设备列表
	 * 
	 * @param organizationId 组织ID
	 * @return 数据列表
	 */
	public List<DeviceSignboardModel> getDeviceModelsByOrganizationId(String organizationId) {
		Object[] args = new Object[1];
		args[0] = organizationId;
		List<DeviceSignboardModel> list = jdbcTemplate.query(Sql_GetModels_By_OrganizationId, args,
				beanPropertyRowMapper);
		return list;
	}

	private static String Sql_GetModels_By_OrganizationId = "SELECT * FROM jdevice_c_signboard WHERE organization_id = ? ORDER BY workcenter_id DESC";

	private BeanPropertyRowMapper<DeviceSignboardModel> beanPropertyRowMapper = new BeanPropertyRowMapper<DeviceSignboardModel>(
			DeviceSignboardModel.class);

	public boolean changeWorkcenter(long deviceId, long workcenterId, long workshopId, long factoryId) {
		Object[] args = new Object[4];
		int i=0;
		args[i++] = workcenterId;
		args[i++] = workshopId;
		args[i++] = factoryId;
		args[i++] = deviceId;
		return jdbcTemplate.update(Sql_ChangeWorkcenter_By_DeviceId, args)>0;
	}
	
	private static String Sql_ChangeWorkcenter_By_DeviceId = "UPDATE jdevice_c_signboard SET workcenter_id=?,workshop_id=?,factory_id=? WHERE device_id=?;";
	
	public boolean changeWorkshop(long workcenterId,long workshopId, long factoryId) {
		Object[] args = new Object[3];
		int i=0;
		args[i++] = workshopId;
		args[i++] = factoryId;
		args[i++] = workcenterId;
		return jdbcTemplate.update(Sql_ChangeWorkshop_By_WorkcenterId, args)>0;
	}
	
	private static String Sql_ChangeWorkshop_By_WorkcenterId="UPDATE jdevice_c_signboard SET workshop_id=?,factory_id=? WHERE workcenter_id=?;";
	
	public boolean changeFactory(long workshopId,long factoryId) {
		Object[] args = new Object[2];
		int i=0;
		args[i++] = factoryId;
		args[i++] = workshopId;
		return jdbcTemplate.update(Sql_ChangeFactory_By_WorkshopId, args)>0;
	}
	
	private static String Sql_ChangeFactory_By_WorkshopId="UPDATE jdevice_c_signboard SET factory_id=? WHERE workshop_id=?;";
}
