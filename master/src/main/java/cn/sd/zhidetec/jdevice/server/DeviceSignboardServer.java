package cn.sd.zhidetec.jdevice.server;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import cn.sd.zhidetec.jdevice.model.DeviceSignboardModel;
import cn.sd.zhidetec.jdevice.service.AndonService;
import cn.sd.zhidetec.jdevice.service.DeviceService;
import cn.sd.zhidetec.jdevice.service.DeviceSignboardService;
import cn.sd.zhidetec.jdevice.service.DeviceTempHumiService;
import cn.sd.zhidetec.jdevice.util.DateUtil;
import cn.sd.zhidetec.jdevice.util.IdMaker;

/**
 * 设备标识牌Tcp服务器
 * */
@Service
public class DeviceSignboardServer {
	
	@Autowired
	protected AndonService andonService;
	@Autowired
	protected DeviceTempHumiService deviceTempHumiService;
	@Autowired
	protected DeviceSignboardService deviceSignboardService;
	@Autowired
	protected DeviceService deviceService;
	protected Logger logger = LoggerFactory.getLogger(getClass());

	@Value("${jdevices.ip}")
	protected String serverIP = "127.0.0.1";
	@Value("${jdevices.port}")
	protected int listernPort = 5060;
	
	protected int coreTaskPoolSize=10;
	protected int maxTaskPoolSize=100;
	protected int taskQueueSize=512;
	protected int taskRecvDataTimeout=DateUtil.MILLISECOND_HALF_MINUTE;
	
	protected boolean started = false;
	protected boolean stoped = true;
	protected ServerSocket serverSocket;
	protected ThreadPoolExecutor taskPoolExecutor;
	protected RejectedExecutionHandler rejectedHandler;
	protected BlockingQueue<Runnable> taskQueue = new ArrayBlockingQueue<>(taskQueueSize);
	/**
	 * 启动服务
	 * 
	 * @throws IOException
	 */
	public void start() throws IOException {
		if (!started&&stoped) {
			started=true;
			stoped=false;
			// 数据接收线程池
			taskQueue = new ArrayBlockingQueue<>(taskQueueSize);
			rejectedHandler = new ThreadPoolExecutor.DiscardPolicy();
			taskPoolExecutor = new ThreadPoolExecutor(coreTaskPoolSize,//
					coreTaskPoolSize,//
					maxTaskPoolSize,//
					TimeUnit.SECONDS,//
					taskQueue,//
					rejectedHandler);
			// ServerSocket
			serverSocket=new ServerSocket(listernPort);
			// Socket连接线程
			new Thread(){
				@Override
				public void run() {
					logger.info("["+DeviceSignboardServer.this.getClass().getSimpleName()+"] Started On Port: "+listernPort);
					while (started) {
						try {
							Thread.sleep(1);
							Socket socket = serverSocket.accept();
							DeviceSignboardClientTask deviceSignboardNetTask = new DeviceSignboardClientTask(DeviceSignboardServer.this,socket,taskRecvDataTimeout);
							taskPoolExecutor.execute(deviceSignboardNetTask);
						} catch (Exception e) {
							// TODO: handle exception
							e.printStackTrace();
						}
					}
				}
			}.start();
			// 同步系统参数
			syncSystemParam();
			// 系统参数同步线程
			new Thread() {
				@Override
				public void run() {
					while (started) {
						try {
							Thread.sleep(500);
							syncSystemParam();
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}
			}.start();
		}
	}
	
	protected byte paramByte_CalendarYear;
	protected byte paramByte_CalendarMonth;
	protected byte paramByte_CalendarDayOfMonth;
	protected byte paramByte_CalendarHourOfDay;
	protected byte paramByte_CalendarMinute;
	protected byte paramByte_CalendarSecond;
	protected byte paramByte_ServerIP_N1;
	protected byte paramByte_ServerIP_N2;
	protected byte paramByte_ServerIP_N3;
	protected byte paramByte_ServerIP_N4;
	protected byte paramByte_ServerPort_N1;
	protected byte paramByte_ServerPort_N2;
	public boolean systemParamIsNew = true;

	/**
	 * 同步系统参数
	 */
	private void syncSystemParam() {
		// 获取当前系统时间
		Calendar calendar = Calendar.getInstance();
		paramByte_CalendarYear = (byte) (calendar.get(Calendar.YEAR)-2000);
		paramByte_CalendarMonth = (byte) (calendar.get(Calendar.MONTH)+1);
		paramByte_CalendarDayOfMonth = (byte) calendar.get(Calendar.DAY_OF_MONTH);
		paramByte_CalendarHourOfDay = (byte) calendar.get(Calendar.HOUR_OF_DAY);
		paramByte_CalendarMinute = (byte) calendar.get(Calendar.MINUTE);
		paramByte_CalendarSecond = (byte) calendar.get(Calendar.SECOND);
		if (systemParamIsNew) {
			systemParamIsNew = false;
			// 服务IP地址
			String[] ipByteStrArr = serverIP.split("\\.");
			if (ipByteStrArr.length == 4) {
				paramByte_ServerIP_N1 = (byte) Integer.parseInt(ipByteStrArr[0].trim());
				paramByte_ServerIP_N2 = (byte) Integer.parseInt(ipByteStrArr[1].trim());
				paramByte_ServerIP_N3 = (byte) Integer.parseInt(ipByteStrArr[2].trim());
				paramByte_ServerIP_N4 = (byte) Integer.parseInt(ipByteStrArr[3].trim());
			}
			// 服务端口号
			paramByte_ServerPort_N1 = (byte) (listernPort >> 8);
			paramByte_ServerPort_N2 = (byte) (listernPort);
		}
	}
	/**
	 * 判断标识牌配置是否需要更改
	 * @return 是否需要更改
	 * */
	public boolean needUpdateConfig(DeviceSignboardModel signboardModel) {
		if (signboardModel==null) {
			return false;
		}
		synchronized (updatedConfigDeviceMapLockObj) {
			if (updatedConfigDeviceMap.containsKey(signboardModel.getCode())//
					&&updatedConfigDeviceMap.get(signboardModel.getCode())!=null) {
				logger.info("标识牌 ["+signboardModel.getCode()+"] 检查配置缓存");
				if (updatedConfigDeviceMap.get(signboardModel.getCode()).needUpdateConfig(signboardModel)) {
					try {
						logger.info("标识牌 ["+signboardModel.getCode()+"] 需要更新配置:"+jsonMapper.writeValueAsString(signboardModel));
					} catch (JsonProcessingException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					return true;
				}
				else {
					logger.info("标识牌 ["+signboardModel.getCode()+"] 无需更新配置");
					return false;
				}
			}else {
				logger.info("标识牌 ["+signboardModel.getCode()+"] 无配置缓存");
				return true;
			}
		}
	}
	/**
	 * 已更新标识牌配置
	 * */
	public boolean updatedConfig(DeviceSignboardModel signboardModel) {
		if (signboardModel==null) {
			return false;
		}
		DeviceSignboardModel configModel=new DeviceSignboardModel();
		synchronized (updatedConfigDeviceMapLockObj) {
			configModel.copyField(signboardModel);
			updatedConfigDeviceMap.put(configModel.getCode(), configModel);
		}
		try {
			logger.info("标识牌 ["+signboardModel.getCode()+"] 更新配置到缓存:"+jsonMapper.writeValueAsString(configModel));
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
	}
	private Map<String, DeviceSignboardModel> updatedConfigDeviceMap=Collections.synchronizedMap(new HashMap<String, DeviceSignboardModel>());
	private Object updatedConfigDeviceMapLockObj=new Object();
	private ObjectMapper jsonMapper=new ObjectMapper();
	
	/**
	 * 停止服务
	 */
	public void stop() {
		started = false;
	}

	/**
	 * 当前服务是否已经启动
	 * 
	 * @return true 是，false 否
	 */
	public boolean hadStarted() {
		return started;
	}

	/**
	 * 当前服务是否已经成功停止
	 * 
	 * @return true 是，false 否
	 */
	public boolean hadStoped() {
		return stoped;
	}

	/**
	 * 获取注册服务端口
	 * 
	 * @return 端口号
	 */
	public int getListernPort() {
		return listernPort;
	}

	/**
	 * 设置注册服务端口
	 * 
	 * @param listernPort 端口号
	 */
	public void setListernPort(int listernPort) {
		this.listernPort = listernPort;
	}

	public void writeRunLog(String runLog) {
		logger.debug("["+getClass().getSimpleName()+"]"+runLog);
		synchronized (runLogListLockObj) {
			if (runLogList.size()>500) {
				runLogList.remove(0);
			}
			runLogList.add(runLog);
		}
	}
	
	public void clearRunLog() {
		synchronized (runLogListLockObj) {
			runLogList.clear();
		}
	}
	
	public List<String> getRunLogList() {
		List<String> list = new ArrayList<String>();
		synchronized (runLogListLockObj) {
			list.addAll(runLogList);
		}
		return list;
	}
	
	private List<String> runLogList=Collections.synchronizedList(new ArrayList<String>());
	private Object runLogListLockObj=new Object();
	
	public final static List<String> buildSignboardCode(byte type,byte version,int begin,int num) {
		List<String> list = new ArrayList<String>();
		Calendar calendar=Calendar.getInstance();
		Random random=new Random();
		int year=calendar.get(Calendar.YEAR)-2020;
		byte yearByte=(byte) year;
		int weekNum=calendar.get(Calendar.WEEK_OF_YEAR);
		byte weekByte=(byte) weekNum;
		for (int i = begin; i < begin+num; i++) {
			String code="";
			code+=String.format("%02x", type);
			code+="-";
			code+=String.format("%02x", version);
			code+="-";
			code+=String.format("%02x", yearByte);
			code+="-";
			code+=String.format("%02x", weekByte);
			code+="-";
			byte h2Byte=(byte) (i>>16);
			byte h1Byte=(byte) (i>>8);
			byte lByte=(byte) (i);
			code+=String.format("%02x", h2Byte);
			code+="-";
			code+=String.format("%02x", h1Byte);
			code+="-";
			code+=String.format("%02x", lByte);
			code+="-";
			//随机号
			byte randomByte=(byte) random.nextInt(255);
			code+=String.format("%02x", randomByte);
			list.add(code);
		}
		return list;
	}
	
	public static long getBeOnLineBeginTimeMs() {
		if(beOnLineBeginTimeMs<=0) {
			try {
				beOnLineBeginTimeMs=DateUtil.getDateTimeLong("2020-08-01 00:00:00.000");
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return beOnLineBeginTimeMs;
	}
	private static long beOnLineBeginTimeMs=0;
	
	public static void main(String[] args) throws IOException {
		IdMaker idMaker=new IdMaker(0);
		int type=01;
		int version=01;
		int index=1;
		int num=1700;
		List<String> list=buildSignboardCode((byte)type, (byte)version, index, num);
		//
		System.out.println(">>>>>>>>>>>>>");
		System.out.println();
		File file=new File("code"+File.separator+DateUtil.getDate()+"."+type+"_"+version+"_"+index+"_"+num+".code"+".txt");
		if (file.exists()) {
			file.delete();
		}
		file.createNewFile();
		FileWriter fileWriter=new FileWriter(file);
		for (String code : list) {
			System.out.println(code);
			fileWriter.append(code+"\n");
		}
		fileWriter.flush();
		fileWriter.close();
		System.out.println();
		System.out.println(">>>>>>>>>>>>>");
		//
		System.out.println(">>>>>>>>>>>>>");
		System.out.println();
		file=new File("code"+File.separator+DateUtil.getDate()+"."+type+"_"+version+"_"+index+"_"+num+".bar"+".txt");
		if (file.exists()) {
			file.delete();
		}
		file.createNewFile();
		fileWriter=new FileWriter(file);
		for (String code : list) {
			System.out.println(code.replaceAll("-", ""));
			fileWriter.append(code.replaceAll("-", "")+"\n");
		}
		fileWriter.flush();
		fileWriter.close();
		System.out.println();
		System.out.println(">>>>>>>>>>>>>");
		//
		System.out.println(">>>>>>>>>>>>>");
		System.out.println();
		file=new File("code"+File.separator+DateUtil.getDate()+"."+type+"_"+version+"_"+index+"_"+num+".url"+".txt");
		if (file.exists()) {
			file.delete();
		}
		file.createNewFile();
		fileWriter=new FileWriter(file);
		for (String code : list) {
			System.out.println("http://andon.cn/signboard?code="+code);
			fileWriter.append("http://andon.cn/signboard?code="+code+"\n");
		}
		fileWriter.flush();
		fileWriter.close();
		System.out.println();
		System.out.println(">>>>>>>>>>>>>");
		//
		System.out.println(">>>>>>>>>>>>>");
		System.out.println();
		file=new File("code"+File.separator+DateUtil.getDate()+"."+type+"_"+version+"_"+index+"_"+num+".url_bar"+".txt");
		if (file.exists()) {
			file.delete();
		}
		file.createNewFile();
		fileWriter=new FileWriter(file);
		for (String code : list) {
			System.out.println("http://andon.cn/signboard?code="+code+","+code.replaceAll("-", ""));
			fileWriter.append("http://andon.cn/signboard?code="+code+","+code.replaceAll("-", "")+"\n");
		}
		fileWriter.flush();
		fileWriter.close();
		System.out.println();
		System.out.println(">>>>>>>>>>>>>");
		//
		System.out.println(">>>>>>>>>>>>>");
		System.out.println();
		file=new File("code"+File.separator+DateUtil.getDate()+"."+type+"_"+version+"_"+index+"_"+num+".sql");
		if (file.exists()) {
			file.delete();
		}
		file.createNewFile();
		fileWriter=new FileWriter(file);
		for (String code : list) {
			fileWriter.append("INSERT INTO `jdevice_c_signboard` ("
					+ "`id`, "
					+ "`code`,"
					+ " `local_time`, "
					+ "`power_voltage`, "
					+ "`status`, "
					+ "`soft_version`, "
					+ "`hard_version`, "
					+ "`imei`, "
					+ "`imsi`, "
					+ "`sigal_intensity`, "
					+ "`temperature`,"
					+ "`humidity`, "
					+ "`error_type`, "
					+ "`last_sync_time_ms`, "
					+ "`type_id`, "
					+ "`server_ip`, "
					+ "`server_port`, "
					+ "`upload_interval_time`, "
					+ "`collect_interval_time`, "
					+ "`temperature_min_val`, "
					+ "`temperature_max_val`, "
					+ "`humidity_min_val`, "
					+ "`humidity_max_val`, "
					+ "`power_voltage_min_val`, "
					+ "`power_voltage_max_val`, "
					+ "`device_id`, "
					+ "`workcenter_id`, "
					+ "`workshop_id`, "
					+ "`factory_id`, "
					+ "`organization_id`"
					+ ") VALUES ('"
					+idMaker.nextId()+"', '"
					+code+"', '2020-07-15 00:00:00', '3.50', '1', '1', '1', '', '', '3', '25.00', '45', '0', '0', '159488700553000', '154.8.233.67', '5060', '30', '10', '-30.00', '80.00', '0', '90', '3.00', '30.00', '0', '0', '0', '0', '');"+"\n");
		}
		fileWriter.flush();
		fileWriter.close();
		System.out.println();
		System.out.println(">>>>>>>>>>>>>");
	}
}
