package cn.sd.zhidetec.jdevice.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * <hr>
 * <h2>简介</h2>
 * Controller的反馈构建器，用于构建Controller的反馈结果
 * <hr>
 * Copyright © 2017 www.zhidetec.com All Rights Reserved. <br>
 * 青岛致德工业技术有限公司 版权所有<br>
 * <hr>
 * <table border="1" cellspacing="0" cellpadding="2">
 * <caption><b>文件修改记录</b></caption>
 * <tr>
 * <th>修改日期</th>
 * <th>修改人</th>
 * <th>修改内容</th>
 * </tr>
 * <tbody>
 * <tr>
 * <td>2017年3月17日</td>
 * <td>linchunsen</td>
 * <td>新建文件，并实现基本功能</td>
 * </tr>
 * </tbody>
 * </table>
 */
@Service
public class RBuilderService {

	public final static String RESPONSE_HEAD_KEY="head";
	public final static String RESPONSE_BODY_KEY="body";
	
	public static final String CONTROLLER_RESULT_SUCCESS = "success";
	public static final String CONTROLLER_RESULT_ERROR = "error";
	
	public Response build(String head,Object body){
		Response responseEntity=new Response(new HashMap<>(), HttpStatus.OK);
		responseEntity.getBody().put(RBuilderService.RESPONSE_HEAD_KEY, head);
		responseEntity.getBody().put(RBuilderService.RESPONSE_BODY_KEY, body);
		return responseEntity;
	}
	
	public Response update(Response response,String head,Object body) {
		if (response==null) {
			response=new Response(new HashMap<>(), HttpStatus.OK);
		}
		response.getBody().put(RBuilderService.RESPONSE_HEAD_KEY, head);
		response.getBody().put(RBuilderService.RESPONSE_BODY_KEY, body);
		return response;
	}
	
	public void setHttpStatus(HttpStatus httpStatus){
		Response.status(httpStatus);
	}
	
	/**
	 * <hr>
	 * <h2>简介</h2>
	 * Controller的反馈，用于反馈Controller的结果
	 * <hr>
	 * Copyright © 2017 www.zhidetec.com All Rights Reserved. <br>
	 * 青岛致德工业技术有限公司 版权所有<br>
	 * <hr>
	 * <table border="1" cellspacing="0" cellpadding="2">
	 * <caption><b>文件修改记录</b></caption>
	 * <tr>
	 * <th>修改日期</th>
	 * <th>修改人</th>
	 * <th>修改内容</th>
	 * </tr>
	 * <tbody>
	 * <tr>
	 * <td>2017年3月17日</td>
	 * <td>linchunsen</td>
	 * <td>新建文件，并实现基本功能</td>
	 * </tr>
	 * </tbody>
	 * </table>
	 */
	public class Response extends ResponseEntity<Map<String, Object>>{

		public Response(Map<String, Object> body, HttpStatus status) {
			super(body, status);
		}
		
		private ObjectMapper objectMapper=new ObjectMapper();
		/**
		 * 将反馈结果的JSON字符串返回
		 * @return JSON字符串
		 * 无参数
		 * @author linchunsen
		 * @throws JsonProcessingException 
		 */
		public String toJSON() throws JsonProcessingException {
			Map<String, Object> map=getBody();
			return this.objectMapper.writeValueAsString(map);
		}
		
	}
	
}
