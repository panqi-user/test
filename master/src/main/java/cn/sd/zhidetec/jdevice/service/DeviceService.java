package cn.sd.zhidetec.jdevice.service;

import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.sd.zhidetec.jdevice.dao.DeviceExtendModelDao;
import cn.sd.zhidetec.jdevice.dao.DeviceModelDao;
import cn.sd.zhidetec.jdevice.model.DeviceModel;
import cn.sd.zhidetec.jdevice.pojo.DeviceStatusSumPojo;
import cn.sd.zhidetec.jdevice.server.JdeviceServer;
import cn.sd.zhidetec.jdevice.model.DeviceStatusModel;
import cn.sd.zhidetec.jdevice.model.DeviceTypeModel;
import cn.sd.zhidetec.jdevice.model.JedgeDeviceModel;
import cn.sd.zhidetec.jdevice.model.WorkcenterModel;
import cn.sd.zhidetec.jdevice.util.StringUtil;

/**
 * 设备代理服务，处理多个数据来源的设备状态，并异步更新到数据库
 * */
@Service
public class DeviceService extends Thread{
	
	@Autowired
	protected DeviceExtendModelDao deviceFieldModelDao;
	@Autowired
	protected DeviceTempHumiService deviceTempHumiService;
	@Autowired
	protected DeviceSignboardService deviceSignboardService;
	@Autowired
	protected AndonService andonService;
	@Autowired
	protected DeviceModelDao deviceModelDao;
	@Autowired
	protected JdeviceServer andonMessageServer;
	@Autowired
	protected JedgeService jedgeService;
	@Autowired
	protected DeviceOeeService deviceOeeService;
	@Autowired
	protected DeviceFaultService deviceFaultService;
	@Autowired
	protected DeviceTypeService deviceTypeService;
	@Autowired
	protected JdeviceServer jdeviceServer;
	protected Logger logger = LoggerFactory.getLogger(getClass());

	@Override
	public void run() {
		started=true;
		logger.info("DeviceService Started");
		DeviceModel signboardDeviceModel=null;
		JedgeDeviceModel jedgeDeviceModel=null;
		Long clearSignboardMonitorDeviceId=null;
		while (started) {
			try {
				//等待
				if (signboardDeviceModel==null||jedgeDeviceModel==null) {
					Thread.sleep(1);	
				}
				/**
				 * 清理设备标识牌监控器
				 * */
				synchronized (clearSignboardMonitorDeviceIdsLockObj) {
					clearSignboardMonitorDeviceId=clearSignboardMonitorDeviceIds.peek();
				}
				while (clearSignboardMonitorDeviceId!=null) {
					//当前设备信息
					DeviceModel deviceModel=getDeviceModelById(clearSignboardMonitorDeviceId);
					if (deviceModel!=null) {
						//设备状态位
						int lastStatusMonitotBits=deviceModel.getStatus_monitor_bits();
						if (lastStatusMonitotBits>=0) {
							/*
							//当前区间状态位
							int statusMonitorBits=DeviceStatusModel.VALUE_Run<<16;
							//Jedge设备状态
							int jedgeStatusVal=lastStatusMonitotBits%10000;
							//新状态位
							int newStatusMonitotBits=jedgeStatusVal+statusMonitorBits;
							//新设备状态
							int newStatusVal=jedgeStatusVal;
							int monitor=DeviceModel.MONITOR_VAL_JEDGE;
							//保存到数据库
							DeviceModel newDeviceModel=new DeviceModel();
							newDeviceModel.setId(deviceModel.getId());
							newDeviceModel.setStatus(DeviceStatusModel.getStatusByValue(newStatusVal));
							newDeviceModel.setStatusValue(newStatusVal);
							newDeviceModel.setStatusName(DeviceStatusModel.getNameByValue(newStatusVal));
							newDeviceModel.setStatus_monitor_bits(newStatusMonitotBits);
							newDeviceModel.setMonitor(monitor);
							deviceModelDao.setDeviceStatus(newDeviceModel);
							//状态变更
							if (newStatusVal!=deviceModel.getStatusValue()) {
								//设备状态历史记录
								deviceModelDao.insertDeviceStatusHistory(deviceModel, newDeviceModel);
								//计算OEE
								deviceOeeService.changeDeviceStatus(newDeviceModel);
								//故障状态
								deviceFaultService.changeDeviceStatus(deviceModel, newDeviceModel);
								//回调Andon服务
								andonService.updateDeviceStatus(deviceModel, newDeviceModel);	
							}
							logger.info("Clear Signboard And Set DeviceStatus To DB,EqpIdentity = "+deviceModel.getEqpIdentity()+", DeviceId = "+deviceModel.getId()+", Status = "+newDeviceModel.getStatus());
							*/
						}
					}
					//下一个实例
					synchronized (signboardDeviceModelsLockObj) {
						clearSignboardMonitorDeviceIds.poll();
						clearSignboardMonitorDeviceId=clearSignboardMonitorDeviceIds.peek();
					}
					
				}
				/**
				 * 设备标识牌状态
				 * */
				synchronized (signboardDeviceModelsLockObj) {
					signboardDeviceModel=signboardDeviceModels.peek();
				}
				while (signboardDeviceModel!=null) {
					//当前设备信息
					DeviceModel deviceModel=getDeviceModelById(signboardDeviceModel.getId());
					if (deviceModel!=null) {
						//设备状态位
						int lastStatusMonitotBits=deviceModel.getStatus_monitor_bits();
						if (lastStatusMonitotBits<0) {
							lastStatusMonitotBits=DeviceStatusModel.VALUE_Offline<<16;
						}
						int lastStatusVal=lastStatusMonitotBits>>16;
						lastStatusVal=lastStatusVal%0x10000;
						//设备状态发生变化
						if (signboardDeviceModel.getStatusValue()!=lastStatusVal) {
							//当前区间状态位
							int statusMonitorBits=signboardDeviceModel.getStatusValue()<<16;
							//Jedge设备状态
							int jedgeStatusVal=lastStatusMonitotBits%0x10000;
							//新状态位
							int newStatusMonitotBits=jedgeStatusVal+statusMonitorBits;
							//新设备状态
							int newStatusVal=0;
							int monitor=DeviceModel.MONITOR_VAL_NONE;
							if (jedgeStatusVal>signboardDeviceModel.getStatusValue()) {
								newStatusVal=jedgeStatusVal;
								monitor=DeviceModel.MONITOR_VAL_JEDGE;
							}else {
								newStatusVal=signboardDeviceModel.getStatusValue();
								monitor=DeviceModel.MONITOR_VAL_SIGNBOARD;
							}
							//保存到数据库
							signboardDeviceModel.setId(deviceModel.getId());
							signboardDeviceModel.setStatus(DeviceStatusModel.getStatusByValue(newStatusVal));
							signboardDeviceModel.setStatusValue(newStatusVal);
							signboardDeviceModel.setStatus_monitor_bits(newStatusMonitotBits);
							signboardDeviceModel.setMonitor(monitor);
							deviceModelDao.setDeviceStatus(signboardDeviceModel);
							//设备状态历史记录
							deviceModelDao.insertDeviceStatusHistory(deviceModel, signboardDeviceModel);
							//计算OEE
							deviceOeeService.changeDeviceStatus(signboardDeviceModel);
							//故障状态
							deviceFaultService.changeDeviceStatus(deviceModel, signboardDeviceModel);
							//回调Andon服务
							andonService.updateDeviceStatus(deviceModel, signboardDeviceModel);
							//Jdevice客户端
							jdeviceServer.changeDeviceStatus(signboardDeviceModel,deviceModel);
							logger.info("Signboard Set DeviceStatus To DB,EqpIdentity = "+deviceModel.getEqpIdentity()+", DeviceId = "+deviceModel.getId()+", Status = "+signboardDeviceModel.getStatus());
						}
					}
					//下一个实例
					synchronized (signboardDeviceModelsLockObj) {
						signboardDeviceModels.poll();
						signboardDeviceModel=signboardDeviceModels.peek();
					}
				}
				/**
				 * Jedge设备状态
				 * */
				synchronized (jedgeDeviceModelsLockObj) {
					jedgeDeviceModel=jedgeDeviceModels.peek();
				}
				while (jedgeDeviceModel!=null) {
					//当前设备信息
					DeviceModel newDeviceModel=new DeviceModel();
					jedgeDeviceModel.fillField(newDeviceModel);
					DeviceModel deviceModel=getDeviceModelByEqpIdentity(newDeviceModel.getEqpIdentity(), newDeviceModel.getWorkshopId());
					if (deviceModel!=null) {
						//设备状态位
						int lastStatusMonitotBits=deviceModel.getStatus_monitor_bits();
						if (lastStatusMonitotBits<0) {
							lastStatusMonitotBits=DeviceStatusModel.VALUE_Offline;
						}
						int lastStatusVal=lastStatusMonitotBits%0x10000;
						//设备状态发生变化
						if (newDeviceModel.getStatusValue()!=lastStatusVal) {
							//当前区间状态位
							int statusMonitorBits=newDeviceModel.getStatusValue();
							//标识牌设备状态
							int signboardStatusVal=lastStatusMonitotBits>>16;
							signboardStatusVal=signboardStatusVal%0x10000;
							//新状态位
							int newStatusMonitotBits=(signboardStatusVal<<16)+statusMonitorBits;
							//新设备状态
							int newStatusVal=0;
							int monitor=DeviceModel.MONITOR_VAL_NONE;
							if (signboardStatusVal<=jedgeDeviceModel.getStatusValue()) {
								newStatusVal=jedgeDeviceModel.getStatusValue();
								monitor=DeviceModel.MONITOR_VAL_JEDGE;
							}else {
								newStatusVal=signboardStatusVal;
								monitor=DeviceModel.MONITOR_VAL_SIGNBOARD;
							}
							//保存到数据库
							newDeviceModel.setId(deviceModel.getId());
							newDeviceModel.setStatus(DeviceStatusModel.getStatusByValue(newStatusVal));
							newDeviceModel.setStatusValue(newStatusVal);
							newDeviceModel.setStatus_monitor_bits(newStatusMonitotBits);
							newDeviceModel.setMonitor(monitor);
							deviceModelDao.setDeviceStatus(newDeviceModel);
							//设备状态历史记录
							deviceModelDao.insertDeviceStatusHistory(deviceModel, newDeviceModel);
							//计算OEE
							deviceOeeService.changeDeviceStatus(newDeviceModel);
							//故障状态
							deviceFaultService.changeDeviceStatus(deviceModel, newDeviceModel);
							//回调Andon服务
							andonService.updateDeviceStatus(deviceModel, newDeviceModel);
							//Jdevice客户端
							jdeviceServer.changeDeviceStatus(newDeviceModel,deviceModel);
							logger.info("Jedge Set DeviceStatus To DB,EqpIdentity = "+deviceModel.getEqpIdentity()+", DeviceId = "+deviceModel.getId()+", Status = "+newDeviceModel.getStatus());
						}	
					}
					//下一个实例
					synchronized (jedgeDeviceModelsLockObj) {
						jedgeDeviceModels.poll();
						jedgeDeviceModel=jedgeDeviceModels.peek();
					}
				}
			} catch (Exception e) {
				logger.error("设备状态服务异常", e);
			}
		}
		logger.info("DeviceService Stoped");
	}

	public void setJedgeDeviceModelStatus(JedgeDeviceModel jedgeDeviceModel) {
		if (jedgeDeviceModel==null//
				||StringUtil.isEmpty(jedgeDeviceModel.getEqpIdentity())//
				||StringUtil.isEmpty(jedgeDeviceModel.getOrganizationId())//
				||jedgeDeviceModel.getFactoryId()<=0
				||jedgeDeviceModel.getWorkshopId()<=0) {
			return;
		}
		synchronized (jedgeDeviceModelsLockObj) {
			jedgeDeviceModels.offer(jedgeDeviceModel);
		}
	}
	
	public void setSignboardDeviceStatus(DeviceModel signboardDeviceModel) {
		if (signboardDeviceModel==null//
				||signboardDeviceModel.getId()<=0
				||signboardDeviceModel.getWorkcenterId()<=0
				||signboardDeviceModel.getWorkshopId()<=0
				||signboardDeviceModel.getFactoryId()<=0
				||StringUtil.isEmpty(signboardDeviceModel.getOrganizationId())) {
			return;
		}
		synchronized (signboardDeviceModelsLockObj) {
			signboardDeviceModels.offer(signboardDeviceModel);
		}
	}
	
	public void clearSignboardMonitor(long deviceId) {
		if (deviceId<=0) {
			return ;
		}
		synchronized (clearSignboardMonitorDeviceIdsLockObj) {
			clearSignboardMonitorDeviceIds.offer(deviceId);
		}
	}
	
	private Queue<JedgeDeviceModel> jedgeDeviceModels=new LinkedBlockingQueue<JedgeDeviceModel>();
	private Object jedgeDeviceModelsLockObj=new Object();
	private Queue<DeviceModel> signboardDeviceModels=new LinkedBlockingQueue<DeviceModel>();
	private Object signboardDeviceModelsLockObj=new Object();
	private Queue<Long> clearSignboardMonitorDeviceIds=new LinkedBlockingQueue<Long>();
	private Object clearSignboardMonitorDeviceIdsLockObj=new Object();
	private boolean started;
	public void stopService() {
		started=false;
	}
	public boolean started() {
		return started;
	}
	
	public List<DeviceStatusSumPojo> getDeviceStatusSumPojoByOrganizationId(String organizationId) {
		return deviceModelDao.getDeviceStatusSumPojoByOrganizationId(organizationId);
	}

	public int getMonitorNum(int monitorValNone, String organizationId) {
		return deviceModelDao.getMonitorNum(monitorValNone, organizationId);
	}

	public boolean deleteDeviceModel(DeviceModel deviceModel) {
		if (deviceModelDao.deleteDeviceModel(deviceModel)) {
			deviceFieldModelDao.deleteFieldModelByDeviceId(deviceModel.getId(), deviceModel.getOrganizationId());
			return true;
		}
		return false;
	}

	public boolean setDeviceModel(DeviceModel deviceModel) {
		//修改
		if (deviceModel.getId()>0) {
			//获取当前设备信息
			DeviceModel lastDeviceModel = deviceModelDao.getDeviceModelById(deviceModel.getId());
			if (lastDeviceModel!=null) {
				//设备更换工作中心、车间、工厂
				if (lastDeviceModel.getWorkcenterId()!=deviceModel.getWorkcenterId()//
						||lastDeviceModel.getWorkshopId()!=deviceModel.getWorkshopId()//
						||lastDeviceModel.getFactoryId()!=deviceModel.getFactoryId()) {
					changeWorkcenter(deviceModel.getId(),deviceModel.getWorkcenterId(),deviceModel.getWorkshopId(),deviceModel.getFactoryId());
				}
				//更新设备信息
				return deviceModelDao.setDeviceModel(deviceModel);
			}else {
				deviceModel.setId(-1);
			}
		}
		//新建
		if(deviceModelDao.setDeviceModel(deviceModel)) {
			deviceOeeService.changeDeviceStatus(deviceModel);	
			return true;
		}
		return false;
	}

	public List<DeviceModel> getDeviceModelsByOrganizationId(String organizationId) {
		return deviceModelDao.getDeviceModelsByOrganizationId(organizationId);
	}

	public List<DeviceModel> getDeviceModelsByFactoryId(long factoryId) {
		return deviceModelDao.getDeviceModelsByFactoryId(factoryId);
	}

	public List<DeviceModel> getDeviceModelsByWorkshopId(long workshopId) {
		return deviceModelDao.getDeviceModelsByWorkshopId(workshopId);
	}

	public List<DeviceModel> getDeviceModelsByWorkcenterId(long workcenterId) {
		return deviceModelDao.getDeviceModelsByWorkcenterId(workcenterId);
	}

	public DeviceModel getDeviceModelById(long id) {
		return deviceModelDao.getDeviceModelById(id);
	}

	public DeviceModel getDeviceModelByEqpIdentity(String eqpIdentity, long workshopId) {
		return deviceModelDao.getDeviceModelByEqpIdentity(eqpIdentity, workshopId);
	}

	public void setDeviceMonitor(long deviceId, int monitorVal) {
		deviceModelDao.setDeviceMonitor(deviceId,monitorVal);
	}

	public List<Map<String, Object>> getWorkshopDeviceStatusNum(long workshopId,//
			int statusValue,//
			long startTimeMs) {
		return deviceModelDao.getWorkshopDeviceStatusNum(workshopId, statusValue, startTimeMs);
	}

	public List<Map<String, Object>> getWorkcenterDeviceStatusNum(long workcenterId,//
			int statusValue,//
			long startTimeMs) {
		return deviceModelDao.getWorkcenterDeviceStatusNum(workcenterId, statusValue, startTimeMs);
	}

	public List<Map<String, Object>> getDeviceStatusHistory(long deviceId,long beginTimeMs,int maxNum){
		return deviceModelDao.getDeviceStatusHistory(deviceId,beginTimeMs,maxNum);
	}
	
	public List<Map<String, Object>> getDeviceStatusNum(long deviceId, int statusValue, long startTimeMs) {
		return deviceModelDao.getDeviceStatusNum(deviceId, statusValue, startTimeMs);
	}

	public List<DeviceStatusSumPojo> getDeviceStatusSumPojoByWorkshopId(long workshopId) {
		return deviceModelDao.getDeviceStatusSumPojoByWorkshopId(workshopId);
	}

	public Integer getDevicePreStatusValue(long deviceId,long statusTimeMs) {
		return deviceModelDao.getDevicePreStatusValue(deviceId, statusTimeMs);
	}
	
	public boolean changeStatusHistoryWorkshop(long workcenterId,long workshopId,long factoryId) {
		return deviceModelDao.changeStatusHistoryWorkshop(workcenterId, workshopId, factoryId);
	}
	
	public boolean changeStatusHistoryFactory(long workshopId,long factoryId) {
		return deviceModelDao.changeStatusHistoryFactory(workshopId, factoryId);
	}
	
	public boolean changeFactory(long workshopId,long factoryId) {
		if (deviceModelDao.changeDeviceFactory(workshopId, factoryId)) {
			//设备标识牌
			deviceSignboardService.changeFactory(workshopId, factoryId);
			//OEE
			deviceOeeService.changeFactory(workshopId, factoryId);
			//故障
			deviceFaultService.changeFactory(workshopId, factoryId);
			//当前温湿度
			deviceTempHumiService.changeFactory(workshopId, factoryId);
			//Andon消息
			andonService.changeFactory(workshopId, factoryId);
			//设备状态历史
			changeStatusHistoryFactory(workshopId, factoryId);
			return true;
		}
		return false;
	}

	public boolean changeWorkshop(long workcenterId, long workshopId, long factoryId) {
		if (deviceModelDao.changeDeviceWorkshop(workcenterId,workshopId, factoryId)) {
			//设备标识牌
			deviceSignboardService.changeWorkshop(workcenterId, workshopId, factoryId);
			//OEE
			deviceOeeService.changeWorkshop(workcenterId, workshopId, factoryId);
			//故障
			deviceFaultService.changeWorkshop(workcenterId, workshopId, factoryId);
			//当前温湿度
			deviceTempHumiService.changeWorkshop(workcenterId, workshopId, factoryId);
			//Andon消息
			andonService.changeWorkshop(workcenterId, workshopId, factoryId);
			//设备状态历史
			changeStatusHistoryWorkshop(workcenterId, workshopId, factoryId);
			return true;
		}
		return false;
	}
	
	public boolean changeWorkcenter(long deviceId,long workcenterId,long workshopId,long factoryId) {
		if (deviceModelDao.changeDeviceWorkcenter(deviceId, workcenterId, workshopId, factoryId)) {
			//设备标识牌
			deviceSignboardService.changeWorkcenter(deviceId, workcenterId, workshopId, factoryId);
			//OEE
			deviceOeeService.changeWorkcenter(deviceId, workcenterId, workshopId, factoryId);
			//故障
			deviceFaultService.changeWorkcenter(deviceId, workcenterId, workshopId, factoryId);
			//当前温湿度
			deviceTempHumiService.changeWorkcenter(deviceId, workcenterId, workshopId, factoryId);
			//Andon消息
			andonService.changeWorkcenter(deviceId, workcenterId, workshopId, factoryId);
			//设备状态历史
			deviceModelDao.changeStatusHistoryWorkcenter(deviceId, workcenterId, workshopId, factoryId);
			return true;	
		}
		return false;
	}

	public boolean setDeviceLogoUrlById(long deviceId, String logoUrl) {
		// TODO Auto-generated method stub
		return deviceModelDao.setDeviceLogoUrlById(deviceId,logoUrl);
	}

	public List<Map<String, Object>> getDevicesStatusMapList(long deviceId,//
			long workcenterId,//
			long workshopId,//
			long factoryId,//
			String organizationId,//
			long beginTimeMs,//
			long endTimeMs,//
			int minIndex,//
			int dataNum) {
		// TODO Auto-generated method stub
		return deviceModelDao.getDevicesStatusMapList(deviceId, workcenterId, workshopId, factoryId, organizationId, beginTimeMs, endTimeMs, minIndex, dataNum);
	}
	
	public DeviceModel initWorkcenter(WorkcenterModel workcenterModel) {
		//
		long deviceTypeId=0;
		List<DeviceTypeModel> list = deviceTypeService.getDeviceTypeModelsByWorkshopId(workcenterModel.getWorkshopId());
		if (list!=null&&list.size()>0&&list.get(0)!=null) {
			deviceTypeId=list.get(0).getId();
		}
		//
		DeviceModel deviceModel=new DeviceModel();
		deviceModel.setWorkcenterId(workcenterModel.getId());
		deviceModel.setWorkshopId(workcenterModel.getWorkshopId());
		deviceModel.setFactoryId(workcenterModel.getFactoryId());
		deviceModel.setOrganizationId(workcenterModel.getOrganizationId());
		deviceModel.setStatusTimeMs(System.currentTimeMillis());
		deviceModel.setTypeId(deviceTypeId);
		if (setDeviceModel(deviceModel)) {
			return deviceModel;
		}
		return null;
	}
}
