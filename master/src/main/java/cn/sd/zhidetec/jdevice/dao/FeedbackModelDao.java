package cn.sd.zhidetec.jdevice.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import cn.sd.zhidetec.jdevice.Starter;
import cn.sd.zhidetec.jdevice.config.DataSourceConfiger;
import cn.sd.zhidetec.jdevice.model.FeedbackModel;
import cn.sd.zhidetec.jdevice.util.StringUtil;

@Service
public class FeedbackModelDao {

	@Autowired
	@Qualifier(DataSourceConfiger.BEAN_NAME_JdeviceJdbcTemplate)
	protected JdbcTemplate jdbcTemplate;
	
	public boolean insertModel(FeedbackModel noticeMessageModel) {
		int i = 0;
		Object[] args = new Object[9];
		//新建并入库
		noticeMessageModel.setId(Starter.IdMaker.nextId());
		args[i++] = noticeMessageModel.getId();
		args[i++] = noticeMessageModel.getTimeMs();
		args[i++] = StringUtil.getEmptyString(noticeMessageModel.getTitle());
		args[i++] = StringUtil.getEmptyString(noticeMessageModel.getContent());
		args[i++] = noticeMessageModel.getUserId();
		args[i++] = StringUtil.getEmptyString(noticeMessageModel.getUserName());
		args[i++] = StringUtil.getEmptyString(noticeMessageModel.getUserPhone());
		args[i++] = StringUtil.getEmptyString(noticeMessageModel.getOrganziationId());
		args[i++] = StringUtil.getEmptyString(noticeMessageModel.getOrganizationName());
		i = jdbcTemplate.update(Sql_InsertModel, args);
		if (i > 0) {
			return true;
		}
		return false;
	}
	private static String Sql_InsertModel = "INSERT INTO jdevice_h_feedback (" + //
			"id," + //
			"time_ms," + //
			"title," + //
			"content," + //
			"user_id," + //
			"user_name," + //
			"user_phone," + //
			"organization_id," + //
			"organization_name" + //
			")VALUES(" + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?" + //
			")";
	
}
