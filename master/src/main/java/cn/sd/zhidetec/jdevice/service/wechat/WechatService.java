package cn.sd.zhidetec.jdevice.service.wechat;

import cn.sd.zhidetec.jdevice.dao.UserModelDao;
import cn.sd.zhidetec.jdevice.dao.WechatModeDao;
import cn.sd.zhidetec.jdevice.model.UserModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * 微信小程序登录service
 */
@Service
public class WechatService {

    @Autowired
    protected WechatModeDao wechatModeDao;
    protected Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    protected UserModelDao userModelDao ;

    /**
     * 更新用户信息
     * @param phone
     * @param openId
     * @param wxToken
     * @return
     */
    public boolean setUserWechatByPhone(String phone,String openId,String wxToken){
        return wechatModeDao.setUserWechatByPhone(phone,openId,wxToken);
    }

    /**
     * 根据手机号获取用户信息
     * @param phone
     * @return
     */
    public List<UserModel>  getUserModelsByPhone(String phone) {
        return  userModelDao.getUserModelsByPhone(phone);
    }

    /**
     * 插入用户信息
     * @param u1
     * @return
     */
    public boolean insertWechatUserModel(UserModel u1) {
        return wechatModeDao.insertWechatUserModel(u1);

    }

    /**
     * 根据token查找用户
     * @param wxToken
     * @return
     */
    public List<UserModel> getUserModelsByToken(String wxToken) {
        return wechatModeDao.getUserModelsByToken(wxToken);
    }


}
