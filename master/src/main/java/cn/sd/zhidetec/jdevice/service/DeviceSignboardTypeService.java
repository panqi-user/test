package cn.sd.zhidetec.jdevice.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.sd.zhidetec.jdevice.dao.DeviceSignboardTypeModelDao;
import cn.sd.zhidetec.jdevice.model.DeviceSignboardStatusModel;
import cn.sd.zhidetec.jdevice.model.DeviceSignboardTypeModel;

@Service
public class DeviceSignboardTypeService {
	@Autowired
	protected DeviceSignboardTypeModelDao deviceSignboardTypeModelDao;
	
	public boolean deleteTypeModel(DeviceSignboardTypeModel model) {
		// TODO Auto-generated method stub
		return deviceSignboardTypeModelDao.deleteTypeModel(model);
	}

	public boolean deleteStatusModel(DeviceSignboardStatusModel model) {
		// TODO Auto-generated method stub
		return deviceSignboardTypeModelDao.deleteStatusModel(model);
	}

	public boolean setTypeModel(DeviceSignboardTypeModel model) {
		// TODO Auto-generated method stub
		return deviceSignboardTypeModelDao.setTypeModel(model);
	}

	public boolean setStatusModel(DeviceSignboardStatusModel model) {
		// TODO Auto-generated method stub
		return deviceSignboardTypeModelDao.setStatusModel(model);
	}

	public List<DeviceSignboardTypeModel> getTypeModels() {
		// TODO Auto-generated method stub
		return deviceSignboardTypeModelDao.getTypeModels();
	}

}
