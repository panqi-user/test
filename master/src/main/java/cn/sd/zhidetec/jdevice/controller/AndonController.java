package cn.sd.zhidetec.jdevice.controller;

import java.io.OutputStream;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.sd.zhidetec.jdevice.HttpResponseMsg;
import cn.sd.zhidetec.jdevice.Starter;
import cn.sd.zhidetec.jdevice.dao.AndonMessageModelDao;
import cn.sd.zhidetec.jdevice.dao.AndonReasonModelDao;
import cn.sd.zhidetec.jdevice.dao.AndonRecordModelDao;
import cn.sd.zhidetec.jdevice.model.AndonMessageModel;
import cn.sd.zhidetec.jdevice.model.AndonRecordModel;
import cn.sd.zhidetec.jdevice.model.DeviceModel;
import cn.sd.zhidetec.jdevice.model.MessageModel;
import cn.sd.zhidetec.jdevice.model.RoleModel;
import cn.sd.zhidetec.jdevice.model.UserModel;
import cn.sd.zhidetec.jdevice.service.DeviceService;
import cn.sd.zhidetec.jdevice.service.MsgPushService;
import cn.sd.zhidetec.jdevice.service.RBuilderService;
import cn.sd.zhidetec.jdevice.util.DateUtil;
import cn.sd.zhidetec.jdevice.util.PoiExcelUtil;
import cn.sd.zhidetec.jdevice.util.StringUtil;

@Controller
@RequestMapping("/andon")
public class AndonController {

	@Autowired
	protected MsgPushService msgPushService;
	@Autowired
	protected AndonMessageModelDao andonMessageModelDao;
	@Autowired
	protected AndonRecordModelDao andonRecordModelDao;
	@Autowired
	protected AndonReasonModelDao andonReasonModelDao;
	@Autowired
	protected DeviceService deviceService;
	@Autowired
	protected RBuilderService rBuilderService;
	protected Logger logger = LoggerFactory.getLogger(getClass());

	@RequestMapping(path = "/getAndonMessageModelsByDeviceId")
	@ResponseBody
	public RBuilderService.Response getAndonMessageModelsByDeviceId(HttpSession httpSession, //
			HttpServletRequest httpServletRequest, //
			@RequestParam("status") int status, //
			@RequestParam("deviceId") long deviceId) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_User, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		// 根据用户权限返回数据
		UserModel userModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		// 数据分页
		int minIndex = 0;
		int dataNum = 100;
		String minIndexStr = httpServletRequest.getParameter("minIndex");
		String dataNumStr = httpServletRequest.getParameter("dataNum");
		if (StringUtil.isNumber(minIndexStr)) {
			minIndex = Integer.parseInt(minIndexStr);
		}
		if (StringUtil.isNumber(dataNumStr)) {
			dataNum = Integer.parseInt(dataNumStr);
		}
		// 数据时间
		long beginTimeMs = 0;
		long endTimeMs = System.currentTimeMillis();
		String beginTimeMsStr = httpServletRequest.getParameter("beginTimeMs");
		String endTimeMsStr = httpServletRequest.getParameter("endTimeMs");
		if (StringUtil.isNumber(beginTimeMsStr)) {
			beginTimeMs = Long.parseLong(beginTimeMsStr);
		}
		if (StringUtil.isNumber(endTimeMsStr)) {
			endTimeMs = Long.parseLong(endTimeMsStr);
		}
		// 获取当前设备
		DeviceModel deviceModel = deviceService.getDeviceModelById(deviceId);
		if (deviceModel.getOrganizationId().equals(userModel.getOrganizationId())) {
			return rBuilderService.update(response, //
					HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, //
					andonMessageModelDao.getModelsByDeviceId(deviceId, status, minIndex, dataNum, beginTimeMs,
							endTimeMs));
		}
		return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
	}

	@RequestMapping(path = "/exportAndonMessagesToExcel")
	public void exportAndonMessagesToExcel(HttpSession httpSession, //
			HttpServletRequest httpServletRequest, //
			HttpServletResponse httpServletResponse, //
			@RequestParam("status") int status, //
			@RequestParam("factoryId") long factoryId, //
			@RequestParam("workshopId") long workshopId, //
			@RequestParam("workcenterId") long workcenterId, //
			@RequestParam("deviceId") long deviceId, //
			@RequestParam("handlerId") long handlerId, //
			@RequestParam("beginTimeMs") long beginTimeMs, //
			@RequestParam("endTimeMs") long endTimeMs) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_User, //
				rBuilderService, //
				response) == false) {
			return;
		}
		// 数据分页
		int minIndex = 0;
		int dataNum = 1000;
		String minIndexStr = httpServletRequest.getParameter("minIndex");
		String dataNumStr = httpServletRequest.getParameter("dataNum");
		String fileName = httpServletRequest.getParameter("fileName");
		if (StringUtil.isEmpty(fileName)) {
			fileName = "" + System.currentTimeMillis();
		}
		if (StringUtil.isNumber(minIndexStr)) {
			minIndex = Integer.parseInt(minIndexStr);
		}
		if (StringUtil.isNumber(dataNumStr)) {
			dataNum = Integer.parseInt(dataNumStr);
		}
		// 根据用户权限返回数据
		UserModel userModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		List<AndonMessageModel> list = null;
		if (userModel.getRoleLevel() < RoleModel.ROLE_LEVEL_WorkcenterAdmin) {
			list = andonMessageModelDao.getModels(status, //
					beginTimeMs, //
					endTimeMs, //
					factoryId, //
					workshopId, //
					workcenterId, //
					deviceId, //
					userModel.getId(), //
					minIndex, //
					dataNum, //
					userModel.getOrganizationId());
		}
		if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_WorkcenterAdmin) {
			list = andonMessageModelDao.getModels(status, //
					beginTimeMs, //
					endTimeMs, //
					factoryId, //
					workshopId, //
					userModel.getWorkcenterId(), //
					deviceId, //
					handlerId, //
					minIndex, //
					dataNum, //
					userModel.getOrganizationId());
		}
		if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_WorkshopAdmin) {
			list = andonMessageModelDao.getModels(status, //
					beginTimeMs, //
					endTimeMs, //
					factoryId, //
					userModel.getWorkshopId(), //
					workcenterId, //
					deviceId, //
					handlerId, //
					minIndex, //
					dataNum, //
					userModel.getOrganizationId());
		}
		if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_FactoryAdmin) {
			list = andonMessageModelDao.getModels(status, //
					beginTimeMs, //
					endTimeMs, //
					userModel.getFactoryId(), //
					workshopId, //
					workcenterId, //
					deviceId, //
					handlerId, //
					minIndex, //
					dataNum, //
					userModel.getOrganizationId());
		}
		if (userModel.getRoleLevel() >= RoleModel.ROLE_LEVEL_OrganizationAdmin) {
			list = andonMessageModelDao.getModels(status, //
					beginTimeMs, //
					endTimeMs, //
					factoryId, //
					workshopId, //
					workcenterId, //
					deviceId, //
					handlerId, //
					minIndex, //
					dataNum, //
					userModel.getOrganizationId());
		}
		// 转成Excel
		String[] title = new String[] { "序号", //
				"状态", //
				"标题", //
				"内容", //
				"原因", //
				"认领人", //
				"响应用时", //
				"解决用时", //
				"触发时间", //
				"认领时间", //
				"解决时间", //
				"关闭时间", //
				"设备识别码", //
				"设备编号", //
				"设备名称", //
				"设备状态" };
		String[][] values = new String[list.size()][16];
		int cindex = 0;
		for (int i = 0; i < list.size(); i++) {
			String waitClaimTime = "";
			String waitSolveTime = "";
			String createTime = DateUtil.getDateLong(list.get(i).getCreateTimeMs());
			String handleTime = "";
			String handlerUser = "";
			String solveTime = "";
			String closeTime = "";
			if (list.get(i).getHandlerTimeMs() <= 0) {
				waitClaimTime = DateUtil
						.getDateTimeLengthDisc(list.get(i).getSolveTimeMs() - list.get(i).getCreateTimeMs());
				waitSolveTime = DateUtil
						.getDateTimeLengthDisc(list.get(i).getSolveTimeMs() - list.get(i).getCreateTimeMs());
				handleTime = "未认领";
				handlerUser = "--";
			} else {
				waitClaimTime = DateUtil
						.getDateTimeLengthDisc(list.get(i).getHandlerTimeMs() - list.get(i).getCreateTimeMs());
				waitSolveTime = DateUtil
						.getDateTimeLengthDisc(list.get(i).getSolveTimeMs() - list.get(i).getHandlerTimeMs());
				handleTime = DateUtil.getDateLong(list.get(i).getHandlerTimeMs());
				handlerUser = list.get(i).getHandlerName();
			}
			if (list.get(i).getSolveTimeMs() > 0) {
				solveTime = DateUtil.getDateLong(list.get(i).getSolveTimeMs());
			}
			if (list.get(i).getCloseTimeMs() > 0) {
				closeTime = DateUtil.getDateLong(list.get(i).getCloseTimeMs());
			}
			cindex = 0;
			values[i][cindex++] = StringUtil.getEmptyString("" + (i + 1));
			values[i][cindex++] = StringUtil.getEmptyString(AndonMessageModel.getStatusName(list.get(i).getStatus()));
			values[i][cindex++] = StringUtil.getEmptyString(list.get(i).getTitle());
			values[i][cindex++] = StringUtil.getEmptyString(list.get(i).getContent());
			values[i][cindex++] = StringUtil.getEmptyString(list.get(i).getReason());
			values[i][cindex++] = StringUtil.getEmptyString(handlerUser);
			values[i][cindex++] = StringUtil.getEmptyString(waitClaimTime);
			values[i][cindex++] = StringUtil.getEmptyString(waitSolveTime);
			values[i][cindex++] = StringUtil.getEmptyString(createTime);
			values[i][cindex++] = StringUtil.getEmptyString(handleTime);
			values[i][cindex++] = StringUtil.getEmptyString(solveTime);
			values[i][cindex++] = StringUtil.getEmptyString(closeTime);
			values[i][cindex++] = StringUtil.getEmptyString(list.get(i).getEqpIdentity());
			values[i][cindex++] = StringUtil.getEmptyString(list.get(i).getDeviceCode());
			values[i][cindex++] = StringUtil.getEmptyString(list.get(i).getDeviceName());
			values[i][cindex++] = StringUtil.getEmptyString(list.get(i).getDeviceStatusName());
		}
		HSSFWorkbook hssfWorkbook = PoiExcelUtil.getHSSFWorkbook(fileName, title, values, null);
		// 返回
		try {
			// 设置头
			httpServletResponse.setContentType("application/octet-stream;charset=UTF-8");
			httpServletResponse.setHeader("Content-Disposition", "attachment;filename=" + fileName + ".xls");
			httpServletResponse.addHeader("Pargam", "no-cache");
			httpServletResponse.addHeader("Cache-Control", "no-cache");
			// 返回
			OutputStream outputStream = httpServletResponse.getOutputStream();
			hssfWorkbook.write(outputStream);
			outputStream.flush();
			outputStream.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	/**
	 * 获取Andon消息列表
	 * 
	 * @param httpSession Session会话
	 * @param minStatus   消息的最小状态
	 * @return 返回给前端的操作结果
	 */
	@RequestMapping(path = "/getAndonMessageModels")
	@ResponseBody
	public RBuilderService.Response getAndonMessageModels(HttpSession httpSession, //
			HttpServletRequest httpServletRequest, //
			@RequestParam("status") int status) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_User, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		// 数据分页
		int minIndex = 0;
		int dataNum = 150;
		String minIndexStr = httpServletRequest.getParameter("minIndex");
		String dataNumStr = httpServletRequest.getParameter("dataNum");
		if (StringUtil.isNumber(minIndexStr)) {
			minIndex = Integer.parseInt(minIndexStr);
		}
		if (StringUtil.isNumber(dataNumStr)) {
			dataNum = Integer.parseInt(dataNumStr);
		}
		// 数据时间
		long beginTimeMs = 0;
		long endTimeMs = System.currentTimeMillis();
		String beginTimeMsStr = httpServletRequest.getParameter("beginTimeMs");
		String endTimeMsStr = httpServletRequest.getParameter("endTimeMs");
		if (StringUtil.isNumber(beginTimeMsStr)) {
			beginTimeMs = Long.parseLong(beginTimeMsStr);
		}
		if (StringUtil.isNumber(endTimeMsStr)) {
			endTimeMs = Long.parseLong(endTimeMsStr);
		}
		// 根据用户权限返回数据
		UserModel userModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		if (userModel.getRoleLevel() < RoleModel.ROLE_LEVEL_WorkcenterAdmin//
				&& status <= AndonMessageModel.STATUS_VALUE_Claimed) {
			return rBuilderService.update(response, //
					HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, //
					andonMessageModelDao.getModelsByHandlerId(userModel.getId(), status, minIndex, dataNum, beginTimeMs,
							endTimeMs));
		}
		if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_WorkcenterAdmin) {
			return rBuilderService.update(response, //
					HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, //
					andonMessageModelDao.getModelsByWorkcenterId(userModel.getWorkcenterId(), status, minIndex, dataNum,
							beginTimeMs, endTimeMs));
		}
		if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_WorkshopAdmin) {
			return rBuilderService.update(response, //
					HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, //
					andonMessageModelDao.getModelsByWorkshopId(userModel.getWorkshopId(), status, minIndex, dataNum,
							beginTimeMs, endTimeMs));
		}
		if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_FactoryAdmin) {
			return rBuilderService.update(response, //
					HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, //
					andonMessageModelDao.getModelsByFactoryId(userModel.getFactoryId(), status, minIndex, dataNum,
							beginTimeMs, endTimeMs));
		}
		if (userModel.getRoleLevel() >= RoleModel.ROLE_LEVEL_OrganizationAdmin) {
			return rBuilderService.update(response, //
					HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, //
					andonMessageModelDao.getModelsByOrganizationId(userModel.getOrganizationId(), status, minIndex,
							dataNum, beginTimeMs, endTimeMs));
		}
		return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_DATA, null);
	}

	/**
	 * 根据车间获取未关闭的消息
	 */
	@RequestMapping(path = "/getNotClosedModelsByWorkshopId")
	@ResponseBody
	public RBuilderService.Response getNotClosedModelsByWorkshopId(HttpSession httpSession, //
			HttpServletRequest httpServletRequest, //
			@RequestParam("workshopId") long workshopId) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_User, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		// 数据分页
		int minIndex = 0;
		int dataNum = 150;
		String minIndexStr = httpServletRequest.getParameter("minIndex");
		String dataNumStr = httpServletRequest.getParameter("dataNum");
		if (StringUtil.isNumber(minIndexStr)) {
			minIndex = Integer.parseInt(minIndexStr);
		}
		if (StringUtil.isNumber(dataNumStr)) {
			dataNum = Integer.parseInt(dataNumStr);
		}
		return rBuilderService.update(response, //
				HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, //
				andonMessageModelDao.getNotClosedModelsByWorkshopId(workshopId, minIndex, dataNum));
	}

	/**
	 * 获取当前用户的Andon消息列表
	 * 
	 * @param httpSession Session会话
	 * @param minStatus   消息的状态
	 * @return 返回给前端的操作结果
	 */
	@RequestMapping(path = "/getUserAndonMessageModels")
	@ResponseBody
	public RBuilderService.Response getUserAndonMessageModels(HttpSession httpSession, //
			HttpServletRequest httpServletRequest, //
			@RequestParam("status") int status) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_User, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		// 数据分页
		int minIndex = 0;
		int dataNum = 150;
		String minIndexStr = httpServletRequest.getParameter("minIndex");
		String dataNumStr = httpServletRequest.getParameter("dataNum");
		if (StringUtil.isNumber(minIndexStr)) {
			minIndex = Integer.parseInt(minIndexStr);
		}
		if (StringUtil.isNumber(dataNumStr)) {
			dataNum = Integer.parseInt(dataNumStr);
		}
		// 数据时间
		long beginTimeMs = 0;
		long endTimeMs = System.currentTimeMillis();
		String beginTimeMsStr = httpServletRequest.getParameter("beginTimeMs");
		String endTimeMsStr = httpServletRequest.getParameter("endTimeMs");
		if (StringUtil.isNumber(beginTimeMsStr)) {
			beginTimeMs = Long.parseLong(beginTimeMsStr);
		}
		if (StringUtil.isNumber(endTimeMsStr)) {
			endTimeMs = Long.parseLong(endTimeMsStr);
		}
		// 根据用户权限返回数据
		UserModel userModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		return rBuilderService.update(response, //
				HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, //
				andonMessageModelDao.getModelsByHandlerId(userModel.getId(), status, minIndex, dataNum, beginTimeMs,
						endTimeMs));
	}

	/**
	 * 根据ID获取Andon消息
	 * 
	 * @param httpSession Session会话
	 * @param id          设备id
	 * @return 返回给前端的操作结果
	 */
	@RequestMapping(path = "/getAndonMessageModelById")
	@ResponseBody
	public RBuilderService.Response getAndonMessageModelById(HttpSession httpSession, @RequestParam("id") long id) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_User, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		// 根据用户权限返回数据
		UserModel userModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		AndonMessageModel andonMessageModel = andonMessageModelDao.getAndonMessageModelById(id);
		if (andonMessageModel != null) {
			if (userModel.getRoleLevel() <= RoleModel.ROLE_LEVEL_WorkcenterAdmin//
					&& userModel.getWorkcenterId() == andonMessageModel.getWorkcenterId()) {
				return rBuilderService.update(response, //
						HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, //
						andonMessageModel);
			}
			if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_WorkshopAdmin//
					&& userModel.getWorkshopId() == andonMessageModel.getWorkshopId()) {
				return rBuilderService.update(response, //
						HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, //
						andonMessageModel);
			}
			if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_FactoryAdmin//
					&& userModel.getFactoryId() == andonMessageModel.getFactoryId()) {
				return rBuilderService.update(response, //
						HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, //
						andonMessageModel);
			}
			if (userModel.getRoleLevel() >= RoleModel.ROLE_LEVEL_OrganizationAdmin//
					&& userModel.getOrganizationId().equals(andonMessageModel.getOrganizationId())) {
				return rBuilderService.update(response, //
						HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, //
						andonMessageModel);
			}
		}
		return rBuilderService.update(response, //
				HttpResponseMsg.HEAD_MSG_NOT_FOUND_DATA, //
				null);
	}

	/**
	 * 认领Andon消息
	 * 
	 * @param httpSession Session会话
	 * @param deviceModel 设备信息
	 * @return 返回给前端的操作结果
	 */
	@RequestMapping(path = "/claimAndonMessageModel")
	@ResponseBody
	public RBuilderService.Response claimAndonMessageModel(HttpSession httpSession, //
			@RequestParam("id") long id) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_User, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		// 根据用户权限操作数据
		UserModel userModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		AndonMessageModel andonMessageModel = andonMessageModelDao.getAndonMessageModelById(id);
		// Andon消息是否已被认领
		if (andonMessageModel == null) {
			return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NOT_FOUND_DATA, null);
		} else if (andonMessageModel.hadClaimed()) {
			return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_ANDON_HAD_CLAIMED,
					andonMessageModel.getId());
		} else {
			andonMessageModel.setStatus(AndonMessageModel.STATUS_VALUE_Claimed);
			andonMessageModel.setHandlerId(userModel.getId());
			andonMessageModel.setHandlerName(userModel.getName());
			andonMessageModel.setHandlerTimeMs(System.currentTimeMillis());
		}
		// 生成记录
		AndonRecordModel andonRecordModel = new AndonRecordModel();
		andonRecordModel.setAndonId(andonMessageModel.getId());
		andonRecordModel.setContent("电话: " + userModel.getPhone());
		andonRecordModel.setId(Starter.IdMaker.nextId());
		andonRecordModel.setTimeMs(System.currentTimeMillis());
		andonRecordModel.setTitle(userModel.getName() + "已认领");
		andonRecordModel.setAndonStatus(AndonMessageModel.STATUS_VALUE_Claimed);
		// 认领Andon消息
		if (userModel.getRoleLevel() <= RoleModel.ROLE_LEVEL_WorkcenterAdmin) {
			if (userModel.getWorkcenterId() == andonMessageModel.getWorkcenterId()//
					&& userModel.getWorkshopId() == andonMessageModel.getWorkshopId()//
					&& userModel.getFactoryId() == andonMessageModel.getFactoryId()//
					&& userModel.getOrganizationId().equals(andonMessageModel.getOrganizationId())) {
				if (andonMessageModelDao.updateAndonModel(andonMessageModel)) {
					// 附加Andon记录
					andonRecordModelDao.addRecordModel(andonRecordModel);
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE,
							andonMessageModel.getId());
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
				}
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
			}
		}
		if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_WorkshopAdmin) {
			if (userModel.getWorkshopId() == andonMessageModel.getWorkshopId()//
					&& userModel.getFactoryId() == andonMessageModel.getFactoryId()//
					&& userModel.getOrganizationId().equals(andonMessageModel.getOrganizationId())) {
				if (andonMessageModelDao.updateAndonModel(andonMessageModel)) {
					// 附加Andon记录
					andonRecordModelDao.addRecordModel(andonRecordModel);
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE,
							andonMessageModel.getId());
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
				}
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
			}
		}
		if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_FactoryAdmin) {
			if (userModel.getFactoryId() == andonMessageModel.getFactoryId()//
					&& userModel.getOrganizationId().equals(andonMessageModel.getOrganizationId())) {
				if (andonMessageModelDao.updateAndonModel(andonMessageModel)) {
					// 附加Andon记录
					andonRecordModelDao.addRecordModel(andonRecordModel);
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE,
							andonMessageModel.getId());
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
				}
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
			}
		}
		if (userModel.getRoleLevel() >= RoleModel.ROLE_LEVEL_OrganizationAdmin) {
			if (userModel.getOrganizationId().equals(andonMessageModel.getOrganizationId())) {
				if (andonMessageModelDao.updateAndonModel(andonMessageModel)) {
					// 附加Andon记录
					andonRecordModelDao.addRecordModel(andonRecordModel);
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE,
							andonMessageModel.getId());
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
				}
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
			}
		}
		return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
	}

	/**
	 * 退回认领Andon消息
	 * 
	 * @param httpSession Session会话
	 * @param deviceModel 设备信息
	 * @return 返回给前端的操作结果
	 */
	@RequestMapping(path = "/giveBackAndonMessageModel")
	@ResponseBody
	public RBuilderService.Response giveBackAndonMessageModel(HttpSession httpSession, //
			@RequestParam("id") long id) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_User, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		// 根据用户权限操作数据
		UserModel userModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		AndonMessageModel andonMessageModel = andonMessageModelDao.getAndonMessageModelById(id);
		// Andon消息放弃认领
		if (andonMessageModel == null) {
			return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NOT_FOUND_DATA, null);
		} else {
			andonMessageModel.setStatus(AndonMessageModel.STATUS_VALUE_Unclaimed);
			andonMessageModel.setHandlerId(-1);
			andonMessageModel.setHandlerName("");
			andonMessageModel.setHandlerTimeMs(-1);
		}
		// 生成记录
		AndonRecordModel andonRecordModel = new AndonRecordModel();
		andonRecordModel.setAndonId(andonMessageModel.getId());
		andonRecordModel.setContent("电话: " + userModel.getPhone());
		andonRecordModel.setId(Starter.IdMaker.nextId());
		andonRecordModel.setTimeMs(System.currentTimeMillis());
		andonRecordModel.setTitle(userModel.getName() + "已退回");
		andonRecordModel.setAndonStatus(AndonMessageModel.STATUS_VALUE_Init);
		// 放弃认领Andon消息
		boolean successDone=false;
		if (userModel.getRoleLevel() <= RoleModel.ROLE_LEVEL_WorkcenterAdmin) {
			if (userModel.getWorkcenterId() == andonMessageModel.getWorkcenterId()//
					&& userModel.getWorkshopId() == andonMessageModel.getWorkshopId()//
					&& userModel.getFactoryId() == andonMessageModel.getFactoryId()//
					&& userModel.getOrganizationId().equals(andonMessageModel.getOrganizationId())) {
				successDone=andonMessageModelDao.updateAndonModel(andonMessageModel);
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
			}
		}
		if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_WorkshopAdmin) {
			if (userModel.getWorkshopId() == andonMessageModel.getWorkshopId()//
					&& userModel.getFactoryId() == andonMessageModel.getFactoryId()//
					&& userModel.getOrganizationId().equals(andonMessageModel.getOrganizationId())) {
				successDone=andonMessageModelDao.updateAndonModel(andonMessageModel);
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
			}
		}
		if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_FactoryAdmin) {
			if (userModel.getFactoryId() == andonMessageModel.getFactoryId()//
					&& userModel.getOrganizationId().equals(andonMessageModel.getOrganizationId())) {
				successDone=andonMessageModelDao.updateAndonModel(andonMessageModel);
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
			}
		}
		if (userModel.getRoleLevel() >= RoleModel.ROLE_LEVEL_OrganizationAdmin) {
			if (userModel.getOrganizationId().equals(andonMessageModel.getOrganizationId())) {
				successDone=andonMessageModelDao.updateAndonModel(andonMessageModel);
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
			}
		}
		//操作结果
		if (successDone) {
			// 附加Andon记录
			andonRecordModelDao.addRecordModel(andonRecordModel);
			// 发送通知
			MessageModel messageModel=MessageModel.buildGiveBackMessage(andonMessageModel, System.currentTimeMillis(), userModel.getName());
			msgPushService.pushMessage(messageModel);
			// 返回
			return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE,
					andonMessageModel.getId());
		}
		return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
	}

	/**
	 * 指派Andon消息
	 * 
	 * @param httpSession Session会话
	 * @return 返回给前端的操作结果
	 */
	@RequestMapping(path = "/assignAndonMessageModel")
	@ResponseBody
	public RBuilderService.Response assignAndonMessageModel(HttpSession httpSession, //
			@RequestParam("andonId") long andonId, //
			@RequestParam("assignorId") long assignorId, //
			@RequestParam("assignorName") String assignorName, //
			@RequestParam("handlerId") long handlerId, //
			@RequestParam("handlerName") String handlerName) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_WorkcenterAdmin, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		// 根据用户权限操作数据
		UserModel userModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		AndonMessageModel andonMessageModel = andonMessageModelDao.getAndonMessageModelById(andonId);
		// Andon消息是否已被认领
		if (andonMessageModel == null) {
			return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NOT_FOUND_DATA, null);
		} else {
			andonMessageModel.setStatus(AndonMessageModel.STATUS_VALUE_Claimed);
			andonMessageModel.setHandlerId(handlerId);
			andonMessageModel.setHandlerName(handlerName);
			andonMessageModel.setHandlerTimeMs(System.currentTimeMillis());
			andonMessageModel.setAssignorId(assignorId);
			andonMessageModel.setAssignorName(assignorName);
			andonMessageModel.setAssignorTimeMs(System.currentTimeMillis());
		}
		// 生成记录
		AndonRecordModel andonRecordModel = new AndonRecordModel();
		andonRecordModel.setAndonId(andonMessageModel.getId());
		andonRecordModel.setContent("指派人电话: " + userModel.getPhone());
		andonRecordModel.setId(Starter.IdMaker.nextId());
		andonRecordModel.setTimeMs(System.currentTimeMillis());
		andonRecordModel.setTitle(assignorName + "指派给: " + handlerName);
		andonRecordModel.setAndonStatus(AndonMessageModel.STATUS_VALUE_Claimed);
		// 认领Andon消息
		boolean successDone=false;
		if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_WorkcenterAdmin) {
			if (userModel.getWorkcenterId() == andonMessageModel.getWorkcenterId()//
					&& userModel.getWorkshopId() == andonMessageModel.getWorkshopId()//
					&& userModel.getFactoryId() == andonMessageModel.getFactoryId()//
					&& userModel.getOrganizationId().equals(andonMessageModel.getOrganizationId())) {
				successDone=andonMessageModelDao.updateAndonModel(andonMessageModel);
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
			}
		}
		if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_WorkshopAdmin) {
			if (userModel.getWorkshopId() == andonMessageModel.getWorkshopId()//
					&& userModel.getFactoryId() == andonMessageModel.getFactoryId()//
					&& userModel.getOrganizationId().equals(andonMessageModel.getOrganizationId())) {
				successDone=andonMessageModelDao.updateAndonModel(andonMessageModel);
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
			}
		}
		if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_FactoryAdmin) {
			if (userModel.getFactoryId() == andonMessageModel.getFactoryId()//
					&& userModel.getOrganizationId().equals(andonMessageModel.getOrganizationId())) {
				successDone=andonMessageModelDao.updateAndonModel(andonMessageModel);
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
			}
		}
		if (userModel.getRoleLevel() >= RoleModel.ROLE_LEVEL_OrganizationAdmin) {
			if (userModel.getOrganizationId().equals(andonMessageModel.getOrganizationId())) {
				successDone=andonMessageModelDao.updateAndonModel(andonMessageModel);
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
			}
		}
		//操作成功
		if (successDone) {
			// 附加Andon记录
			andonRecordModelDao.addRecordModel(andonRecordModel);
			// 发送通知
			MessageModel messageModel=MessageModel.build(andonMessageModel, System.currentTimeMillis(), assignorName, handlerName);
			msgPushService.pushMessage(messageModel);
			// 返回
			return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE,
					andonMessageModel.getId());
		}
		return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
	}

	/**
	 * 关闭Andon消息
	 * 
	 * @param httpSession Session会话
	 * @param deviceModel 设备信息
	 * @return 返回给前端的操作结果
	 */
	@RequestMapping(path = "/closeAndonMessageModel")
	@ResponseBody
	public RBuilderService.Response closeAndonMessageModel(HttpSession httpSession, //
			@RequestParam("id") long id, //
			@RequestParam("reason") String reason) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_WorkcenterAdmin, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		// 根据用户权限操作数据
		UserModel userModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		AndonMessageModel andonMessageModel = andonMessageModelDao.getAndonMessageModelById(id);
		// Andon消息是否已被认领
		if (andonMessageModel == null) {
			return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NOT_FOUND_DATA, null);
		} else if (!andonMessageModel.ebableClosed()) {
			return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, andonMessageModel.getId());
		} else {
			andonMessageModel.setStatus(AndonMessageModel.STATUS_VALUE_Closed);
			andonMessageModel.setHandlerId(userModel.getId());
			andonMessageModel.setHandlerName(userModel.getName());
			andonMessageModel.setCloseTimeMs(System.currentTimeMillis());
			andonMessageModel.setReason(reason);
		}
		// 生成记录
		AndonRecordModel andonRecordModel = new AndonRecordModel();
		andonRecordModel.setAndonId(andonMessageModel.getId());
		andonRecordModel.setContent("电话: " + userModel.getPhone());
		andonRecordModel.setId(Starter.IdMaker.nextId());
		andonRecordModel.setTimeMs(System.currentTimeMillis());
		andonRecordModel.setTitle(userModel.getName() + "已关闭");
		andonRecordModel.setAndonStatus(AndonMessageModel.STATUS_VALUE_Closed);
		// 关闭Andon消息
		if (userModel.getRoleLevel() <= RoleModel.ROLE_LEVEL_WorkcenterAdmin) {
			if (userModel.getWorkcenterId() == andonMessageModel.getWorkcenterId()//
					&& userModel.getWorkshopId() == andonMessageModel.getWorkshopId()//
					&& userModel.getFactoryId() == andonMessageModel.getFactoryId()//
					&& userModel.getOrganizationId().equals(andonMessageModel.getOrganizationId())) {
				if (andonMessageModelDao.updateAndonModel(andonMessageModel)) {
					// 附加Andon记录
					andonRecordModelDao.addRecordModel(andonRecordModel);
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE,
							andonMessageModel.getId());
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
				}
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
			}
		}
		if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_WorkshopAdmin) {
			if (userModel.getWorkshopId() == andonMessageModel.getWorkshopId()//
					&& userModel.getFactoryId() == andonMessageModel.getFactoryId()//
					&& userModel.getOrganizationId().equals(andonMessageModel.getOrganizationId())) {
				if (andonMessageModelDao.updateAndonModel(andonMessageModel)) {
					// 附加Andon记录
					andonRecordModelDao.addRecordModel(andonRecordModel);
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE,
							andonMessageModel.getId());
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
				}
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
			}
		}
		if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_FactoryAdmin) {
			if (userModel.getFactoryId() == andonMessageModel.getFactoryId()//
					&& userModel.getOrganizationId().equals(andonMessageModel.getOrganizationId())) {
				if (andonMessageModelDao.updateAndonModel(andonMessageModel)) {
					// 附加Andon记录
					andonRecordModelDao.addRecordModel(andonRecordModel);
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE,
							andonMessageModel.getId());
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
				}
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
			}
		}
		if (userModel.getRoleLevel() >= RoleModel.ROLE_LEVEL_OrganizationAdmin) {
			if (userModel.getOrganizationId().equals(andonMessageModel.getOrganizationId())) {
				if (andonMessageModelDao.updateAndonModel(andonMessageModel)) {
					// 附加Andon记录
					andonRecordModelDao.addRecordModel(andonRecordModel);
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE,
							andonMessageModel.getId());
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
				}
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
			}
		}
		return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
	}
}
