package cn.sd.zhidetec.jdevice.controller;

import cn.sd.zhidetec.jdevice.HttpResponseMsg;
import cn.sd.zhidetec.jdevice.model.UserModel;
import cn.sd.zhidetec.jdevice.service.RBuilderService;
import cn.sd.zhidetec.jdevice.service.TelMsgService;
import cn.sd.zhidetec.jdevice.service.wechat.WechatService;
import cn.sd.zhidetec.jdevice.service.wechat.utils.HttpRequest;
import cn.sd.zhidetec.jdevice.service.wechat.utils.HttpsUrlValidator;
import cn.sd.zhidetec.jdevice.service.wechat.utils.JWTUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * 微小程序controller
 */
@Controller
@RequestMapping("/weChat")
public class WechatController {

    @Autowired
    protected WechatService wechatService;
    @Autowired
    protected TelMsgService telMsgService;
    @Autowired
    protected RBuilderService rBuilderService;
    protected Logger logger = LoggerFactory.getLogger(getClass());

    /**
     * 和微信后台交互获取用户的openid
     */
    public String obtainId(String code) throws JSONException {

        RBuilderService.Response response = rBuilderService.build(null, null);

        //这是用户第一次登录

        // 登录凭证不能为空
        if (code == null || code.length() == 0 || StringUtils.isEmpty(code)) {
            return null;
        }

        // 小程序唯一标识 (在微信小程序管理后台获取)
        String wxspAppid = "wx398f8d53afbb04ad";
        // 小程序的 app secret (在微信小程序管理后台获取)
        String wxspSecret = "d1defaffefe7ebff4580d31f678062f4";
        // 授权（必填）
        String grant_type = "authorization_code";
        //1、向微信服务器 使用登录凭证 code 获取 session_key 和 openid
        // 请求参数
        String params = "appid=" + wxspAppid + "&secret=" + wxspSecret + "&js_code=" + code + "&grant_type="
                + grant_type;
        // 发送请求
        String url = "https://api.weixin.qq.com/sns/jscode2session";
        //先调用下忽略https证书的再请求才可以
        HttpsUrlValidator.retrieveResponseFromServer(url);
        String sr = HttpRequest.sendGet(url, params);
        // 解析相应内容（转换成json对象）
        JSONObject json = new JSONObject(sr);
        // 获取会话密钥（session_key）
        //String session_key = json.get("session_key").toString();
        // 用户的唯一标识（openid）
        String openid = null;
        try {
            openid = (String) json.get("openid");
        } catch (JSONException e) {
            return null;
        }
        //return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, openid);
        return openid;
    }


    /**
     * 通过openid生成token
     *
     * @param openid 用户的唯一标识
     * @param phone  用户的手机号
     * @param vCode  用户输入的验证码
     * @return
     */

    @RequestMapping(value = "/obtainWechatToken", method = RequestMethod.GET)
    @ResponseBody
    public RBuilderService.Response token(HttpSession httpSession, String openid, String phone, String vCode) {

        RBuilderService.Response response = rBuilderService.build(null, null);

        //判断用户的手机号和验证码是否正确
        boolean flag = telMsgService.checkVCode(phone, vCode);


        if (flag) {

            UserModel user = new UserModel();
            user.setWxOpenId(openid);
            //生成token
            String wxToken = JWTUtils.geneJsonWebToken(user);

            List<UserModel> userModelsByPhone = wechatService.getUserModelsByPhone(phone);
            if (userModelsByPhone.size() == 0) {
                //插入数据
                UserModel u1 = new UserModel();
                u1.setWxOpenId(openid);
                u1.setPhone(phone);
                u1.setWxToken(wxToken);
                wechatService.insertWechatUserModel(u1);
            }
            else {

                //更新
                wechatService.setUserWechatByPhone(phone, openid, wxToken);
            }

            UserModel userModel = new UserModel();
            //拿到用户的信息
            List<UserModel> userModelList = wechatService.getUserModelsByPhone(phone);

            for (UserModel userModel2 : userModelList) {
                userModel.setId(userModel2.getId());
                userModel.setStatus(userModel2.getStatus());
                userModel.setWxToken(userModel2.getWxToken());
                userModel.setRoleLevel(userModel2.getRoleLevel());
                userModel.setWxOpenId(userModel2.getWxOpenId());
                userModel.setPassword("");
                userModel.setOrganizationId(userModel2.getOrganizationId());
                userModel.setDeviceBrand(userModel2.getDeviceBrand());
                userModel.setSex(userModel2.getSex());
                userModel.setPhone(userModel2.getPhone());
                userModel.setWarnLevel(userModel2.getWarnLevel());
                userModel.setPosition(userModel2.getPosition());
                userModel.setFactoryId(userModel2.getFactoryId());
                userModel.setAge(userModel2.getAge());
                userModel.setPhotoUrl(userModel2.getPhotoUrl());
                userModel.setCode(userModel2.getCode());
                userModel.setName(userModel2.getName());
                userModel.setWorkshopId(userModel2.getWorkshopId());
                userModel.setDeviceToken(userModel2.getDeviceToken());
                userModel.setWorkcenterId(userModel2.getWorkcenterId());
            }

            //设置session会话域
            httpSession.setAttribute(LoginController.SESSION_KEY_LOGIN_USER, userModel);


            //需要完善个人信息
            if (userModel.needPerfectInfo()) {
                return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_LOGIN_NEED_PERFECT_INFO, userModel);
            }
            //需要加入组织
            if (userModel.noOrganization()) {
                return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_ORGANIZATION, userModel);
            }
            //组织审核中
            if (userModel.underReviewByOrganization()) {
                return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_UNDER_REVIEW_BY_ORGANIZATION, userModel);
            }


            return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, userModel);
        }
        else {
            return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_ERROR_PARAM, null);
        }
    }

    /**
     * 利用wx_token进行登录验证
     */
    @RequestMapping(value = "/wxLogin", method = RequestMethod.POST)
    @ResponseBody
    public RBuilderService.Response checkToken(HttpSession httpSession, @RequestParam("token") String token) {
        RBuilderService.Response response = rBuilderService.build(null, null);
        //查询数据库
        List<UserModel> userModelsByToken = wechatService.getUserModelsByToken(token);
        UserModel userModel = new UserModel();
        if (userModelsByToken.size() != 0) {
            for (UserModel userModel2 : userModelsByToken) {
                userModel.setId(userModel2.getId());
                userModel.setStatus(userModel2.getStatus());
                userModel.setWxToken(userModel2.getWxToken());
                userModel.setRoleLevel(userModel2.getRoleLevel());
                userModel.setWxOpenId(userModel2.getWxOpenId());
                userModel.setPassword("");
                userModel.setOrganizationId(userModel2.getOrganizationId());
                userModel.setDeviceBrand(userModel2.getDeviceBrand());
                userModel.setSex(userModel2.getSex());
                userModel.setPhone(userModel2.getPhone());
                userModel.setWarnLevel(userModel2.getWarnLevel());
                userModel.setPosition(userModel2.getPosition());
                userModel.setFactoryId(userModel2.getFactoryId());
                userModel.setAge(userModel2.getAge());
                userModel.setPhotoUrl(userModel2.getPhotoUrl());
                userModel.setCode(userModel2.getCode());
                userModel.setName(userModel2.getName());
                userModel.setWorkshopId(userModel2.getWorkshopId());
                userModel.setDeviceToken(userModel2.getDeviceToken());
                userModel.setWorkcenterId(userModel2.getWorkcenterId());
            }
            //放进session会话域
            httpSession.setAttribute(LoginController.SESSION_KEY_LOGIN_USER, userModel);

            //需要完善个人信息
            if (userModel.needPerfectInfo()) {
                return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_LOGIN_NEED_PERFECT_INFO, userModel);
            }
            //需要加入组织
            if (userModel.noOrganization()) {
                return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_ORGANIZATION, userModel);
            }
            //组织审核中
            if (userModel.underReviewByOrganization()) {
                return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_UNDER_REVIEW_BY_ORGANIZATION, userModel);
            }

            return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, userModel);//查询成功，有此用户，直接登录
        }
        else {
            return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_ERROR_PARAM, null);//不成功，错误的token
        }


    }


    /**
     * 利用wx_token进行登录验证
     */
    @RequestMapping(value = "/findWechatToken", method = RequestMethod.POST)
    @ResponseBody
    public RBuilderService.Response findWechatToken(HttpSession httpSession, HttpSession session, @RequestParam("code") String code, @RequestParam("userCode") String userCode, @RequestParam("telVCode") String telVCode) {
        RBuilderService.Response response = rBuilderService.build(null, null);
        String openid = null;
        try {
            openid = this.obtainId(code);
            if (openid == null) {
                return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_ERROR_PARAM, "code为空或失效");//不成功，错误的参数
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return token(httpSession, openid, userCode, telVCode);
    }


}
