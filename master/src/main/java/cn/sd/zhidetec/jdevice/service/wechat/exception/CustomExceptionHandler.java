package cn.sd.zhidetec.jdevice.service.wechat.exception;


import cn.sd.zhidetec.jdevice.HttpResponseMsg;
import cn.sd.zhidetec.jdevice.service.RBuilderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;


/**
 * 异常处理类
 */
@ControllerAdvice
public class CustomExceptionHandler {

    @Autowired
    protected RBuilderService rBuilderService;
    private final static Logger logger = LoggerFactory.getLogger(CustomExceptionHandler.class);

    @ExceptionHandler(value = Exception.class)//拦截所有异常
    @ResponseBody
    public RBuilderService.Response handle(Exception e) {

        RBuilderService.Response response = rBuilderService.build(null,null);
        logger.error("[ 系统异常 ]{}", e.getMessage());

        if (e instanceof XDException) {

            XDException xdException = (XDException) e;
            return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE,  xdException.getMsg());
        }
        else {

            return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, "全局异常，code失效");

        }

    }

}
