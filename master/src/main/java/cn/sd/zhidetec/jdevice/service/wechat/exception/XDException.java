package cn.sd.zhidetec.jdevice.service.wechat.exception;

/**
 * 自定义异常类
 */
public class XDException extends RuntimeException {

    //状态码
    private Integer code;
    //错误信息
    private String msg;

    public XDException(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
