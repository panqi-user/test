package cn.sd.zhidetec.jdevice.controller;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.sd.zhidetec.jdevice.HttpResponseMsg;
import cn.sd.zhidetec.jdevice.model.RoleModel;
import cn.sd.zhidetec.jdevice.model.UserModel;
import cn.sd.zhidetec.jdevice.model.WorkcenterModel;
import cn.sd.zhidetec.jdevice.service.DeviceService;
import cn.sd.zhidetec.jdevice.service.RBuilderService;
import cn.sd.zhidetec.jdevice.service.WorkcenterService;

import java.util.List;

/**
 * 工作中心Controller
 */
@Controller
@RequestMapping("/workcenter")
public class WorkcenterController {

	@Autowired
	protected DeviceService deviceService;
	@Autowired
	protected WorkcenterService workcenterService;
	@Autowired
	protected RBuilderService rBuilderService;
	protected Logger logger = LoggerFactory.getLogger(getClass());
	
	/**
	 * 根据工作中心ID获取工作中心
	 * 
	 * @param httpSession  Session会话
	 * @param id id
	 * @return 返回给前端的操作结果
	 */
	@RequestMapping(path = "/getWorkcenterModelById")
	@ResponseBody
	public RBuilderService.Response getWorkcenterModelById(HttpSession httpSession,@RequestParam("id")long id) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_User, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		// 根据用户权限返回数据
		UserModel userModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		WorkcenterModel workcenterModel = workcenterService.getWorkcenterModelById(id);
		if (workcenterModel!=null) {
			if (userModel.getRoleLevel() <= RoleModel.ROLE_LEVEL_WorkcenterAdmin//
					&&userModel.getWorkcenterId()==workcenterModel.getId()) {
				return rBuilderService.update(response, //
						HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, //
						workcenterModel);
			}
			if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_WorkshopAdmin//
					&&userModel.getWorkshopId()==workcenterModel.getWorkshopId()) {
				return rBuilderService.update(response, //
						HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, //
						workcenterModel);
			}
			if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_FactoryAdmin//
					&&userModel.getFactoryId()==workcenterModel.getFactoryId()) {
				return rBuilderService.update(response, //
						HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, //
						workcenterModel);
			}
			if (userModel.getRoleLevel() >= RoleModel.ROLE_LEVEL_OrganizationAdmin//
					&&userModel.getOrganizationId().equals(workcenterModel.getOrganizationId())) {
				return rBuilderService.update(response, //
						HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, //
						workcenterModel);
			}
		}
		return rBuilderService.update(response, //
				HttpResponseMsg.HEAD_MSG_NOT_FOUND_DATA, //
				null);
	}
	
	/**
	 * 从Session中获取组织ID,然后获取工作中心信息
	 * 
	 * @param httpSession Session
	 * @return 组织信息
	 */
	@RequestMapping(path = "/getWorkcenterModels")
	@ResponseBody
	public RBuilderService.Response getWorkcenterModels(HttpSession httpSession) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_User, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		// 根据用户权限返回数据
		UserModel userModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		if (userModel.getRoleLevel() <= RoleModel.ROLE_LEVEL_WorkcenterAdmin) {
			return rBuilderService.update(response, //
					HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, //
					workcenterService.getWorkcenterModelsById(userModel.getWorkcenterId()));
		}
		if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_WorkshopAdmin) {
			return rBuilderService.update(response, //
					HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, //
					workcenterService.getWorkcenterModelsByWorkshopId(userModel.getWorkshopId()));
		}
		if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_FactoryAdmin) {
			return rBuilderService.update(response, //
					HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, //
					workcenterService.getWorkcenterModelsByFactoryId(userModel.getFactoryId()));
		}
		if (userModel.getRoleLevel() >= RoleModel.ROLE_LEVEL_OrganizationAdmin) {
			return rBuilderService.update(response, //
					HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, //
					workcenterService.getWorkcenterModelsByOrganizationId(userModel.getOrganizationId()));
		}
		return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_DATA, null);
	}

	/**
	 * 从Session中获取用户信息，然后修改工作中心
	 * 
	 * @param httpSession Session
	 * @return 操作结果
	 */
	@RequestMapping(path = "/setWorkcenterModel")
	@ResponseBody
	public RBuilderService.Response setWorkcenterModel(HttpSession httpSession, //
			@RequestBody WorkcenterModel workcenterModel) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_WorkcenterAdmin, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		// 根据用户权限操作数据
		UserModel userModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		//新建
		boolean isNew=false;
		if (workcenterModel.getId()<=0) {
			workcenterModel.setOrganizationId(userModel.getOrganizationId());
			isNew=true;
		}
		if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_WorkcenterAdmin) {
			if (userModel.getWorkcenterId()==workcenterModel.getId()//
					&& userModel.getWorkshopId()==workcenterModel.getWorkshopId()//
					&& userModel.getFactoryId()==workcenterModel.getFactoryId()//
					&& userModel.getOrganizationId().equals(workcenterModel.getOrganizationId())) {
				if (workcenterService.setWorkcenterModel(workcenterModel)) {
					if (isNew) {
						deviceService.initWorkcenter(workcenterModel);
					}
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, null);
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
				}
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
			}
		}
		if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_WorkshopAdmin) {
			if (userModel.getWorkshopId()==workcenterModel.getWorkshopId()//
					&& userModel.getFactoryId()==workcenterModel.getFactoryId()//
					&& userModel.getOrganizationId().equals(workcenterModel.getOrganizationId())) {
				if (workcenterService.setWorkcenterModel(workcenterModel)) {
					if (isNew) {
						deviceService.initWorkcenter(workcenterModel);
					}
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, null);
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
				}
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
			}
		}
		if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_FactoryAdmin) {
			if (userModel.getFactoryId()==workcenterModel.getFactoryId()//
					&& userModel.getOrganizationId().equals(workcenterModel.getOrganizationId())) {
				if (workcenterService.setWorkcenterModel(workcenterModel)) {
					if (isNew) {
						deviceService.initWorkcenter(workcenterModel);
					}
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, null);
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
				}
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
			}
		}
		if (userModel.getRoleLevel() >= RoleModel.ROLE_LEVEL_OrganizationAdmin) {
			if (userModel.getOrganizationId().equals(workcenterModel.getOrganizationId())) {
				if (workcenterService.setWorkcenterModel(workcenterModel)) {
					if (isNew) {
						deviceService.initWorkcenter(workcenterModel);
					}
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, null);
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
				}
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
			}
		}
		return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
	}

	/**
	 * 从Session中获取用户信息，然后删除工作中心
	 * 
	 * @param httpSession Session
	 * @return 操作结果
	 */
	@RequestMapping(path = "/deleteWorkcenterModel")
	@ResponseBody
	public RBuilderService.Response deleteWorkcenterModel(HttpSession httpSession, //
			@RequestBody WorkcenterModel workcenterModel) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_WorkshopAdmin, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		// 根据用户权限操作数据
		UserModel userModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_WorkshopAdmin) {
			if (userModel.getWorkshopId()==workcenterModel.getWorkshopId()//
					&& userModel.getFactoryId()==workcenterModel.getFactoryId()//
					&& userModel.getOrganizationId().equals(workcenterModel.getOrganizationId())) {
				if (workcenterService.deleteWorkcenterModel(workcenterModel)) {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, null);
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
				}
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
			}
		}
		if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_FactoryAdmin) {
			if (userModel.getFactoryId()==workcenterModel.getFactoryId()//
					&& userModel.getOrganizationId().equals(workcenterModel.getOrganizationId())) {
				if (workcenterService.deleteWorkcenterModel(workcenterModel)) {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, null);
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
				}
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
			}
		}
		if (userModel.getRoleLevel() >= RoleModel.ROLE_LEVEL_OrganizationAdmin) {
			if (userModel.getOrganizationId().equals(workcenterModel.getOrganizationId())) {
				if (workcenterService.deleteWorkcenterModel(workcenterModel)) {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, null);
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
				}
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
			}
		}
		return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
	}


    /**
     *根据车间id获取所有设备组
     * @param workshopId   车间id
     * @return
     */
	@RequestMapping(path = "findByWorkShopId")
	@ResponseBody
	public RBuilderService.Response getWorkcenterModelsByWorkshopId(long workshopId){
        RBuilderService.Response response = rBuilderService.build(null,null);
        List<WorkcenterModel> workcenterModelList = workcenterService.getWorkcenterModelsByWorkshopId(workshopId);
        if (workcenterModelList.size() != 0){
            return rBuilderService.update(response,HttpResponseMsg.HEAD_MSG_SUCCESS_DONE,workcenterModelList);
        }else {
            return rBuilderService.update(response,HttpResponseMsg.HEAD_MSG_ERROR_PARAM,null);
        }

    }




}
