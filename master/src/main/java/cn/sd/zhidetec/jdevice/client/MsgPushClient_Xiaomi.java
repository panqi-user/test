package cn.sd.zhidetec.jdevice.client;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingDeque;

import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.xiaomi.xmpush.server.Constants;
import com.xiaomi.xmpush.server.Message;
import com.xiaomi.xmpush.server.Result;
import com.xiaomi.xmpush.server.Sender;

import cn.sd.zhidetec.jdevice.dao.UserModelDao;
import cn.sd.zhidetec.jdevice.model.MessageModel;
import cn.sd.zhidetec.jdevice.util.StringUtil;

/**
 * 小米消息推送服务
 * */
@Service
public class MsgPushClient_Xiaomi extends Thread{
	
	@Value("${xiaomi.appSecretkey}")
	private String appSecretkey = "nPBj0toRu5T1D/8afTb6Ow==";
	@Value("${xiaomi.packageName}")
	private String packageName = "cn.sd.zhidetec.jdevice.app";
	@Autowired
	protected UserModelDao userModelDao;
	protected Logger logger = LoggerFactory.getLogger(getClass());

	private boolean started;
	private Queue<MessageModel> andonMessageModels = new LinkedBlockingDeque<>();
	private Object andonMessageModelsLockObj = new Object();
	public void stopService() {
		started=false;
	}
	public boolean started() {
		return started;
	}
	public void sendMessageModel(MessageModel andonMessageModel) {
		synchronized (andonMessageModelsLockObj) {
			andonMessageModels.add(andonMessageModel);
		}
	}
	@Override
	public void run() {
		started=true;
		logger.info(getClass().getSimpleName()+" Started");
		while (started) {
			try {
				Thread.sleep(1);
				if (andonMessageModels.size()<=0) {
					Thread.sleep(10);
					continue;
				}
				MessageModel messageModel=andonMessageModels.peek();
				if (messageModel==null) {
					andonMessageModels.poll();
					Thread.sleep(10);
					continue;
				}
				//获取所有设备Token
				List<String> list = null;
				if (messageModel.getType()==MessageModel.TYPE_System_Notice) {
					list = userModelDao.getAllDeviceTokenList();
				}else {
					list = userModelDao.getDeviceTokenList(messageModel.getOrganizationId(),//
							messageModel.getFactoryId(),//
							messageModel.getWorkshopId(),//
							messageModel.getWorkcenterId(),//
							DEVICE_BRAND,//
							messageModel.getWarnLevel());
				}
				if (list==null||list.size()<=0) {
				}else {
					//开始推送
					syncSendPushMessage(list, messageModel);	
				}
				andonMessageModels.poll();
			}catch (Exception e) {
				logger.error(getClass().getSimpleName()+" 消息推送服务异常", e);
			}
		}
	}
	/**
	 * 以线程同步的方式推送消息
	 * @param deviceTokenList 目标设备Token
	 * @param andonMessageModel Andon消息
	 * @throws ParseException 
	 * */
	public void syncSendPushMessage(List<String> deviceTokenList,MessageModel andonMessageModel) throws IOException, ParseException {  
		if (sender==null) {
		    Constants.useOfficial();
		    sender = new Sender(appSecretkey);	
		}
		if (notifyId>=Integer.MAX_VALUE) {
			notifyId=1;
		}
	    String messagePayload = ""+andonMessageModel.getId();
	    String title = andonMessageModel.getTitle();
	    String description = andonMessageModel.getContent();
	    Message message = new Message.Builder()//
	        .title(title)//
	        .description(description)//
	        .payload(messagePayload)//
	        .restrictedPackageName(packageName)//
	        .notifyType(1)// 使用默认提示音提示
	        .notifyId(notifyId++)
	        .extra(Constants.EXTRA_PARAM_NOTIFY_EFFECT, Constants.NOTIFY_ACTIVITY)
            .extra(Constants.EXTRA_PARAM_INTENT_URI, "intent://cn.sd.zhidetec.jdevice.app:7080/app?#Intent;"//
            		+ "scheme=mainscheme;"//
            		+ "launchFlags=0x4000000;"//
            		+ "S.WEBVIEW_LOAD_URL="+andonMessageModel.getUrl()+";"//
            		+ "end")
	        .build();
	    Result result = sender.send(message, deviceTokenList, 3);
	    logger.debug(getClass().getName()+" Server response: "+"MessageId: " + result.getMessageId()
        + " ErrorCode: " + result.getErrorCode().toString()
        + " Reason: " + result.getReason());
    }
	private Sender sender=null;
	private int notifyId=1;
	
	public final static String DEVICE_BRAND="Xiaomi";
	public static boolean isXiaomiDevice(String deviceBrand) {
		if (StringUtil.isEmpty(deviceBrand)) {
			return false;
		}
		if (deviceBrand.equalsIgnoreCase("xiaomi")) {
			return true;
		}
		return false;
	}
	
	public static void main(String[] args) {
		MsgPushClient_Xiaomi xiaoMiPushService=new MsgPushClient_Xiaomi();
		List<String> tokenList=new ArrayList<String>();
		tokenList.add("CWOhllroYSZKN/jNTA0CTmw8ojWf3eZBDg693MlvwTDMjDte07cWYachIFvkLoi4");
		MessageModel andonMessageModel=new MessageModel();
		andonMessageModel.setTitle("title");
		andonMessageModel.setContent("content");
		andonMessageModel.setTimeMs(System.currentTimeMillis());
		try {
			xiaoMiPushService.syncSendPushMessage(tokenList, andonMessageModel);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
