package cn.sd.zhidetec.jdevice.dao;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import cn.sd.zhidetec.jdevice.Starter;
import cn.sd.zhidetec.jdevice.config.DataSourceConfiger;
import cn.sd.zhidetec.jdevice.model.ShiftModel;
import cn.sd.zhidetec.jdevice.model.WorkshopModel;
import cn.sd.zhidetec.jdevice.util.StringUtil;

@Service
public class ShiftModelDao {

	@Autowired
	@Qualifier(DataSourceConfiger.BEAN_NAME_JdeviceJdbcTemplate)
	protected JdbcTemplate jdbcTemplate;
	protected Logger logger = LoggerFactory.getLogger(getClass());
	private BeanPropertyRowMapper<ShiftModel> beanPropertyRowMapper = new BeanPropertyRowMapper<ShiftModel>(
			ShiftModel.class);
	
	/**
	 * 设置班次信息，如果不存在则新建
	 * 
	 * @param model 班次信息
	 * @return 操作是否成功
	 */
	public boolean setShiftModel(ShiftModel model) {
		if (StringUtil.isEmpty(model.getOrganizationId())) {
			return false;
		}
		//
		int i = 0;
		Object[] args = new Object[7];
		// 修改信息
		if (model.getId() > 0) {
			args[i++] = StringUtil.getEmptyString(model.getName());
			args[i++] = model.getBeginTimeMin();
			args[i++] = model.getEndTimeMin();
			args[i++] = model.getWorkshopId();
			args[i++] = model.getFactoryId();
			args[i++] = StringUtil.getEmptyString(model.getOrganizationId());
			args[i++] = model.getId();
			i = jdbcTemplate.update(Sql_UpdateModel_By_Id, args);
		}
		// 新建信息
		else {
			model.setId(Starter.IdMaker.nextId());
			args[i++] = model.getId();
			args[i++] = StringUtil.getEmptyString(model.getName());
			args[i++] = model.getBeginTimeMin();
			args[i++] = model.getEndTimeMin();
			args[i++] = model.getWorkshopId();
			args[i++] = model.getFactoryId();
			args[i++] = StringUtil.getEmptyString(model.getOrganizationId());
			i = jdbcTemplate.update(Sql_InsertModel, args);
		}
		if (i > 0) {
			return true;
		}
		return false;
	}

	private static String Sql_UpdateModel_By_Id = "UPDATE jdevice_c_shift" + //
			" SET " + //
			"name = ?," + //
			"begin_time_min = ?," + //
			"end_time_min = ?," + //
			"workshop_id = ?," + //
			"factory_id = ?," + //
			"organization_id = ?" + //
			" WHERE " + //
			"id=?";
	private static String Sql_InsertModel = "INSERT INTO jdevice_c_shift (" + //
			"id," + //
			"name," + //
			"begin_time_min," + //
			"end_time_min," + //
			"workshop_id," + //
			"factory_id," + //
			"organization_id" + //
			")VALUES(" + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?" + //
			")";
	
	/**
	 * 根据车间ID获取班次列表
	 * 
	 * @param workshopId 车间ID
	 * @return 数据列表
	 */
	public List<ShiftModel> getShiftModelsByWorkshopId(long workshopId) {
		Object[] args = new Object[1];
		args[0] = workshopId;
		List<ShiftModel> list = jdbcTemplate.query(Sql_GetModels_By_WorkshopId, args, beanPropertyRowMapper);
		return list;
	}

	private static String Sql_GetModels_By_WorkshopId = "SELECT * FROM jdevice_c_shift WHERE workshop_id = ?";
	
	/**
	 * 删除班次
	 * 
	 * @param workshopModel 车间信息
	 * @return 操作是否成功
	 */
	public boolean deleteShiftModel(ShiftModel model) {
		int i = 0;
		Object[] args = new Object[1];
		args[i++] = model.getId();
		if (model.getId() > 0) {
			i = jdbcTemplate.update(Sql_DeleteModel_By_Id, args);
		}
		if (i > 0) {
			return true;
		}
		return false;
	}

	private static String Sql_DeleteModel_By_Id = "DELETE FROM jdevice_c_shift WHERE id=?";

	public List<ShiftModel> getAllShiftModels() {
		List<ShiftModel> list = jdbcTemplate.query(Sql_GetAllShiftModels, beanPropertyRowMapper);
		return list;
	}

	private static String Sql_GetAllShiftModels="SELECT * FROM jdevice_c_shift ORDER BY workshop_id DESC";
	
	public List<ShiftModel> initWorkshop(WorkshopModel workshopModel) {
		List<ShiftModel> list=new ArrayList<ShiftModel>();
		if (workshopModel==null) {
			return list;
		}
		//白班
		ShiftModel shiftModel=new ShiftModel();
		shiftModel.setBeginTimeMin(8*60);
		shiftModel.setEndTimeMin(20*60);
		shiftModel.setName("白班");
		shiftModel.setOrganizationId(workshopModel.getOrganizationId());
		shiftModel.setFactoryId(workshopModel.getFactoryId());
		shiftModel.setWorkshopId(workshopModel.getId());
		if (setShiftModel(shiftModel)) {
			list.add(shiftModel);
		}
		//夜班
		shiftModel=new ShiftModel();
		shiftModel.setBeginTimeMin(20*60);
		shiftModel.setEndTimeMin(8*60);
		shiftModel.setName("夜班");
		shiftModel.setOrganizationId(workshopModel.getOrganizationId());
		shiftModel.setFactoryId(workshopModel.getFactoryId());
		shiftModel.setWorkshopId(workshopModel.getId());
		if (setShiftModel(shiftModel)) {
			list.add(shiftModel);
		}
		return list;
	}

	public ShiftModel getShiftModelById(long id) {
		Object[] args = new Object[1];
		args[0] = id;
		List<ShiftModel> list = jdbcTemplate.query(Sql_GetModels_By_Id, args, beanPropertyRowMapper);
		if (list!=null&&list.size()>0) {
			return list.get(0);
		}
		return null;
	}

	private static String Sql_GetModels_By_Id = "SELECT * FROM jdevice_c_shift WHERE id = ?";
	
	public boolean changeFactory(long workshopId,long factoryId) {
		Object[] args = new Object[2];
		int i=0;
		args[i++] = factoryId;
		args[i++] = workshopId;
		return jdbcTemplate.update(Sql_ChangeFactory_By_WorkshopId, args)>0;
	}
	
	private static String Sql_ChangeFactory_By_WorkshopId="UPDATE jdevice_c_shift SET factory_id=? WHERE workshop_id=?;";
	
}
