package cn.sd.zhidetec.jdevice.controller;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.sd.zhidetec.jdevice.HttpResponseMsg;
import cn.sd.zhidetec.jdevice.dao.FactoryModelDao;
import cn.sd.zhidetec.jdevice.model.FactoryModel;
import cn.sd.zhidetec.jdevice.model.RoleModel;
import cn.sd.zhidetec.jdevice.model.UserModel;
import cn.sd.zhidetec.jdevice.service.RBuilderService;
import cn.sd.zhidetec.jdevice.service.WorkshopService;

/**
 * 工厂管理Controller
 * */
@Controller
@RequestMapping("/factory")
public class FactoryController {

	@Autowired
	protected FactoryModelDao factoryModelDao;
	@Autowired
	protected WorkshopService workshopModelDao;
	@Autowired
	protected RBuilderService rBuilderService;
	protected Logger logger = LoggerFactory.getLogger(getClass());

	/**
	 * 根据工厂ID获取工厂
	 * 
	 * @param httpSession  Session会话
	 * @param id id
	 * @return 返回给前端的操作结果
	 */
	@RequestMapping(path = "/getFactoryModelById")
	@ResponseBody
	public RBuilderService.Response getFactoryModelById(HttpSession httpSession,@RequestParam("id")long id) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_User, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		// 根据用户权限返回数据
		UserModel userModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		FactoryModel model = factoryModelDao.getFactoryModelById(id);
		if (model!=null) {
			if (userModel.getRoleLevel() <= RoleModel.ROLE_LEVEL_FactoryAdmin//
					&&userModel.getFactoryId()==model.getId()) {
				return rBuilderService.update(response, //
						HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, //
						model);
			}
			if (userModel.getRoleLevel() >= RoleModel.ROLE_LEVEL_OrganizationAdmin//
					&&userModel.getOrganizationId().equals(model.getOrganizationId())) {
				return rBuilderService.update(response, //
						HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, //
						model);
			}
		}
		return rBuilderService.update(response, //
				HttpResponseMsg.HEAD_MSG_NOT_FOUND_DATA, //
				null);
	}
	
	/**
	 * 获取工厂信息，如果工厂编号为空，则返回全部
	 * 
	 * @param httpSession Session会话
	 * @return 返回给前端的操作结果
	 */
	@RequestMapping(path = "/getFactoryModels")
	@ResponseBody
	public RBuilderService.Response getFactoryModels(HttpSession httpSession) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_User, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		// 根据用户权限返回数据
		UserModel userModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		if (userModel.getRoleLevel() <= RoleModel.ROLE_LEVEL_FactoryAdmin) {
			return rBuilderService.update(response, //
					HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, //
					factoryModelDao.getFactoryModelsById(userModel.getFactoryId()));
		}
		if (userModel.getRoleLevel() >= RoleModel.ROLE_LEVEL_OrganizationAdmin) {
			return rBuilderService.update(response, //
					HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, //
					factoryModelDao.getFactoryModelsByOrganizationId(userModel.getOrganizationId()));
		}
		return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_DATA, null);
	}

	/**
	 * 设置工厂信息
	 * 
	 * @param httpSession  Session会话
	 * @param factoryModel 工厂信息
	 * @return 返回给前端的操作结果
	 */
	@RequestMapping(path = "/setFactoryModel")
	@ResponseBody
	public RBuilderService.Response setFactoryModel(HttpSession httpSession, //
			@RequestBody FactoryModel factoryModel) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_FactoryAdmin, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		// 根据用户权限操作数据
		UserModel userModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		//新建
		boolean isNew=false;
		if (factoryModel.getId()<=0) {
			factoryModel.setOrganizationId(userModel.getOrganizationId());
			isNew=true;
		}
		if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_FactoryAdmin) {
			if (userModel.getFactoryId()==factoryModel.getId()//
					&& userModel.getOrganizationId().equals(factoryModel.getOrganizationId())) {
				if (factoryModelDao.setFactoryModel(factoryModel)) {
					//默认值
					if (isNew) {
						workshopModelDao.initFactory(factoryModel);	
					}
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, null);
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
				}
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
			}
		}
		if (userModel.getRoleLevel() >= RoleModel.ROLE_LEVEL_OrganizationAdmin) {
			if (userModel.getOrganizationId().equals(factoryModel.getOrganizationId())) {
				if (factoryModelDao.setFactoryModel(factoryModel)) {
					//默认值
					if (isNew) {
						workshopModelDao.initFactory(factoryModel);	
					}
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, null);
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
				}
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
			}
		}
		return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
	}

	/**
	 * 从Session中获取用户信息，然后删除工厂
	 * 
	 * @param httpSession Session
	 * @return 操作结果
	 */
	@RequestMapping(path = "/deleteFactoryModel")
	@ResponseBody
	public RBuilderService.Response deleteFactoryModel(HttpSession httpSession, //
			@RequestBody FactoryModel factoryModel) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_OrganizationAdmin, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		// 根据用户权限操作数据
		UserModel userModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		if (userModel.getRoleLevel() >= RoleModel.ROLE_LEVEL_OrganizationAdmin) {
			if (userModel.getOrganizationId().equals(factoryModel.getOrganizationId())) {
				if (factoryModelDao.deleteFactoryModel(factoryModel)) {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, null);
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
				}
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
			}
		}
		return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
	}
}
