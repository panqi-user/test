package cn.sd.zhidetec.jdevice.service;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.sd.zhidetec.jdevice.dao.DeviceOeeModelDao;
import cn.sd.zhidetec.jdevice.model.DeviceModel;
import cn.sd.zhidetec.jdevice.model.DeviceOeeModel;
import cn.sd.zhidetec.jdevice.model.DeviceStatusModel;
import cn.sd.zhidetec.jdevice.model.ShiftModel;
import cn.sd.zhidetec.jdevice.util.DateUtil;

@Service
public class DeviceOeeService {

	@Autowired
	protected DeviceOeeModelDao deviceOeeModelDao;
	@Autowired
	protected ShiftService shiftService;
	@Autowired
	protected DeviceService deviceService;
	protected Logger logger = LoggerFactory.getLogger(getClass());
	public DeviceOeeModel computeDeviceOeeAndRunTime(DeviceOeeModel deviceOeeModel) {
		return computeDeviceOeeAndRunTime(deviceOeeModel, System.currentTimeMillis());
	}
	public DeviceOeeModel computeDeviceOeeAndRunTime(DeviceOeeModel deviceOeeModel,long nowTimeMs) {
		if (deviceOeeModel==null) {
			return null;
		}
		long runTimeMs=deviceOeeModel.getRunTimeMs();
		if (deviceOeeModel.getStatusValue()==DeviceStatusModel.VALUE_Run) {
			if (nowTimeMs>deviceOeeModel.getEndTimeMs()) {
				runTimeMs+=deviceOeeModel.getEndTimeMs()-deviceOeeModel.getStatusTimeMs();
			}else {
				runTimeMs+=nowTimeMs-deviceOeeModel.getStatusTimeMs();
			}
			deviceOeeModel.setRunTimeMs(runTimeMs);
		}
		long sumTimeMs=nowTimeMs-deviceOeeModel.getBeginTimeMs();
		int oee=0;
		if (sumTimeMs>0) {
			oee=Math.round(runTimeMs*100f/sumTimeMs);	
		}
		deviceOeeModel.setOee(oee);
		return deviceOeeModel;
	}
	public List<DeviceOeeModel> computeDeviceOeeAndRunTime(List<DeviceOeeModel> list){
		long nowTimeMs=System.currentTimeMillis();
		for (DeviceOeeModel deviceOeeModel : list) {
			computeDeviceOeeAndRunTime(deviceOeeModel,nowTimeMs);
		}
		//根据Oee进行升序
		Collections.sort(list,deviceOeeComparator);
		return list;
	}
	public static Comparator<DeviceOeeModel> deviceOeeComparator=new Comparator<DeviceOeeModel>() {
		@Override
		public int compare(DeviceOeeModel o1, DeviceOeeModel o2) {
			return o1.getOee()-o2.getOee();
		}
	};
	
	public List<DeviceOeeModel> getWorkshopAShiftDeviceOeeModels(long workshopId, long shiftTimeMs) {
		return computeDeviceOeeAndRunTime(deviceOeeModelDao.getWorkshopAShiftDeviceOeeModels(workshopId, shiftTimeMs));
	}
	
	public List<DeviceOeeModel> getWorkcenterAShiftDeviceOeeModels(long workcenterId, long shiftTimeMs) {
		// TODO Auto-generated method stub
		return computeDeviceOeeAndRunTime(deviceOeeModelDao.getWorkcenterAShiftDeviceOeeModels(workcenterId, shiftTimeMs));
	}
	
	public List<DeviceOeeModel> getDeviceOeeModels(long deviceId,long shiftBeginTimeMs) {
		return computeDeviceOeeAndRunTime(deviceOeeModelDao.getDeviceOeeModels(deviceId, shiftBeginTimeMs));
	}
	
	private DeviceOeeModel getLastDeviceOeeModelByDeviceId(long deviceId) {
		return deviceOeeModelDao.getLastDeviceOeeModelByDeviceId(deviceId);
	}
	
	public DeviceOeeModel getNoOeeModelByDeviceIdAndShiftTimeMs(long deviceId,long shiftTimeMs) {
		return deviceOeeModelDao.getDeviceOeeModelByDeviceIdAndShiftTimeMs(deviceId,shiftTimeMs);
	}
	
	public DeviceOeeModel setDeviceOeeModel(DeviceOeeModel deviceOeeModel) {
		return deviceOeeModelDao.setDeviceOeeModel(deviceOeeModel);
	}
	
	public void changeDeviceStatus(DeviceModel deviceModel) {
		synchronized (changeDeviceStatusQueueLockObj) {
			//加入队列
			DeviceModel model = new DeviceModel();
			model.copyField(deviceModel);
			changeDeviceStatusQueue.offer(model);
		}
	}
	
	private Thread thread;
	private boolean started=false;
	public void start() {
		if (started) {
			return;
		}
		started=true;
		thread=new Thread(){
			@Override
			public void run() {
				logger.info("DeviceOeeService Started");
				DeviceModel changeDeviceModel=null;
				ShiftModel changeShiftModel=null;
				while (started) {
					try {
						Thread.sleep(1);
						/**
						 * 设备状态更新
						 * */
						synchronized (changeDeviceStatusQueueLockObj) {
							changeDeviceModel=changeDeviceStatusQueue.peek();
						}
						while (changeDeviceModel!=null) {
							//获取当前OEE
							DeviceOeeModel deviceOeeModel=getLastDeviceOeeModelByDeviceId(changeDeviceModel.getId());
							long nowTimeMs=System.currentTimeMillis();
							//不存在则新建
							if (deviceOeeModel==null) {
								//获取所在班次
								ShiftModel shiftModel=shiftService.getShiftModelByWorkshopId(changeDeviceModel.getWorkshopId(),nowTimeMs);
								//不存在
								if (shiftModel==null) {
									
								}else {
									long todayTimeMs=DateUtil.getTodayBeginTimeMs();
									//不跨天
									long shiftBeginTimeMs=todayTimeMs+shiftModel.getBeginTimeMin()*DateUtil.MILLISECOND_ONE_MINUTE;
									long shiftEndTimeMs;
									if (!shiftModel.daySpan()) {
										shiftEndTimeMs=todayTimeMs+shiftModel.getEndTimeMin()*DateUtil.MILLISECOND_ONE_MINUTE;
									}
									//跨天
									else {
										shiftEndTimeMs=todayTimeMs+shiftModel.getEndTimeMin()*DateUtil.MILLISECOND_ONE_MINUTE+DateUtil.MILLISECOND_ONE_DAY;
									}
									//新建
									deviceOeeModel=new DeviceOeeModel();
									deviceOeeModel.setBeginTimeMs(shiftBeginTimeMs);
									deviceOeeModel.setDeviceId(changeDeviceModel.getId());
									deviceOeeModel.setEndTimeMs(shiftEndTimeMs);
									deviceOeeModel.setFactoryId(changeDeviceModel.getFactoryId());
									//新建
									deviceOeeModel.setId(-1);
									deviceOeeModel.setOee(-1);
									deviceOeeModel.setOrganizationId(changeDeviceModel.getOrganizationId());
									deviceOeeModel.setRunTimeMs(0);
									deviceOeeModel.setShiftBeginTimeMin(shiftModel.getBeginTimeMin());
									deviceOeeModel.setShiftEndTimeMin(shiftModel.getEndTimeMin());
									deviceOeeModel.setShiftName(shiftModel.getName());
									deviceOeeModel.setStatusTimeMs(nowTimeMs);
									deviceOeeModel.setStatusValue(changeDeviceModel.getStatusValue());
									deviceOeeModel.setWorkcenterId(changeDeviceModel.getWorkcenterId());
									deviceOeeModel.setWorkshopId(changeDeviceModel.getWorkshopId());
									//入库
									deviceOeeModelDao.setDeviceOeeModel(deviceOeeModel);
								}
							}
							//存在则修改
							else {
								//设备状态变更
								if (deviceOeeModel.getStatusValue()!=changeDeviceModel.getStatusValue()) {
									long runTimeMs=deviceOeeModel.getRunTimeMs();
									//由运行切换到其他状态
									if (deviceOeeModel.getStatusValue()==DeviceStatusModel.VALUE_Run) {
										runTimeMs+=nowTimeMs-deviceOeeModel.getStatusTimeMs();
									}
									//入库
									deviceOeeModel.setRunTimeMs(runTimeMs);
									deviceOeeModel.setStatusTimeMs(nowTimeMs);
									deviceOeeModel.setStatusValue(changeDeviceModel.getStatusValue());
									deviceOeeModelDao.setDeviceOeeModel(deviceOeeModel);
								}
							}
							//下一个
							synchronized (changeDeviceStatusQueueLockObj) {
								changeDeviceStatusQueue.poll();
								changeDeviceModel=changeDeviceStatusQueue.peek();
							}
						}
						/**
						 * 换班
						 * */
						synchronized (changeShiftQueueLockObj) {
							changeShiftModel=changeShiftQueue.peek();	
						}
						while (changeShiftModel!=null) {
							//获取车间所有设备
							List<DeviceModel> deviceModels=deviceService.getDeviceModelsByWorkshopId(changeShiftModel.getWorkshopId());
							if (deviceModels!=null&&deviceModels.size()>0) {
								for (DeviceModel deviceModel : deviceModels) {
									//新建OEE
									long todayTimeMs=DateUtil.getTodayBeginTimeMs();
									//不跨天
									long shiftBeginTimeMs=todayTimeMs+changeShiftModel.getBeginTimeMin()*DateUtil.MILLISECOND_ONE_MINUTE;
									long shiftEndTimeMs;
									if (!changeShiftModel.daySpan()) {
										shiftEndTimeMs=todayTimeMs+changeShiftModel.getEndTimeMin()*DateUtil.MILLISECOND_ONE_MINUTE;
									}
									//跨天
									else {
										shiftEndTimeMs=todayTimeMs+changeShiftModel.getEndTimeMin()*DateUtil.MILLISECOND_ONE_MINUTE+DateUtil.MILLISECOND_ONE_DAY;
									}
									//验证OEE是否已经存在
									DeviceOeeModel dbDeviceOeeModel=getNoOeeModelByDeviceIdAndShiftTimeMs(deviceModel.getId(),shiftBeginTimeMs);
									if (dbDeviceOeeModel==null) {
										DeviceOeeModel model=new DeviceOeeModel();
										model.setBeginTimeMs(shiftBeginTimeMs);
										model.setDeviceId(deviceModel.getId());
										model.setEndTimeMs(shiftEndTimeMs);
										model.setFactoryId(deviceModel.getFactoryId());
										//新建
										model.setId(-1);
										model.setOee(-1);
										model.setOrganizationId(deviceModel.getOrganizationId());
										model.setRunTimeMs(0);
										model.setShiftBeginTimeMin(changeShiftModel.getBeginTimeMin());
										model.setShiftEndTimeMin(changeShiftModel.getEndTimeMin());
										model.setShiftName(changeShiftModel.getName());
										model.setStatusTimeMs(shiftBeginTimeMs);
										model.setStatusValue(deviceModel.getStatusValue());
										model.setWorkcenterId(deviceModel.getWorkcenterId());
										model.setWorkshopId(deviceModel.getWorkshopId());
										//入库
										setDeviceOeeModel(model);	
									}
								}
							}
							//下一个
							synchronized (changeShiftQueueLockObj) {
								changeShiftQueue.poll();
								changeShiftModel=changeShiftQueue.peek();
							}
						}
					} catch (Exception e) {
						logger.error("设备OEE服务异常", e);
					}
				}
				logger.info("DeviceOeeService Stoped");
			}
		};
		thread.start();
	}
	private Queue<DeviceModel> changeDeviceStatusQueue=new LinkedBlockingQueue<DeviceModel>();
	private Object changeDeviceStatusQueueLockObj=new Object();
	private Queue<ShiftModel> changeShiftQueue=new LinkedBlockingQueue<ShiftModel>();
	private Object changeShiftQueueLockObj=new Object();

	public void changeShift(ShiftModel shiftModel) {
		synchronized (changeShiftQueueLockObj) {
			//加入队列
			changeShiftQueue.offer(shiftModel);
		}
	}

	public boolean changeWorkcenter(long deviceId,long workcenterId,long workshopId,long factoryId) {
		return deviceOeeModelDao.changeWorkcenter(deviceId,workcenterId,workshopId,factoryId);
	}
	
	public boolean changeWorkshop(long workcenterId,long workshopId,long factoryId) {
		return deviceOeeModelDao.changeWorkshop(workcenterId, workshopId, factoryId);
	}
	
	public boolean changeFactory(long workshopId,long factoryId) {
		return deviceOeeModelDao.changeFactory(workshopId, factoryId);
	}
}
