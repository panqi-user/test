package cn.sd.zhidetec.jdevice.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import cn.sd.zhidetec.jdevice.Starter;
import cn.sd.zhidetec.jdevice.config.DataSourceConfiger;
import cn.sd.zhidetec.jdevice.model.NoticeMessageModel;
import cn.sd.zhidetec.jdevice.util.StringUtil;

@Service
public class NoticeMessageModelDao {

	@Autowired
	@Qualifier(DataSourceConfiger.BEAN_NAME_JdeviceJdbcTemplate)
	protected JdbcTemplate jdbcTemplate;
	
	public boolean deleteNoticeMessageModelById(long id) {
		int i = 0;
		Object[] args = new Object[1];
		args[i++] = id;
		if (id>0) {
			i = jdbcTemplate.update(Sql_DeleteModel_By_Id, args);
		}
		if (i > 0) {
			return true;
		}
		return false;
	}

	private static String Sql_DeleteModel_By_Id = "DELETE FROM jdevice_h_notice WHERE id=?";
	
	public List<NoticeMessageModel> getMessageModels(long beginTimeMs,long endTimeMs,int minIndex,int dataNum) {
		int i = 0;
		Object[] args = new Object[4];
		args[i++]=beginTimeMs;
		args[i++]=endTimeMs;
		args[i++]=minIndex;
		args[i++]=dataNum;
		return jdbcTemplate.query(Sql_GetModels, args, beanPropertyRowMapper);
	}
	private static String Sql_GetModels = "SELECT * FROM jdevice_h_notice WHERE time_ms >= ? AND time_ms <= ? ORDER BY time_ms DESC LIMIT ?,?";
	
	public NoticeMessageModel getNoticeMessageModelById(long id) {
		Object[] args = new Object[1];
		args[0] = id;
		List<NoticeMessageModel> list = jdbcTemplate.query(Sql_GetModels_By_Id, args, beanPropertyRowMapper);
		if (list.size()>0) {
			return list.get(0);
		}
		return null;
	}

	private static String Sql_GetModels_By_Id = "SELECT * FROM jdevice_h_notice WHERE id = ?";
	
	public boolean setNoticeMessageModel(NoticeMessageModel noticeMessageModel) {
		int i = 0;
		Object[] args = new Object[7];
		// 修改信息
		if (noticeMessageModel.getId() > 0
				&& getNoticeMessageModelById(noticeMessageModel.getId())!=null) {
			args[i++] = noticeMessageModel.getType();
			args[i++] = noticeMessageModel.getTimeMs();
			args[i++] = StringUtil.getEmptyString(noticeMessageModel.getTitle());
			args[i++] = StringUtil.getEmptyString(noticeMessageModel.getSummary());
			args[i++] = StringUtil.getEmptyString(noticeMessageModel.getContent());
			args[i++] = StringUtil.getEmptyString(noticeMessageModel.getPublisher());
			args[i++] = noticeMessageModel.getId();
			i = jdbcTemplate.update(Sql_UpdateModel_By_Id, args);
		}
		// 新建信息
		else {
			noticeMessageModel.setId(Starter.IdMaker.nextId());
			args[i++] = noticeMessageModel.getId();
			args[i++] = noticeMessageModel.getType();
			args[i++] = noticeMessageModel.getTimeMs();
			args[i++] = StringUtil.getEmptyString(noticeMessageModel.getTitle());
			args[i++] = StringUtil.getEmptyString(noticeMessageModel.getSummary());
			args[i++] = StringUtil.getEmptyString(noticeMessageModel.getContent());
			args[i++] = StringUtil.getEmptyString(noticeMessageModel.getPublisher());
			i = jdbcTemplate.update(Sql_InsertModel, args);
		}
		if (i > 0) {
			return true;
		}
		return false;
	}

	private static String Sql_UpdateModel_By_Id = "UPDATE jdevice_h_notice" + //
			" SET " + //
			"type = ?," + //
			"time_ms = ?," + //
			"title = ?," + //
			"summary = ?," + //
			"content = ?," + //
			"publisher = ?" + //
			" WHERE " + //
			"id=?";
	private static String Sql_InsertModel = "INSERT INTO jdevice_h_notice (" + //
			"id," + //
			"type," + //
			"time_ms," + //
			"title," + //
			"summary," + //
			"content," + //
			"publisher" + //
			")VALUES(" + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?" + //
			")";
	
	private BeanPropertyRowMapper<NoticeMessageModel> beanPropertyRowMapper = new BeanPropertyRowMapper<NoticeMessageModel>(
			NoticeMessageModel.class);
	
}
