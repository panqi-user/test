package cn.sd.zhidetec.jdevice;

import java.io.UnsupportedEncodingException;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.event.ContextRefreshedEvent;

import cn.sd.zhidetec.jdevice.util.IdMaker;

/**
 * 项目启动类
 * */
@SpringBootApplication
@ComponentScan(basePackageClasses={Starter.class})
@EnableAutoConfiguration
public class Starter extends SpringBootServletInitializer implements ApplicationListener<ContextRefreshedEvent> {
	/**
     * 启动数据接收服务器
     * */
	public void onApplicationEvent(ContextRefreshedEvent event) {
		
	}

	/**
	 * 项目启动
	 * @param args 启动参数
	 * @throws UnsupportedEncodingException 
	 * */
	public static void main(String[] args) throws UnsupportedEncodingException {  
    	//执行文件路径
    	SystemExeFileName = "";
    	SystemExeFilePath = Starter.class.getProtectionDomain().getCodeSource().getLocation().getFile(); 
    	SystemExeFilePath = java.net.URLDecoder.decode(SystemExeFilePath, "UTF-8");//转换处理中文及空格 
    	SystemExeFilePath = SystemExeFilePath.replace("!/BOOT-INF/classes!/", "");
    	if (SystemExeFilePath!=null&&!SystemExeFilePath.trim().equals("")) {
    		String [] exeFilePath = SystemExeFilePath.split("/");
        	if (exeFilePath!=null && exeFilePath.length>1) {
				SystemExeFileName = exeFilePath[exeFilePath.length-1];
			}
		}
    	//应用版本
    	String[] strArr=SystemExeFileName.replace(".jar", "").split("-");
    	SystemVersionName=""+strArr[strArr.length-1];
		//后台服务启动
		new Thread(){
			@Override
			public void run() {
		    	SpringApplication.run(Starter.class, args);
			}
		}.start();
    }
	public static int ServerId=0;
	public static String ServerCode="";
	public static IdMaker IdMaker=new IdMaker(ServerId);
	public static String SystemVersionName="";
	public static String SystemExeFileName="";
	public static String SystemExeFilePath="";
}
