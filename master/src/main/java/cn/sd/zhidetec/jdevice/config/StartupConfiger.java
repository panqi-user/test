package cn.sd.zhidetec.jdevice.config;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.ContextRefreshedEvent;

import cn.sd.zhidetec.jdevice.server.JdeviceServer;
import cn.sd.zhidetec.jdevice.client.MsgPushClient_Hms;
import cn.sd.zhidetec.jdevice.client.MsgPushClient_UMeng;
import cn.sd.zhidetec.jdevice.client.MsgPushClient_Xiaomi;
import cn.sd.zhidetec.jdevice.server.DeviceSignboardServer;
import cn.sd.zhidetec.jdevice.service.AndroidApkService;
import cn.sd.zhidetec.jdevice.service.DeviceFaultService;
import cn.sd.zhidetec.jdevice.service.DeviceOeeService;
import cn.sd.zhidetec.jdevice.service.DeviceService;
import cn.sd.zhidetec.jdevice.service.DeviceSignboardService;
import cn.sd.zhidetec.jdevice.service.JedgeService;
import cn.sd.zhidetec.jdevice.service.ShiftService;
import cn.sd.zhidetec.jdevice.service.SpotCheckConfigService;
@Configuration
public class StartupConfiger implements ApplicationListener<ContextRefreshedEvent>{
	@Autowired
	JedgeService jedgeService;
	@Autowired
	DeviceSignboardServer deviceSignboardServer;
	@Autowired
	DeviceSignboardService deviceSignboardService;
	@Autowired
	DeviceService deviceService;
	@Autowired
	DeviceOeeService deviceOeeService;
	@Autowired
	ShiftService shiftService;
	@Autowired
	JdeviceServer andonMessageServer;
	@Autowired
	DeviceFaultService deviceFaultService;
	@Autowired
	MsgPushClient_Hms msgPushClient_Hms;
	@Autowired
	MsgPushClient_UMeng msgPushClient_UMeng;
	@Autowired
	MsgPushClient_Xiaomi msgPushClient_Xiaomi;
	@Autowired
	AndroidApkService androidApkService;
	@Autowired
	SpotCheckConfigService spotCheckConfigService;
	protected Logger logger = LoggerFactory.getLogger(getClass());
    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
    	//jedge服务
    	try {
			jedgeService.start();
		} catch (IOException e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
    	//设备标识牌服务
    	try {
    		deviceSignboardServer.start();
		} catch (IOException e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
    	//设备标识牌服务
    	try {
    		deviceSignboardService.start();
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
    	//Andon-SDK消息服务
    	try {
    		andonMessageServer.start();
		} catch (IOException e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
    	//Android客户端服务
    	try {
    		androidApkService.start();
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
    	//华为HMS推送
    	try {
    		msgPushClient_Hms.start();
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
    	//小米推送
    	try {
    		msgPushClient_Xiaomi.start();
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
    	//友盟推送
    	try {
    		msgPushClient_UMeng.start();
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
    	//设备代理服务
    	try {
    		deviceService.start();
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
    	//设备OEE服务
    	try {
			deviceOeeService.start();
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
    	//班次服务
    	try {
			shiftService.start();
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
    	//设备故障服务
    	try {
			deviceFaultService.start();
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
    	//点巡检服务
    	try {
			spotCheckConfigService.start();
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
    }
}
