package cn.sd.zhidetec.jdevice.controller;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.sd.zhidetec.jdevice.HttpResponseMsg;
import cn.sd.zhidetec.jdevice.pojo.AndroidPackagePojo;
import cn.sd.zhidetec.jdevice.service.AndroidApkService;
import cn.sd.zhidetec.jdevice.service.RBuilderService;

@Controller
@RequestMapping("/android")
public class AndroidController {
	@Autowired
	protected AndroidApkService androidApkService;
	@Autowired
	protected RBuilderService rBuilderService;
	protected Logger logger = LoggerFactory.getLogger(getClass());
	
	/**
	 * 重新加载Apk文件
	 * @param response 给前端的回复
	 * */
	@RequestMapping("/reloadAndroidApk")
	@ResponseBody
	public RBuilderService.Response reloadAndroidApk() {
		RBuilderService.Response response = rBuilderService.build(null,null);
		if (androidApkService.reload()) {
			return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, null); 
		}else {
			return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
		}
	}
	
	/**
	 * 下载最新版本的Android终端
	 * @param response 给前端的回复
	 * */
	@RequestMapping("/downloadNewVersion")
	public void downloadNewVersion(HttpServletResponse response) {
		if (androidApkService.getAndroidPackagePojos().size()<=0) {
			return;
		}
		String fileName=androidApkService.getAndroidPackagePojos().get(androidApkService.getAndroidPackagePojos().size()-1).getPath();
		download(response, fileName);
	}
	
	/**
	 * 获取最新的版本号
	 * @return 返回给前端的操作结果
	 * */
	@RequestMapping(path="/getNewVersionCode")
	@ResponseBody
	public RBuilderService.Response getNewVersionCode(HttpSession httpSession) {
		RBuilderService.Response response = rBuilderService.build(null,null);
		//操作返回
		if (androidApkService.getAndroidPackagePojos().size()<=0) {
			return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_DATA, 0);
		}
		AndroidPackagePojo packagePojo=androidApkService.getAndroidPackagePojos().get(androidApkService.getAndroidPackagePojos().size()-1);
		return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE,packagePojo.getApkData().getVersionCode());
	}
	
	/**
	 * 获取最新的版本信息
	 * @return 返回给前端的操作结果
	 * */
	@RequestMapping(path="/getNewVersionInfo")
	@ResponseBody
	public RBuilderService.Response getNewVersionInfo(HttpSession httpSession) {
		RBuilderService.Response response = rBuilderService.build(null,null);
		//操作返回
		if (androidApkService.getAndroidPackagePojos().size()<=0) {
			return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_DATA, null);
		}
		return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, androidApkService.getAndroidPackagePojos().get(androidApkService.getAndroidPackagePojos().size()-1));
	}
	
	/**
	 * 根据Android客户端的文件名进行下载
	 * @param response 给前端的回复
	 * @param fileName Android客户端文件名
	 * */
	@RequestMapping("/download")
	public void download(HttpServletResponse response,//
			@RequestParam String fileName) {
		File file = new File(AndroidApkService.DIR_DOWNLOAD_ANDROID + File.separator + fileName);
		logger.info("开始下载文件："+fileName);
        if(file.exists()){ //判断文件父目录是否存在
            response.setContentType("application/octet-stream");
            response.setContentLengthLong(file.length());
            response.setHeader("Content-Disposition", "attachment;fileName=" + file.getName());
            
            byte[] buffer = new byte[1024];
            FileInputStream fileInputStream = null; //文件输入流
            BufferedInputStream bufferedInputStream = null;
            OutputStream outputStream = null; //输出流
            
            try {
                outputStream = response.getOutputStream();
                fileInputStream = new FileInputStream(file); 
                bufferedInputStream = new BufferedInputStream(fileInputStream);
                int i = bufferedInputStream.read(buffer);
                while(i != -1){
                    outputStream.write(buffer,0,i);
                    i = bufferedInputStream.read(buffer);
                }
                
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            logger.info("成功下载文件："+fileName);
            try {
                bufferedInputStream.close();
                fileInputStream.close();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }else {
        	logger.info("不存在目标文件："+fileName);
		}
	}
}
