package cn.sd.zhidetec.jdevice.controller;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.sd.zhidetec.jdevice.HttpResponseMsg;
import cn.sd.zhidetec.jdevice.model.RoleModel;
import cn.sd.zhidetec.jdevice.model.ShiftModel;
import cn.sd.zhidetec.jdevice.model.UserModel;
import cn.sd.zhidetec.jdevice.pojo.ShiftPojo;
import cn.sd.zhidetec.jdevice.service.RBuilderService;
import cn.sd.zhidetec.jdevice.service.ShiftService;

import java.util.List;

@Controller
@RequestMapping("/shift")
public class ShiftController {
	
	@Autowired
	protected ShiftService shiftService;
	@Autowired
	protected RBuilderService rBuilderService;
	protected Logger logger = LoggerFactory.getLogger(getClass());
	
	@RequestMapping(path = "/getCurrentShiftPojoByWorkshopId")
	@ResponseBody
	public RBuilderService.Response getCurrentShiftPojoByWorkshopId(HttpSession httpSession,//
			@RequestParam("workshopId")long workshopId) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_User, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		// 根据用户权限返回数据
		ShiftModel shiftModel=shiftService.getShiftModelByWorkshopId(workshopId, System.currentTimeMillis());
		ShiftPojo shiftPojo=new ShiftPojo();
		shiftPojo.fillFields(shiftModel);
		return rBuilderService.update(response, //
				HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, //
				shiftPojo);
	}
	
	@RequestMapping(path = "/getShiftModelsByWorkshopId")
	@ResponseBody
	public RBuilderService.Response getShiftModelsByWorkshopId(HttpSession httpSession,//
			@RequestParam("workshopId")long workshopId) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_User, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		// 根据用户权限返回数据
		return rBuilderService.update(response, //
				HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, //
				shiftService.getShiftModelsByWorkshopId(workshopId));
	}
	
	@RequestMapping(path = "/getShiftModelById")
	@ResponseBody
	public RBuilderService.Response getShiftModelById(HttpSession httpSession,//
			@RequestParam("id")long id) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_User, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		// 根据用户权限返回数据
		UserModel userModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		ShiftModel shiftModel = shiftService.getShiftModelById(id);
		if (shiftModel!=null) {
			if (userModel.getRoleLevel() <= RoleModel.ROLE_LEVEL_WorkshopAdmin//
					&&userModel.getWorkshopId()==shiftModel.getWorkshopId()) {
				return rBuilderService.update(response, //
						HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, //
						shiftModel);
			}
			if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_FactoryAdmin//
					&&userModel.getFactoryId()==shiftModel.getFactoryId()) {
				return rBuilderService.update(response, //
						HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, //
						shiftModel);
			}
			if (userModel.getRoleLevel() >= RoleModel.ROLE_LEVEL_OrganizationAdmin//
					&&userModel.getOrganizationId().equals(shiftModel.getOrganizationId())) {
				return rBuilderService.update(response, //
						HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, //
						shiftModel);
			}
		}
		return rBuilderService.update(response, //
				HttpResponseMsg.HEAD_MSG_NOT_FOUND_DATA, //
				null);
	}
	
	@RequestMapping(path = "/getShiftModels")
	@ResponseBody
	public RBuilderService.Response getShiftModels(HttpSession httpSession) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_User, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		// 根据用户权限返回数据
		UserModel userModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		if (userModel.getRoleLevel() <= RoleModel.ROLE_LEVEL_WorkshopAdmin) {
			return rBuilderService.update(response, //
					HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, //
					shiftService.getShiftModelsByWorkshopId(userModel.getWorkshopId()));
		}
		if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_FactoryAdmin) {
			return rBuilderService.update(response, //
					HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, //
					shiftService.getShiftModelsByFactoryId(userModel.getFactoryId()));
		}
		if (userModel.getRoleLevel() >= RoleModel.ROLE_LEVEL_OrganizationAdmin) {
			return rBuilderService.update(response, //
					HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, //
					shiftService.getShiftModelsByOrganizationId(userModel.getOrganizationId()));
		}
		return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_DATA, null);
	}
	/**
	 * 设置车间班次信息
	 * @param httpSession 请求Session
	 * @param shiftModel 班次信息
	 * @return 操作结果
	 * */
	@RequestMapping(path = "/setShiftModel")
	@ResponseBody
	private RBuilderService.Response setShiftModel(HttpSession httpSession,//
			@RequestBody ShiftModel shiftModel) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_WorkshopAdmin, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		// 根据用户权限操作数据
		UserModel userModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		//新建
		if (shiftModel.getId()<=0) {
			shiftModel.setOrganizationId(userModel.getOrganizationId());
		}
		if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_WorkshopAdmin) {
			if (userModel.getWorkshopId()==shiftModel.getId()//
					&& userModel.getFactoryId()==shiftModel.getFactoryId()//
					&& userModel.getOrganizationId().equals(shiftModel.getOrganizationId())) {
				if (shiftService.setShiftModel(shiftModel)) {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, null);
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
				}
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
			}
		}
		if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_FactoryAdmin) {
			if (userModel.getFactoryId()==shiftModel.getFactoryId()//
					&& userModel.getOrganizationId().equals(shiftModel.getOrganizationId())) {
				if (shiftService.setShiftModel(shiftModel)) {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, null);
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
				}
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
			}
		}
		if (userModel.getRoleLevel() >= RoleModel.ROLE_LEVEL_OrganizationAdmin) {
			if (userModel.getOrganizationId().equals(shiftModel.getOrganizationId())) {
				if (shiftService.setShiftModel(shiftModel)) {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, null);
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
				}
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
			}
		}
		return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
	}
	/**
	 * 从Session中获取用户信息，然后删除班次
	 * 
	 * @param httpSession Session
	 * @return 操作结果
	 */
	@RequestMapping(path = "/deleteShiftModel")
	@ResponseBody
	public RBuilderService.Response deleteShiftModel(HttpSession httpSession, //
			@RequestBody ShiftModel shiftModel) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_FactoryAdmin, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		// 根据用户权限操作数据
		UserModel userModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_FactoryAdmin) {
			if (userModel.getFactoryId()==shiftModel.getFactoryId()//
					&& userModel.getOrganizationId().equals(shiftModel.getOrganizationId())) {
				if (shiftService.deleteShiftModel(shiftModel)) {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, null);
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
				}
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
			}
		}
		if (userModel.getRoleLevel() >= RoleModel.ROLE_LEVEL_OrganizationAdmin) {
			if (userModel.getOrganizationId().equals(shiftModel.getOrganizationId())) {
				if (shiftService.deleteShiftModel(shiftModel)) {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, null);
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
				}
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
			}
		}
		return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
	}


    /**
     * 根据车间id获取车间班次list
     * @param workshopId
     * @return
     */
    @RequestMapping(path = "/findShiftModelsByWorkshopId")
    @ResponseBody
    public RBuilderService.Response findShiftModelsByWorkshopId(long workshopId){


        RBuilderService.Response response = rBuilderService.build(null, null);

        List<ShiftModel> shiftList = shiftService.findShiftModelsByWorkshopId(workshopId);
        if (shiftList.size()!=0){

            return rBuilderService.update(response,HttpResponseMsg.HEAD_MSG_SUCCESS_DONE,shiftList);

        }else{
            return rBuilderService.update(response,HttpResponseMsg.HEAD_MSG_ERROR_PARAM,null);
        }


    }


}
