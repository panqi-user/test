package cn.sd.zhidetec.jdevice.service;

import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.sd.zhidetec.jdevice.dao.DeviceFaultModelDao;
import cn.sd.zhidetec.jdevice.model.DeviceFaultModel;
import cn.sd.zhidetec.jdevice.model.DeviceModel;
import cn.sd.zhidetec.jdevice.model.DeviceStatusModel;

@Service
public class DeviceFaultService {

	@Autowired
	protected DeviceFaultModelDao deviceFaultModelDao;
	protected Logger logger = LoggerFactory.getLogger(getClass());

	public void changeDeviceStatus(DeviceModel lastDeviceModel,DeviceModel deviceModel) {
		if (lastDeviceModel==null||deviceModel==null) {
			return;
		}
		if (lastDeviceModel.getStatusValue()==deviceModel.getStatusValue()) {
			return;
		}
		//新建
		DeviceModel model=new DeviceModel();
		model.setId(lastDeviceModel.getId());
		model.setWorkcenterId(lastDeviceModel.getWorkcenterId());
		model.setWorkshopId(lastDeviceModel.getWorkshopId());
		model.setFactoryId(lastDeviceModel.getFactoryId());
		model.setOrganizationId(lastDeviceModel.getOrganizationId());
		model.setStatus(deviceModel.getStatus());
		model.setStatusName(deviceModel.getStatusName());
		model.setStatusValue(deviceModel.getStatusValue());
		model.setStatusTimeMs(deviceModel.getStatusTimeMs());
		//加入队列
		synchronized (changeDeviceModelsLockObj) {
			changeDeviceModels.offer(model);
		}
	}
	
	private Thread thread=new Thread() {

		@Override
		public void run() {
			logger.info("DeviceFaultService Started");
			DeviceModel deviceModel=null;
			while (started) {
				try {
					Thread.sleep(1);
					synchronized (changeDeviceModelsLockObj) {
						deviceModel=changeDeviceModels.peek();	
					}
					while (deviceModel!=null) {
						//查询上次故障记录
						DeviceFaultModel lastFaultModel=getLastDeviceModelByDeviceId(deviceModel.getId());
						//新增故障记录
						if (lastFaultModel==null&&deviceModel.getStatusValue()==DeviceStatusModel.VALUE_Fault) {
							DeviceFaultModel newFaultModel=new DeviceFaultModel();
							newFaultModel.copyIdField(deviceModel);
							newFaultModel.setBeginTimeMs(deviceModel.getStatusTimeMs());
							newFaultModel.setEndTimeMs(-1);
							newFaultModel.setIntervalTimeMs(-1);
							newFaultModel.setRepairTimeMs(-1);
							//入库
							insertDeviceFaultModel(newFaultModel);
						}
						//更新故障记录
						else if(lastFaultModel!=null){
							//上次故障已经关闭则新增
							if (lastFaultModel.getEndTimeMs()>0//
									&&deviceModel.getStatusValue()==DeviceStatusModel.VALUE_Fault) {
								long intervalTimeMs=deviceModel.getStatusTimeMs()-lastFaultModel.getBeginTimeMs();
								DeviceFaultModel newFaultModel1=new DeviceFaultModel();
								newFaultModel1.copyIdField(deviceModel);
								newFaultModel1.setBeginTimeMs(deviceModel.getStatusTimeMs());
								newFaultModel1.setEndTimeMs(-1);
								newFaultModel1.setIntervalTimeMs(intervalTimeMs);
								newFaultModel1.setRepairTimeMs(-1);
								//入库
								insertDeviceFaultModel(newFaultModel1);
							}
							//关闭上次故障
							else if(deviceModel.getStatusValue()!=DeviceStatusModel.VALUE_Fault//
									&&deviceModel.getStatusValue()!=DeviceStatusModel.VALUE_Repair//
									&&deviceModel.getStatusValue()!=DeviceStatusModel.VALUE_Offline){
								lastFaultModel.copyIdField(deviceModel);
								lastFaultModel.setEndTimeMs(deviceModel.getStatusTimeMs());
								lastFaultModel.setRepairTimeMs(deviceModel.getStatusTimeMs()-lastFaultModel.getBeginTimeMs());
								//更新入库
								updateDeviceFaultModel(lastFaultModel);	
							}
						}
						//下一个设备状态
						synchronized (changeDeviceModelsLockObj) {
							changeDeviceModels.poll();
							deviceModel=changeDeviceModels.peek();
						}
					}
				} catch (Exception e) {
					logger.error("DeviceFaultService Throw Exception", e);
				}
			}
			logger.info("DeviceFaultService Stoped");
		}
		
	};
	private boolean started=false;
	private Queue<DeviceModel> changeDeviceModels=new LinkedBlockingQueue<DeviceModel>();
	private Object changeDeviceModelsLockObj=new Object();
	
	public void start() {
		synchronized (changeDeviceModelsLockObj) {
			if (started==false) {
				started=true;
				thread.start();
			}
		}
	}
	
	public boolean insertDeviceFaultModel(DeviceFaultModel deviceFaultModel) {
		return deviceFaultModelDao.insertDeviceFaultModel(deviceFaultModel);
	}

	public boolean updateDeviceFaultModel(DeviceFaultModel deviceFaultModel) {
		return deviceFaultModelDao.updateDeviceFaultModel(deviceFaultModel);
	}

	public List<DeviceFaultModel> getLast2DeviceModelByDeviceId(long deviceId) {
		return deviceFaultModelDao.getLast2DeviceModelByDeviceId(deviceId);
	}
	
	public DeviceFaultModel getLastDeviceModelByDeviceId(long deviceId) {
		return deviceFaultModelDao.getLastDeviceModelByDeviceId(deviceId);
	}

	public List<DeviceFaultModel> getDeviceFaultModelsByBeginTimsMs(long beginTimeMs, long deviceId) {
		return deviceFaultModelDao.getDeviceFaultModelsByBeginTimsMs(beginTimeMs, deviceId);
	}

	public Map<String, Object> getDeviceMTTR(long deviceId, long beginTimeMs) {
		return deviceFaultModelDao.getDeviceMTTR(deviceId, beginTimeMs);
	}
	
	public Map<String, Object> getDeviceMTBF(long deviceId, long beginTimeMs) {
		return deviceFaultModelDao.getDeviceMTBF(deviceId, beginTimeMs);
	}
	
	public List<Map<String, Object>> getWorkshopMTTR(long workshopId, long beginTimeMs) {
		return deviceFaultModelDao.getWorkshopMTTR(workshopId, beginTimeMs);
	}

	public List<Map<String, Object>> getWorkshopMTBF(long workshopId, long beginTimeMs) {
		return deviceFaultModelDao.getWorkshopMTBF(workshopId, beginTimeMs);
	}

	public List<Map<String, Object>> getWorkcenterMTTR(long workcenterId, long beginTimeMs) {
		return deviceFaultModelDao.getWorkcenterMTTR(workcenterId, beginTimeMs);
	}

	public List<Map<String, Object>> getWorkcenterMTBF(long workcenterId, long beginTimeMs) {
		return deviceFaultModelDao.getWorkcenterMTBF(workcenterId, beginTimeMs);
	}

	public boolean changeWorkcenter(long deviceId,long workcenterId,long workshopId,long factoryId) {
		return deviceFaultModelDao.changeWorkcenter(deviceId,workcenterId,workshopId,factoryId);
	}
	
	public boolean changeWorkshop(long workcenterId,long workshopId,long factoryId) {
		return deviceFaultModelDao.changeWorkshop(workcenterId, workshopId, factoryId);
	}
	
	public boolean changeFactory(long workshopId,long factoryId) {
		return deviceFaultModelDao.changeFactory(workshopId, factoryId);
	}
}
