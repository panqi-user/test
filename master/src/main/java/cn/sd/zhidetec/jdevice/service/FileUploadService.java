package cn.sd.zhidetec.jdevice.service;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import cn.sd.zhidetec.jdevice.config.UploadConfiger;
import cn.sd.zhidetec.jdevice.model.OrganizationModel;
import cn.sd.zhidetec.jdevice.model.UserModel;
import cn.sd.zhidetec.jdevice.util.StringUtil;

@Service
public class FileUploadService {
	
	protected Logger logger = LoggerFactory.getLogger(getClass());
	
	public boolean uploadOrganizationLogoImg(MultipartFile multipartFile,OrganizationModel model){
		String fileName=multipartFile.getOriginalFilename();
		//修改文件名
		String[] fileNameArr = fileName.split("\\.");
		if (fileNameArr.length<=0) {
			logger.error(model.getName()+"组织LOGO："+fileName+"文件名不合法！！");
			return false;
		}
		fileName=StringUtil.get32BitUUID()+"."+fileNameArr[fileNameArr.length-1];
		//保存到对象
		model.setLogoUrl(UploadConfiger.RESOURCE_HANDLER_UPLOAD+DIR_NAME_ORGANIZATION+"/"+DIR_NAME_LOGO+"/"+model.getId()+"/"+fileName);
		//保存到文件
		String dirPath=UploadConfiger.PATH_DIR_UPLOAD+DIR_NAME_ORGANIZATION+File.separator+DIR_NAME_LOGO+File.separator+model.getId()+File.separator;
		File dirFile = new File(dirPath);
		if (!dirFile.exists()) {
			dirFile.mkdirs();
		}
		File file=new File(dirPath+fileName);
		try {
			if (file.exists()==false) {
				file.createNewFile();
			}
			multipartFile.transferTo(file);
			logger.info(model.getName()+"组织LOGO："+file.getPath()+"上传成功！！");
			return true;
		} catch (Exception e) {
			// TODO: handle exception
			logger.error(model.getName()+"组织LOGO："+file.getPath()+"上传失败！！", e);
			return false;
		}
	}
	
	public boolean uploadUserPhotoImg(MultipartFile multipartFile,UserModel userModel){
		String fileName=multipartFile.getOriginalFilename();
		//修改文件名
		String[] fileNameArr = fileName.split("\\.");
		if (fileNameArr.length<=0) {
			logger.error(userModel.getPhone()+"用户头像："+fileName+"文件名不合法！！");
			return false;
		}
		fileName=StringUtil.get32BitUUID()+"."+fileNameArr[fileNameArr.length-1];
		//保存到用户对象
		userModel.setPhotoUrl(UploadConfiger.RESOURCE_HANDLER_UPLOAD+DIR_NAME_USER+"/"+DIR_NAME_PHOTO+"/"+userModel.getPhone()+"/"+fileName);
		//保存到文件
		String dirPath=UploadConfiger.PATH_DIR_UPLOAD+DIR_NAME_USER+File.separator+DIR_NAME_PHOTO+File.separator+userModel.getPhone()+File.separator;
		File dirFile = new File(dirPath);
		if (!dirFile.exists()) {
			dirFile.mkdirs();
		}
		File file=new File(dirPath+fileName);
		try {
			if (file.exists()==false) {
				file.createNewFile();
			}
			multipartFile.transferTo(file);
			logger.info(userModel.getPhone()+"用户头像："+file.getPath()+"上传成功！！");
			return true;
		} catch (Exception e) {
			// TODO: handle exception
			logger.error(userModel.getPhone()+"用户头像："+file.getPath()+"上传失败！！", e);
			return false;
		}
	}
	
	public boolean uploadDevicePhotoImg(MultipartFile multipartFile,String organizationId,StringBuilder logoUrl){
		String fileName=multipartFile.getOriginalFilename();
		//修改文件名
		String[] fileNameArr = fileName.split("\\.");
		if (fileNameArr.length<=0) {
			logger.error(organizationId+"设备LOGO："+fileName+"文件名不合法！！");
			return false;
		}
		fileName=StringUtil.get32BitUUID()+"."+fileNameArr[fileNameArr.length-1];
		//保存到对象
		logoUrl.append(UploadConfiger.RESOURCE_HANDLER_UPLOAD+DIR_NAME_DEVICE+"/"+DIR_NAME_LOGO+"/"+organizationId+"/"+fileName);
		//保存到文件
		String dirPath=UploadConfiger.PATH_DIR_UPLOAD+DIR_NAME_DEVICE+File.separator+DIR_NAME_LOGO+File.separator+organizationId+File.separator;
		File dirFile = new File(dirPath);
		if (!dirFile.exists()) {
			dirFile.mkdirs();
		}
		File file=new File(dirPath+fileName);
		try {
			if (file.exists()==false) {
				file.createNewFile();
			}
			multipartFile.transferTo(file);
			logger.info(organizationId+"设备LOGO："+file.getPath()+"上传成功！！");
			return true;
		} catch (Exception e) {
			// TODO: handle exception
			logger.error(organizationId+"设备LOGO："+file.getPath()+"上传失败！！", e);
			return false;
		}
	}
	
	public final static String DIR_NAME_ORGANIZATION="organization";
	public final static String DIR_NAME_LOGO="logo";
	public final static String DIR_NAME_PHOTO="photo";
	public final static String DIR_NAME_USER="user";
	public final static String DIR_NAME_DEVICE="device";
}
