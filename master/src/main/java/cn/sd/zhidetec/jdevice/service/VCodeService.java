package cn.sd.zhidetec.jdevice.service;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.Random;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Service;

/**
 * 登录界面的验证码
 * 
 * @author linchunsen
 */
@Service
@EnableAutoConfiguration
public class VCodeService {
	// 验证码的元素集合
	private char[] codes = {
			'1',
			'2',
			'3',
			'4',
			'5',
			'6',
			'7',
			'8',
			'9',
			'a',
			'b',
			'c',
			'd',
			'e',
			'f',
			'g',
			'h',
			'i',
			'j',
			'k',
			'm',
			'n',
			'p',
			'q',
			'r',
			's',
			't',
			'u',
			'v',
			'w',
			'x',
			'y',
			'z',
			'A',
			'B',
			'C',
			'D',
			'E',
			'F',
			'G',
			'H',
			'I',
			'J',
			'K',
			'L',
			'M',
			'N',
			'P',
			'Q',
			'R',
			'S',
			'T',
			'U',
			'V',
			'W',
			'X',
			'Y',
			'Z' };

	/**
	 * 获取随机生成的验证码字符串
	 * 
	 * @param 字符串的长度
	 * @return 随机生成的字符串
	 */
	public String getCodeStr(int strLength) {
		StringBuffer codeStr = new StringBuffer("");
		int length = codes.length;
		Random random = new Random();
		int index = 0;
		for (int i = 0; i < strLength; i++) {
			index = random.nextInt(length);
			codeStr.append(codes[index]);
		}
		return codeStr.toString();
	}

	/**
	 * 生成验证码图片
	 * 
	 * @param fontSize
	 *            字体大小
	 * @param codeStr
	 *            验证码字符串
	 * @return 验证码图片
	 */
	public BufferedImage getVerificationCode(int fontSize, String codeStr) {
		BufferedImage image = null;
		// 获取验证码
		int strLength = codeStr.length();
		// 字体大小
		int fontWidth = fontSize + 1;
		// 图片宽度
		int width = strLength * fontWidth + 6;
		// 图片高度
		int height = fontSize * 2 + 1;
		// 验证码图片
		image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		Graphics2D graphics = image.createGraphics();
		// 设置背景颜色
		graphics.setColor(Color.WHITE);
		// 填充背景
		graphics.fillRect(0, 0, width, height);
		// 设置边框颜色
		graphics.setColor(Color.LIGHT_GRAY);
		// 边框字体样式
		graphics.setFont(new Font("Arial", Font.ITALIC, height - 2));
		// 绘制边框
		graphics.drawRect(0, 0, width - 1, height - 1);
		// 绘制噪点
		Random random = new Random();
		// 设置噪点颜色
		graphics.setColor(Color.LIGHT_GRAY);
		for (int i = 0; i < strLength * 6; i++) {
			int x = random.nextInt(width);
			int y = random.nextInt(height);
			// 绘制1*1大小的矩形
			graphics.drawRect(x, y, 1, 1);
		}
		// 绘制验证码
		int codeY = height - 10;
		// 设置字体颜色和样式
		graphics.setColor(new Color(19, 148, 246));
		graphics.setFont(new Font("Georgia", Font.BOLD, fontSize));
		for (int i = 0; i < strLength; i++) {
			graphics.drawString(String.valueOf(codeStr.charAt(i)), i * 16 + 5, codeY);
		}
		// 关闭资源
		graphics.dispose();
		return image;
	}

	/**
	 * 获取验证码的流
	 * 
	 * @param fontSize
	 *            字体大小
	 * @param strLength
	 *            验证码字符串的长度
	 * @return ByteArrayInputStream 流
	 */
	public ByteArrayInputStream getVerificationCodeInputStream(int fontSize, String codeStr) {
		BufferedImage image = getVerificationCode(fontSize, codeStr);
		return convertImageToStream(image);
	}

	/**
	 * 以字体大小为15的默认值创建验证码图片，并返回验证码图片流
	 * 
	 * @param codeStr
	 *            验证码字符串
	 * @return ByteArrayInputStream 流
	 */
	public ByteArrayInputStream getVerificationCodeInputStream(String codeStr) {
		BufferedImage image = getVerificationCode(15, codeStr);
		return convertImageToStream(image);
	}

	/**
	 * 将BufferedImage转换成ByteArrayInputStream
	 * 
	 * @param image
	 *            图片
	 * @return ByteArrayInputStream 流
	 */
	private static ByteArrayInputStream convertImageToStream(BufferedImage image) {
		ByteArrayInputStream inputStream = null;
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		byte[] bts = bos.toByteArray();
		inputStream = new ByteArrayInputStream(bts);
		return inputStream;
	}
}