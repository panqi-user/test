 package cn.sd.zhidetec.jdevice.service;

import java.io.UnsupportedEncodingException;

import com.tencentcloudapi.common.Credential;
import com.tencentcloudapi.common.exception.TencentCloudSDKException;

 // 导入对应SMS模块的client
import com.tencentcloudapi.sms.v20190711.SmsClient;

 // 导入要请求接口对应的request response类
import com.tencentcloudapi.sms.v20190711.models.SendSmsRequest;
import com.tencentcloudapi.sms.v20190711.models.SendSmsResponse;

 /**
  * Tencent Cloud Sms Sendsms
  * https://cloud.tencent.com/document/product/382/38778
  *
  */
 public class TencentCloudSMS
 {
     private static final String SECRET_ID = "AKIDEH7i3C3MRL2Tgt8U2tVv5zgLktlYcIGe";
     private static final String SECRET_KEY = "GFy2rPp58qLzN9gwWszqWvSkkZ7RUifL";
     private static final String SMS_APP_ID = "1400348247";
     private static final String SMS_SIGN = "致德工业";
     private static final String SMS_SESSION = "ZHIDETEC";
     private static final String TEMPLATE_ID = "579609";
     public static String sendSMS(String phoneNumber,String vcode,int interval) 
     {
         try {
             /* 必要步骤：
              * 实例化一个认证对象，入参需要传入腾讯云账户密钥对secretId，secretKey。
              * 这里采用的是从环境变量读取的方式，需要在环境变量中先设置这两个值。
              * 你也可以直接在代码中写死密钥对，但是小心不要将代码复制、上传或者分享给他人，
              * 以免泄露密钥对危及你的财产安全。
              * CAM密匙查询: https://console.cloud.tencent.com/cam/capi*/
             Credential cred = new Credential(SECRET_ID, SECRET_KEY);

             SmsClient client = new SmsClient(cred, "");
             /* 实例化一个请求对象，根据调用的接口和实际情况，可以进一步设置请求参数
              * 
              */
             SendSmsRequest req = new SendSmsRequest();

             /* 填充请求参数,这里request对象的成员变量即对应接
              */

             /* 短信应用ID: 短信SdkAppid在 [短信控制台] 添加应用后生成的实际SdkAppid，示例如1400006666 */
             req.setSmsSdkAppid(SMS_APP_ID);

             /* 短信签名内容: 使用 UTF-8 编码，必须填写已审核通过的签名，签名信息可登录 [短信控制台] 查看 */
             String sign = new String(SMS_SIGN.getBytes(),"UTF-8");
             req.setSign(sign);

             /* 国际/港澳台短信 senderid: 国内短信填空，默认未开通，如需开通请联系 [sms helper] */
             String senderid = "";
             req.setSenderId(senderid);

             /* 用户的 session 内容: 可以携带用户侧 ID 等上下文信息，server 会原样返回 */
             req.setSessionContext(SMS_SESSION);

             /* 短信码号扩展号: 默认未开通，如需开通请联系 [sms helper] */
             String extendcode = "";
            req.setExtendCode(extendcode);

             /* 模板 ID: 必须填写已审核通过的模板 ID。模板ID可登录 [短信控制台] 查看 */
             req.setTemplateID(TEMPLATE_ID);
             

             /* 下发手机号码，采用 e.164 标准，+[国家或地区码][手机号]
                            * 示例如：+8613711112222， 其中前面有一个+号 ，86为国家码，13711112222为手机号，最多不要超过200个手机号*/
             String[] phoneNumbers = {"+86"+phoneNumber};
             req.setPhoneNumberSet(phoneNumbers);

             /* 模板参数: 若无模板参数，则设置为空*/
             String[] templateParams = {vcode,String.valueOf(interval)};
             req.setTemplateParamSet(templateParams);

             /* 通过 client 对象调用 SendSms 方法发起请求。注意请求方法名与请求对象是对应的
                              * 返回的 res 是一个 SendSmsResponse 类的实例，与请求对象对应 */
             SendSmsResponse res = client.SendSms(req);

             // 输出json格式的字符串回包
             //   System.out.println(SendSmsResponse.toJsonString(res));

             // 也可以取出单个值，你可以通过官网接口文档或跳转到response对象的定义处查看返回字段的定义
             //System.out.println(res.getRequestId());
             return SendSmsResponse.toJsonString(res);

         } catch (TencentCloudSDKException e) {
             e.printStackTrace();
             return "Send error! [TencentCloudSDK]";
         } catch (UnsupportedEncodingException e) {
             e.printStackTrace();
             return "Send error! [UnsupportedencodingException]";
        }
     }
     public static void main(String[] args) {
         // TODO Auto-generated method stub
//         TelMsgService ts = new TelMsgService();
//         int  xx = ts.sendVCodeMsg("13506485820");
//         System.out.println(xx);
//         DeviceStatusModelnew run = DeviceStatusModelnew.RUN;
         String sendSMS = TencentCloudSMS.sendSMS("+8613506485820", "1234", 12);
         System.out.println(sendSMS);
         

 }
 } 