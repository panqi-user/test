package cn.sd.zhidetec.jdevice.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.sd.zhidetec.jdevice.dao.SpotCheckConfigModelDao;
import cn.sd.zhidetec.jdevice.dao.SpotCheckModelDao;
import cn.sd.zhidetec.jdevice.model.SpotCheckItemConfigModel;
import cn.sd.zhidetec.jdevice.model.SpotCheckItemModel;
import cn.sd.zhidetec.jdevice.model.SpotCheckPlanConfigModel;
import cn.sd.zhidetec.jdevice.model.SpotCheckPlanModel;

@Service
public class SpotCheckConfigService {

	@Autowired
	protected SpotCheckConfigModelDao spotCheckConfigModelDao;
	@Autowired
	protected SpotCheckModelDao spotCheckModelDao;
	protected Logger logger = LoggerFactory.getLogger(getClass());
	
	private List<SpotCheckPlanConfigModel> cachedCheckPlanConfigModels;
	private Object cachedCheckPlanConfigModelsLockObj=new Object();
	private boolean started=false;
	private Thread thread;
	public void start() {
		if (started) {
			return;
		}
		started=true;
		thread=new Thread(){
			
			@Override
			public void run() {
				String tag=SpotCheckConfigService.this.getClass().getSimpleName()+" InstanceThread";
				logger.info(tag+" Started!!");
				synchronized (cachedCheckPlanConfigModelsLockObj) {
					cachedCheckPlanConfigModels=spotCheckConfigModelDao.getAllPlanConfiges();
				}
				logger.info(tag+" Loaded All SpotCheckPlanConfigModel From DB");
				List<SpotCheckPlanConfigModel> models=new ArrayList<>();
				while (started) {
					try {
						Thread.sleep(500);
						//
						long nowTimeMs=System.currentTimeMillis();
						models.clear();
						//判断计划是否到时间
						synchronized (cachedCheckPlanConfigModelsLockObj) {
							for (SpotCheckPlanConfigModel spotCheckPlanConfigModel : cachedCheckPlanConfigModels) {
								if (spotCheckPlanConfigModel!=null&&spotCheckPlanConfigModel.getTriggerIntervalTimeM()>0) {
									if (spotCheckPlanConfigModel.getTriggerNextTimeMs()<=nowTimeMs) {
										models.add(spotCheckPlanConfigModel);
									}
								}
							}
						}
						//新建计划实例
						for (SpotCheckPlanConfigModel spotCheckPlanConfigModel : models) {
							//计划状态修改
							SpotCheckPlanModel instance=spotCheckPlanConfigModel.buildInstance(spotCheckPlanConfigModel.getCreateTimeMs(), nowTimeMs);
							spotCheckPlanConfigModel.setTriggerLastTimeMs(nowTimeMs);
							spotCheckPlanConfigModel.setTriggerNextTimeMs(nowTimeMs+spotCheckPlanConfigModel.getTriggerIntervalTimeM()*60000L);
							//配置入库
							spotCheckConfigModelDao.setCheckPlanConfig(spotCheckPlanConfigModel);
							//新建计划实例
							if (spotCheckModelDao.setCheckPlan(instance)) {
								//点巡检项
								List<SpotCheckItemConfigModel> itemConfigModels = getCheckItemConfigesByPlanId(spotCheckPlanConfigModel.getId(),spotCheckPlanConfigModel.getOrganizationId());
								List<SpotCheckItemModel> itemModels = instance.buildCheckItemInstances(itemConfigModels);
								//入库
								spotCheckModelDao.newCheckItemIntances(itemModels);
								//发送消息
							}
						}
					} catch (Exception e) {
						logger.error(tag+" Throws Exception",e);
					}
				}
				logger.info(tag+" Stoped!!");
			}
			
		};
		thread.start();
	}
	
	public List<SpotCheckPlanConfigModel> getWorkcenterCheckPlanConfiges(long workcenterId, long workshopId, long factoryId,
			String organizationId, int minIndex, int dataNum) {
		List<SpotCheckPlanConfigModel> result = new ArrayList<SpotCheckPlanConfigModel>();
		int index=0;
		int num=0;
		synchronized (cachedCheckPlanConfigModelsLockObj) {
			for (SpotCheckPlanConfigModel item : cachedCheckPlanConfigModels) {
				if (item.getWorkcenterId()==workcenterId//
						&&item.getWorkshopId()==workshopId//
						&&item.getFactoryId()==factoryId//
						&&item.getOrganizationId().equals(organizationId)) {
					//
					if (index>=minIndex&&dataNum>=num) {
						result.add(item);
						num++;
					}
					//
					index++;
				}
			}
		}
		return result;
	}

	public Object getWorkshopCheckPlanConfiges(long workshopId,//
			long factoryId,//
			String organizationId,//
			int minIndex,//
			int dataNum) {
		List<SpotCheckPlanConfigModel> result = new ArrayList<SpotCheckPlanConfigModel>();
		int index=0;
		int num=0;
		synchronized (cachedCheckPlanConfigModelsLockObj) {
			for (SpotCheckPlanConfigModel item : cachedCheckPlanConfigModels) {
				if (item.getWorkshopId()==workshopId//
						&&item.getFactoryId()==factoryId//
						&&item.getOrganizationId().equals(organizationId)) {
					//
					if (index>=minIndex&&dataNum>=num) {
						result.add(item);
						num++;
					}
					//
					index++;
				}
			}
		}
		return result;
	}

	public Object getFactoryCheckPlanConfiges(long factoryId, String organizationId, int minIndex, int dataNum) {
		List<SpotCheckPlanConfigModel> result = new ArrayList<SpotCheckPlanConfigModel>();
		int index=0;
		int num=0;
		synchronized (cachedCheckPlanConfigModelsLockObj) {
			for (SpotCheckPlanConfigModel item : cachedCheckPlanConfigModels) {
				if (item.getFactoryId()==factoryId//
						&&item.getOrganizationId().equals(organizationId)) {
					//
					if (index>=minIndex&&dataNum>=num) {
						result.add(item);
						num++;
					}
					//
					index++;
				}
			}
		}
		return result;
	}

	public Object getOrganizationCheckPlanConfiges(String organizationId, int minIndex, int dataNum) {
		List<SpotCheckPlanConfigModel> result = new ArrayList<SpotCheckPlanConfigModel>();
		int index=0;
		int num=0;
		synchronized (cachedCheckPlanConfigModelsLockObj) {
			for (SpotCheckPlanConfigModel item : cachedCheckPlanConfigModels) {
				if (item.getOrganizationId().equals(organizationId)) {
					//
					if (index>=minIndex&&dataNum>=num) {
						result.add(item);
						num++;
					}
					//
					index++;
				}
			}
		}
		return result;
	}

	public SpotCheckPlanConfigModel getCheckPlanConfigById(long id, String organizationId) {
		SpotCheckPlanConfigModel spotCheckPlanConfigModel=null;
		synchronized (cachedCheckPlanConfigModelsLockObj) {
			for (SpotCheckPlanConfigModel item : cachedCheckPlanConfigModels) {
				if (item.getId()==id&&item.getOrganizationId().equals(organizationId)) {
					spotCheckPlanConfigModel=item; 
				}
			}
		}
		return spotCheckPlanConfigModel;
	}

	public boolean setCheckPlanConfig(SpotCheckPlanConfigModel model) {
		if (model==null) {
			return false;
		}
		//新建
		if (model.getId()<=0) {
			if (spotCheckConfigModelDao.setCheckPlanConfig(model)) {
				//缓存
				synchronized (cachedCheckPlanConfigModelsLockObj) {
					cachedCheckPlanConfigModels.add(model);
				}
				return true;
			}
		}
		//修改
		else {
			if (spotCheckConfigModelDao.setCheckPlanConfig(model)) {
				//缓存
				SpotCheckPlanConfigModel spotCheckPlanConfigModel=getCheckPlanConfigById(model.getId(), model.getOrganizationId());
				if (spotCheckPlanConfigModel!=null) {
					spotCheckPlanConfigModel.copyFieds(model);
				}
				return true;
			}
		}
		return false;
	}
	
	public boolean deleteCheckPlanConfigById(long id, String organizationId) {
		//缓存
		synchronized (cachedCheckPlanConfigModelsLockObj) {
			for (int i = 0; i < cachedCheckPlanConfigModels.size(); i++) {
				if (cachedCheckPlanConfigModels.get(i).getId()==id//
						&&cachedCheckPlanConfigModels.get(i).getOrganizationId().equals(organizationId)) {
					cachedCheckPlanConfigModels.remove(i);
					break;
				}
			}
		}
		//删库
		return spotCheckConfigModelDao.deleteCheckPlanConfigById(id, organizationId);
	}

	public boolean deleteCheckItemConfigById(long id, String organizationId) {
		return spotCheckConfigModelDao.deleteCheckItemConfigById(id, organizationId);
	}
	
	public List<SpotCheckItemConfigModel> getCheckItemConfigesByPlanId(long planId, String organizationId) {
		return spotCheckConfigModelDao.getCheckItemConfigesByPlanId(planId, organizationId);
	}

	public boolean setCheckItemConfig(SpotCheckItemConfigModel model) {
		return spotCheckConfigModelDao.setCheckItemConfig(model);
	}
}
