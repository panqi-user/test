package cn.sd.zhidetec.jdevice.dao;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import cn.sd.zhidetec.jdevice.Starter;
import cn.sd.zhidetec.jdevice.config.DataSourceConfiger;
import cn.sd.zhidetec.jdevice.model.DeviceTypeModel;
import cn.sd.zhidetec.jdevice.util.StringUtil;

@Service
public class DeviceTypeModelDao {

	@Autowired
	@Qualifier(DataSourceConfiger.BEAN_NAME_JdeviceJdbcTemplate)
	protected JdbcTemplate jdbcTemplate;
	protected Logger logger = LoggerFactory.getLogger(getClass());

	public DeviceTypeModel getDeviceTypeModelById(long id) {
		Object[] args = new Object[1];
		args[0] = id;
		List<DeviceTypeModel> list = jdbcTemplate.query(Sql_GetModel_By_Id, args, beanPropertyRowMapper);
		if (list == null || list.size() <= 0) {
			return null;
		}
		DeviceTypeModel model = list.get(0);
		return model;
	}

	private static String Sql_GetModel_By_Id = "SELECT * FROM jdevice_c_device_type WHERE id = ?";

	public List<DeviceTypeModel> getDeviceTypeModelsByWorkshopId(long workshopId) {
		Object[] args = new Object[1];
		args[0] = workshopId;
		List<DeviceTypeModel> list = jdbcTemplate.query(Sql_GetModels_By_WorkshopId, args, beanPropertyRowMapper);
		return list;
	}

	private static String Sql_GetModels_By_WorkshopId = "SELECT * FROM jdevice_c_device_type WHERE workshop_id = ?";

	public List<DeviceTypeModel> getDeviceTypeModelsByFactoryId(long factoryId) {
		Object[] args = new Object[1];
		args[0] = factoryId;
		List<DeviceTypeModel> list = jdbcTemplate.query(Sql_GetModels_By_FactoryId, args, beanPropertyRowMapper);
		return list;
	}

	private static String Sql_GetModels_By_FactoryId = "SELECT * FROM jdevice_c_device_type WHERE factory_id = ? ORDER BY workshop_id DESC";

	public List<DeviceTypeModel> getDeviceTypeModelsByOrganizationId(String organizationId) {
		Object[] args = new Object[1];
		args[0] = organizationId;
		List<DeviceTypeModel> list = jdbcTemplate.query(Sql_GetModels_By_OrganizationId, args, beanPropertyRowMapper);
		return list;
	}

	private static String Sql_GetModels_By_OrganizationId = "SELECT * FROM jdevice_c_device_type WHERE organization_id = ? ORDER BY workshop_id DESC";

	public boolean setDeviceTypeModel(DeviceTypeModel deviceTypeModel) {
		if (StringUtil.isEmpty(deviceTypeModel.getOrganizationId())) {
			return false;
		}
		//
		int i = 0;
		Object[] args = new Object[6];
		// 修改信息
		if (deviceTypeModel.getId()>0
				&& getDeviceTypeModelById(deviceTypeModel.getId()) != null) {
			args[i++] = StringUtil.getEmptyString(deviceTypeModel.getCode());
			args[i++] = StringUtil.getEmptyString(deviceTypeModel.getName());
			args[i++] = deviceTypeModel.getWorkshopId();
			args[i++] = deviceTypeModel.getFactoryId();
			args[i++] = StringUtil.getEmptyString(deviceTypeModel.getOrganizationId());
			args[i++] = deviceTypeModel.getId();
			i = jdbcTemplate.update(Sql_UpdateModel_By_Id, args);
		}
		// 新建信息
		else {
			args = new Object[6];
			deviceTypeModel.setId(Starter.IdMaker.nextId());
			args[i++] = deviceTypeModel.getId();
			args[i++] = StringUtil.getEmptyString(deviceTypeModel.getCode());
			args[i++] = StringUtil.getEmptyString(deviceTypeModel.getName());
			args[i++] = deviceTypeModel.getWorkshopId();
			args[i++] = deviceTypeModel.getFactoryId();
			args[i++] = StringUtil.getEmptyString(deviceTypeModel.getOrganizationId());
			i = jdbcTemplate.update(Sql_InsertModel, args);
		}
		if (i > 0) {
			return true;
		}
		return false;
	}

	private static String Sql_UpdateModel_By_Id = "UPDATE jdevice_c_device_type" + //
			" SET " + //
			"code = ?," + //
			"name = ?," + //
			"workshop_id = ?," + //
			"factory_id = ?," + //
			"organization_id = ?" + //
			" WHERE " + //
			"id=?";
	private static String Sql_InsertModel = "INSERT INTO jdevice_c_device_type (" + //
			"id," + //
			"code," + //
			"name," + //
			"workshop_id," + //
			"factory_id," + //
			"organization_id" + //
			")VALUES(" + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?" + //
			")";

	public boolean deleteDeviceTypeModel(DeviceTypeModel deviceTypeModel) {
		int i = 0;
		Object[] args = new Object[1];
		args[i++] = deviceTypeModel.getId();
		if (deviceTypeModel.getId() > 0) {
			i = jdbcTemplate.update(Sql_DeleteModel_By_Id, args);
		}
		if (i > 0) {
			return true;
		}
		return false;
	}

	private static String Sql_DeleteModel_By_Id = "DELETE FROM jdevice_c_device_type WHERE id=?";
	
	private BeanPropertyRowMapper<DeviceTypeModel> beanPropertyRowMapper = new BeanPropertyRowMapper<DeviceTypeModel>(
			DeviceTypeModel.class);
	
	public boolean changeWorkshop(long deviceTypeId,long workshopId, long factoryId) {
		Object[] args = new Object[3];
		int i=0;
		args[i++] = workshopId;
		args[i++] = factoryId;
		args[i++] = deviceTypeId;
		return jdbcTemplate.update(Sql_ChangeWorkshop_By_DeviceTypeId, args)>0;
	}
	
	private static String Sql_ChangeWorkshop_By_DeviceTypeId="UPDATE jdevice_c_device_type SET workshop_id=?,factory_id=? WHERE id=?;";
	
	public boolean changeFactory(long workshopId,long factoryId) {
		Object[] args = new Object[2];
		int i=0;
		args[i++] = factoryId;
		args[i++] = workshopId;
		return jdbcTemplate.update(Sql_ChangeFactory_By_WorkshopId, args)>0;
	}
	
	private static String Sql_ChangeFactory_By_WorkshopId="UPDATE jdevice_c_device_type SET factory_id=? WHERE workshop_id=?;";
}
