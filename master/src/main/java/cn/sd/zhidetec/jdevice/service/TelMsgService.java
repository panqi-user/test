package cn.sd.zhidetec.jdevice.service;

import java.io.Serializable;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.zhidetec.utility.common.DateTimeUtil;
import com.zhidetec.utility.common.StringUtil;


@Service
public class TelMsgService {

    public static final long CACHE_MAXSIZE = 10000; //缓存最大容量
    public static final long EXPIRE_AFTERWRITE = 600;   //缓存超时时间（写）单位s，默认10分钟
    public static final int MIN_SEND_INTERVAL = 60; //最小发送间隔时间，单位s 默认1分钟
    
    
    
    protected Logger logger = LoggerFactory.getLogger(getClass());
    //验证码缓存，超时时间10分钟
    Cache<String, VcodeWithTime> cache = CacheBuilder.newBuilder().maximumSize(CACHE_MAXSIZE).expireAfterWrite(EXPIRE_AFTERWRITE, TimeUnit.SECONDS).build();


	/*
	 * 发送手机验证码方法
	 * 参数 phoneNum：手机号
	 * 返回：四位整形随机数，异常返回小于等于0的整数。
	 * 验证码10分钟内有效，同一手机号1分钟内只发送一次短信。
	 * 异常代码：
	 *  0 ： 手机号不合法
	 *  -1：
	 *  
	 */
	public int sendVCodeMsg(String phoneNum) {
	    if(!checkMobilePhone(phoneNum)) {
	        logger.warn("Fail to send SMS to Phone： "+ phoneNum  + "illegal phone number!");
	        return 0;
	    }
	    VcodeWithTime cachevcodewithTime = cache.getIfPresent(phoneNum);
	    if(cachevcodewithTime != null) {
	        //如果缓存结果距离现在少于最小间隔，则不发送短信,返回-1
	       if( cachevcodewithTime.getDt().plusSeconds(MIN_SEND_INTERVAL).isAfterNow()) {
	           return -1;
	       }
	    }
	    
	    /*
	     * 调用腾讯云的短信发送接口
	     */
	    //生产四位随机数,作为验证码
         int randomVcode = new Random().nextInt(8999) + 1000;//四位随机数1000-9999

	    String sendInfo = TencentCloudSMS.sendSMS(phoneNum, String.valueOf(randomVcode), 10);
	    logger.info(sendInfo);
	    cache.put(phoneNum, new VcodeWithTime(randomVcode, DateTimeUtil.now()));
//	    SendSmsResponse sendObj = JsonUtil.readValue(sendInfo, SendSmsResponse.class);
//	    SendStatus[] sendStatusSet = sendObj.getSendStatusSet();
//	    for( SendStatus ss : sendStatusSet) {
//	        String code = ss.getCode();
//	        if(!"Ok".equals(code))
//	            return -2;
//	        
//	    }
	    return randomVcode;
	    /*
	     * 初始化云片客户端
	     * TODO:此处考虑做成单例
	     * TODO: 改成腾讯云 这一段代码暂时不用  20200416
	     */
//	    YunpianClient clnt = new YunpianClient("0365d03357d948abf2e6865e149acb60").init();
//	    int randomVcode = new Random().nextInt(8999) + 1000;//四位随机数1000-9999
//	    
//	    Map<String, String> param = clnt.newParam(2);
//	    param.put(YunpianClient.MOBILE, phoneNum);
//	    param.put(YunpianClient.TEXT, "【致德工业】您的验证码是" + randomVcode + "。如非本人操作，请忽略。");
//	   
//	    Result<SmsSingleSend> r = clnt.sms().single_send(param);
//	    if(r.isSucc()) {
//	        //发送成功，放入缓存
//	        cache.put(phoneNum, new VcodeWithTime(randomVcode, DateTimeUtil.now()));
//	        logger.info("Send SMS vCode("+randomVcode+") To Phone:"+phoneNum);
//	        clnt.close();
//	        return randomVcode;
//	    }
//	    logger.info("Failed to Send SMS To Phone:" + phoneNum );
//        clnt.close();
//		return r.getCode();
	    
	    
	    
	}
	/*
	 * 判断验证码是否一致
	 * TODO 测试期间直接返回true
	 */
	public boolean checkVCode(String phoneNum,String vCode) {
	    if(!checkMobilePhone(phoneNum) || StringUtil.isEmpty(vCode))
	        return false;
	    VcodeWithTime value = cache.getIfPresent(phoneNum);
	    if (value==null) {
	           //测试先直接返回true
	            return false;
	        }
	    if(String.valueOf(value.getVcode()).equals(vCode))
	        return true;
	    //测试先直接返回true
	    return false;
	}
	
	public static boolean checkMobilePhone(String string) {
		if (string==null||string.trim().equals("")) {
			return false;
		}
        return patternMobilePhone.matcher(string).matches();//matches() 方法用于检测字符串是否匹配给定的正则表达式。
    }
	private static Pattern patternMobilePhone = Pattern.compile("^[1]\\d{10}$");
	
	
	class VcodeWithTime implements Serializable {

        public int getVcode() {
            return vcode;
        }
        public void setVcode(int vcode) {
            this.vcode = vcode;
        }
        public DateTime getDt() {
            return dt;
        }
        public void setDt(DateTime dt) {
            this.dt = dt;
        }

        /**
         *
         */
        private static final long serialVersionUID = 1L;
        
        public VcodeWithTime(int vcode,DateTime dt) {
            this.vcode =  vcode;
            this.dt = dt;
        }
        
        private int vcode;
        private DateTime dt;
	    
	    
	}
	
}
