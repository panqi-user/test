package cn.sd.zhidetec.jdevice.server;

import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.sd.zhidetec.jdevice.client.JedgeClientInfo;
import cn.sd.zhidetec.jdevice.model.AndonMessageModel;
import cn.sd.zhidetec.jdevice.model.WarnDataModel;
import cn.sd.zhidetec.jdevice.service.JedgeTelegramService;
import cn.sd.zhidetec.jdevice.util.ByteUtil;

public class JedgeClientTask implements Runnable{
	private JedgeClientInfo jedgeInfoModel;
	private Object socketObjLock = new Object();
	private Socket socket=null;
	private Thread thread;
	private boolean started=false;
	private boolean stoped=true;
	private long noDataTimeMsToOffline;
	private ObjectMapper jsonMapper=new ObjectMapper();
	protected Logger logger = LoggerFactory.getLogger(getClass());
	@Override
	public void run() {
		// TODO Auto-generated method stub
		if (jedgeInfoModel==null) {
			started=false;
			stoped=true;
			return;
		}
		started=true;
		stoped=false;
		noDataTimeMsToOffline=System.currentTimeMillis()+jedgeInfoModel.getNoDataTimeMsToOffline();    	
		List<Integer> telegramIndexList = new ArrayList<>();
		List<WarnDataModel> updatingWarnDataModels = new ArrayList<>();
		List<AndonMessageModel> sendingAndonMessageModels = new ArrayList<>();
		while (started) {
			try {
				Thread.sleep(1);
				if (socket==null) {
					if (lastSyncJedgeInfoModel!=null) {
						syncJedgeInfoModel=lastSyncJedgeInfoModel;
					}
					updatingWarnDataModels.clear();
					sendingAndonMessageModels.clear();
					break;
				}
				if (noDataTimeMsToOffline<System.currentTimeMillis()) {
					socket.close();
					socket=null;
					updatingWarnDataModels.clear();
					sendingAndonMessageModels.clear();
					break;
				}
				//修改报警信息下发
				if (updateWarnDataModels.size()>0//
						&&updatingWarnDataModels.size()<=0) {
					synchronized (updateWarnDataModelsLockObject) {
						updatingWarnDataModels.addAll(updateWarnDataModels);
					}
					//报文组合
					byte[] jsonBytes = jsonMapper.writeValueAsBytes(updatingWarnDataModels);
					byte[] telegram=new byte[jsonBytes.length+10];
					int i=0;
					telegram[i++]=JedgeTelegramService.TELEGRAM_BYTE_Begin;
					telegram[i++]=0x00;
					telegram[i++]=JedgeTelegramService.TELEGRAM_BYTE_FUNC_UpdateWarnData;
					telegram[i++]=(byte)(jsonBytes.length>>24);
					telegram[i++]=(byte)(jsonBytes.length>>16);
					telegram[i++]=(byte)(jsonBytes.length>>8);
					telegram[i++]=(byte)(jsonBytes.length);
					for (int j=0; j < jsonBytes.length; j++) {
						telegram[i++]=jsonBytes[j];
					}
					telegram[i++]=0x00;
					telegram[i++]=0x00;
					telegram[i++]=JedgeTelegramService.TELEGRAM_BYTE_End;
					//CRC校验位
					
					//下发
					this.socket.getOutputStream().write(telegram);
					this.socket.getOutputStream().flush();
				}
				//下发Andon消息
				if (sendAndonMessageModels.size()>0//
						&&sendingAndonMessageModels.size()<=0) {
					synchronized (sendAndonMessageModelsLockObject) {
						sendingAndonMessageModels.addAll(sendAndonMessageModels);
					}
					//报文组合
					byte[] jsonBytes = jsonMapper.writeValueAsBytes(sendingAndonMessageModels);
					byte[] telegram=new byte[jsonBytes.length+10];
					int i=0;
					telegram[i++]=JedgeTelegramService.TELEGRAM_BYTE_Begin;
					telegram[i++]=0x00;
					telegram[i++]=JedgeTelegramService.TELEGRAM_BYTE_FUNC_SendAndonMessage;
					telegram[i++]=(byte)(jsonBytes.length>>24);
					telegram[i++]=(byte)(jsonBytes.length>>16);
					telegram[i++]=(byte)(jsonBytes.length>>8);
					telegram[i++]=(byte)(jsonBytes.length);
					for (int j=0; j < jsonBytes.length; j++) {
						telegram[i++]=jsonBytes[j];
					}
					telegram[i++]=0x00;
					telegram[i++]=0x00;
					telegram[i++]=JedgeTelegramService.TELEGRAM_BYTE_End;
					//下发
					this.socket.getOutputStream().write(telegram);
					this.socket.getOutputStream().flush();
					logger.debug("Andon消息下发");
				}
				//终端信息修改
				if (syncJedgeInfoModel!=null) {
					//系统终端信息报文
					byte[] jedgeInfoModelJsonStr = jsonMapper.writeValueAsBytes(syncJedgeInfoModel);
			    	byte[] jedgeInfoModelTelegram = new byte[jedgeInfoModelJsonStr.length+10];
			    	jedgeInfoModelTelegram[0]=JedgeTelegramService.TELEGRAM_BYTE_Begin;
			    	jedgeInfoModelTelegram[1]=0x00;
			    	jedgeInfoModelTelegram[2]=JedgeTelegramService.TELEGRAM_BYTE_FUNC_UpdateClientInfo;
			    	jedgeInfoModelTelegram[3]=(byte)(jedgeInfoModelJsonStr.length>>24);
			    	jedgeInfoModelTelegram[4]=(byte)(jedgeInfoModelJsonStr.length>>16);
			    	jedgeInfoModelTelegram[5]=(byte)(jedgeInfoModelJsonStr.length>>8);
			    	jedgeInfoModelTelegram[6]=(byte)(jedgeInfoModelJsonStr.length);
			    	for (int j = 0; j < jedgeInfoModelJsonStr.length; j++) {
			    		jedgeInfoModelTelegram[j+7]=jedgeInfoModelJsonStr[j];
					}
			    	jedgeInfoModelTelegram[7+jedgeInfoModelJsonStr.length]=0x00;
			    	jedgeInfoModelTelegram[8+jedgeInfoModelJsonStr.length]=0x00;
			    	jedgeInfoModelTelegram[9+jedgeInfoModelJsonStr.length]=JedgeTelegramService.TELEGRAM_BYTE_End;
			    	//CRC校验位
					
			    	//回复
			    	this.socket.getOutputStream().write(jedgeInfoModelTelegram);
			    	this.socket.getOutputStream().flush();
			    	lastSyncJedgeInfoModel=syncJedgeInfoModel;
			    	syncJedgeInfoModel=null;
				}
				//报文获取
				synchronized (socketObjLock) {
	    			if (this.socket.getInputStream().available() > 0) {
						// 保证数据完整性
						int num = this.socket.getInputStream().available();
						Thread.sleep(1);
						if (this.socket.getInputStream().available() == num&&num>=8) {
							// 读取数据
							noDataTimeMsToOffline=System.currentTimeMillis()+jedgeInfoModel.getNoDataTimeMsToOffline();
							byte[] response = new byte[num];
							this.socket.getInputStream().read(response);
							telegramIndexList.clear();
							// 报文分割
							if (response[0]==JedgeTelegramService.TELEGRAM_BYTE_Begin&&response[response.length-1]==JedgeTelegramService.TELEGRAM_BYTE_End) {
								for (int i = 0; i < response.length; ) {
									telegramIndexList.add(i);
									int index=telegramIndexList.get(telegramIndexList.size()-1);
									int length = ByteUtil.getInt32(response, index+3);
									i+=length+10;
								}
							}
							// 解析数据
							for (int i = 0; i < telegramIndexList.size(); i++) {
								int index=telegramIndexList.get(telegramIndexList.size()-1);
								int length = ByteUtil.getInt32(response, index+3);
								// 合法性校验
								// 回复心跳请求
								if (response[index+1]==0x00&&response[index+2]==JedgeTelegramService.TELEGRAM_BYTE_FUNC_SendHeartRequest) {
									socket.getOutputStream().write(JedgeTelegramService.getRecvHeartResponse());
								}
								// 修改终端配置
								else if (response[index+1]==0x00&&response[index+2]==JedgeTelegramService.TELEGRAM_BYTE_FUNC_UpdateClientInfo) {
									String jsonStr = new String(response, 7, length);
									if (jsonStr!=null&&!jsonStr.trim().equals("")) {
										JedgeClientInfo jedgeInfoModel = jsonMapper.readValue(jsonStr, JedgeClientInfo.class);
										this.setJedgeInfoModel(jedgeInfoModel);
										// 操作成功的回复
										byte[] telegram=JedgeTelegramService.getDoneSuccessResponse();
										telegram[7]=0x00;
										telegram[8]=JedgeTelegramService.TELEGRAM_BYTE_FUNC_UpdateClientInfo;
										//CRC校验位
										//发送
										this.socket.getOutputStream().write(telegram);
										this.socket.getOutputStream().flush();
									}else {
										//操作失败的回复
										byte[] telegram=JedgeTelegramService.getDoneFailResponse();
										telegram[7]=0x00;
										telegram[8]=JedgeTelegramService.TELEGRAM_BYTE_FUNC_UpdateClientInfo;
										//CRC校验位
										//发送
										this.socket.getOutputStream().write(telegram);
										this.socket.getOutputStream().flush();
									}
								}
								// 操作成功的提示
								else if (response[index+1]==0x00&&response[index+2]==JedgeTelegramService.TELEGRAM_BYTE_FUNC_DoneSuccess) {
									//修改报警信息成功
									if (response[index+7]==0x00&&response[index+8]==JedgeTelegramService.TELEGRAM_BYTE_FUNC_UpdateWarnData) {
										synchronized (updateWarnDataModelsLockObject) {
											for (int j = updatingWarnDataModels.size()-1; j >= 0; j--) {
												updatingWarnDataModels.remove(j);
												updateWarnDataModels.remove(j);
											}	
										}
									}
									//Andon消息下发成功
									if (response[index+7]==0x00&&response[index+8]==JedgeTelegramService.TELEGRAM_BYTE_FUNC_SendAndonMessage) {
										synchronized (sendAndonMessageModelsLockObject) {
											for (int j = sendingAndonMessageModels.size()-1; j >= 0; j--) {
												sendingAndonMessageModels.remove(j);
												sendAndonMessageModels.remove(j);
											}	
										}
										logger.debug("Andon消息下发成功");
									}
									//修改终端信息成功
									else if (response[index+7]==0x00&&response[index+8]==JedgeTelegramService.TELEGRAM_BYTE_FUNC_UpdateClientInfo) {
										if (lastSyncJedgeInfoModel!=null) {
											jedgeInfoModel.copyFields(lastSyncJedgeInfoModel);
											lastSyncJedgeInfoModel=null;
										}
									}
								}
								// 操作失败的提示
								else if (response[index+1]==0x00&&response[index+2]==JedgeTelegramService.TELEGRAM_BYTE_FUNC_DoneFail) {
									if (response[index+7]==0x00&&response[index+8]==JedgeTelegramService.TELEGRAM_BYTE_FUNC_UpdateWarnData) {
										updatingWarnDataModels.clear();
									}
								}
							}
						}
					}
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
		stoped=true;
		started=false;
	}
	public void syncJedgeInfoModel(JedgeClientInfo jedgeInfoModel) {
		this.syncJedgeInfoModel=jedgeInfoModel;
	}
	private JedgeClientInfo syncJedgeInfoModel;
	private JedgeClientInfo lastSyncJedgeInfoModel;
	/**
	 * 修改报警信息，如果报警信息不存在则进行新建
	 * @param model 报警信息
	 * */
	public void updateWarnDataModel(WarnDataModel model) {
		synchronized (updateWarnDataModelsLockObject) {
			updateWarnDataModels.add(model);
		}
	}
	private List<WarnDataModel> updateWarnDataModels=Collections.synchronizedList(new ArrayList<>());
	private Object updateWarnDataModelsLockObject=new Object();
	/**
	 * Andon消息下发
	 * @param model Andon信息
	 * */
	public void sendAndonMessage(AndonMessageModel model) {
		synchronized (sendAndonMessageModelsLockObject) {
			sendAndonMessageModels.add(model);
		}
	}
	private List<AndonMessageModel> sendAndonMessageModels=Collections.synchronizedList(new ArrayList<>());
	private Object sendAndonMessageModelsLockObject=new Object();
	public void startThread() {
		if (started==false&&stoped==true) {
			thread=new Thread(this);
			thread.start();
		}
	}
	public void stopThread() {
		started=false;
	}
	public boolean hadStarted() {
		return started;
	}
	public boolean hadStoped() {
		return stoped;
	}
	public Socket getSocket() {
		return socket;
	}
	public void setSocket(Socket socket) {
		this.socket = socket;
	}
	public JedgeClientInfo getJedgeInfoModel() {
		return jedgeInfoModel;
	}
	public void setJedgeInfoModel(JedgeClientInfo jedgeInfoModel) {
		this.jedgeInfoModel = jedgeInfoModel;
	}
	public Object getSocketObjLock() {
		return socketObjLock;
	}
	public void setSocketObjLock(Object socketObjLock) {
		this.socketObjLock = socketObjLock;
	}
	public boolean equals(JedgeClientTask jedgeModel) {
		// TODO Auto-generated method stub
		if (jedgeModel.getJedgeInfoModel()==null||jedgeInfoModel==null) {
			return false;
		}
		return false;
	}
	public void changeFactory(long workshopId, long factoryId) {
		if (jedgeInfoModel.getWorkshopId()==workshopId) {
			jedgeInfoModel.setFactoryId(factoryId);
			syncJedgeInfoModel(jedgeInfoModel);
		}
	}
}
