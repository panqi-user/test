package cn.sd.zhidetec.jdevice.dao;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import cn.sd.zhidetec.jdevice.config.DataSourceConfiger;
import cn.sd.zhidetec.jdevice.model.DeviceFaultModel;

@Service
public class DeviceFaultModelDao {

	@Autowired
	@Qualifier(DataSourceConfiger.BEAN_NAME_JdeviceJdbcTemplate)
	protected JdbcTemplate jdbcTemplate;
	protected Logger logger = LoggerFactory.getLogger(getClass());
	private BeanPropertyRowMapper<DeviceFaultModel> beanPropertyRowMapper = new BeanPropertyRowMapper<DeviceFaultModel>(
			DeviceFaultModel.class);
	
	public boolean insertDeviceFaultModel(DeviceFaultModel deviceFaultModel) {
		if (deviceFaultModel==null) {
			return false;
		}
		//插入
		Object[] args = new Object[9];
		int i=0;
		args[i++] = deviceFaultModel.getBeginTimeMs();
		args[i++] = deviceFaultModel.getEndTimeMs();
		args[i++] = deviceFaultModel.getRepairTimeMs();
		args[i++] = deviceFaultModel.getIntervalTimeMs();
		args[i++] = deviceFaultModel.getDeviceId();
		args[i++] = deviceFaultModel.getWorkcenterId();
		args[i++] = deviceFaultModel.getWorkshopId();
		args[i++] = deviceFaultModel.getFactoryId();
		args[i++] = deviceFaultModel.getOrganizationId();
		i=jdbcTemplate.update(Sql_InsertModel, args);
		return i>0;
	}
	private static String Sql_InsertModel="INSERT INTO jdevice_h_device_fault ("+//
			"begin_time_ms,"+//
			"end_time_ms,"+//
			"repair_time_ms,"+//
			"interval_time_ms,"+//
			"device_id,"+//
			"workcenter_id,"+//
			"workshop_id,"+//
			"factory_id,"+//
			"organization_id"+//
			")"+//
			"VALUES"+//
			"("+//
			"?,"+//
			"?,"+//
			"?,"+//
			"?,"+//
			"?,"+//
			"?,"+//
			"?,"+//
			"?,"+//
			"?"+//
			");";
	
	public boolean updateDeviceFaultModel(DeviceFaultModel deviceFaultModel) {
		if (deviceFaultModel==null) {
			return false;
		}
		//插入
		Object[] args = new Object[11];
		int i=0;
		args[i++] = deviceFaultModel.getBeginTimeMs();
		args[i++] = deviceFaultModel.getEndTimeMs();
		args[i++] = deviceFaultModel.getRepairTimeMs();
		args[i++] = deviceFaultModel.getIntervalTimeMs();
		args[i++] = deviceFaultModel.getDeviceId();
		args[i++] = deviceFaultModel.getWorkcenterId();
		args[i++] = deviceFaultModel.getWorkshopId();
		args[i++] = deviceFaultModel.getFactoryId();
		args[i++] = deviceFaultModel.getOrganizationId();
		args[i++] = deviceFaultModel.getBeginTimeMs();
		args[i++] = deviceFaultModel.getDeviceId();
		i=jdbcTemplate.update(Sql_UpdateModel, args);
		return i>0;
	}
	private static String Sql_UpdateModel="UPDATE jdevice_h_device_fault"+//
			" SET "+//
			"begin_time_ms=?,"+//
			"end_time_ms=?,"+//
			"repair_time_ms=?,"+//
			"interval_time_ms=?,"+//
			"device_id=?,"+//
			"workcenter_id=?,"+//
			"workshop_id=?,"+//
			"factory_id=?,"+//
			"organization_id=?"+//
			" WHERE "+//
			"begin_time_ms=?"+//
			" AND "+//
			"device_id=?";
	
	public List<DeviceFaultModel> getLast2DeviceModelByDeviceId(long deviceId) {
		Object[] args = new Object[1];
		args[0] = deviceId;
		List<DeviceFaultModel> list = jdbcTemplate.query(Sql_GetLast2Model_By_DeviceId, args,
				beanPropertyRowMapper);
		return list;
	}
	private static String Sql_GetLast2Model_By_DeviceId="SELECT * FROM jdevice_h_device_fault WHERE device_id=? ORDER BY begin_time_ms DESC LIMIT 0,2";
	
	public DeviceFaultModel getLastDeviceModelByDeviceId(long deviceId) {
		Object[] args = new Object[1];
		args[0] = deviceId;
		List<DeviceFaultModel> list = jdbcTemplate.query(Sql_GetLastModel_By_DeviceId, args,
				beanPropertyRowMapper);
		if (list==null||list.size()<=0) {
			return null;
		}
		return list.get(0);
	}
	private static String Sql_GetLastModel_By_DeviceId="SELECT * FROM jdevice_h_device_fault WHERE device_id=? ORDER BY begin_time_ms DESC LIMIT 0,1";
	
	public List<DeviceFaultModel> getDeviceFaultModelsByBeginTimsMs(long beginTimeMs,long deviceId) {
		Object[] args = new Object[2];
		args[0] = deviceId;
		args[1] = beginTimeMs;
		List<DeviceFaultModel> list = jdbcTemplate.query(Sql_GetModels_By_BeginTimeMs_DeviceId, args,
				beanPropertyRowMapper);
		return list;
	}
	private static String Sql_GetModels_By_BeginTimeMs_DeviceId="SELECT * FROM jdevice_h_device_fault WHERE device_id=? AND begin_time_ms>=? ORDER BY begin_time_ms DESC";
	
	public Map<String, Object> getDeviceMTTR(long deviceId,long beginTimeMs) {
		Object[] args = new Object[2];
		args[0] = deviceId;
		args[1] = beginTimeMs;
		List<Map<String, Object>> list = jdbcTemplate.queryForList(Sql_GetMTTR_By_Id, args);
		if (list==null||list.size()<=0) {
			return null;
		}
		return list.get(0);
	}
	private static String Sql_GetMTTR_By_Id="SELECT "+//
			" AVG(repair_time_ms) AS MTTR,"+//
			"device_id"+//
			" FROM "+//
			"jdevice_h_device_fault"+//
			" WHERE "+//
			"repair_time_ms > 0"+//
			" AND device_id =?"+//
			" AND begin_time_ms >= ?;";
	
	public List<Map<String, Object>> getWorkshopMTTR(long workshopId,long beginTimeMs) {
		Object[] args = new Object[2];
		args[0] = workshopId;
		args[1] = beginTimeMs;
		List<Map<String, Object>> list = jdbcTemplate.queryForList(Sql_GetMTTR_By_WorkshopId, args);
		return list;
	}
	private static String Sql_GetMTTR_By_WorkshopId="SELECT "+//
			" AVG(repair_time_ms) AS MTTR,"+//
			"device_id"+//
			" FROM "+//
			"jdevice_h_device_fault"+//
			" WHERE "+//
			"repair_time_ms > 0"+//
			" AND workshop_id =?"+//
			" AND begin_time_ms >= ?"+//
			" GROUP BY device_id;";
	
	public Map<String, Object> getDeviceMTBF(long deviceId,long beginTimeMs) {
		Object[] args = new Object[2];
		args[0] = deviceId;
		args[1] = beginTimeMs;
		List<Map<String, Object>> list = jdbcTemplate.queryForList(Sql_GetMTBF_By_Id, args);
		if (list==null||list.size()<=0) {
			return null;
		}
		return list.get(0);
	}
	private static String Sql_GetMTBF_By_Id="SELECT "+//
			" AVG(interval_time_ms) AS MTBF,"+//
			"device_id"+//
			" FROM "+//
			"jdevice_h_device_fault"+//
			" WHERE "+//
			"interval_time_ms > 0"+//
			" AND device_id =?"+//
			" AND begin_time_ms >= ?;";
	
	public List<Map<String, Object>> getWorkshopMTBF(long workshopId,long beginTimeMs) {
		Object[] args = new Object[2];
		args[0] = workshopId;
		args[1] = beginTimeMs;
		List<Map<String, Object>> list = jdbcTemplate.queryForList(Sql_GetMTBF_By_WorkshopId, args);
		return list;
	}
	private static String Sql_GetMTBF_By_WorkshopId="SELECT "+//
			" AVG(interval_time_ms) AS MTBF,"+//
			"device_id"+//
			" FROM "+//
			"jdevice_h_device_fault"+//
			" WHERE "+//
			"interval_time_ms > 0"+//
			" AND workshop_id =?"+//
			" AND begin_time_ms >= ?"+//
			" GROUP BY device_id;";
	
	public List<Map<String, Object>> getWorkcenterMTTR(long workcenterId,long beginTimeMs) {
		Object[] args = new Object[2];
		args[0] = workcenterId;
		args[1] = beginTimeMs;
		List<Map<String, Object>> list = jdbcTemplate.queryForList(Sql_GetMTTR_By_WorkcenterId, args);
		return list;
	}
	private static String Sql_GetMTTR_By_WorkcenterId="SELECT "+//
			" AVG(repair_time_ms) AS MTTR,"+//
			"device_id"+//
			" FROM "+//
			"jdevice_h_device_fault"+//
			" WHERE "+//
			"repair_time_ms > 0"+//
			" AND workcenter_id =?"+//
			" AND begin_time_ms >= ?"+//
			" GROUP BY device_id;";
	
	public List<Map<String, Object>> getWorkcenterMTBF(long workcenterId,long beginTimeMs) {
		Object[] args = new Object[2];
		args[0] = workcenterId;
		args[1] = beginTimeMs;
		List<Map<String, Object>> list = jdbcTemplate.queryForList(Sql_GetMTBF_By_WorkcenterId, args);
		return list;
	}
	private static String Sql_GetMTBF_By_WorkcenterId="SELECT "+//
			" AVG(interval_time_ms) AS MTBF,"+//
			"device_id"+//
			" FROM "+//
			"jdevice_h_device_fault"+//
			" WHERE "+//
			"interval_time_ms > 0"+//
			" AND workcenter_id =?"+//
			" AND begin_time_ms >= ?"+//
			" GROUP BY device_id;";
	
	public boolean changeWorkcenter(long deviceId, long workcenterId, long workshopId, long factoryId) {
		Object[] args = new Object[4];
		int i=0;
		args[i++] = workcenterId;
		args[i++] = workshopId;
		args[i++] = factoryId;
		args[i++] = deviceId;
		return jdbcTemplate.update(Sql_ChangeWorkcenter_By_DeviceId, args)>0;
	}
	
	private static String Sql_ChangeWorkcenter_By_DeviceId = "UPDATE jdevice_h_device_fault SET workcenter_id=?,workshop_id=?,factory_id=? WHERE device_id=?;";
	
	public boolean changeWorkshop(long workcenterId,long workshopId, long factoryId) {
		Object[] args = new Object[3];
		int i=0;
		args[i++] = workshopId;
		args[i++] = factoryId;
		args[i++] = workcenterId;
		return jdbcTemplate.update(Sql_ChangeWorkshop_By_WorkcenterId, args)>0;
	}
	
	private static String Sql_ChangeWorkshop_By_WorkcenterId="UPDATE jdevice_h_device_fault SET workshop_id=?,factory_id=? WHERE workcenter_id=?;";
	
	public boolean changeFactory(long workshopId,long factoryId) {
		Object[] args = new Object[2];
		int i=0;
		args[i++] = factoryId;
		args[i++] = workshopId;
		return jdbcTemplate.update(Sql_ChangeFactory_By_WorkshopId, args)>0;
	}
	
	private static String Sql_ChangeFactory_By_WorkshopId="UPDATE jdevice_h_device_fault SET factory_id=? WHERE workshop_id=?;";
}
