package cn.sd.zhidetec.jdevice.controller;

import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.sd.zhidetec.jdevice.HttpResponseMsg;
import cn.sd.zhidetec.jdevice.dao.SpotCheckModelDao;
import cn.sd.zhidetec.jdevice.model.RoleModel;
import cn.sd.zhidetec.jdevice.model.SpotCheckItemModel;
import cn.sd.zhidetec.jdevice.model.SpotCheckPlanConfigModel;
import cn.sd.zhidetec.jdevice.model.SpotCheckPlanModel;
import cn.sd.zhidetec.jdevice.model.UserModel;
import cn.sd.zhidetec.jdevice.service.RBuilderService;
import cn.sd.zhidetec.jdevice.util.DateUtil;
import cn.sd.zhidetec.jdevice.util.PoiExcelUtil;
import cn.sd.zhidetec.jdevice.util.StringUtil;

@Controller
@RequestMapping("/spotCheck")
public class SpotCheckController {

	@Autowired
	protected SpotCheckModelDao spotCheckModelDao;
	@Autowired
	protected RBuilderService rBuilderService;
	protected Logger logger = LoggerFactory.getLogger(getClass());

	/**
	 * 根据状态获取点巡检计划实例列表数目
	 * 
	 * @param status 状态
	 */
	@RequestMapping("/getCheckPlanInstancesNumByStatus")
	@ResponseBody
	public RBuilderService.Response getCheckPlanInstancesNumByStatus(HttpSession httpSession, //
			HttpServletRequest httpServletRequest, //
			@RequestParam("status") int status) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_User, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		// 根据用户权限返回数据
		UserModel userModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		// 数据时间
		long beginTimeMs = 0;
		long endTimeMs = System.currentTimeMillis();
		String beginTimeMsStr = httpServletRequest.getParameter("beginTimeMs");
		String endTimeMsStr = httpServletRequest.getParameter("endTimeMs");
		if (StringUtil.isNumber(beginTimeMsStr)) {
			beginTimeMs = Long.parseLong(beginTimeMsStr);
		}
		if (StringUtil.isNumber(endTimeMsStr)) {
			endTimeMs = Long.parseLong(endTimeMsStr);
		}
		if (endTimeMs <= 0) {
			endTimeMs = System.currentTimeMillis();
		}
		if (beginTimeMs <= 0) {
			beginTimeMs = endTimeMs - DateUtil.MILLISECOND_ONE_MONTH;
		}
		// 数据过滤
		long filterDeviceId = -1;
		if (StringUtil.isNumber(httpServletRequest.getParameter("deviceId"))) {
			filterDeviceId = Long.parseLong(httpServletRequest.getParameter("deviceId"));
		}
		long filterWorkcenterId = -1;
		if (StringUtil.isNumber(httpServletRequest.getParameter("workcenterId"))) {
			filterWorkcenterId = Long.parseLong(httpServletRequest.getParameter("workcenterId"));
		}
		long filterWorkshopId = -1;
		if (StringUtil.isNumber(httpServletRequest.getParameter("workshopId"))) {
			filterWorkshopId = Long.parseLong(httpServletRequest.getParameter("workshopId"));
		}
		long filterFactoryId = -1;
		if (StringUtil.isNumber(httpServletRequest.getParameter("factoryId"))) {
			filterFactoryId = Long.parseLong(httpServletRequest.getParameter("factoryId"));
		}
		long filterExecutorId = -1;
		if (StringUtil.isNumber(httpServletRequest.getParameter("executorId"))) {
			filterExecutorId = Long.parseLong(httpServletRequest.getParameter("executorId"));
		}
		// 根据用户权限返回
		if (userModel.getRoleLevel() <= RoleModel.ROLE_LEVEL_WorkcenterAdmin) {
			return rBuilderService.update(response, //
					HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, //
					spotCheckModelDao.getWorkcenterCheckPlansNum(status, //
							userModel.getWorkcenterId(), //
							userModel.getWorkshopId(), //
							userModel.getFactoryId(), //
							userModel.getOrganizationId(), //
							beginTimeMs, //
							endTimeMs, //
							filterFactoryId, //
							filterWorkshopId, //
							filterWorkcenterId, //
							filterDeviceId, //
							filterExecutorId));
		}
		if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_WorkshopAdmin) {
			return rBuilderService.update(response, //
					HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, //
					spotCheckModelDao.getWorkshopCheckPlansNum(status, //
							userModel.getWorkshopId(), //
							userModel.getFactoryId(), //
							userModel.getOrganizationId(), //
							beginTimeMs, //
							endTimeMs, //
							filterFactoryId, //
							filterWorkshopId, //
							filterWorkcenterId, //
							filterDeviceId, //
							filterExecutorId));
		}
		if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_FactoryAdmin) {
			return rBuilderService.update(response, //
					HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, //
					spotCheckModelDao.getFactoryCheckPlansNum(status, //
							userModel.getFactoryId(), //
							userModel.getOrganizationId(), //
							beginTimeMs, //
							endTimeMs, //
							filterFactoryId, //
							filterWorkshopId, //
							filterWorkcenterId, //
							filterDeviceId, //
							filterExecutorId));
		}
		if (userModel.getRoleLevel() >= RoleModel.ROLE_LEVEL_OrganizationAdmin) {
			return rBuilderService.update(response, //
					HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, //
					spotCheckModelDao.getOrganizationCheckPlansNum(status, //
							userModel.getOrganizationId(), //
							beginTimeMs, //
							endTimeMs, //
							filterFactoryId, //
							filterWorkshopId, //
							filterWorkcenterId, //
							filterDeviceId, //
							filterExecutorId));
		}
		return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
	}

	/**
	 * 根据状态获取点巡检计划实例列表
	 * 
	 * @param status 状态
	 */
	@RequestMapping("/getCheckPlanInstancesByStatus")
	@ResponseBody
	public RBuilderService.Response getCheckPlanInstancesByStatus(HttpSession httpSession, //
			HttpServletRequest httpServletRequest, //
			@RequestParam("status") int status) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_User, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		// 根据用户权限返回数据
		UserModel userModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		// 数据分页
		int minIndex = 0;
		int dataNum = 1000;
		String minIndexStr = httpServletRequest.getParameter("minIndex");
		String dataNumStr = httpServletRequest.getParameter("dataNum");
		if (StringUtil.isNumber(minIndexStr)) {
			minIndex = Integer.parseInt(minIndexStr);
		}
		if (StringUtil.isNumber(dataNumStr)) {
			dataNum = Integer.parseInt(dataNumStr);
		}
		// 数据时间
		long beginTimeMs = 0;
		long endTimeMs = System.currentTimeMillis();
		String beginTimeMsStr = httpServletRequest.getParameter("beginTimeMs");
		String endTimeMsStr = httpServletRequest.getParameter("endTimeMs");
		if (StringUtil.isNumber(beginTimeMsStr)) {
			beginTimeMs = Long.parseLong(beginTimeMsStr);
		}
		if (StringUtil.isNumber(endTimeMsStr)) {
			endTimeMs = Long.parseLong(endTimeMsStr);
		}
		if (endTimeMs <= 0) {
			endTimeMs = System.currentTimeMillis();
		}
		if (beginTimeMs <= 0) {
			beginTimeMs = endTimeMs - DateUtil.MILLISECOND_ONE_MONTH;
		}
		// 数据过滤
		long filterDeviceId = -1;
		if (StringUtil.isNumber(httpServletRequest.getParameter("deviceId"))) {
			filterDeviceId = Long.parseLong(httpServletRequest.getParameter("deviceId"));
		}
		long filterWorkcenterId = -1;
		if (StringUtil.isNumber(httpServletRequest.getParameter("workcenterId"))) {
			filterWorkcenterId = Long.parseLong(httpServletRequest.getParameter("workcenterId"));
		}
		long filterWorkshopId = -1;
		if (StringUtil.isNumber(httpServletRequest.getParameter("workshopId"))) {
			filterWorkshopId = Long.parseLong(httpServletRequest.getParameter("workshopId"));
		}
		long filterFactoryId = -1;
		if (StringUtil.isNumber(httpServletRequest.getParameter("factoryId"))) {
			filterFactoryId = Long.parseLong(httpServletRequest.getParameter("factoryId"));
		}
		long filterExecutorId = -1;
		if (StringUtil.isNumber(httpServletRequest.getParameter("executorId"))) {
			filterExecutorId = Long.parseLong(httpServletRequest.getParameter("executorId"));
		}
		// 根据用户权限返回
		if (userModel.getRoleLevel() <= RoleModel.ROLE_LEVEL_WorkcenterAdmin) {
			return rBuilderService.update(response, //
					HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, //
					spotCheckModelDao.getWorkcenterCheckPlans(status, //
							userModel.getWorkcenterId(), //
							userModel.getWorkshopId(), //
							userModel.getFactoryId(), //
							userModel.getOrganizationId(), //
							minIndex, //
							dataNum, //
							beginTimeMs, //
							endTimeMs, //
							filterFactoryId, //
							filterWorkshopId, //
							filterWorkcenterId, //
							filterDeviceId, //
							filterExecutorId));
		}
		if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_WorkshopAdmin) {
			return rBuilderService.update(response, //
					HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, //
					spotCheckModelDao.getWorkshopCheckPlans(status, //
							userModel.getWorkshopId(), //
							userModel.getFactoryId(), //
							userModel.getOrganizationId(), //
							minIndex, //
							dataNum, //
							beginTimeMs, //
							endTimeMs, //
							filterFactoryId, //
							filterWorkshopId, //
							filterWorkcenterId, //
							filterDeviceId, //
							filterExecutorId));
		}
		if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_FactoryAdmin) {
			return rBuilderService.update(response, //
					HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, //
					spotCheckModelDao.getFactoryCheckPlans(status, //
							userModel.getFactoryId(), //
							userModel.getOrganizationId(), //
							minIndex, //
							dataNum, //
							beginTimeMs, //
							endTimeMs, //
							filterFactoryId, //
							filterWorkshopId, //
							filterWorkcenterId, //
							filterDeviceId, //
							filterExecutorId));
		}
		if (userModel.getRoleLevel() >= RoleModel.ROLE_LEVEL_OrganizationAdmin) {
			return rBuilderService.update(response, //
					HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, //
					spotCheckModelDao.getOrganizationCheckPlans(status, //
							userModel.getOrganizationId(), //
							minIndex, //
							dataNum, //
							beginTimeMs, //
							endTimeMs, //
							filterFactoryId, //
							filterWorkshopId, //
							filterWorkcenterId, //
							filterDeviceId, //
							filterExecutorId));
		}
		return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
	}

	/**
	 * 设置点巡检计划实例
	 */
	@RequestMapping("/setCheckPlanInstance")
	@ResponseBody
	public RBuilderService.Response setCheckPlanInstance(HttpSession httpSession, //
			@RequestBody SpotCheckPlanModel model) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_WorkcenterAdmin, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		// 获取登录用户
		UserModel userModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		model.setOrganizationId(userModel.getOrganizationId());
		// 填充新建信息
		if (model.getId() <= 0) {
			model.setCreateTimeMs(System.currentTimeMillis());
			model.setCreaterName(model.getName());
			model.setCreaterId(userModel.getId());
		}
		// 根据权限操作
		boolean hadPermission = false;
		if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_WorkcenterAdmin) {
			if (model.getType() == SpotCheckPlanConfigModel.TYPE_Device) {
				hadPermission = true;
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
			}
		}
		if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_WorkshopAdmin) {
			if (model.getType() == SpotCheckPlanConfigModel.TYPE_Device//
					|| model.getType() == SpotCheckPlanConfigModel.TYPE_Workshop) {
				hadPermission = true;
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
			}
		}
		if (userModel.getRoleLevel() >= RoleModel.ROLE_LEVEL_FactoryAdmin) {
			if (model.getType() == SpotCheckPlanConfigModel.TYPE_Device//
					|| model.getType() == SpotCheckPlanConfigModel.TYPE_Workshop//
					|| model.getType() == SpotCheckPlanConfigModel.TYPE_Factory) {
				hadPermission = true;
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
			}
		}
		// 操作
		if (hadPermission) {
			if (spotCheckModelDao.setCheckPlan(model)) {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, model);
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, model);
			}
		}
		return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
	}

	/**
	 * 根据计划ID获取点巡检项实例列表
	 * 
	 * @param planId 计划ID
	 * @return 反馈给前端的操作结果
	 */
	@RequestMapping("/getCheckItemInstancesByPlanId")
	@ResponseBody
	public RBuilderService.Response getCheckItemInstancesByPlanId(HttpSession httpSession, //
			@RequestParam("planId") long planId) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_User, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		// 获取当前登录用户
		UserModel userModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		// 获取当前计划
		SpotCheckPlanModel planModel = spotCheckModelDao.getCheckPlanById(planId, userModel.getOrganizationId());
		if (planModel == null) {
			return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NOT_FOUND_DATA, null);
		} else {
			// 根据权限操作
			boolean hadPermission = false;
			if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_WorkcenterAdmin) {
				if (planModel.getType() == SpotCheckPlanConfigModel.TYPE_Device) {
					hadPermission = true;
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
				}
			}
			if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_WorkshopAdmin) {
				if (planModel.getType() == SpotCheckPlanConfigModel.TYPE_Device//
						|| planModel.getType() == SpotCheckPlanConfigModel.TYPE_Workshop) {
					hadPermission = true;
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
				}
			}
			if (userModel.getRoleLevel() >= RoleModel.ROLE_LEVEL_FactoryAdmin) {
				if (planModel.getType() == SpotCheckPlanConfigModel.TYPE_Device//
						|| planModel.getType() == SpotCheckPlanConfigModel.TYPE_Workshop//
						|| planModel.getType() == SpotCheckPlanConfigModel.TYPE_Factory) {
					hadPermission = true;
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
				}
			}
			// 操作
			if (hadPermission) {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE,
						spotCheckModelDao.getCheckItemsByPlanId(planId));
			}
			return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
		}
	}

	/**
	 * 开始点巡检计划
	 */
	@RequestMapping("/startSpotCheckPlanInstance")
	@ResponseBody
	public RBuilderService.Response startSpotCheckPlan(HttpSession httpSession, //
			@RequestParam("id") long id) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_User, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		// 获取当前登录用户
		UserModel userModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		// 获取当前计划
		SpotCheckPlanModel planModel = spotCheckModelDao.getCheckPlanById(id, userModel.getOrganizationId());
		if (planModel == null) {
			return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NOT_FOUND_DATA, null);
		} else {
			// 根据权限操作
			boolean hadPermission = false;
			if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_WorkcenterAdmin) {
				if (planModel.getType() == SpotCheckPlanConfigModel.TYPE_Device) {
					hadPermission = true;
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
				}
			}
			if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_WorkshopAdmin) {
				if (planModel.getType() == SpotCheckPlanConfigModel.TYPE_Device//
						|| planModel.getType() == SpotCheckPlanConfigModel.TYPE_Workshop) {
					hadPermission = true;
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
				}
			}
			if (userModel.getRoleLevel() >= RoleModel.ROLE_LEVEL_FactoryAdmin) {
				if (planModel.getType() == SpotCheckPlanConfigModel.TYPE_Device//
						|| planModel.getType() == SpotCheckPlanConfigModel.TYPE_Workshop//
						|| planModel.getType() == SpotCheckPlanConfigModel.TYPE_Factory) {
					hadPermission = true;
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
				}
			}
			// 操作
			if (hadPermission) {
				planModel.setStartTimeMs(System.currentTimeMillis());
				planModel.setStatus(SpotCheckPlanModel.STATUS_DoChecking);
				planModel.setExecutorId(userModel.getId());
				planModel.setExecutorName(userModel.getName());
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE,
						spotCheckModelDao.setCheckPlan(planModel));
			}
			return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
		}
	}

	/**
	 * 设置点巡检项实例
	 */
	@RequestMapping("/setCheckItemInstance")
	@ResponseBody
	public RBuilderService.Response setCheckItemInstance(HttpSession httpSession, //
			@RequestBody SpotCheckItemModel model) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_User, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		if (model.getId() < 0) {
			return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
		}
		// 获取当前登录用户
		UserModel userModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		// 获取当前计划
		SpotCheckPlanModel planModel = spotCheckModelDao.getCheckPlanById(model.gethPlanId(),
				userModel.getOrganizationId());
		if (planModel == null) {
			return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NOT_FOUND_DATA, null);
		} else {
			// 根据权限操作
			boolean hadPermission = false;
			if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_WorkcenterAdmin) {
				if (planModel.getType() == SpotCheckPlanConfigModel.TYPE_Device) {
					hadPermission = true;
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
				}
			}
			if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_WorkshopAdmin) {
				if (planModel.getType() == SpotCheckPlanConfigModel.TYPE_Device//
						|| planModel.getType() == SpotCheckPlanConfigModel.TYPE_Workshop) {
					hadPermission = true;
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
				}
			}
			if (userModel.getRoleLevel() >= RoleModel.ROLE_LEVEL_FactoryAdmin) {
				if (planModel.getType() == SpotCheckPlanConfigModel.TYPE_Device//
						|| planModel.getType() == SpotCheckPlanConfigModel.TYPE_Workshop//
						|| planModel.getType() == SpotCheckPlanConfigModel.TYPE_Factory) {
					hadPermission = true;
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
				}
			}
			// 操作
			if (hadPermission) {
				model.doSpotCheck();
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE,
						spotCheckModelDao.setCheckItem(model));
			}
			return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
		}
	}

	/**
	 * 完成设备点巡检计划
	 */
	@RequestMapping("/finishSpotCheckPlanInstance")
	@ResponseBody
	public RBuilderService.Response finishSpotCheckPlan(HttpSession httpSession, //
			@RequestParam("id") long id) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_User, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		// 获取当前登录用户
		UserModel userModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		// 获取当前计划
		SpotCheckPlanModel planModel = spotCheckModelDao.getCheckPlanById(id, userModel.getOrganizationId());
		if (planModel == null || planModel.getStatus() != SpotCheckPlanModel.STATUS_DoChecking) {
			return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_ERROR_PARAM, "请先开始点巡检");
		} else {
			// 根据权限操作
			boolean hadPermission = false;
			if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_WorkcenterAdmin) {
				if (planModel.getType() == SpotCheckPlanConfigModel.TYPE_Device) {
					hadPermission = true;
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
				}
			}
			if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_WorkshopAdmin) {
				if (planModel.getType() == SpotCheckPlanConfigModel.TYPE_Device//
						|| planModel.getType() == SpotCheckPlanConfigModel.TYPE_Workshop) {
					hadPermission = true;
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
				}
			}
			if (userModel.getRoleLevel() >= RoleModel.ROLE_LEVEL_FactoryAdmin) {
				if (planModel.getType() == SpotCheckPlanConfigModel.TYPE_Device//
						|| planModel.getType() == SpotCheckPlanConfigModel.TYPE_Workshop//
						|| planModel.getType() == SpotCheckPlanConfigModel.TYPE_Factory) {
					hadPermission = true;
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
				}
			}
			// 操作
			if (hadPermission) {
				// 获取点巡检记录
				List<SpotCheckItemModel> itemModels = spotCheckModelDao.getCheckItemsByPlanId(id);
				// 生成点巡检结果
				boolean success = true;
				String errorDisc = "异常：";
				for (SpotCheckItemModel spotCheckItemModel : itemModels) {
					if (spotCheckItemModel.getStatus() == SpotCheckPlanModel.STATUS_WaitCheck) {
						return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_ERROR_PARAM,
								"点检项未执行完成，点检项：" + spotCheckItemModel.getName());
					} else {
						if (spotCheckItemModel.getResultSuccessBool() == false) {
							errorDisc += spotCheckItemModel.getName() + ",";
						}
					}
				}
				// 更新
				planModel.setFinishTimeMs(System.currentTimeMillis());
				planModel.setResultDisc(success ?"正常":errorDisc);
				planModel.setResultSuccess(success ? 1 : 0);
				planModel.setStatus(SpotCheckPlanModel.STATUS_CheckFinished);
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE,
						spotCheckModelDao.setCheckPlan(planModel));
			}
			return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
		}
	}

	/**
	 * 根据ID获取点巡检计划实例
	 * 
	 * @param id 实例ID
	 * @return 返回给前端的操作结果
	 */
	@RequestMapping("/getCheckPlanInstanceById")
	@ResponseBody
	public RBuilderService.Response getCheckPlanInstanceById(HttpSession httpSession, //
			@RequestParam("id") long id) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_User, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		// 获取当前登录用户
		UserModel userModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		// 获取当前计划
		SpotCheckPlanModel planModel = spotCheckModelDao.getCheckPlanById(id, userModel.getOrganizationId());
		if (planModel == null) {
			return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NOT_FOUND_DATA, null);
		} else {
			// 根据权限操作
			boolean hadPermission = false;
			if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_WorkcenterAdmin) {
				if (planModel.getType() == SpotCheckPlanConfigModel.TYPE_Device) {
					hadPermission = true;
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
				}
			}
			if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_WorkshopAdmin) {
				if (planModel.getType() == SpotCheckPlanConfigModel.TYPE_Device//
						|| planModel.getType() == SpotCheckPlanConfigModel.TYPE_Workshop) {
					hadPermission = true;
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
				}
			}
			if (userModel.getRoleLevel() >= RoleModel.ROLE_LEVEL_FactoryAdmin) {
				if (planModel.getType() == SpotCheckPlanConfigModel.TYPE_Device//
						|| planModel.getType() == SpotCheckPlanConfigModel.TYPE_Workshop//
						|| planModel.getType() == SpotCheckPlanConfigModel.TYPE_Factory) {
					hadPermission = true;
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
				}
			}
			// 操作
			if (hadPermission) {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, planModel);
			}
			return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
		}
	}

	@RequestMapping(path = "/exportSpotCheckPlansToExcel")
	public void exportSpotCheckPlansToExcel(HttpSession httpSession, //
			HttpServletRequest httpServletRequest, //
			HttpServletResponse httpServletResponse, //
			@RequestParam("status") int status, //
			@RequestParam("factoryId") long factoryId, //
			@RequestParam("workshopId") long workshopId, //
			@RequestParam("workcenterId") long workcenterId, //
			@RequestParam("deviceId") long deviceId, //
			@RequestParam("executorId") long executorId, //
			@RequestParam("beginTimeMs") long beginTimeMs, //
			@RequestParam("endTimeMs") long endTimeMs) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_User, //
				rBuilderService, //
				response) == false) {
			return;
		}
		// 数据分页
		int minIndex = 0;
		int dataNum = 3600;
		String minIndexStr = httpServletRequest.getParameter("minIndex");
		String dataNumStr = httpServletRequest.getParameter("dataNum");
		String fileName = httpServletRequest.getParameter("fileName");
		if (StringUtil.isEmpty(fileName)) {
			fileName = "" + System.currentTimeMillis();
		}
		if (StringUtil.isNumber(minIndexStr)) {
			minIndex = Integer.parseInt(minIndexStr);
		}
		if (StringUtil.isNumber(dataNumStr)) {
			dataNum = Integer.parseInt(dataNumStr);
		}
		if (endTimeMs <= 0) {
			endTimeMs = System.currentTimeMillis();
		}
		if (beginTimeMs <= 0) {
			beginTimeMs = endTimeMs - DateUtil.MILLISECOND_ONE_MONTH;
		}
		// 根据用户权限返回数据
		UserModel userModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		List<SpotCheckPlanModel> list = null;
		if (userModel.getRoleLevel() <= RoleModel.ROLE_LEVEL_WorkcenterAdmin) {
			list = spotCheckModelDao.getWorkcenterCheckPlans(status, //
					userModel.getWorkcenterId(), //
					userModel.getWorkshopId(), //
					userModel.getFactoryId(), //
					userModel.getOrganizationId(), //
					minIndex, //
					dataNum, //
					beginTimeMs, //
					endTimeMs, //
					factoryId, //
					workshopId, //
					workcenterId, //
					deviceId, //
					executorId);
		}
		if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_WorkshopAdmin) {
			list = spotCheckModelDao.getWorkshopCheckPlans(status, //
					userModel.getWorkshopId(), //
					userModel.getFactoryId(), //
					userModel.getOrganizationId(), //
					minIndex, //
					dataNum, //
					beginTimeMs, //
					endTimeMs, //
					factoryId, //
					workshopId, //
					workcenterId, //
					deviceId, //
					executorId);
		}
		if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_FactoryAdmin) {
			list = spotCheckModelDao.getFactoryCheckPlans(status, //
					userModel.getFactoryId(), //
					userModel.getOrganizationId(), //
					minIndex, //
					dataNum, //
					beginTimeMs, //
					endTimeMs, //
					factoryId, //
					workshopId, //
					workcenterId, //
					deviceId, //
					executorId);
		}
		if (userModel.getRoleLevel() >= RoleModel.ROLE_LEVEL_OrganizationAdmin) {
			list = spotCheckModelDao.getOrganizationCheckPlans(status, //
					userModel.getOrganizationId(), //
					minIndex, //
					dataNum, //
					beginTimeMs, //
					endTimeMs, //
					factoryId, //
					workshopId, //
					workcenterId, //
					deviceId, //
					executorId);
		}
		if (list == null) {
			list = new ArrayList<SpotCheckPlanModel>();
		}
		// 转成Excel
		String[] title = new String[] { "序号", //
				"状态", //
				"计划名称", //
				"计划类型", //
				"执行结果", //
				"结果概述", //
				"触发时间", //
				"开始时间", //
				"结束时间", //
				"响应用时", //
				"执行用时", //
				"执行人", //
				"设备", //
				"设备组", //
				"车间", //
				"工厂" };
		String[][] values = new String[list.size()][16];
		int cindex = 0;
		for (int i = 0; i < list.size(); i++) {
			//
			String waitCheckTimeDisc = DateUtil
					.getDateTimeLengthDisc(list.get(i).getStartTimeMs() - list.get(i).getTriggerTimeMs());
			String doCheckTimeDisc = DateUtil
					.getDateTimeLengthDisc(list.get(i).getFinishTimeMs() - list.get(i).getStartTimeMs());
			String deviceName = "";
			String workcenterName = "";
			String workshopName = "";
			String factoryName = "";
			//
			cindex = 0;
			values[i][cindex++] = StringUtil.getEmptyString("" + (i + 1));
			values[i][cindex++] = StringUtil.getEmptyString(SpotCheckPlanModel.getStatusName(list.get(i).getStatus()));
			values[i][cindex++] = StringUtil.getEmptyString(list.get(i).getName());
			values[i][cindex++] = StringUtil.getEmptyString(SpotCheckPlanModel.getStatusName(list.get(i).getType()));
			values[i][cindex++] = StringUtil
					.getEmptyString(SpotCheckPlanModel.getResultSuccessName(list.get(i).getResultSuccess()));
			values[i][cindex++] = StringUtil.getEmptyString(list.get(i).getResultDisc());
			values[i][cindex++] = StringUtil.getEmptyString(DateUtil.getDateLong(list.get(i).getTriggerTimeMs()));
			values[i][cindex++] = StringUtil.getEmptyString(DateUtil.getDateLong(list.get(i).getStartTimeMs()));
			values[i][cindex++] = StringUtil.getEmptyString(DateUtil.getDateLong(list.get(i).getFinishTimeMs()));
			values[i][cindex++] = StringUtil.getEmptyString(waitCheckTimeDisc);
			values[i][cindex++] = StringUtil.getEmptyString(doCheckTimeDisc);
			values[i][cindex++] = StringUtil.getEmptyString(list.get(i).getExecutorName());
			values[i][cindex++] = StringUtil.getEmptyString(deviceName);
			values[i][cindex++] = StringUtil.getEmptyString(workcenterName);
			values[i][cindex++] = StringUtil.getEmptyString(workshopName);
			values[i][cindex++] = StringUtil.getEmptyString(factoryName);
		}
		HSSFWorkbook hssfWorkbook = PoiExcelUtil.getHSSFWorkbook(fileName, title, values, null);
		// 返回
		try {
			// 设置头
			httpServletResponse.setContentType("application/octet-stream;charset=UTF-8");
			httpServletResponse.setHeader("Content-Disposition", "attachment;filename=" + fileName + ".xls");
			httpServletResponse.addHeader("Pargam", "no-cache");
			httpServletResponse.addHeader("Cache-Control", "no-cache");
			// 返回
			OutputStream outputStream = httpServletResponse.getOutputStream();
			hssfWorkbook.write(outputStream);
			outputStream.flush();
			outputStream.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

}
