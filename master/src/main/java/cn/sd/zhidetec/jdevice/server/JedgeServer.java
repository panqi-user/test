package cn.sd.zhidetec.jdevice.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import cn.sd.zhidetec.jdevice.client.JedgeClientInfo;
import cn.sd.zhidetec.jdevice.model.AndonMessageModel;
import cn.sd.zhidetec.jdevice.model.MessageModel;
import cn.sd.zhidetec.jdevice.service.JedgeTelegramService;
/**
 * 采集系统服务器
 * */
public class JedgeServer {
	@Value("${jedges.ip}")
	protected String serverIP = "127.0.0.1";
	@Value("${jedges.port}")
	protected int listernPort = 5050;
	@Value("${jedges.createSessionTimeoutMS}")
	protected int createSessionTimeoutMS=3000;
	protected Logger logger = LoggerFactory.getLogger(getClass());
	
	private List<JedgeClientTask> clientTasks = Collections.synchronizedList(new ArrayList<>());
	private Object clientTasksLockObj = new Object();
	private List<Socket> waitCreateSessionSockets = new ArrayList<>();
	private Object waitCreateSessionSocketsLockObj = new Object();
	private boolean started=false;
	private boolean stoped=true;
	private ServerSocket serverSocket;
	private ObjectMapper jsonMapper = new ObjectMapper();
	/**
	 * 启动服务
	 * @throws IOException 
	 * */
	public void start() throws IOException {
		if (!started&&stoped) {
			started=true;
			stoped=false;
			serverSocket=new ServerSocket(listernPort);
			//Socket连接线程
			new Thread(){
				@Override
				public void run() {
					logger.info("JedgeServer Started On Port: "+listernPort);
					while (started) {
						try {
							Thread.sleep(1);
							Socket socket = serverSocket.accept();
							synchronized (waitCreateSessionSocketsLockObj) {
								waitCreateSessionSockets.add(socket);	
							}
						} catch (Exception e) {
							// TODO: handle exception
							e.printStackTrace();
						}
					}
				}
			}.start();
			//Session会话线程
			new Thread(){
				@Override
				public void run() {
					Map<Integer, Long> lastCreateSessionTimeMap=new HashMap<Integer, Long>(); 
					while (started) {
						try {
							Thread.sleep(1);
							//Jedge活性诊断
							synchronized (clientTasksLockObj) {
								for (int i = clientTasks.size()-1; i >=0; i--) {
									JedgeClientTask item = clientTasks.get(i);
									if (item.hadStarted()==false&&item.hadStoped()) {
										item.getJedgeInfoModel().setConnected(false);
									}else {
										item.getJedgeInfoModel().setConnected(true);
									}
								}
							}
							//会话
							synchronized (waitCreateSessionSocketsLockObj) {
								for (int i = waitCreateSessionSockets.size()-1; i >=0; i--) {
									Socket socket = waitCreateSessionSockets.get(i);
									try {
										//移除会话超时连接
										if (socket==null||socket.isClosed()||socket.isInputShutdown()||socket.isOutputShutdown()||socket.isConnected()==false) {
											waitCreateSessionSockets.remove(i);
											if (socket!=null) {
												if (lastCreateSessionTimeMap.containsKey(socket.getLocalPort())) {
													lastCreateSessionTimeMap.remove(socket.getLocalPort());
												}
												socket.close();
											}
											continue;
										}
										//会话开始
										Long lastCreateSessionTimeMs=lastCreateSessionTimeMap.get(socket.getLocalPort());
										if (lastCreateSessionTimeMs==null) {
											lastCreateSessionTimeMs=System.currentTimeMillis()+createSessionTimeoutMS;
											lastCreateSessionTimeMap.put(socket.getLocalPort(), lastCreateSessionTimeMs);
										}
										//会话检查
										if (socket.getInputStream().available()>0) {
											JedgeClientTask clientTask = new JedgeClientTask();
											clientTask.setSocket(socket);
											StringBuffer stringBuffer = new StringBuffer();
											if (JedgeTelegramService.recvCreateSessionTelegramString(socket, stringBuffer, 100)) {
												JedgeClientInfo clientInfo = jsonMapper.readValue(stringBuffer.toString(), JedgeClientInfo.class);
												clientTask.setJedgeInfoModel(clientInfo);
												synchronized (clientTasksLockObj) {
													boolean isNewJedgeModel=true;
													for (JedgeClientTask item : clientTasks) {
														if (item!=null//
																&&item.getJedgeInfoModel()!=null//
																&&clientTask.getJedgeInfoModel()!=null//
																&&item.getJedgeInfoModel().equals(clientTask.getJedgeInfoModel())) {
															item.setSocket(socket);
															item.setJedgeInfoModel(clientTask.getJedgeInfoModel());
															item.startThread();
															isNewJedgeModel=false;
															break;
														}
													}
													if (isNewJedgeModel) {
														clientTask.startThread();
														clientTasks.add(clientTask);	
													}
												}
												//会话创建成功
												socket.getOutputStream().write(JedgeTelegramService.getCreateSessionResponse());
											}
											waitCreateSessionSockets.remove(i);
											lastCreateSessionTimeMap.remove(socket.getLocalPort());
										}
										//超时后关闭
										else if (lastCreateSessionTimeMs<System.currentTimeMillis()) {
											socket.close();
											waitCreateSessionSockets.remove(i);
											lastCreateSessionTimeMap.remove(socket.getLocalPort());
											continue;
										}
									} catch (Exception e) {
										// TODO: handle exception
										// e.printStackTrace();
										try {
											waitCreateSessionSockets.remove(i);
											if (socket!=null) {
												if (lastCreateSessionTimeMap.containsKey(socket.getLocalPort())) {
													lastCreateSessionTimeMap.remove(socket.getLocalPort());
												}
												socket.close();
											}
										} catch (Exception e2) {
										}
									}
								}
							}
						} catch (Exception e) {
							// TODO: handle exception
							e.printStackTrace();
						}
					}
					stoped=true;
				}
			}.start();
		}
	}
	/**
	 * 停止服务
	 * */
	public void stop() {
		started=false;
	}
	/**
	 * 当前服务是否已经启动
	 * @return true 是，false 否
	 * */
	public boolean hadStarted() {
		return started;
	}
	/**
	 * 当前服务是否已经成功停止
	 * @return true 是，false 否
	 * */
	public boolean hadStoped() {
		return stoped;
	}
	/**
	 * 获取注册服务端口
	 * @return 端口号
	 * */
	public int getListernPort() {
		return listernPort;
	}
	/**
	 * 设置注册服务端口
	 * @param listernPort 端口号
	 * */
	public void setListernPort(int listernPort) {
		this.listernPort = listernPort;
	}
	/**
	 * 创建会话超时时间
	 * @return 时间，MS
	 * */
	public int getCreateSessionTimeoutMS() {
		return createSessionTimeoutMS;
	}
	/**
	 * 设置会话创建超时时间
	 * @param createSessionTimeoutMS 时间，MS
	 * */
	public void setCreateSessionTimeoutMS(int createSessionTimeoutMS) {
		this.createSessionTimeoutMS = createSessionTimeoutMS;
	}
	/**
	 * 获取所有的Jedge采集系统终端
	 * @return Jedge采集系统终端列表
	 * */
	public List<JedgeClientTask> getJedgeModels() {
		List<JedgeClientTask> list = new ArrayList<>();
		synchronized (clientTasksLockObj) {
			for (JedgeClientTask item : clientTasks) {
				list.add(item);
			}
		}
		return list;
	}
	/**
	 * 获取所有Jedge终端描述
	 * */
	public List<JedgeClientInfo> getJedgeInfoModels() {
		List<JedgeClientInfo> list = new ArrayList<>();
		synchronized (clientTasksLockObj) {
			for (JedgeClientTask item : clientTasks) {
				list.add(item.getJedgeInfoModel());
			}
		}
		return list;
	}
	/**
	 * 获取Jedge终端描述
	 * @param workshopId 车间ID
	 * @return Jedge终端描述列表
	 * */
	public List<JedgeClientInfo> getJedgeInfoModelsByWorkshopId(long workshopId) {
		List<JedgeClientInfo> list = new ArrayList<>();
		if (workshopId<=0) {
			return list;
		}
		synchronized (clientTasksLockObj) {
			for (JedgeClientTask item : clientTasks) {
				if (item.getJedgeInfoModel().getWorkshopId()==workshopId) {
					list.add(item.getJedgeInfoModel());
				}
			}
		}
		return list;
	}
	/**
	 * 获取Jedge终端描述
	 * @param factoryId 工厂ID
	 * @return Jedge终端描述列表
	 * */
	public List<JedgeClientInfo> getJedgeInfoModelsByFactoryId(long factoryId) {
		List<JedgeClientInfo> list = new ArrayList<>();
		if (factoryId<=0) {
			return list;
		}
		synchronized (clientTasksLockObj) {
			for (JedgeClientTask item : clientTasks) {
				if (item.getJedgeInfoModel().getFactoryId()==factoryId) {
					list.add(item.getJedgeInfoModel());	
				}
			}
		}
		return list;
	}
	/**
	 * 获取Jedge终端描述
	 * @param organizationId 组织ID
	 * @return Jedge终端描述列表
	 * */
	public List<JedgeClientInfo> getJedgeInfoModelsByOrganizationId(String organizationId) {
		List<JedgeClientInfo> list = new ArrayList<>();
		if (organizationId==null||organizationId.trim().equals("")) {
			return list;
		}
		synchronized (clientTasksLockObj) {
			for (JedgeClientTask item : clientTasks) {
				if (item.getJedgeInfoModel().getOrganizationId().equals(organizationId)) {
					list.add(item.getJedgeInfoModel());	
				}
			}
		}
		return list;
	}
	public boolean delJedgeInfoModel(JedgeClientInfo jedgeInfoModel) {
		if (jedgeInfoModel==null) {
			return false;
		}
		synchronized (clientTasksLockObj) {
			for (int i = 0; i < clientTasks.size(); i++) {
				JedgeClientTask taskItem = clientTasks.get(i);
				JedgeClientInfo taskJedgeClientInfo = taskItem.getJedgeInfoModel();
				if (taskJedgeClientInfo!=null//
						&&taskJedgeClientInfo.getAppId()!=null//
						&&jedgeInfoModel.getAppId()!=null//
						&&taskJedgeClientInfo.getAppId().equals(jedgeInfoModel.getAppId())) {
					try {
						taskItem.stopThread();
					} catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}
					clientTasks.remove(i);
					return true;
				}
			}
		}
		return false;
	}
	public boolean sendAndonMessage(AndonMessageModel andonMessageModel) {
		// TODO Auto-generated method stub
		synchronized (clientTasksLockObj) {
			for (int i = 0; i < clientTasks.size(); i++) {
				JedgeClientTask taskItem = clientTasks.get(i);
				JedgeClientInfo taskJedgeClientInfo = taskItem.getJedgeInfoModel();
				if (taskJedgeClientInfo!=null//
						&&taskJedgeClientInfo.getOrganizationId()!=null//
						&&taskJedgeClientInfo.getOrganizationId().equals(andonMessageModel.getOrganizationId())
						&&taskJedgeClientInfo.getWorkshopId()==andonMessageModel.getWorkshopId()) {
					taskItem.sendAndonMessage(andonMessageModel);
					return true;
				}
			}
		}
		return false;
	}
	public void sendMessageModel(MessageModel messageModel) {
		// TODO Auto-generated method stub
		
	}
}
