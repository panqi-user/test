package cn.sd.zhidetec.jdevice.config;

import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;

@Configuration
public class DataSourceConfiger {
    
    @Bean(name = BEAN_NAME_JdeviceDataSource)
    @Qualifier(BEAN_NAME_JdeviceDataSource)
    @Primary
    @ConfigurationProperties(prefix=CONFIG_PREFIX_JdeviceDataSource)
    public DataSource andonDataSource() {
    	BasicDataSource basicDataSource = new BasicDataSource();
    	return basicDataSource;
    }
    
    @Bean(name = BEAN_NAME_JdeviceJdbcTemplate)
    public JdbcTemplate andonJdbcTemplate(
            @Qualifier(BEAN_NAME_JdeviceDataSource) DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }
    
    public final static String BEAN_NAME_JdeviceJdbcTemplate="jdeviceJdbcTemplate";
    public final static String BEAN_NAME_JdeviceDataSource="jdeviceDataSource";
    public final static String CONFIG_PREFIX_JdeviceDataSource="spring.datasource.jdevice";
}
