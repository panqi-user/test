package cn.sd.zhidetec.jdevice.dao;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import cn.sd.zhidetec.jdevice.Starter;
import cn.sd.zhidetec.jdevice.config.DataSourceConfiger;
import cn.sd.zhidetec.jdevice.model.SpotCheckItemConfigModel;
import cn.sd.zhidetec.jdevice.model.SpotCheckPlanConfigModel;
import cn.sd.zhidetec.jdevice.util.StringUtil;

@Service
public class SpotCheckConfigModelDao {

	@Autowired
	@Qualifier(DataSourceConfiger.BEAN_NAME_JdeviceJdbcTemplate)
	protected JdbcTemplate jdbcTemplate;
	protected Logger logger = LoggerFactory.getLogger(getClass());
	private BeanPropertyRowMapper<SpotCheckPlanConfigModel> planBeanRowMapper = new BeanPropertyRowMapper<SpotCheckPlanConfigModel>(
			SpotCheckPlanConfigModel.class);
	private BeanPropertyRowMapper<SpotCheckItemConfigModel> itemBeanRowMapper = new BeanPropertyRowMapper<SpotCheckItemConfigModel>(
			SpotCheckItemConfigModel.class);
	
	public List<SpotCheckPlanConfigModel> getWorkcenterCheckPlanConfiges(long workcenterId,//
			long workshopId,//
			long factoryId,//
			String organizationId,//
			int beginIndex,//
			int dataNum) {
		Object[] args=new Object[11];
		int i=0;
		args[i++]=workcenterId;
		args[i++]=workshopId;
		args[i++]=factoryId;
		args[i++]=organizationId;
		args[i++]=workshopId;
		args[i++]=factoryId;
		args[i++]=organizationId;
		args[i++]=factoryId;
		args[i++]=organizationId;
		args[i++]=beginIndex;
		args[i++]=dataNum;
		return jdbcTemplate.query(SQL_GetCheckPlanConfiges_By_Workcenter, args, planBeanRowMapper);
	}
	public final static String SQL_GetCheckPlanConfiges_By_Workcenter="SELECT "+//
			" * "+//
			"FROM jdevice_c_check_plan "+//
			"WHERE "+//
			" ( "+//
			"  type = "+SpotCheckPlanConfigModel.TYPE_Device+//
			"  AND workcenter_id =? "+//
			"  AND workshop_id =? "+//
			"  AND factory_id =? "+//
			"  AND organization_id =? "+//
			" ) "+//
			"OR ( "+//
			"  type = "+SpotCheckPlanConfigModel.TYPE_Workshop+//
			"  AND workshop_id =? "+//
			"  AND factory_id =? "+//
			"  AND organization_id =? "+//
			") "+//
			"OR ( "+//
			"  type = "+SpotCheckPlanConfigModel.TYPE_Factory+//
			"  AND factory_id =? "+//
			"  AND organization_id =? "+//
			") LIMIT ?,?;";
	
	public List<SpotCheckPlanConfigModel> getWorkshopCheckPlanConfiges(long workshopId,//
			long factoryId,//
			String organizationId,//
			int beginIndex,//
			int dataNum) {
		Object[] args=new Object[10];
		int i=0;
		args[i++]=workshopId;
		args[i++]=factoryId;
		args[i++]=organizationId;
		args[i++]=workshopId;
		args[i++]=factoryId;
		args[i++]=organizationId;
		args[i++]=factoryId;
		args[i++]=organizationId;
		args[i++]=beginIndex;
		args[i++]=dataNum;
		return jdbcTemplate.query(SQL_GetCheckPlanConfiges_By_Workshop, args, planBeanRowMapper);
	}
	public final static String SQL_GetCheckPlanConfiges_By_Workshop="SELECT "+//
			" * "+//
			"FROM jdevice_c_check_plan "+//
			"WHERE "+//
			" ( "+//
			"  type = "+SpotCheckPlanConfigModel.TYPE_Device+//
			"  AND workshop_id =? "+//
			"  AND factory_id =? "+//
			"  AND organization_id =? "+//
			" ) "+//
			"OR ( "+//
			"  type = "+SpotCheckPlanConfigModel.TYPE_Workshop+//
			"  AND workshop_id =? "+//
			"  AND factory_id =? "+//
			"  AND organization_id =? "+//
			") "+//
			"OR ( "+//
			"  type = "+SpotCheckPlanConfigModel.TYPE_Factory+//
			"  AND factory_id =? "+//
			"  AND organization_id =? "+//
			") LIMIT ?,?;";
	
	public List<SpotCheckPlanConfigModel> getFactoryCheckPlanConfiges(long factoryId,//
			String organizationId,//
			int beginIndex,//
			int dataNum) {
		Object[] args=new Object[4];
		int i=0;
		args[i++]=factoryId;
		args[i++]=organizationId;
		args[i++]=beginIndex;
		args[i++]=dataNum;
		return jdbcTemplate.query(SQL_GetCheckPlanConfiges_By_Factory, args, planBeanRowMapper);
	}
	public final static String SQL_GetCheckPlanConfiges_By_Factory="SELECT "+//
			" * "+//
			"FROM jdevice_c_check_plan "+//
			"WHERE "+//
			"  factory_id =? "+//
			"  AND organization_id =? LIMIT ?,?;";
	
	public List<SpotCheckPlanConfigModel> getOrganizationCheckPlanConfiges(
			String organizationId,//
			int beginIndex,//
			int dataNum) {
		Object[] args=new Object[3];
		int i=0;
		args[i++]=organizationId;
		args[i++]=beginIndex;
		args[i++]=dataNum;
		return jdbcTemplate.query(SQL_GetCheckPlanConfiges_By_Organization, args, planBeanRowMapper);
	}
	public final static String SQL_GetCheckPlanConfiges_By_Organization="SELECT "+//
			" * "+//
			"FROM jdevice_c_check_plan "+//
			"WHERE "+//
			"  organization_id =? LIMIT ?,?;";
	
	public boolean setCheckPlanConfig(SpotCheckPlanConfigModel model) {
		if (model==null) {
			return false;
		}
		//新增
		if (model.getId()<=0) {
			Object[] args = new Object[14];
			int i=0;
			model.setId(Starter.IdMaker.nextId());
			args[i++]=model.getId();
			args[i++]=StringUtil.getEmptyString(model.getName());
			args[i++]=model.getType();
			args[i++]=model.getTriggerIntervalTimeM();
			args[i++]=model.getTriggerLastTimeMs();
			args[i++]=model.getTriggerNextTimeMs();
			args[i++]=model.getDeviceId();
			args[i++]=model.getWorkcenterId();
			args[i++]=model.getWorkshopId();
			args[i++]=model.getFactoryId();
			args[i++]=StringUtil.getEmptyString(model.getOrganizationId());
			args[i++]=model.getCreateTimeMs();
			args[i++]=model.getCreaterId();
			args[i++]=StringUtil.getEmptyString(model.getCreaterName());
			return jdbcTemplate.update(SQL_InsertCheckPlanConfig, args)>0;
		}
		//修改
		else {
			Object[] args = new Object[14];
			int i=0;
			args[i++]=StringUtil.getEmptyString(model.getName());
			args[i++]=model.getType();
			args[i++]=model.getTriggerIntervalTimeM();
			args[i++]=model.getTriggerLastTimeMs();
			args[i++]=model.getTriggerNextTimeMs();
			args[i++]=model.getDeviceId();
			args[i++]=model.getWorkcenterId();
			args[i++]=model.getWorkshopId();
			args[i++]=model.getFactoryId();
			args[i++]=StringUtil.getEmptyString(model.getOrganizationId());
			args[i++]=model.getCreateTimeMs();
			args[i++]=model.getCreaterId();
			args[i++]=StringUtil.getEmptyString(model.getCreaterName());
			args[i++]=model.getId();
			return jdbcTemplate.update(SQL_UpdateCheckPlanConfig, args)>0;
		}
	}
	public final static String SQL_InsertCheckPlanConfig="INSERT INTO jdevice_c_check_plan ( "+//
			" id, "+//
			" name, "+//
			" type, "+//
			" trigger_interval_time_m, "+//
			" trigger_last_time_ms, "+//
			" trigger_next_time_ms, "+//
			" device_id, "+//
			" workcenter_id, "+//
			" workshop_id, "+//
			" factory_id, "+//
			" organization_id, "+//
			" create_time_ms, "+//
			" creater_id, "+//
			" creater_name "+//
			") "+//
			"VALUES "+//
			" ( "+//
			" ?, "+//
			" ?, "+//
			" ?, "+//
			" ?, "+//
			" ?, "+//
			" ?, "+//
			" ?, "+//
			" ?, "+//
			" ?, "+//
			" ?, "+//
			" ?, "+//
			" ?, "+//
			" ?, "+//
			" ? "+//
			" );";
	public final static String SQL_UpdateCheckPlanConfig="UPDATE jdevice_c_check_plan SET "+//
			" name = ?, "+//
			" type = ?, "+//
			" trigger_interval_time_m = ?, "+//
			" trigger_last_time_ms = ?, "+//
			" trigger_next_time_ms = ?, "+//
			" device_id = ?, "+//
			" workcenter_id = ?, "+//
			" workshop_id = ?, "+//
			" factory_id = ?, "+//
			" organization_id = ?, "+//
			" create_time_ms = ?, "+//
			" creater_id = ?, "+//
			" creater_name = ? "+//
			"WHERE "+//
			" id=?";
	
	public SpotCheckPlanConfigModel getCheckPlanConfigById(long id,String organizationId) {
		if (id<=0||StringUtil.isEmpty(organizationId)) {
			return null;
		}
		Object[] args=new Object[2];
		int i=0;
		args[i++]=id;
		args[i++]=organizationId;
		List<SpotCheckPlanConfigModel> models = jdbcTemplate.query(SQL_GetCheckPlanConfig_By_Id, args, planBeanRowMapper);
		if (models.size()>0) {
			return models.get(0);
		}
		return null;
	}
	public final static String SQL_GetCheckPlanConfig_By_Id="SELECT * FROM jdevice_c_check_plan WHERE id=? AND organization_id=?";
	
	public boolean deleteCheckPlanConfigById(long id,String organizationId) {
		if (id<=0||StringUtil.isEmpty(organizationId)) {
			return false;
		}
		Object[] args=new Object[2];
		int i=0;
		args[i++]=id;
		args[i++]=organizationId;
		return jdbcTemplate.update(SQL_DeleteCheckPlanConfig_By_Id, args)>0;
	}
	public final static String SQL_DeleteCheckPlanConfig_By_Id="DELETE FROM jdevice_c_check_plan WHERE id=? AND organization_id=?";
	
	public List<SpotCheckItemConfigModel> getCheckItemConfigesByPlanId(long id,String organizationId) {
		if (id<=0||StringUtil.isEmpty(organizationId)) {
			return null;
		}
		Object[] args=new Object[2];
		int i=0;
		args[i++]=id;
		args[i++]=organizationId;
		return jdbcTemplate.query(SQL_GetCheckItemConfiges_By_PlanId, args, itemBeanRowMapper);
	}
	public final static String SQL_GetCheckItemConfiges_By_PlanId="SELECT * FROM jdevice_c_check_item WHERE plan_id=? AND organization_id=?";
	
	public boolean setCheckItemConfig(SpotCheckItemConfigModel model) {
		if (model==null) {
			return false;
		}
		//新增
		if (model.getId()<=0) {
			Object[] args = new Object[11];
			int i=0;
			model.setId(Starter.IdMaker.nextId());
			args[i++]=model.getId();
			args[i++]=StringUtil.getEmptyString(model.getName());
			args[i++]=model.getPlanId();
			args[i++]=model.getType();
			args[i++]=model.getValueBool();
			args[i++]=StringUtil.getEmptyString(model.getValueBoolTrueName());
			args[i++]=StringUtil.getEmptyString(model.getValueBoolFalseName());
			args[i++]=model.getValueNumber();
			args[i++]=model.getValueNumberMin();
			args[i++]=model.getValueNumberMax();
			args[i++]=StringUtil.getEmptyString(model.getOrganizationId());
			return jdbcTemplate.update(SQL_InsertCheckItemConfig, args)>0;
		}
		//修改
		else {
			Object[] args = new Object[11];
			int i=0;
			args[i++]=StringUtil.getEmptyString(model.getName());
			args[i++]=model.getPlanId();
			args[i++]=model.getType();
			args[i++]=model.getValueBool();
			args[i++]=StringUtil.getEmptyString(model.getValueBoolTrueName());
			args[i++]=StringUtil.getEmptyString(model.getValueBoolFalseName());
			args[i++]=model.getValueNumber();
			args[i++]=model.getValueNumberMin();
			args[i++]=model.getValueNumberMax();
			args[i++]=StringUtil.getEmptyString(model.getOrganizationId());
			args[i++]=model.getId();
			return jdbcTemplate.update(SQL_UpdateCheckItemConfig, args)>0;
		}
	}
	public final static String SQL_InsertCheckItemConfig="INSERT INTO jdevice_c_check_item ( "+//
			" id, "+//
			" name, "+//
			" plan_id, "+//
			" type, "+//
			" value_bool, "+//
			" value_bool_true_name, "+//
			" value_bool_false_name, "+//
			" value_number, "+//
			" value_number_min, "+//
			" value_number_max, "+//
			" organization_id "+//
			" )VALUES( "+//
			" ?, "+//
			" ?, "+//
			" ?, "+//
			" ?, "+//
			" ?, "+//
			" ?, "+//
			" ?, "+//
			" ?, "+//
			" ?, "+//
			" ?, "+//
			" ? "+//
			" );";
	public final static String SQL_UpdateCheckItemConfig="UPDATE jdevice_c_check_item SET  "+//
			" name = ?, "+//
			" plan_id = ?, "+//
			" type = ?, "+//
			" value_bool = ?, "+//
			" value_bool_true_name = ?, "+//
			" value_bool_false_name = ?, "+//
			" value_number = ?, "+//
			" value_number_min = ?, "+//
			" value_number_max = ?, "+//
			" organization_id = ? "+//
			"WHERE "+//
			" id=?;";
	
	public boolean deleteCheckItemConfigById(long id,String organizationId) {
		if (id<=0||StringUtil.isEmpty(organizationId)) {
			return false;
		}
		Object[] args=new Object[2];
		int i=0;
		args[i++]=id;
		args[i++]=organizationId;
		return jdbcTemplate.update(SQL_DeleteCheckItemConfig_By_Id, args)>0;
	}
	public final static String SQL_DeleteCheckItemConfig_By_Id="DELETE FROM jdevice_c_check_item WHERE id=? AND organization_id=?";
	
	public SpotCheckItemConfigModel getCheckItemConfigById(long id,String organizationId) {
		if (id<=0||StringUtil.isEmpty(organizationId)) {
			return null;
		}
		Object[] args=new Object[2];
		int i=0;
		args[i++]=id;
		args[i++]=organizationId;
		List<SpotCheckItemConfigModel> models = jdbcTemplate.query(SQL_GetCheckItemConfig_By_Id, args, itemBeanRowMapper);
		if (models.size()>0) {
			return models.get(0);
		}
		return null;
	}
	public final static String SQL_GetCheckItemConfig_By_Id="SELECT * FROM jdevice_c_check_item WHERE id=? AND organization_id=?";
	
	public List<SpotCheckPlanConfigModel> getAllPlanConfiges() {
		return jdbcTemplate.query(SQL_GetAllPLanConfiges, planBeanRowMapper);
	}
	public final static String SQL_GetAllPLanConfiges="SELECT * FROM jdevice_c_check_plan ORDER BY organization_id";
}
