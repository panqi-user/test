package cn.sd.zhidetec.jdevice.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.sd.zhidetec.jdevice.dao.UserModelDao;

@Service
public class UserService {

	@Autowired
	protected UserModelDao userModelDao;
	protected Logger logger = LoggerFactory.getLogger(getClass());
	
	public boolean changeWorkcenter(long userId,long workcenterId,long workshopId,long factoryId) {
		return userModelDao.changeWorkcenter(userId, workcenterId, workshopId, factoryId);
	}
	
	public boolean changeWorkshop(long workcenterId,long workshopId,long factoryId) {
		return userModelDao.changeWorkshop(workcenterId, workshopId, factoryId);
	}
	
	public boolean changeFactory(long workshopId,long factoryId) {
		return userModelDao.changeFactory(workshopId, factoryId);
	}
	
}
