package cn.sd.zhidetec.jdevice.dao;

import java.util.ArrayList;
import java.util.List;

import cn.sd.zhidetec.jdevice.model.UserModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import cn.sd.zhidetec.jdevice.Starter;
import cn.sd.zhidetec.jdevice.config.DataSourceConfiger;
import cn.sd.zhidetec.jdevice.model.AndonReasonModel;
import cn.sd.zhidetec.jdevice.model.WorkshopModel;
import cn.sd.zhidetec.jdevice.util.StringUtil;

@Service
public class AndonReasonModelDao {

	@Autowired
	@Qualifier(DataSourceConfiger.BEAN_NAME_JdeviceJdbcTemplate)
	protected JdbcTemplate jdbcTemplate;
	private BeanPropertyRowMapper<AndonReasonModel> reasonPropertyRowMapper = new BeanPropertyRowMapper<AndonReasonModel>(
			AndonReasonModel.class);
	
	public List<AndonReasonModel> getReasonModelsByWorkshopId(long workshopId) {
		if (workshopId<=0) {
			return null;
		}
		Object[] args=new Object[1];
		args[0]=workshopId;
		return jdbcTemplate.query(Sql_GetReasonModels_By_WorkshopId, args, reasonPropertyRowMapper);
	}
	private static String Sql_GetReasonModels_By_WorkshopId="SELECT * FROM jdevice_c_andon_reason WHERE workshop_id=?";
	
	public boolean initWorkshop(WorkshopModel workshopModel) {
		long factoryId=workshopModel.getFactoryId();
		long workshopId=workshopModel.getId();
		String organizationId=workshopModel.getOrganizationId();
		if (workshopId<=0||factoryId<=0||StringUtil.isEmpty(organizationId)) {
			return false;
		}
		//删除旧标签
		deleteWorkshopReasonModel(workshopId);
		//初始化标签
		List<Object[]> batchArgs=new ArrayList<Object[]>();
		batchArgs.add(new Object[] {Starter.IdMaker.nextId(),"缺料",workshopId,factoryId,organizationId});
		batchArgs.add(new Object[] {Starter.IdMaker.nextId(),"卡料",workshopId,factoryId,organizationId});
		batchArgs.add(new Object[] {Starter.IdMaker.nextId(),"电气故障",workshopId,factoryId,organizationId});
		batchArgs.add(new Object[] {Starter.IdMaker.nextId(),"机械故障",workshopId,factoryId,organizationId});
		batchArgs.add(new Object[] {Starter.IdMaker.nextId(),"软件故障",workshopId,factoryId,organizationId});
		batchArgs.add(new Object[] {Starter.IdMaker.nextId(),"电器老化",workshopId,factoryId,organizationId});
		batchArgs.add(new Object[] {Starter.IdMaker.nextId(),"机械磨损",workshopId,factoryId,organizationId});
		batchArgs.add(new Object[] {Starter.IdMaker.nextId(),"点检不足",workshopId,factoryId,organizationId});
		batchArgs.add(new Object[] {Starter.IdMaker.nextId(),"缺乏保养",workshopId,factoryId,organizationId});
		batchArgs.add(new Object[] {Starter.IdMaker.nextId(),"环境不佳",workshopId,factoryId,organizationId});
		batchArgs.add(new Object[] {Starter.IdMaker.nextId(),"违规作业",workshopId,factoryId,organizationId});
		batchArgs.add(new Object[] {Starter.IdMaker.nextId(),"参数异常",workshopId,factoryId,organizationId});
		batchArgs.add(new Object[] {Starter.IdMaker.nextId(),"原因不明",workshopId,factoryId,organizationId});
		batchArgs.add(new Object[] {Starter.IdMaker.nextId(),"其他(标注)",workshopId,factoryId,organizationId});
		batchArgs.add(new Object[] {Starter.IdMaker.nextId(),"误触发",workshopId,factoryId,organizationId});
		jdbcTemplate.batchUpdate(Sql_InsertReasonModel, batchArgs);
		return true;
	}
	
	
	public boolean deleteWorkshopReasonModel(long workshopId) {
		if (workshopId<=0) {
			return false;
		}
		Object[] args=new Object[1];
		args[0]=workshopId;
		return jdbcTemplate.update(Sql_DeleteReasonModel_By_WorkshopId, args)>0;
	}
	private static String Sql_DeleteReasonModel_By_WorkshopId="DELETE FROM jdevice_c_andon_reason WHERE workshop_id=?";
	
	public boolean deleteReasonModelById(long id) {
		if (id<=0) {
			return false;
		}
		Object[] args=new Object[1];
		args[0]=id;
		return jdbcTemplate.update(Sql_DeleteReasonModel_By_Id, args)>0;
	}
	private static String Sql_DeleteReasonModel_By_Id="DELETE FROM jdevice_c_andon_reason WHERE id=?";
	
	public boolean setReasonModel(AndonReasonModel model) {
		int i = 0;
		Object[] args = new Object[5];
		// 修改信息
		if (model.getId() > 0) {
			args[i++] = StringUtil.getEmptyString(model.getName());
			args[i++] = model.getWorkshopId();
			args[i++] = model.getFactoryId();
			args[i++] = StringUtil.getEmptyString(model.getOrganizationId());
			args[i++] = model.getId();
			i = jdbcTemplate.update(Sql_UpdateReasonModel_By_Id, args);
		}
		// 新建信息
		else {
			model.setId(Starter.IdMaker.nextId());
			args[i++] = model.getId();
			args[i++] = StringUtil.getEmptyString(model.getName());
			args[i++] = model.getWorkshopId();
			args[i++] = model.getFactoryId();
			args[i++] = StringUtil.getEmptyString(model.getOrganizationId());
			i = jdbcTemplate.update(Sql_InsertReasonModel, args);
		}
		if (i > 0) {
			return true;
		}
		return false;
	}
	private static String Sql_InsertReasonModel="INSERT INTO jdevice_c_andon_reason (id, name, workshop_id, factory_id, organization_id) VALUES (?, ?, ?, ?, ?);";
	private static String Sql_UpdateReasonModel_By_Id="UPDATE jdevice_c_andon_reason SET name=?, workshop_id=?, factory_id=?, organization_id=? WHERE id=?;";

	public List<AndonReasonModel> getReasonModelsByFactoryId(long factoryId) {
		if (factoryId<=0) {
			return null;
		}
		Object[] args=new Object[1];
		args[0]=factoryId;
		return jdbcTemplate.query(Sql_GetReasonModels_By_FactoryId, args, reasonPropertyRowMapper);
	}
	private static String Sql_GetReasonModels_By_FactoryId="SELECT * FROM jdevice_c_andon_reason WHERE factory_id=? ORDER BY workshop_id DESC";


	public List<AndonReasonModel> getReasonModelsByOrganizationId(String organizationId) {
		if (StringUtil.isEmpty(organizationId)) {
			return null;
		}
		Object[] args=new Object[1];
		args[0]=organizationId;
		return jdbcTemplate.query(Sql_GetReasonModels_By_OrganizationId, args, reasonPropertyRowMapper);
	}
	private static String Sql_GetReasonModels_By_OrganizationId="SELECT * FROM jdevice_c_andon_reason WHERE organization_id=? ORDER BY workshop_id DESC";

	public AndonReasonModel getAndonReasonModelById(long id) {
		if (id<=0) {
			return null;
		}
		Object[] args=new Object[1];
		args[0]=id;
		List<AndonReasonModel> list=jdbcTemplate.query(Sql_GetReasonModel_By_Id, args, reasonPropertyRowMapper);
		if (list!=null&&list.size()>0) {
			return list.get(0);
		}
		return null;
	}
	private static String Sql_GetReasonModel_By_Id="SELECT * FROM jdevice_c_andon_reason WHERE id=?";






    private BeanPropertyRowMapper<UserModel> beanPropertyRowMapper = new BeanPropertyRowMapper<UserModel>(
            UserModel.class);

    /**
     * 改变工厂id
     * @param workshopId
     * @param factoryId
     * @return
     */
    public boolean changeFactory(long workshopId, long factoryId) {
        Object[] args = new Object[2];
        int i=0;
        args[i++] = factoryId;
        args[i++] = workshopId;
        return jdbcTemplate.update(Sql_ChangeUserFactory_By_WorkshopId, args)>0;
    }

    private static String Sql_ChangeUserFactory_By_WorkshopId="UPDATE jdevice_c_andon_reason SET factory_id=? WHERE workshop_id=?;";
}
