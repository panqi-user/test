package cn.sd.zhidetec.jdevice.controller;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.sd.zhidetec.jdevice.HttpResponseMsg;
import cn.sd.zhidetec.jdevice.dao.FeedbackModelDao;
import cn.sd.zhidetec.jdevice.dao.OrganizationModelDao;
import cn.sd.zhidetec.jdevice.model.FeedbackModel;
import cn.sd.zhidetec.jdevice.model.OrganizationModel;
import cn.sd.zhidetec.jdevice.model.RoleModel;
import cn.sd.zhidetec.jdevice.model.UserModel;
import cn.sd.zhidetec.jdevice.service.RBuilderService;

@Controller
@RequestMapping("/help")
public class HelpController {

	@Autowired
	protected OrganizationModelDao organizationModelDao;
	@Autowired
	protected FeedbackModelDao feedbackModelDao;
	@Autowired
	protected RBuilderService rBuilderService;
	protected Logger logger = LoggerFactory.getLogger(getClass());
	
	@RequestMapping(path = "/uploadUserFeedback")
	@ResponseBody
	public RBuilderService.Response uploadUserFeedback(HttpSession httpSession,//
			@RequestParam("title")String title,//
			@RequestParam("content")String content) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_Browser, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		UserModel userModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		OrganizationModel organizationModel = organizationModelDao.getOrganizationModel(userModel.getOrganizationId());
		String organizationName="";
		if (organizationModel!=null) {
			organizationName=organizationModel.getName();
		}
		FeedbackModel feedbackModel = FeedbackModel.buildNew(userModel, title, content, System.currentTimeMillis(), organizationName);
		//入库
		if (feedbackModelDao.insertModel(feedbackModel)) {
			//发送通知
			return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, feedbackModel);	
		}
		return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, feedbackModel);
	}
	
}
