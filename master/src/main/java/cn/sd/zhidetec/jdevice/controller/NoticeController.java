package cn.sd.zhidetec.jdevice.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.sd.zhidetec.jdevice.HttpResponseMsg;
import cn.sd.zhidetec.jdevice.dao.AndonMessageModelDao;
import cn.sd.zhidetec.jdevice.dao.NoticeMessageModelDao;
import cn.sd.zhidetec.jdevice.dao.UserModelDao;
import cn.sd.zhidetec.jdevice.model.NoticeMessageModel;
import cn.sd.zhidetec.jdevice.model.RoleModel;
import cn.sd.zhidetec.jdevice.model.UserModel;
import cn.sd.zhidetec.jdevice.pojo.NoticePojo;
import cn.sd.zhidetec.jdevice.service.RBuilderService;
import cn.sd.zhidetec.jdevice.util.StringUtil;

@Controller
@RequestMapping("/notice")
public class NoticeController {

	@Autowired
	protected NoticeMessageModelDao noticeMessageModelDao;
	@Autowired
	protected UserModelDao userModelDao;
	@Autowired
	protected AndonMessageModelDao andonMessageModelDao;
	@Autowired
	protected RBuilderService rBuilderService;
	protected Logger logger = LoggerFactory.getLogger(getClass());
	
	@RequestMapping(path = "/getNoticeMessages")
	@ResponseBody
	public RBuilderService.Response getNoticeMessages(HttpSession httpSession,//
			HttpServletRequest httpServletRequest) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_User, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		// 数据分页
		int minIndex = 0;
		int dataNum = 1;
		String minIndexStr = httpServletRequest.getParameter("minIndex");
		String dataNumStr = httpServletRequest.getParameter("dataNum");
		if (StringUtil.isNumber(minIndexStr)) {
			minIndex = Integer.parseInt(minIndexStr);
		}
		if (StringUtil.isNumber(dataNumStr)) {
			dataNum = Integer.parseInt(dataNumStr);
		}
		// 数据时间
		long beginTimeMs = 0;
		long endTimeMs = System.currentTimeMillis();
		String beginTimeMsStr = httpServletRequest.getParameter("beginTimeMs");
		String endTimeMsStr = httpServletRequest.getParameter("endTimeMs");
		if (StringUtil.isNumber(beginTimeMsStr)) {
			beginTimeMs = Long.parseLong(beginTimeMsStr);
		}
		if (StringUtil.isNumber(endTimeMsStr)) {
			endTimeMs = Long.parseLong(endTimeMsStr);
		}
		return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, noticeMessageModelDao.getMessageModels(beginTimeMs, endTimeMs, minIndex, dataNum));
	}
	
	@RequestMapping(path = "/setNoticeMessage")
	@ResponseBody
	public RBuilderService.Response setNoticeMessage(HttpSession httpSession,//
			@RequestBody NoticeMessageModel noticeMessageModel) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_SystemAdmin, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		UserModel userModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		noticeMessageModel.setPublisher(userModel.getName());
		//入库
		if (noticeMessageModelDao.setNoticeMessageModel(noticeMessageModel)) {
			//发送通知
			return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, noticeMessageModel);	
		}
		return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, noticeMessageModel);
	}
	
	/**
	 * 获取当前通知
	 * 
	 * @param httpSession Session
	 * @return 组织信息
	 */
	@RequestMapping(path = "/getNoticePojo")
	@ResponseBody
	public RBuilderService.Response getNoticePojo(HttpSession httpSession) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_Browser, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		UserModel userModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		// 是否已经退出组织
		if (StringUtil.isEmpty(userModel.getOrganizationId())) {
			httpSession.setAttribute(OrganizationController.SESSION_KEY_ORGANIZATION, null);
			return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NOT_FOUND_DATA, null);
		}
		NoticePojo noticePojo = new NoticePojo();
		// 获取Andon消息数目
		if (userModel.getRoleLevel() < RoleModel.ROLE_LEVEL_WorkcenterAdmin) {
			noticePojo.setMessageNum(andonMessageModelDao.getNotClosedNumByHandlerId(userModel.getId(),
					userModel.getWorkcenterId(), userModel.getWarnLevel()));
		}
		if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_WorkcenterAdmin) {
			noticePojo.setMessageNum(andonMessageModelDao.getNotClosedNumByWorkcenterId(userModel.getWorkcenterId(),
					userModel.getWarnLevel()));
		}
		if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_WorkshopAdmin) {
			noticePojo.setMessageNum(andonMessageModelDao.getNotClosedNumByWorkshopId(userModel.getWorkshopId(),
					userModel.getWarnLevel()));
		}
		if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_FactoryAdmin) {
			noticePojo.setMessageNum(andonMessageModelDao.getNotClosedNumByFactoryId(userModel.getFactoryId(),
					userModel.getWarnLevel()));
		}
		if (userModel.getRoleLevel() >= RoleModel.ROLE_LEVEL_OrganizationAdmin) {
			noticePojo.setMessageNum(andonMessageModelDao.getNotClosedNumByOrganizationId(userModel.getOrganizationId(),
					userModel.getWarnLevel()));
		}
		// 获取待审核用户数目
		if (userModel.getRoleLevel() >= RoleModel.ROLE_LEVEL_OrganizationAdmin) {
			noticePojo.setUserReviewNum(userModelDao.getReviewUserNumByOrganizationId(userModel.getOrganizationId()));
		}
		return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, noticePojo);
	}

	@RequestMapping(path = "/getUserAndonNum")
	@ResponseBody
	public RBuilderService.Response getUserAndonNum(HttpSession httpSession) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_User, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		UserModel userModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		int notClosedNum = 0;
		int closedNum = 0;
		int unClaimedNum=0;
		// 获取未关闭的Andon消息数目
		if (userModel.getRoleLevel() < RoleModel.ROLE_LEVEL_WorkcenterAdmin) {
			notClosedNum = andonMessageModelDao.getNotClosedNumByHandlerId(userModel.getId(),
					userModel.getWorkcenterId(), userModel.getWarnLevel());
			unClaimedNum = andonMessageModelDao.getUnclaimedNumByHandlerId(userModel.getId(),
					userModel.getWorkcenterId(), userModel.getWarnLevel());
		}
		if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_WorkcenterAdmin) {
			notClosedNum = andonMessageModelDao.getNotClosedNumByWorkcenterId(userModel.getWorkcenterId(),
					userModel.getWarnLevel());
			unClaimedNum = andonMessageModelDao.getUnclaimedNumByWorkcenterId(userModel.getWorkcenterId(),
					userModel.getWarnLevel());
		}
		if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_WorkshopAdmin) {
			notClosedNum = andonMessageModelDao.getNotClosedNumByWorkshopId(userModel.getWorkshopId(),
					userModel.getWarnLevel());
			unClaimedNum = andonMessageModelDao.getUnclaimedNumByWorkshopId(userModel.getWorkshopId(),
					userModel.getWarnLevel());
		}
		if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_FactoryAdmin) {
			notClosedNum = andonMessageModelDao.getNotClosedNumByFactoryId(userModel.getFactoryId(),
					userModel.getWarnLevel());
			unClaimedNum = andonMessageModelDao.getUnclaimedNumByFactoryId(userModel.getFactoryId(),
					userModel.getWarnLevel());
		}
		if (userModel.getRoleLevel() >= RoleModel.ROLE_LEVEL_OrganizationAdmin) {
			notClosedNum = andonMessageModelDao.getNotClosedNumByOrganizationId(userModel.getOrganizationId(),
					userModel.getWarnLevel());
			unClaimedNum = andonMessageModelDao.getUnclaimedNumByOrganizationId(userModel.getOrganizationId(),
					userModel.getWarnLevel());
		}
		// 获取已关闭的Andon消息数目
		closedNum = andonMessageModelDao.getClosedNumByHandlerId(userModel.getId(),
				userModel.getWarnLevel());
		//返回
		Map<String, Integer> map=new HashMap<String, Integer>();
		map.put("sumNum", closedNum+notClosedNum);
		map.put("closedNum", closedNum);
		map.put("notClosedNum", notClosedNum);
		map.put("waitClosedNum", notClosedNum-unClaimedNum);
		map.put("unClaimedNum", unClaimedNum);
		return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, map);
	}

}
