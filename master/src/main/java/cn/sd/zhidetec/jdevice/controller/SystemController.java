package cn.sd.zhidetec.jdevice.controller;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.sd.zhidetec.jdevice.HttpResponseMsg;
import cn.sd.zhidetec.jdevice.Starter;
import cn.sd.zhidetec.jdevice.service.RBuilderService;

@Controller
@RequestMapping("/system")
public class SystemController {
	
	@Autowired
	protected RBuilderService rBuilderService;
	protected Logger logger = LoggerFactory.getLogger(getClass());

	/**
	 * 获取当前应用文件名称
	 * @return 返回给前端的操作结果
	 * */
	@RequestMapping(path="/getExeFileName")
	@ResponseBody
	public RBuilderService.Response getExeFileName(HttpSession httpSession) {
		RBuilderService.Response response = rBuilderService.build(null,null);
		return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, Starter.SystemExeFileName);
	}
	
	/**
	 * 获取当前应用文件路径
	 * @return 返回给前端的操作结果
	 * */
	@RequestMapping(path="/getExeFilePath")
	@ResponseBody
	public RBuilderService.Response getExeFilePath(HttpSession httpSession) {
		RBuilderService.Response response = rBuilderService.build(null,null);
		return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, Starter.SystemExeFilePath);
	}
	
	/**
	 * 获取当前应用版本信息
	 * @return 返回给前端的操作结果
	 * */
	@RequestMapping(path="/getVersionName")
	@ResponseBody
	public RBuilderService.Response getVersionName(HttpSession httpSession) {
		RBuilderService.Response response = rBuilderService.build(null,null);
		return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, Starter.SystemVersionName);
	}
	
}
