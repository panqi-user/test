package cn.sd.zhidetec.jdevice.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import cn.sd.zhidetec.jdevice.config.DataSourceConfiger;
import cn.sd.zhidetec.jdevice.model.AndonRecordImgModel;
import cn.sd.zhidetec.jdevice.model.AndonRecordModel;

@Service
public class AndonRecordModelDao {

	@Autowired
	@Qualifier(DataSourceConfiger.BEAN_NAME_JdeviceJdbcTemplate)
	protected JdbcTemplate jdbcTemplate;
	
	public boolean addRecordModel(AndonRecordModel model) {
		if (model==null||model.getAndonId()<=0) {
			return false;
		}
		Object[] args=new Object[6];
		int i=0;
		args[i++]=model.getId();
		args[i++]=model.getTimeMs();
		args[i++]=model.getTitle();
		args[i++]=model.getContent();
		args[i++]=model.getAndonId();
		args[i++]=model.getAndonStatus();
		i=jdbcTemplate.update(Sql_InsertRecordModel, args);
		return i>0;
	}
	private static String Sql_InsertRecordModel="INSERT INTO jdevice_h_andon_record (id,time_ms, title, content, andon_id, andon_status) VALUES (?,?, ?, ?, ?, ?);";
	
	public List<AndonRecordModel> getRecordModels(long andonId) {
		if (andonId<=0) {
			return null;
		}
		Object[] args=new Object[1];
		args[0]=andonId;
		return jdbcTemplate.query(Sql_GetRecordModels_By_AndonId, args, andonRecordBeanPropertyRowMapper);
	}
	private static String Sql_GetRecordModels_By_AndonId="SELECT * FROM jdevice_h_andon_record WHERE andon_id=? ORDER BY time_ms DESC";
	
	public boolean addRecordImgModel(AndonRecordImgModel model) {
		if (model==null||model.getAndonId()<=0||model.getRecordId()<=0) {
			return false;
		}
		Object[] args=new Object[4];
		int i=0;
		args[i++]=model.getTimeMs();
		args[i++]=model.getAndonId();
		args[i++]=model.getRecordId();
		args[i++]=model.getImgUrl();
		return jdbcTemplate.update(Sql_InsertRecordImgModel, args)>0;
	}
	private static String Sql_InsertRecordImgModel="INSERT INTO jdevice_h_andon_record_img (time_ms, andon_id, record_id, img_url) VALUES (?, ?, ?, ?);";
	
	public List<AndonRecordImgModel> getRecordImgModels(long andonId) {
		if (andonId<=0) {
			return null;
		}
		Object[] args=new Object[1];
		args[0]=andonId;
		return jdbcTemplate.query(Sql_GetRecordImgModels_By_AndonId, args, andonRecordImgBeanPropertyRowMapper);
	}
	private static String Sql_GetRecordImgModels_By_AndonId="SELECT * FROM jdevice_h_andon_record_img WHERE andon_id=?";
	
	private BeanPropertyRowMapper<AndonRecordModel> andonRecordBeanPropertyRowMapper = new BeanPropertyRowMapper<AndonRecordModel>(
			AndonRecordModel.class);
	private BeanPropertyRowMapper<AndonRecordImgModel> andonRecordImgBeanPropertyRowMapper = new BeanPropertyRowMapper<AndonRecordImgModel>(
			AndonRecordImgModel.class);
}
