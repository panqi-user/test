package cn.sd.zhidetec.jdevice.controller;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.sd.zhidetec.jdevice.HttpResponseMsg;
import cn.sd.zhidetec.jdevice.Starter;
import cn.sd.zhidetec.jdevice.dao.AndonRecordModelDao;
import cn.sd.zhidetec.jdevice.model.AndonRecordImgModel;
import cn.sd.zhidetec.jdevice.model.AndonRecordModel;
import cn.sd.zhidetec.jdevice.model.RoleModel;
import cn.sd.zhidetec.jdevice.service.RBuilderService;

@Controller
@RequestMapping("/andon")
public class AndonRecordController {

	@Autowired
	protected AndonRecordModelDao andonRecordModelDao;
	@Autowired
	protected RBuilderService rBuilderService;
	protected Logger logger = LoggerFactory.getLogger(getClass());
	
	@RequestMapping(path = "/addAndonRecordModel")
	@ResponseBody
	public RBuilderService.Response addAndonRecordModel(HttpSession httpSession, //
			@RequestBody AndonRecordModel andonRecordModel) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_WorkcenterAdmin, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		andonRecordModel.setId(Starter.IdMaker.nextId());
		andonRecordModel.setTimeMs(System.currentTimeMillis());
		if (andonRecordModelDao.addRecordModel(andonRecordModel)) {
			return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, andonRecordModel.getId());
		} else {
			return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
		}
	}

	@RequestMapping(path = "/getAndonRecordModels")
	@ResponseBody
	public RBuilderService.Response getAndonRecordModels(HttpSession httpSession, //
			@RequestParam("andonId") long andonId) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_WorkcenterAdmin, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		if (andonId <= 0) {
			return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_ERROR_PARAM, null);
		}
		return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE,
				andonRecordModelDao.getRecordModels(andonId));
	}

	@RequestMapping(path = "/addAndonRecordImgModel")
	@ResponseBody
	public RBuilderService.Response addAndonRecordImgModel(HttpSession httpSession, //
			@RequestBody AndonRecordImgModel andonRecordImgModel) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_WorkcenterAdmin, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		andonRecordImgModel.setTimeMs(System.currentTimeMillis());
		if (andonRecordModelDao.addRecordImgModel(andonRecordImgModel)) {
			return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, andonRecordImgModel);
		} else {
			return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
		}
	}

	@RequestMapping(path = "/getAndonRecordImgModels")
	@ResponseBody
	public RBuilderService.Response getAndonRecordImgModels(HttpSession httpSession, //
			@RequestParam("andonId") long andonId) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_WorkcenterAdmin, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		if (andonId <= 0) {
			return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_ERROR_PARAM, null);
		}
		return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE,
				andonRecordModelDao.getRecordImgModels(andonId));
	}
	
}
