package cn.sd.zhidetec.jdevice.controller;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.sd.zhidetec.jdevice.HttpResponseMsg;
import cn.sd.zhidetec.jdevice.dao.DeviceExtendModelDao;
import cn.sd.zhidetec.jdevice.model.DeviceExtendModel;
import cn.sd.zhidetec.jdevice.model.DeviceModel;
import cn.sd.zhidetec.jdevice.model.RoleModel;
import cn.sd.zhidetec.jdevice.model.UserModel;
import cn.sd.zhidetec.jdevice.service.DeviceService;
import cn.sd.zhidetec.jdevice.service.RBuilderService;

@Controller
@RequestMapping("/deviceExtend")
public class DeviceExtendController {
	
	@Autowired
	protected DeviceService deviceService;
	@Autowired
	protected DeviceExtendModelDao deviceExtendModelDao;
	@Autowired
	protected RBuilderService rBuilderService;
	protected Logger logger = LoggerFactory.getLogger(getClass());

	/**
	 * 
	 * */
	@RequestMapping(path = "/getExtendModelsByDeviceId")
	@ResponseBody
	public RBuilderService.Response getExtendModelsByDeviceId(HttpSession httpSession, @RequestParam("deviceId") long deviceId) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_User, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		// 获取设备数据
		UserModel userModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		DeviceModel deviceModel = deviceService.getDeviceModelById(deviceId);
		boolean hadPermission=false;
		if (deviceModel != null) {
			if (userModel.getRoleLevel() <= RoleModel.ROLE_LEVEL_WorkcenterAdmin//
					&& userModel.getWorkcenterId() == deviceModel.getWorkcenterId()) {
				hadPermission=true;
			}
			if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_WorkshopAdmin//
					&& userModel.getWorkshopId() == deviceModel.getWorkshopId()) {
				hadPermission=true;
			}
			if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_FactoryAdmin//
					&& userModel.getFactoryId() == deviceModel.getFactoryId()) {
				hadPermission=true;
			}
			if (userModel.getRoleLevel() >= RoleModel.ROLE_LEVEL_OrganizationAdmin//
					&& userModel.getOrganizationId().equals(deviceModel.getOrganizationId())) {
				hadPermission=true;
			}
		}
		// 返回
		if (hadPermission) {
			return rBuilderService.update(response, //
					HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, //
					deviceExtendModelDao.getFieldModelsByDeviceId(deviceId,userModel.getOrganizationId()));
		}else {
			return rBuilderService.update(response, //
					HttpResponseMsg.HEAD_MSG_NO_PERMISSION, //
					null);
		}
	}
	
	/**
	 * 
	 * */
	@RequestMapping(path = "/setExtendModel")
	@ResponseBody
	public RBuilderService.Response setExtendModel(HttpSession httpSession,@RequestBody DeviceExtendModel deviceFieldModel) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_WorkcenterAdmin, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		// 获取设备数据
		UserModel userModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		DeviceModel deviceModel = deviceService.getDeviceModelById(deviceFieldModel.getDeviceId());
		boolean hadPermission=false;
		if (deviceModel != null) {
			if (userModel.getRoleLevel() <= RoleModel.ROLE_LEVEL_WorkcenterAdmin//
					&& userModel.getWorkcenterId() == deviceModel.getWorkcenterId()) {
				hadPermission=true;
			}
			if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_WorkshopAdmin//
					&& userModel.getWorkshopId() == deviceModel.getWorkshopId()) {
				hadPermission=true;
			}
			if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_FactoryAdmin//
					&& userModel.getFactoryId() == deviceModel.getFactoryId()) {
				hadPermission=true;
			}
			if (userModel.getRoleLevel() >= RoleModel.ROLE_LEVEL_OrganizationAdmin//
					&& userModel.getOrganizationId().equals(deviceModel.getOrganizationId())) {
				hadPermission=true;
			}
		}
		// 返回
		if (hadPermission) {
			return rBuilderService.update(response, //
					HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, //
					deviceExtendModelDao.setFieldModel(deviceFieldModel));
		}else {
			return rBuilderService.update(response, //
					HttpResponseMsg.HEAD_MSG_NO_PERMISSION, //
					deviceFieldModel);
		}
	}
	
	/**
	 * 
	 * */
	@RequestMapping(path = "/deleteExtendModelById")
	@ResponseBody
	public RBuilderService.Response deleteExtendModelById(HttpSession httpSession,//
			@RequestParam("id") long id,//
			@RequestParam("deviceId")long deviceId) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_User, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		// 获取设备数据
		UserModel userModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		DeviceModel deviceModel = deviceService.getDeviceModelById(deviceId);
		boolean hadPermission=false;
		if (deviceModel != null) {
			if (userModel.getRoleLevel() <= RoleModel.ROLE_LEVEL_WorkcenterAdmin//
					&& userModel.getWorkcenterId() == deviceModel.getWorkcenterId()) {
				hadPermission=true;
			}
			if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_WorkshopAdmin//
					&& userModel.getWorkshopId() == deviceModel.getWorkshopId()) {
				hadPermission=true;
			}
			if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_FactoryAdmin//
					&& userModel.getFactoryId() == deviceModel.getFactoryId()) {
				hadPermission=true;
			}
			if (userModel.getRoleLevel() >= RoleModel.ROLE_LEVEL_OrganizationAdmin//
					&& userModel.getOrganizationId().equals(deviceModel.getOrganizationId())) {
				hadPermission=true;
			}
		}
		// 返回
		if (hadPermission) {
			return rBuilderService.update(response, //
					HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, //
					deviceExtendModelDao.deleteFieldModelById(id,userModel.getOrganizationId()));
		}else {
			return rBuilderService.update(response, //
					HttpResponseMsg.HEAD_MSG_NO_PERMISSION, //
					deviceId);
		}
	}
	
}
