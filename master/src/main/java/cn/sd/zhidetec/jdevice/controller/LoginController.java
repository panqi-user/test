package cn.sd.zhidetec.jdevice.controller;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.List;

import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.sd.zhidetec.jdevice.HttpResponseMsg;
import cn.sd.zhidetec.jdevice.dao.UserModelDao;
import cn.sd.zhidetec.jdevice.model.RoleModel;
import cn.sd.zhidetec.jdevice.model.UserModel;
import cn.sd.zhidetec.jdevice.model.WarnLevelModel;
import cn.sd.zhidetec.jdevice.service.RBuilderService;
import cn.sd.zhidetec.jdevice.service.TelMsgService;
import cn.sd.zhidetec.jdevice.service.VCodeService;
import cn.sd.zhidetec.jdevice.util.StringUtil;

@Controller
@RequestMapping("/login")
public class LoginController {

	@Autowired
	protected TelMsgService telMsgService;
	@Autowired
	protected UserModelDao userModelDao;
	@Autowired
	protected VCodeService vCodeService;
	@Autowired
	protected RBuilderService rBuilderService;
	protected Logger logger = LoggerFactory.getLogger(getClass());
	
	/**
	 * 获取当前用户登录信息
	 * @param httpSession Session会话
	 * @return 返回给前端的操作结果
	 * */
	@RequestMapping(path="/getLoginUserModel")
	@ResponseBody
	public RBuilderService.Response getLoginUserModel(HttpSession session){
		UserModel sessionUserModel = (UserModel) session.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		RBuilderService.Response response = rBuilderService.build(null,null);
		if (sessionUserModel==null) {
			return rBuilderService.update(response,HttpResponseMsg.HEAD_MSG_LOGIN_NEED, null);	
		}else {
			sessionUserModel.setPassword(null);
			//需要完善个人信息
			if (sessionUserModel.needPerfectInfo()) {
				return rBuilderService.update(response,HttpResponseMsg.HEAD_MSG_LOGIN_NEED_PERFECT_INFO, sessionUserModel);
			}
			//需要加入组织
			if (sessionUserModel.noOrganization()) {
				return rBuilderService.update(response,HttpResponseMsg.HEAD_MSG_NO_ORGANIZATION, sessionUserModel);
			}
			//组织审核中
			if (sessionUserModel.underReviewByOrganization()) {
				UserModel dbUserModel = userModelDao.getUserModelById(sessionUserModel.getId());
				//组织审核中
				if (dbUserModel.underReviewByOrganization()) {
					return rBuilderService.update(response,HttpResponseMsg.HEAD_MSG_UNDER_REVIEW_BY_ORGANIZATION, sessionUserModel);	
				}
				//已经审核完成
				else {
					dbUserModel.setPassword(null);
					session.setAttribute(SESSION_KEY_LOGIN_USER, dbUserModel);
					return rBuilderService.update(response,HttpResponseMsg.HEAD_MSG_LOGIN_SUCCESS, dbUserModel);
				}
			}
			return rBuilderService.update(response,HttpResponseMsg.HEAD_MSG_LOGIN_SUCCESS, sessionUserModel);
		}
	}
	
	/**
	 * 根据手机号、手机验证码、登录密码和用户名进行注册
	 * @param httpSession Session会话
	 * @return 返回给前端的操作结果
	 * */
	@RequestMapping("/register")
	@ResponseBody
	public RBuilderService.Response register(@RequestParam("phoneNum") String phoneNum,//
			@RequestParam("telVCode") String telVCode,//
			@RequestParam("userPassword") String userPassword,//
			@RequestParam("userName") String userName,//
			HttpSession session) {
		RBuilderService.Response response = rBuilderService.build(null,null);
		//手机号格式判断
		if (!TelMsgService.checkMobilePhone(phoneNum)) {
			return rBuilderService.update(response,HttpResponseMsg.HEAD_MSG_REGISTER_ERROR_UID, null);
		}
		//手机验证码验证
		if (!telMsgService.checkVCode(phoneNum, telVCode)) {
			return rBuilderService.update(response,HttpResponseMsg.HEAD_MSG_REGISTER_ERROR_VCODE, null);
		}
		//手机号是否已经注册
		if (userModelDao.getUserModelsByPhone(phoneNum).size()>0) {
			return rBuilderService.update(response,HttpResponseMsg.HEAD_MSG_REGISTER_ERROR_UID_EXIST, null);
		}
		//新建用户
		UserModel userModel=new UserModel();
		userModel.setCode(phoneNum);
		userModel.setName(userName);
		userModel.setPassword(userPassword);
		userModel.setPhone(phoneNum);
		userModel.setRoleLevel(RoleModel.ROLE_LEVEL_Browser);
		userModel.setStatus(UserModel.STATUS_VAL_Applying);
		userModel.setWarnLevel(WarnLevelModel.WarnLevel_Role);
		if (userModelDao.newUserModel(userModel)) {
			session.setAttribute(LoginController.SESSION_KEY_LOGIN_USER, userModel);
			//需要完善个人信息
			if (userModel.needPerfectInfo()) {
				return rBuilderService.update(response,HttpResponseMsg.HEAD_MSG_LOGIN_NEED_PERFECT_INFO, userModel);
			}
			//需要加入组织
			if (userModel.noOrganization()) {
				return rBuilderService.update(response,HttpResponseMsg.HEAD_MSG_NO_ORGANIZATION, userModel);
			}
			//组织审核中
			if (userModel.underReviewByOrganization()) {
				return rBuilderService.update(response,HttpResponseMsg.HEAD_MSG_UNDER_REVIEW_BY_ORGANIZATION, userModel);
			}
			return rBuilderService.update(response,HttpResponseMsg.HEAD_MSG_REGISTER_SUCCESS, null);
		}
		return rBuilderService.update(response,HttpResponseMsg.HEAD_MSG_REGISTER_FAIL, null);
	}
	/**
	 * 根据设备Token进行登录操作
	 * @param httpSession Session会话
	 * @return 返回给前端的操作结果
	 * */
	@RequestMapping(value="/loginByDeviceToken")
	@ResponseBody
	public RBuilderService.Response loginByDeviceToken(@RequestParam("deviceToken") String deviceToken,//
			HttpSession session) {
		RBuilderService.Response response = rBuilderService.build(null,null);
		//用户登录
		UserModel userModel = userModelDao.getUserModelByDeviceToken(deviceToken);
		if (userModel!=null) {
			userModel.setPassword(null);
			session.setAttribute(LoginController.SESSION_KEY_LOGIN_USER, userModel);
			UserModel sessionUserModel = (UserModel) session.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
			//需要完善个人信息
			if (sessionUserModel.needPerfectInfo()) {
				return rBuilderService.update(response,HttpResponseMsg.HEAD_MSG_LOGIN_NEED_PERFECT_INFO, sessionUserModel);
			}
			//需要加入组织
			if (sessionUserModel.noOrganization()) {
				return rBuilderService.update(response,HttpResponseMsg.HEAD_MSG_NO_ORGANIZATION, sessionUserModel);
			}
			//组织审核中
			if (sessionUserModel.underReviewByOrganization()) {
				return rBuilderService.update(response,HttpResponseMsg.HEAD_MSG_UNDER_REVIEW_BY_ORGANIZATION, sessionUserModel);
			}
			return rBuilderService.update(response,HttpResponseMsg.HEAD_MSG_LOGIN_SUCCESS, sessionUserModel);
		}else {
			return rBuilderService.update(response,HttpResponseMsg.HEAD_MSG_LOGIN_ERROR, null);
		}
	}
	/**
	 * 根据手机号和密码进行登录
	 * @param httpSession Session会话
	 * @return 返回给前端的操作结果
	 * */
	@RequestMapping(value="/login")
	@ResponseBody
	public RBuilderService.Response login(@RequestParam("userCode") String phoneNum,//
			@RequestParam("userPassword") String userPassword,//
			@RequestParam("vcode") String vcode,//
			HttpServletRequest httpServletRequest,//
			HttpSession session) {
		RBuilderService.Response response = rBuilderService.build(null,null);
		//验证码判断
		String vCode = null;
		if (session.getAttribute(LoginController.SESSION_KEY_VERIFICATION_CODE)!=null) {
			vCode = session.getAttribute(LoginController.SESSION_KEY_VERIFICATION_CODE).toString();
		}
		if (vCode==null||vCode.trim().equals("")||vcode==null||vcode.trim().equals("")||!vcode.equalsIgnoreCase(vCode)) {
			return rBuilderService.update(response,HttpResponseMsg.HEAD_MSG_LOGIN_ERROR_VCODE, null);
		}
		//用户登录
		List<UserModel> userModels = userModelDao.getUserModelsByPhone(phoneNum);
		if (userModels.size()<=0) {
			return rBuilderService.update(response,HttpResponseMsg.HEAD_MSG_LOGIN_ERROR_NOT_REGISTER, null);
		}
		UserModel userModel = userModels.get(0);
		String deviceBrand = httpServletRequest.getParameter("deviceBrand");
		String deviceToken = httpServletRequest.getParameter("deviceToken");
		if (userModel.getPassword().equals(userPassword)) {//登录成功
			userModel.setPassword(null);
			session.setAttribute(LoginController.SESSION_KEY_LOGIN_USER, userModel);
			UserModel sessionUserModel = (UserModel) session.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
			//更换登录设备
			if (!StringUtil.isEmpty(deviceBrand)&&!StringUtil.isEmpty(deviceToken)) {
				if (userModel.getDeviceBrand()!=null//
						&&userModel.getDeviceToken()!=null//
						&&userModel.getDeviceBrand().equals(deviceBrand)//
						&&userModel.getDeviceToken().equals(deviceToken)) {
					//不需要更新
				}else {
					//更换登录设备
					if (userModelDao.setAppDeviceInfoByPhone(phoneNum, deviceBrand, deviceToken)) {
						userModel.setDeviceBrand(deviceBrand);
						userModel.setDeviceToken(deviceToken);
					}
				}
			}
			//需要完善个人信息
			if (sessionUserModel.needPerfectInfo()) {
				return rBuilderService.update(response,HttpResponseMsg.HEAD_MSG_LOGIN_NEED_PERFECT_INFO, sessionUserModel);
			}
			//需要加入组织
			if (sessionUserModel.noOrganization()) {
				return rBuilderService.update(response,HttpResponseMsg.HEAD_MSG_NO_ORGANIZATION, sessionUserModel);
			}
			//组织审核中
			if (sessionUserModel.underReviewByOrganization()) {
				return rBuilderService.update(response,HttpResponseMsg.HEAD_MSG_UNDER_REVIEW_BY_ORGANIZATION, sessionUserModel);
			}
			return rBuilderService.update(response,HttpResponseMsg.HEAD_MSG_LOGIN_SUCCESS, sessionUserModel);
		} else {//登录失败
			session.removeAttribute(LoginController.SESSION_KEY_LOGIN_USER);
			return rBuilderService.update(response,HttpResponseMsg.HEAD_MSG_LOGIN_ERROR, null);
		}
	}
	
	/**
	 * 根据手机号和手机验证码进行登录
	 * @param httpSession Session会话
	 * @return 返回给前端的操作结果
	 * */
	@RequestMapping("/loginByTelVCode")
	@ResponseBody
	public RBuilderService.Response loginByTelVCode(@RequestParam("userCode") String phoneNum,//
			@RequestParam("telVCode") String telVCode,//
			HttpServletRequest httpServletRequest,//
			HttpSession session) {
		RBuilderService.Response response = rBuilderService.build(null,null);
		//验证码判断
		if (!telMsgService.checkVCode(phoneNum, telVCode)) {
			return rBuilderService.update(response,HttpResponseMsg.HEAD_MSG_LOGIN_ERROR_VCODE, null);
		}
		//用户登录
		List<UserModel> userModels = userModelDao.getUserModelsByPhone(phoneNum);
		if (userModels.size()<=0) {
			return rBuilderService.update(response,HttpResponseMsg.HEAD_MSG_LOGIN_ERROR_NOT_REGISTER, null);
		}
		UserModel userModel = userModels.get(0);
		String deviceBrand = httpServletRequest.getParameter("deviceBrand");
		String deviceToken = httpServletRequest.getParameter("deviceToken");
		if (userModel != null) {//登录成功
			userModel.setPassword(null);
			session.setAttribute(LoginController.SESSION_KEY_LOGIN_USER, userModel);
			UserModel sessionUserModel = (UserModel) session.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
			//更换登录设备
			if (!StringUtil.isEmpty(deviceBrand)&&!StringUtil.isEmpty(deviceToken)) {
				if (userModel.getDeviceBrand()!=null//
						&&userModel.getDeviceToken()!=null//
						&&userModel.getDeviceBrand().equals(deviceBrand)//
						&&userModel.getDeviceToken().equals(deviceToken)) {
					//不需要更新
				}else {
					//更换登录设备
					if (userModelDao.setAppDeviceInfoByPhone(phoneNum, deviceBrand, deviceToken)) {
						userModel.setDeviceBrand(deviceBrand);
						userModel.setDeviceToken(deviceToken);
					}
				}
			}
			//需要完善个人信息
			if (sessionUserModel.needPerfectInfo()) {
				return rBuilderService.update(response,HttpResponseMsg.HEAD_MSG_LOGIN_NEED_PERFECT_INFO, sessionUserModel);
			}
			//需要加入组织
			if (sessionUserModel.noOrganization()) {
				return rBuilderService.update(response,HttpResponseMsg.HEAD_MSG_NO_ORGANIZATION, sessionUserModel);
			}
			//组织审核中
			if (sessionUserModel.underReviewByOrganization()) {
				return rBuilderService.update(response,HttpResponseMsg.HEAD_MSG_UNDER_REVIEW_BY_ORGANIZATION, sessionUserModel);
			}
			return rBuilderService.update(response,HttpResponseMsg.HEAD_MSG_LOGIN_SUCCESS, sessionUserModel);
		} else {//登录失败
			session.removeAttribute(LoginController.SESSION_KEY_LOGIN_USER);
			return rBuilderService.update(response,HttpResponseMsg.HEAD_MSG_LOGIN_ERROR, null);
		}
	}
	
	/**
	 * 根据手机号和手机验证码进行登录&注册
	 * @param httpSession Session会话
	 * @return 返回给前端的操作结果
	 * */
	@RequestMapping("/loginAndRegisteByTelVCode")
	@ResponseBody
	public RBuilderService.Response loginAndRegisteByTelVCode(@RequestParam("userCode") String phoneNum,//
			@RequestParam("telVCode") String telVCode,//
			HttpServletRequest httpServletRequest,//
			HttpSession session) {
		RBuilderService.Response response = rBuilderService.build(null,null);
		//验证码判断
		if (!telMsgService.checkVCode(phoneNum, telVCode)) {
			return rBuilderService.update(response,HttpResponseMsg.HEAD_MSG_LOGIN_ERROR_VCODE, null);
		}
		//用户登录
		UserModel userModel = null;
		String deviceBrand = httpServletRequest.getParameter("deviceBrand");
		String deviceToken = httpServletRequest.getParameter("deviceToken");
		List<UserModel> userModels = userModelDao.getUserModelsByPhone(phoneNum);
		if (userModels.size()<=0) {
			//用户注册
			this.register(phoneNum, telVCode, StringUtil.get32BitUUID(), "", session);
			userModel = (UserModel) session.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		}else {
			userModel = userModels.get(0);
			userModel.setPassword(null);
			session.setAttribute(LoginController.SESSION_KEY_LOGIN_USER, userModel);
		}
		if (userModel != null) {//登录成功
			userModel.setPassword(null);
			//更换登录设备
			if (!StringUtil.isEmpty(deviceBrand)&&!StringUtil.isEmpty(deviceToken)) {
				if (userModel.getDeviceBrand()!=null//
						&&userModel.getDeviceToken()!=null//
						&&userModel.getDeviceBrand().equals(deviceBrand)//
						&&userModel.getDeviceToken().equals(deviceToken)) {
					//不需要更新
				}else {
					//更换登录设备
					if (userModelDao.setAppDeviceInfoByPhone(phoneNum, deviceBrand, deviceToken)) {
						userModel.setDeviceBrand(deviceBrand);
						userModel.setDeviceToken(deviceToken);
					}
				}
			}
			//需要完善个人信息
			if (userModel.needPerfectInfo()) {
                return rBuilderService.update(response,HttpResponseMsg.HEAD_MSG_LOGIN_NEED_PERFECT_INFO, userModel);
            }
            //需要加入组织
            if (userModel.noOrganization()) {
                return rBuilderService.update(response,HttpResponseMsg.HEAD_MSG_NO_ORGANIZATION, userModel);
            }
            //组织审核中
            if (userModel.underReviewByOrganization()) {
                return rBuilderService.update(response,HttpResponseMsg.HEAD_MSG_UNDER_REVIEW_BY_ORGANIZATION, userModel);
            }
			return rBuilderService.update(response,HttpResponseMsg.HEAD_MSG_LOGIN_SUCCESS, userModel);
		} else {//登录失败
			session.removeAttribute(LoginController.SESSION_KEY_LOGIN_USER);
			return rBuilderService.update(response,HttpResponseMsg.HEAD_MSG_LOGIN_ERROR, null);
		}
	}
	
	/**
	 * 向指定的手机号发送验证码
	 * @param httpSession Session会话
	 * @return 返回给前端的操作结果
	 * */
	@RequestMapping("/sendTelVCode")
	@ResponseBody
	public RBuilderService.Response sendTelVCode(@RequestParam("phoneNum") String phoneNum) {
		RBuilderService.Response response = rBuilderService.build(null,null);
		if (TelMsgService.checkMobilePhone(phoneNum)&&telMsgService.sendVCodeMsg(phoneNum) > 0 ) {
			return rBuilderService.update(response,HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, null);	
		}else {
			return rBuilderService.update(response,HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
		}
	}

	/**
	 * @param httpSession Session会话
	 * @return 返回给前端的操作结果
	 * */
	@RequestMapping("/logout")
	@ResponseBody
	public  RBuilderService.Response logout(HttpSession session) {
		RBuilderService.Response response = rBuilderService.build(null,null);
		session.removeAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		session.removeAttribute(LoginController.SESSION_KEY_VERIFICATION_CODE);
		session.removeAttribute(OrganizationController.SESSION_KEY_ORGANIZATION);
		return rBuilderService.update(response,HttpResponseMsg.HEAD_MSG_LOGOUT_SUCCESS, null);
	}
	
	/**
	 * 更换手机号
	 * @param httpSession Session会话
	 * @return 返回给前端的操作结果
	 * */
	@RequestMapping("/resetPhone")
	@ResponseBody
	public RBuilderService.Response resetPhone(@RequestParam("phoneNum") String phoneNum, //
			@RequestParam("telVCode") String telVCode, //
			@RequestParam("phoneNumNew") String phoneNumNew, //
			@RequestParam("telVCodeNew") String telVCodeNew, //
			HttpSession session) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 验证码判断
		if (!telMsgService.checkVCode(phoneNum, telVCode)) {
			return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_LOGIN_ERROR_VCODE, null);
		}
		// 验证码判断
		if (!telMsgService.checkVCode(phoneNumNew, telVCodeNew)) {
			return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_LOGIN_ERROR_VCODE, null);
		}
		// 用户信息
		List<UserModel> userModels = userModelDao.getUserModelsByPhone(phoneNum);
		if (userModels.size() <= 0) {
			return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_LOGIN_ERROR_NOT_REGISTER, null);
		}
		List<UserModel> newUserModels = userModelDao.getUserModelsByPhone(phoneNumNew);
		if (newUserModels.size() > 0) {
			return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_REGISTER_ERROR_UID_EXIST, null);
		}
		// 修改
		if (userModelDao.setUserPhoneByPhone(phoneNum, phoneNumNew)) {
			return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_LOGIN_RESET_PHONE_SUCCESS, null);
		} else {
			return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_LOGIN_RESET_PHONE_FAIL, null);
		}
	}
	
	/**
	 * @param httpSession Session会话
	 * @return 返回给前端的操作结果
	 * */
	@RequestMapping("/resetPassword")
	@ResponseBody
	public RBuilderService.Response resetPassword(@RequestParam("phoneNum") String phoneNum, //
			@RequestParam("telVCode") String telVCode, //
			@RequestParam("userPassword") String userPassword, //
			HttpSession session) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 验证码判断
		if (!telMsgService.checkVCode(phoneNum, telVCode)) {
			return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_LOGIN_ERROR_VCODE, null);
		}
		// 用户信息
		List<UserModel> userModels = userModelDao.getUserModelsByPhone(phoneNum);
		if (userModels.size() <= 0) {
			return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_LOGIN_ERROR_NOT_REGISTER, null);
		}
		// 修改密码
		if (userModelDao.setUserPasswordByPhone(phoneNum, userPassword)) {
			return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_LOGIN_RESET_PWD_SUCCESS, null);
		} else {
			return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_LOGIN_RESET_PWD_FAIL, null);
		}
	}

	/**
	 * @param httpSession Session会话
	 * @return 返回给前端的操作结果
	 * */
	@RequestMapping("/getVCode")
	public void getVCode(HttpSession session,//
			HttpServletResponse response) {
		if (vCodeService != null) {
			response.setDateHeader("Expires", 0);
			response.addHeader("Accept-Ranges", "bytes");
			response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
			response.addHeader("Cache-Control", "post-check=0, pre-check=0");
			response.setHeader("Pragma", "no-cache");
			response.setContentType("image/jepg");
			String vcode = vCodeService.getCodeStr(4);
			session.setAttribute(LoginController.SESSION_KEY_VERIFICATION_CODE, vcode);
			BufferedImage img = vCodeService.getVerificationCode(15, vcode);
			ServletOutputStream outputStream;
			try {
				outputStream = response.getOutputStream();
				ImageIO.write(img, "jpeg", outputStream);
				outputStream.flush();
				outputStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * 判断当前登录用户是否用于足够的权限
	 * @param session 当前Session会话
	 * @param minRoleLevel 最小用户角色
	 * @param rBuilderService 构建结果服务
	 * @param response 返回给前端的结果
	 * @return 是否有足够的权限
	 * */
	public static boolean hadPermission(HttpSession session,//
			int minRoleLevel,//
			RBuilderService rBuilderService,//
			RBuilderService.Response response) {
		if (minRoleLevel<=RoleModel.ROLE_LEVEL_NotLoginedIn) {
			return true;
		}
		UserModel sessionUserModel = (UserModel) session.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		if (sessionUserModel==null) {
			if (response!=null) {
				rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_LOGIN_NEED, null);
			}
			return false;
		}else {
			if (minRoleLevel<=RoleModel.ROLE_LEVEL_Browser) {
				return true;
			}
			if (sessionUserModel.getRoleLevel()>=minRoleLevel//
					&&sessionUserModel.getStatus()<=UserModel.STATUS_VAL_Enable) {
				return true;
			}else {
				rBuilderService.update(response,HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
				return false;
			}
		}
	}
	
	public static final String SESSION_KEY_VERIFICATION_CODE = "SESSION_VERIFICATION_CODE";
	public static final String SESSION_KEY_LOGIN_USER = "SESSION_LOGIN_USER";
}
