package cn.sd.zhidetec.jdevice.dao;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import cn.sd.zhidetec.jdevice.Starter;
import cn.sd.zhidetec.jdevice.config.DataSourceConfiger;
import cn.sd.zhidetec.jdevice.model.DeviceOeeModel;
import cn.sd.zhidetec.jdevice.util.StringUtil;

@Service
public class DeviceOeeModelDao {

	@Autowired
	@Qualifier(DataSourceConfiger.BEAN_NAME_JdeviceJdbcTemplate)
	protected JdbcTemplate jdbcTemplate;
	protected Logger logger = LoggerFactory.getLogger(getClass());
	private BeanPropertyRowMapper<DeviceOeeModel> beanPropertyRowMapper = new BeanPropertyRowMapper<DeviceOeeModel>(
			DeviceOeeModel.class);
	
	public List<DeviceOeeModel> getWorkshopAShiftDeviceOeeModels(long workshopId, long shiftTimeMs) {
		Object[] args = new Object[3];
		int i=0;
		args[i++] = shiftTimeMs;
		args[i++] = shiftTimeMs;
		args[i++] = workshopId;
		return jdbcTemplate.query(Sql_GetWorkshopAShiftDeviceOeeModels_By_WorkshopId, args, beanPropertyRowMapper);
	}
	private static String Sql_GetWorkshopAShiftDeviceOeeModels_By_WorkshopId="SELECT "+//
			" * "+//
			" FROM "+//
			"jdevice_h_device_oee "+//
			" WHERE "+//
			"begin_time_ms <= ? "+//
			" AND end_time_ms >= ? "+//
			" AND workshop_id = ? ";
	
	public List<DeviceOeeModel> getWorkcenterAShiftDeviceOeeModels(long workcenterId, long shiftTimeMs) {
		Object[] args = new Object[3];
		int i=0;
		args[i++] = shiftTimeMs;
		args[i++] = shiftTimeMs;
		args[i++] = workcenterId;
		return jdbcTemplate.query(Sql_GetWorkcenterAShiftDeviceOeeModels_By_WorkcenterId, args, beanPropertyRowMapper);
	}
	private static String Sql_GetWorkcenterAShiftDeviceOeeModels_By_WorkcenterId="SELECT "+//
			" * "+//
			" FROM "+//
			"jdevice_h_device_oee "+//
			" WHERE "+//
			"begin_time_ms <= ? "+//
			" AND end_time_ms >= ? "+//
			" AND workcenter_id = ? ";
	
	public List<DeviceOeeModel> getDeviceOeeModels(long deviceId,long shiftBeginTimeMs) {
		Object[] args = new Object[2];
		int i=0;
		args[i++] = shiftBeginTimeMs;
		args[i++] = deviceId;
		return jdbcTemplate.query(Sql_GetDeviceOeeModels_By_DeviceId, args, beanPropertyRowMapper);
	}
	private static String Sql_GetDeviceOeeModels_By_DeviceId="SELECT "+//
			" * "+//
			" FROM "+//
			"jdevice_h_device_oee "+//
			" WHERE "+//
			"begin_time_ms <= ? "+//
			" AND device_id = ? ";

	public DeviceOeeModel getLastDeviceOeeModelByDeviceId(long deviceId) {
		Object[] args = new Object[1];
		int i=0;
		args[i++] = deviceId;
		List<DeviceOeeModel> list=jdbcTemplate.query(Sql_GetLastDeviceOeeModel_By_DeviceId, args, beanPropertyRowMapper);
		if (list!=null&&list.size()>0) {
			return list.get(0);
		}
		return null;
	}
	private static String Sql_GetLastDeviceOeeModel_By_DeviceId="SELECT "+//
			" * "+//
			" FROM "+//
			"jdevice_h_device_oee "+//
			" WHERE "+//
			" device_id = ?"+//
			" ORDER BY end_time_ms DESC"+//
			" LIMIT 0,1";

	public DeviceOeeModel setDeviceOeeModel(DeviceOeeModel model) {
		int i = 0;
		Object[] args = new Object[15];
		// 修改信息
		if (model.getId() > 0) {
			args[i++] = model.getStatusValue();
			args[i++] = model.getStatusTimeMs();
			args[i++] = model.getRunTimeMs();
			args[i++] = model.getBeginTimeMs();
			args[i++] = model.getEndTimeMs();
			args[i++] = StringUtil.getEmptyString(model.getShiftName());
			args[i++] = model.getShiftBeginTimeMin();
			args[i++] = model.getShiftEndTimeMin();
			args[i++] = model.getOee();
			args[i++] = model.getDeviceId();
			args[i++] = model.getWorkcenterId();
			args[i++] = model.getWorkshopId();
			args[i++] = model.getFactoryId();
			args[i++] = StringUtil.getEmptyString(model.getOrganizationId());
			args[i++] = model.getId();
			i = jdbcTemplate.update(Sql_UpdateModel_By_Id, args);
		}
		// 新建信息
		else {
			model.setId(Starter.IdMaker.nextId());
			args[i++] = model.getId();
			args[i++] = model.getStatusValue();
			args[i++] = model.getStatusTimeMs();
			args[i++] = model.getRunTimeMs();
			args[i++] = model.getBeginTimeMs();
			args[i++] = model.getEndTimeMs();
			args[i++] = StringUtil.getEmptyString(model.getShiftName());
			args[i++] = model.getShiftBeginTimeMin();
			args[i++] = model.getShiftEndTimeMin();
			args[i++] = model.getOee();
			args[i++] = model.getDeviceId();
			args[i++] = model.getWorkcenterId();
			args[i++] = model.getWorkshopId();
			args[i++] = model.getFactoryId();
			args[i++] = StringUtil.getEmptyString(model.getOrganizationId());
			i = jdbcTemplate.update(Sql_InsertModel, args);
		}
		if (i > 0) {
			return model;
		}
		return null;
	}
	private static String Sql_UpdateModel_By_Id = "UPDATE jdevice_h_device_oee" + //
			" SET " + //
			"status_value = ?," + //
			"status_time_ms = ?," + //
			"run_time_ms = ?," + //
			"begin_time_ms = ?," + //
			"end_time_ms = ?," + //
			"shift_name = ?," + //
			"shift_begin_time_min = ?," + //
			"shift_end_time_min = ?," + //
			"oee = ?," + //
			"device_id = ?," + //
			"workcenter_id = ?," + //
			"workshop_id = ?," + //
			"factory_id = ?," + //
			"organization_id = ?" + //
			" WHERE " + //
			"id=?";
	private static String Sql_InsertModel = "INSERT INTO jdevice_h_device_oee (" + //
			"id," + //
			"status_value," + //
			"status_time_ms," + //
			"run_time_ms," + //
			"begin_time_ms," + //
			"end_time_ms," + //
			"shift_name," + //
			"shift_begin_time_min," + //
			"shift_end_time_min," + //
			"oee," + //
			"device_id," + //
			"workcenter_id," + //
			"workshop_id," + //
			"factory_id," + //
			"organization_id" + //
			")VALUES(" + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?" + //
			")";

	public DeviceOeeModel getDeviceOeeModelByDeviceIdAndShiftTimeMs(long deviceId, long shiftTimeMs) {
		Object[] args = new Object[3];
		int i=0;
		args[i++] = deviceId;
		args[i++] = shiftTimeMs;
		args[i++] = shiftTimeMs;
		List<DeviceOeeModel> list=jdbcTemplate.query(Sql_GetLastDeviceOeeModel_By_DeviceId_And_ShiftTimeMs, args, beanPropertyRowMapper);
		if (list!=null&&list.size()>0) {
			return list.get(0);
		}
		return null;
	}
	private static String Sql_GetLastDeviceOeeModel_By_DeviceId_And_ShiftTimeMs="SELECT "+//
			" * "+//
			" FROM "+//
			"jdevice_h_device_oee "+//
			" WHERE "+//
			" device_id = ?"+//
			" AND "+//
			" begin_time_ms <= ?"+//
			" AND "+//
			" end_time_ms > ?"+//
			" ORDER BY end_time_ms DESC"+//
			" LIMIT 0,1";

	public List<DeviceOeeModel> getLastDeviceOeeModel(long deviceId) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public boolean changeWorkcenter(long deviceId, long workcenterId, long workshopId, long factoryId) {
		Object[] args = new Object[4];
		int i=0;
		args[i++] = workcenterId;
		args[i++] = workshopId;
		args[i++] = factoryId;
		args[i++] = deviceId;
		return jdbcTemplate.update(Sql_ChangeWorkcenter_By_DeviceId, args)>0;
	}
	
	private static String Sql_ChangeWorkcenter_By_DeviceId = "UPDATE jdevice_h_device_oee SET workcenter_id=?,workshop_id=?,factory_id=? WHERE device_id=?;";
	
	public boolean changeWorkshop(long workcenterId,long workshopId, long factoryId) {
		Object[] args = new Object[3];
		int i=0;
		args[i++] = workshopId;
		args[i++] = factoryId;
		args[i++] = workcenterId;
		return jdbcTemplate.update(Sql_ChangeWorkshop_By_WorkcenterId, args)>0;
	}
	
	private static String Sql_ChangeWorkshop_By_WorkcenterId="UPDATE jdevice_h_device_oee SET workshop_id=?,factory_id=? WHERE workcenter_id=?;";
	
	public boolean changeFactory(long workshopId,long factoryId) {
		Object[] args = new Object[2];
		int i=0;
		args[i++] = factoryId;
		args[i++] = workshopId;
		return jdbcTemplate.update(Sql_ChangeFactory_By_WorkshopId, args)>0;
	}
	
	private static String Sql_ChangeFactory_By_WorkshopId="UPDATE jdevice_h_device_oee SET factory_id=? WHERE workshop_id=?;";
}
