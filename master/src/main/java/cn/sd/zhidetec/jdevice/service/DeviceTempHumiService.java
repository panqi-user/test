package cn.sd.zhidetec.jdevice.service;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.sd.zhidetec.jdevice.dao.DeviceTempHumiModelDao;
import cn.sd.zhidetec.jdevice.model.DeviceTempHumiModel;

@Service
public class DeviceTempHumiService {

	@Autowired
	protected DeviceTempHumiModelDao deviceTempHumiModelDao;
	protected Logger logger = LoggerFactory.getLogger(getClass());
	
	private Queue<List<DeviceTempHumiModel>> uploadTempHumiQueue = new LinkedList<List<DeviceTempHumiModel>>();
	private Object uploadTempHumiQueueObjLock = new Object();
	private boolean started=false;
	
	public void uploadHistoryModels(List<DeviceTempHumiModel> list) {
		synchronized (uploadTempHumiQueueObjLock) {
			uploadTempHumiQueue.offer(list);
			if (started==false) {
				started=true;
				// 数据提交线程启动
				thread.start();
			}
		}
	}
	private Thread thread = new Thread() {
		@Override
		public void run() {
			logger.info("DeviceTempHumiService UploadTempHumiTask Started!!");
			while (started) {
				try {
					Thread.sleep(10);
					//更新设备温湿度记录
					List<DeviceTempHumiModel> deviceTempHumiModels=null;
					synchronized (uploadTempHumiQueueObjLock) {
						deviceTempHumiModels=uploadTempHumiQueue.peek();
					}
					while (deviceTempHumiModels!=null) {
						// 保存到数据库
						deviceTempHumiModelDao.addModels(deviceTempHumiModels);
						synchronized (uploadTempHumiQueueObjLock) {
							uploadTempHumiQueue.poll();
							deviceTempHumiModels=uploadTempHumiQueue.peek();
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	};
	
	public boolean updateLastModel(DeviceTempHumiModel model) {
		return deviceTempHumiModelDao.setDeviceLastModel(model);
	}
	
	public DeviceTempHumiModel getDeviceLastModel(long deviceId) {
		return deviceTempHumiModelDao.getDeviceLastModel(deviceId);
	}
	
	public List<DeviceTempHumiModel> getDeviceModels(long deviceId,long beginTimeMs) {
		return deviceTempHumiModelDao.getDeviceModels(deviceId, beginTimeMs);
	}
	
	public List<DeviceTempHumiModel> getWorkshopLastModels(long workshopId) {
		return deviceTempHumiModelDao.getWorkshopLastModels(workshopId);
	}
	
	public List<DeviceTempHumiModel> getWorkcenterLastModels(long workcenterId) {
		return deviceTempHumiModelDao.getWorkcenterLastModels(workcenterId);
	}
	
	public boolean changeWorkcenter(long deviceId,long workcenterId,long workshopId,long factoryId) {
		return deviceTempHumiModelDao.changeWorkcenter(deviceId,workcenterId,workshopId,factoryId);
	}
	
	public boolean changeWorkshop(long workcenterId,long workshopId,long factoryId) {
		return deviceTempHumiModelDao.changeWorkshop(workcenterId, workshopId, factoryId);
	}
	
	public boolean changeFactory(long workshopId,long factoryId) {
		return deviceTempHumiModelDao.changeFactory(workshopId, factoryId);
	}
}
