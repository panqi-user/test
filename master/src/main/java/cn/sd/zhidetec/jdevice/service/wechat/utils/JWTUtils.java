package cn.sd.zhidetec.jdevice.service.wechat.utils;


import cn.sd.zhidetec.jdevice.model.UserModel;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.util.Date;

/**
 * Jwt工具类
 * 注意点：
 * 1、生成的token，是可以通过base64进行解密出明文信息
 * 2、base64进行解密出明文信息，修改再进行编码，则会解密失败
 * 3、无法作废已经颁布的token，除非该改秘钥
 */
public class JWTUtils {


    /**
     * 过期时间 一周
     */
    //static final long EXIRE = 60000 * 60 * 24 * 7;//token的过期时间，毫秒为单位
    //static final long EXIRE = 1

    /**
     * 加密秘钥
     */
    private static final String SECRET = "zhide.net168";//secret是保存在服务器端的,服务端的私钥，在任何场景都不应该流露出去

    /**
     * 令牌前缀
     */
    private static final String TOKEN_PREFIX = "zhide";

    /**
     * subject
     */
    private static final String SUBJECT = "zhide";

    /**
     * 根据用户信息，生成令牌，把用户的信息都封装到了token
     *
     * @param user
     * @return
     */
    public static String geneJsonWebToken(UserModel user) {

        String token = Jwts.builder().setSubject(SUBJECT)//设置主体
                .claim("openId", user.getWxOpenId())//设置信息
                .setIssuedAt(new Date())//下发时间
                //.setExpiration(new Date(System.currentTimeMillis() + EXIRE))//过期时间
                .signWith(SignatureAlgorithm.HS256, SECRET).compact();//签名

        token = TOKEN_PREFIX + token;
        return token;

    }


    /**
     * 校验的方法
     *
     * @param token
     * @return
     */
    public static Claims checkJWT(String token) {

        try {
            final Claims claims = Jwts.parser().setSigningKey(SECRET).parseClaimsJws(token.replace(TOKEN_PREFIX, "")).getBody();
            return claims;
        } catch (Exception e) {
            return null;
        }
    }
    public static void main(String[] args) {
        Claims claims = checkJWT("zhideeyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ6aGlkZSIsIm9wZW5JZCI6Im9xRFFxNXRzRTJ0OG5IRmVqaUh0Zkk4eE5PVjgiLCJzZXNzaW9uS2V5IjoiYnNzYkYvV3ZiZlQ1RmNvajRKZG5oQT09IiwiaWF0IjoxNjA0NDgyODAwfQ.JgfmML29fp3zNc2ZDq5vBGxRAGe_f7dr0dOSWgcYBrU");
        System.out.println(claims.get("openId"));
        System.out.println(claims.get("sessionKey"));
    }

}
