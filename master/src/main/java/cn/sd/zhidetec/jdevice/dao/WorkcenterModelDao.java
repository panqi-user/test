package cn.sd.zhidetec.jdevice.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import cn.sd.zhidetec.jdevice.Starter;
import cn.sd.zhidetec.jdevice.config.DataSourceConfiger;
import cn.sd.zhidetec.jdevice.model.WorkcenterModel;
import cn.sd.zhidetec.jdevice.model.WorkshopModel;
import cn.sd.zhidetec.jdevice.util.StringUtil;

@Service
public class WorkcenterModelDao {

	@Autowired
	@Qualifier(DataSourceConfiger.BEAN_NAME_JdeviceJdbcTemplate)
	protected JdbcTemplate jdbcTemplate;

	public int getWorkcenterNum(String organizationId) {
		Object[] args = new Object[1];
		args[0] = organizationId;
		return jdbcTemplate.queryForObject(Sql_GetNum_By_OrganizationId, args, Integer.class);
	}

	private static String Sql_GetNum_By_OrganizationId = "SELECT COUNT(*) AS Num FROM jdevice_c_workcenter WHERE organization_id=?";

	/**
	 * 删除工作中心
	 * 
	 * @param workcenterModel 中心信息
	 * @return 操作是否成功
	 */
	public boolean deleteWorkcenterModel(WorkcenterModel workcenterModel) {
		int i = 0;
		Object[] args = new Object[1];
		args[i++] = workcenterModel.getId();
		if (workcenterModel.getId() > 0) {
			i = jdbcTemplate.update(Sql_DeleteModel_By_Id, args);
		}
		if (i > 0) {
			return true;
		}
		return false;
	}

	private static String Sql_DeleteModel_By_Id = "DELETE FROM jdevice_c_workcenter WHERE id=?";

	/**
	 * 设置工作中心信息，如果不存在则新建
	 * 
	 * @param workcenterModel 中心信息
	 * @return 操作是否成功
	 */
	public boolean setWorkcenterModel(WorkcenterModel workcenterModel) {
		if (StringUtil.isEmpty(workcenterModel.getOrganizationId())) {
			return false;
		}
		int i = 0;
		Object[] args = new Object[7];
		// 修改信息
		if (workcenterModel.getId() > 0) {
			args[i++] = StringUtil.getEmptyString(workcenterModel.getCode());
			args[i++] = StringUtil.getEmptyString(workcenterModel.getName());
			args[i++] = StringUtil.getEmptyString(workcenterModel.getLogoUrl());
			args[i++] = workcenterModel.getWorkshopId();
			args[i++] = workcenterModel.getFactoryId();
			args[i++] = StringUtil.getEmptyString(workcenterModel.getOrganizationId());
			args[i++] = workcenterModel.getId();
			i = jdbcTemplate.update(Sql_UpdateModel_By_Id, args);
		}
		// 新建信息
		else {
			workcenterModel.setId(Starter.IdMaker.nextId());
			args[i++] = workcenterModel.getId();
			args[i++] = StringUtil.getEmptyString(workcenterModel.getCode());
			args[i++] = StringUtil.getEmptyString(workcenterModel.getName());
			args[i++] = StringUtil.getEmptyString(workcenterModel.getLogoUrl());
			args[i++] = workcenterModel.getWorkshopId();
			args[i++] = workcenterModel.getFactoryId();
			args[i++] = StringUtil.getEmptyString(workcenterModel.getOrganizationId());
			i = jdbcTemplate.update(Sql_InsertModel, args);
		}
		if (i > 0) {
			return true;
		}
		return false;
	}

	private static String Sql_UpdateModel_By_Id = "UPDATE jdevice_c_workcenter" + //
			" SET " + //
			"code = ?," + //
			"name = ?," + //
			"logo_url = ?," + //
			"workshop_id = ?," + //
			"factory_id = ?," + //
			"organization_id = ?" + //
			" WHERE " + //
			"id=?";
	private static String Sql_InsertModel = "INSERT INTO jdevice_c_workcenter (" + //
			"id," + //
			"code," + //
			"name," + //
			"logo_url," + //
			"workshop_id," + //
			"factory_id," + //
			"organization_id" + //
			")VALUES(" + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?," + //
			"?" + //
			")";

	/**
	 * 根据工作中心ID获取工作中心列表
	 * 
	 * @param workcenterId 工作中心ID
	 * @return 数据列表
	 */
	public List<WorkcenterModel> getWorkcenterModelsById(long workcenterId) {
		Object[] args = new Object[1];
		args[0] = workcenterId;
		List<WorkcenterModel> list = jdbcTemplate.query(Sql_GetModels_By_Id, args, beanPropertyRowMapper);
		return list;
	}

	private static String Sql_GetModels_By_Id = "SELECT * FROM jdevice_c_workcenter WHERE id = ?";

	/**
	 * 根据工作中心ID获取工作中心
	 * 
	 * @param id 工作中心ID
	 * @return 工作中心
	 */
	public WorkcenterModel getWorkcenterModelById(long id) {
		List<WorkcenterModel> list = getWorkcenterModelsById(id);
		if (list != null && list.size() > 0) {
			return list.get(0);
		}
		return null;
	}

	/**
	 * 根据车间ID获取工作中心列表
	 * 
	 * @param workshopId 车间ID
	 * @return 数据列表
	 */
	public List<WorkcenterModel> getWorkcenterModelsByWorkshopId(long workshopId) {
		Object[] args = new Object[1];
		args[0] = workshopId;
		List<WorkcenterModel> list = jdbcTemplate.query(Sql_GetModels_By_WorkshopId, args, beanPropertyRowMapper);
		return list;
	}

	private static String Sql_GetModels_By_WorkshopId = "SELECT * FROM jdevice_c_workcenter WHERE workshop_id = ?";

	/**
	 * 根据工厂ID获取工作中心列表
	 * 
	 * @param factoryId 工厂ID
	 * @return 数据列表
	 */
	public List<WorkcenterModel> getWorkcenterModelsByFactoryId(long factoryId) {
		Object[] args = new Object[1];
		args[0] = factoryId;
		List<WorkcenterModel> list = jdbcTemplate.query(Sql_GetModels_By_FactoryId, args, beanPropertyRowMapper);
		return list;
	}

	private static String Sql_GetModels_By_FactoryId = "SELECT * FROM jdevice_c_workcenter WHERE factory_id = ? ORDER BY workshop_id DESC";

	/**
	 * 根据组织ID获取工作中心列表
	 * 
	 * @param organizationId 组织ID
	 * @return 数据列表
	 */
	public List<WorkcenterModel> getWorkcenterModelsByOrganizationId(String organizationId) {
		Object[] args = new Object[1];
		args[0] = organizationId;
		List<WorkcenterModel> list = jdbcTemplate.query(Sql_GetModels_By_OrganizationId, args, beanPropertyRowMapper);
		return list;
	}

	private static String Sql_GetModels_By_OrganizationId = "SELECT * FROM jdevice_c_workcenter WHERE organization_id = ? ORDER BY workshop_id DESC";

	public WorkcenterModel initWorkshop(WorkshopModel workshopModel) {
		if (workshopModel==null) {
			return null;
		}
		WorkcenterModel workcenterModel=new WorkcenterModel();
		workcenterModel.setOrganizationId(workshopModel.getOrganizationId());
		workcenterModel.setFactoryId(workshopModel.getFactoryId());
		workcenterModel.setWorkshopId(workshopModel.getId());
		if (setWorkcenterModel(workcenterModel)) {
			return workcenterModel;
		}
		return null;
	}
	
	private BeanPropertyRowMapper<WorkcenterModel> beanPropertyRowMapper = new BeanPropertyRowMapper<WorkcenterModel>(
			WorkcenterModel.class);
	
	public boolean changeFactory(long workshopId,long factoryId) {
		Object[] args = new Object[2];
		int i=0;
		args[i++] = factoryId;
		args[i++] = workshopId;
		return jdbcTemplate.update(Sql_ChangeFactory_By_WorkshopId, args)>0;
	}
	
	private static String Sql_ChangeFactory_By_WorkshopId="UPDATE jdevice_c_workcenter SET factory_id=? WHERE workshop_id=?;";
}
