package cn.sd.zhidetec.jdevice.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import cn.sd.zhidetec.jdevice.client.JdeviceClientInfo;
import cn.sd.zhidetec.jdevice.dao.UserModelDao;
import cn.sd.zhidetec.jdevice.model.DeviceModel;
import cn.sd.zhidetec.jdevice.model.UserModel;
import cn.sd.zhidetec.jdevice.service.JedgeTelegramService;
import cn.sd.zhidetec.jdevice.util.DateUtil;

@Service
public class JdeviceServer {

	@Value("${jandons.ip}")
	protected String serverIP = "127.0.0.1";
	@Value("${jandons.port}")
	protected int listernPort = 5070;
	@Value("${jandons.createSessionTimeoutMS}")
	protected int createSessionTimeoutMS=3000;
	protected Logger logger = LoggerFactory.getLogger(getClass());
	@Autowired
	protected UserModelDao userModelDao;
	
	protected int coreTaskPoolSize=10;
	protected int maxTaskPoolSize=100;
	protected int taskQueueSize=512;
	protected int taskRecvDataTimeout=DateUtil.MILLISECOND_HALF_MINUTE;
	
	protected boolean started = false;
	protected boolean stoped = true;
	protected ServerSocket serverSocket;
	protected ThreadPoolExecutor taskPoolExecutor;
	protected RejectedExecutionHandler rejectedHandler;
	protected BlockingQueue<Runnable> taskQueue = new ArrayBlockingQueue<>(taskQueueSize);
	
	private List<JdeviceClientTask> clientTasks = Collections.synchronizedList(new ArrayList<>());
	private Object clientTasksLockObj = new Object();
	private List<Socket> waitCreateSessionSockets = new ArrayList<>();
	private Object waitCreateSessionSocketsLockObj = new Object();
	private ObjectMapper jsonMapper = new ObjectMapper();
	
	public void start() throws IOException {
		if (!started&&stoped) {
			started=true;
			stoped=false;
			// 数据接收线程池
			taskQueue = new ArrayBlockingQueue<>(taskQueueSize);
			rejectedHandler = new ThreadPoolExecutor.DiscardPolicy();
			taskPoolExecutor = new ThreadPoolExecutor(coreTaskPoolSize,//
					coreTaskPoolSize,//
					maxTaskPoolSize,//
					TimeUnit.SECONDS,//
					taskQueue,//
					rejectedHandler);
			// ServerSocket
			serverSocket=new ServerSocket(listernPort);
			// Socket连接线程
			new Thread(){
				@Override
				public void run() {
					logger.info("AndonMessageServer Started On Port: "+listernPort);
					while (started) {
						try {
							Thread.sleep(1);
							Socket socket = serverSocket.accept();
							synchronized (waitCreateSessionSocketsLockObj) {
								waitCreateSessionSockets.add(socket);	
							}
						} catch (Exception e) {
							// TODO: handle exception
							e.printStackTrace();
						}
					}
				}
			}.start();
			// 会话建立线程
			new Thread(){
				@Override
				public void run() {
					Map<Integer, Long> lastCreateSessionTimeMap=new HashMap<Integer, Long>(); 
					while (started) {
						try {
							Thread.sleep(1);
							//活性诊断
							synchronized (clientTasksLockObj) {
								for (int i = clientTasks.size()-1; i >=0; i--) {
									JdeviceClientTask item = clientTasks.get(i);
									if (item.hadStarted()==false&&item.hadStoped()) {
										item.getJdeviceClientInfo().setConnected(false);
									}else {
										item.getJdeviceClientInfo().setConnected(true);
									}
								}
							}
							//会话
							synchronized (waitCreateSessionSocketsLockObj) {
								for (int i = waitCreateSessionSockets.size()-1; i >=0; i--) {
									Socket socket = waitCreateSessionSockets.get(i);
									try {
										//移除会话超时连接
										if (socket==null||socket.isClosed()||socket.isInputShutdown()||socket.isOutputShutdown()||socket.isConnected()==false) {
											waitCreateSessionSockets.remove(i);
											if (socket!=null) {
												if (lastCreateSessionTimeMap.containsKey(socket.getLocalPort())) {
													lastCreateSessionTimeMap.remove(socket.getLocalPort());
												}
												socket.close();
											}
											continue;
										}
										//会话开始
										Long lastCreateSessionTimeMs=lastCreateSessionTimeMap.get(socket.getLocalPort());
										if (lastCreateSessionTimeMs==null) {
											lastCreateSessionTimeMs=System.currentTimeMillis()+createSessionTimeoutMS;
											lastCreateSessionTimeMap.put(socket.getLocalPort(), lastCreateSessionTimeMs);
										}
										//会话检查
										if (socket.getInputStream().available()>0) {
											JdeviceClientTask clientTask = new JdeviceClientTask();
											clientTask.setSocket(socket);
											StringBuffer stringBuffer = new StringBuffer();
											if (JedgeTelegramService.recvCreateSessionTelegramString(socket, stringBuffer, 100)) {
												JdeviceClientInfo clientInfo = jsonMapper.readValue(stringBuffer.toString(), JdeviceClientInfo.class);
												//身份认证
												List<UserModel> userModels = userModelDao.getUserModelsByPhone(clientInfo.getPhone());
												if (userModels.size()>0) {
													UserModel userModel=userModels.get(0);
													//身份确认完成
													if (userModel!=null//
															&&userModel.getOrganizationId().equals(""+clientInfo.getOrganizationId())//
															&&userModel.getPassword().equals(""+clientInfo.getPassword())) {
														clientTask.setJdeviceClientInfo(clientInfo);
														clientInfo.setUserModel(userModel);
														synchronized (clientTasksLockObj) {
															boolean isNewJedgeModel=true;
															for (JdeviceClientTask item : clientTasks) {
																if (item!=null//
																		&&item.getJdeviceClientInfo()!=null//
																		&&clientInfo!=null//
																		&&item.getJdeviceClientInfo().equals(clientInfo)) {
																	item.setSocket(socket);
																	item.setJdeviceClientInfo(clientInfo);
																	item.startThread();
																	isNewJedgeModel=false;
																	break;
																}
															}
															if (isNewJedgeModel) {
																clientTask.startThread();
																clientTasks.add(clientTask);	
															}
														}
														//会话创建成功
														socket.getOutputStream().write(JedgeTelegramService.getCreateSessionResponse());
														System.out.println("会话创建成功:"+clientInfo.getPhone());		
													}else {
														socket.close();
													}
												}else {
													socket.close();
												}
											}
											waitCreateSessionSockets.remove(i);
											lastCreateSessionTimeMap.remove(socket.getLocalPort());
										}
										//超时后关闭
										else if (lastCreateSessionTimeMs<System.currentTimeMillis()) {
											socket.close();
											waitCreateSessionSockets.remove(i);
											lastCreateSessionTimeMap.remove(socket.getLocalPort());
											continue;
										}
									} catch (Exception e) {
										try {
											//移除连接
											waitCreateSessionSockets.remove(i);
											if (socket!=null) {
												if (lastCreateSessionTimeMap.containsKey(socket.getLocalPort())) {
													lastCreateSessionTimeMap.remove(socket.getLocalPort());
												}
												socket.close();
											}
										} catch (Exception e2) {
										}
									}
								}
							}
						} catch (Exception e) {
							// TODO: handle exception
							e.printStackTrace();
						}
					}
					stoped=true;
				}
			}.start();
		}
	}
	
	/**
	 * 停止服务
	 */
	public void stop() {
		started = false;
	}

	/**
	 * 当前服务是否已经启动
	 * 
	 * @return true 是，false 否
	 */
	public boolean hadStarted() {
		return started;
	}

	/**
	 * 当前服务是否已经成功停止
	 * 
	 * @return true 是，false 否
	 */
	public boolean hadStoped() {
		return stoped;
	}

	/**
	 * 获取注册服务端口
	 * 
	 * @return 端口号
	 */
	public int getListernPort() {
		return listernPort;
	}

	/**
	 * 设置注册服务端口
	 * 
	 * @param listernPort 端口号
	 */
	public void setListernPort(int listernPort) {
		this.listernPort = listernPort;
	}
	
	/**
	 * 发送设备状态
	 * */
	public void changeDeviceStatus(DeviceModel deviceModel,DeviceModel deviceInfoModel) {
		if (deviceModel==null) {
			return;
		}
		synchronized (clientTasksLockObj) {
			for (JdeviceClientTask clientTask : clientTasks) {
				JdeviceClientInfo clientInfo=clientTask.getJdeviceClientInfo();
				if (clientTask!=null//
						&&clientInfo.getOrganizationId()!=null
						&&deviceInfoModel.getOrganizationId()!=null
						&&clientInfo.getOrganizationId().equals(deviceInfoModel.getOrganizationId())) {
					clientTask.sendDeviceStatus(deviceModel,deviceInfoModel);
				}
			}
		}
	}
	
}
