package cn.sd.zhidetec.jdevice.controller;

import javax.servlet.http.HttpSession;

import cn.sd.zhidetec.jdevice.model.WorkcenterModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.sd.zhidetec.jdevice.HttpResponseMsg;
import cn.sd.zhidetec.jdevice.dao.AndonReasonModelDao;
import cn.sd.zhidetec.jdevice.model.RoleModel;
import cn.sd.zhidetec.jdevice.model.UserModel;
import cn.sd.zhidetec.jdevice.model.WorkshopModel;
import cn.sd.zhidetec.jdevice.service.DeviceTypeService;
import cn.sd.zhidetec.jdevice.service.RBuilderService;
import cn.sd.zhidetec.jdevice.service.ShiftService;
import cn.sd.zhidetec.jdevice.service.WorkcenterService;
import cn.sd.zhidetec.jdevice.service.WorkshopService;

import java.util.List;

/**
 * 车间管理Controller
 * */
@Controller
@RequestMapping("/workshop")
public class WorkshopController {

	@Autowired
	protected WorkshopService workshopModelDao;
	@Autowired
	protected WorkcenterService workcenterService;
	@Autowired
	protected ShiftService shiftService;
	@Autowired
	protected DeviceTypeService deviceTypeService;
	@Autowired
	protected AndonReasonModelDao andonReasonModelDao;
	@Autowired
	protected RBuilderService rBuilderService;
	@Autowired
	protected WorkshopService workshops;
	protected Logger logger = LoggerFactory.getLogger(getClass());
	
	/**
	 * 根据车间ID获取车间
	 * 
	 * @param httpSession  Session会话
	 * @param id id
	 * @return 返回给前端的操作结果
	 */
	@RequestMapping(path = "/getWorkshopModelById")
	@ResponseBody
	public RBuilderService.Response getWorkshopModelById(HttpSession httpSession,@RequestParam("id")long id) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_User, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		// 根据用户权限返回数据
		UserModel userModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		WorkshopModel workshopModel = workshopModelDao.getWorkshopModelById(id);
		if (workshopModel!=null) {
			if (userModel.getRoleLevel() <= RoleModel.ROLE_LEVEL_WorkshopAdmin//
					&&userModel.getWorkshopId()==workshopModel.getId()) {
				return rBuilderService.update(response, //
						HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, //
						workshopModel);
			}
			if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_FactoryAdmin//
					&&userModel.getFactoryId()==workshopModel.getFactoryId()) {
				return rBuilderService.update(response, //
						HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, //
						workshopModel);
			}
			if (userModel.getRoleLevel() >= RoleModel.ROLE_LEVEL_OrganizationAdmin//
					&&userModel.getOrganizationId().equals(workshopModel.getOrganizationId())) {
				return rBuilderService.update(response, //
						HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, //
						workshopModel);
			}
		}
		return rBuilderService.update(response, //
				HttpResponseMsg.HEAD_MSG_NOT_FOUND_DATA, //
				null);
	}

	/**
	 * 获取车间信息
	 * 
	 * @param httpSession Session会话
	 * @return 返回给前端的操作结果
	 */
	@RequestMapping(path = "/getWorkshopModels")
	@ResponseBody
	public RBuilderService.Response getWorkshopModels(HttpSession httpSession) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_User, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		// 根据用户权限返回数据
		UserModel userModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		if (userModel.getRoleLevel() <= RoleModel.ROLE_LEVEL_WorkshopAdmin) {
			return rBuilderService.update(response, //
					HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, //
					workshopModelDao.getWorkshopModelsById(userModel.getWorkshopId()));
		}
		if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_FactoryAdmin) {
			return rBuilderService.update(response, //
					HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, //
					workshopModelDao.getWorkshopModelsByFactoryId(userModel.getFactoryId()));
		}
		if (userModel.getRoleLevel() >= RoleModel.ROLE_LEVEL_OrganizationAdmin) {
			return rBuilderService.update(response, //
					HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, //
					workshopModelDao.getWorkshopModelsByOrganizationId(userModel.getOrganizationId()));
		}
		return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_DATA, null);
	}

	/**
	 * 设置车间信息
	 * 
	 * @param httpSession    Session会话
	 * @param warnLevelModel 预警级别
	 * @return 返回给前端的操作结果
	 */
	@RequestMapping(path = "/setWorkshopModel")
	@ResponseBody
	public RBuilderService.Response setWorkshopModel(HttpSession httpSession, //
			@RequestBody WorkshopModel workshopModel) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_WorkshopAdmin, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		// 根据用户权限操作数据
		UserModel userModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		//新建
		boolean isNew=false;
		if (workshopModel.getId()<=0) {
			workshopModel.setOrganizationId(userModel.getOrganizationId());
			isNew=true;
		}
		if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_WorkshopAdmin) {
			if (userModel.getWorkshopId()==workshopModel.getId()//
					&& userModel.getFactoryId()==workshopModel.getFactoryId()//
					&& userModel.getOrganizationId().equals(workshopModel.getOrganizationId())) {
				if (workshopModelDao.setWorkshopModel(workshopModel)) {
					//默认值
					if (isNew) {
						workcenterService.initWorkshop(workshopModel);
						deviceTypeService.initWorkshop(workshopModel);
						shiftService.initWorkshop(workshopModel);
						andonReasonModelDao.initWorkshop(workshopModel);
					}
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, null);
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
				}
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
			}
		}
		if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_FactoryAdmin) {
			if (userModel.getFactoryId()==workshopModel.getFactoryId()//
					&& userModel.getOrganizationId().equals(workshopModel.getOrganizationId())) {
				if (workshopModelDao.setWorkshopModel(workshopModel)) {
					//默认值
					if (isNew) {
						workcenterService.initWorkshop(workshopModel);
						deviceTypeService.initWorkshop(workshopModel);
						shiftService.initWorkshop(workshopModel);
						andonReasonModelDao.initWorkshop(workshopModel);
					}else{

                    }
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, null);
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
				}
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
			}
		}
		if (userModel.getRoleLevel() >= RoleModel.ROLE_LEVEL_OrganizationAdmin) {
			if (userModel.getOrganizationId().equals(workshopModel.getOrganizationId())) {
				if (workshopModelDao.setWorkshopModel(workshopModel)) {
					//默认值
					if (isNew) {
						workcenterService.initWorkshop(workshopModel);
						deviceTypeService.initWorkshop(workshopModel);
						shiftService.initWorkshop(workshopModel);
						andonReasonModelDao.initWorkshop(workshopModel);
					}
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, null);
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
				}
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
			}
		}
		return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
	}

	/**
	 * 从Session中获取用户信息，然后删除车间
	 * 
	 * @param httpSession Session
	 * @return 操作结果
	 */
	@RequestMapping(path = "/deleteWorkshopModel")
	@ResponseBody
	public RBuilderService.Response deleteWorkshopModel(HttpSession httpSession, //
			@RequestBody WorkshopModel workshopModel) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_FactoryAdmin, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		// 根据用户权限操作数据
		UserModel userModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_FactoryAdmin) {
			if (userModel.getFactoryId()==workshopModel.getFactoryId()//
					&& userModel.getOrganizationId().equals(workshopModel.getOrganizationId())) {
				if (workshopModelDao.deleteWorkshopModel(workshopModel)) {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, null);
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
				}
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
			}
		}
		if (userModel.getRoleLevel() >= RoleModel.ROLE_LEVEL_OrganizationAdmin) {
			if (userModel.getOrganizationId().equals(workshopModel.getOrganizationId())) {
				if (workshopModelDao.deleteWorkshopModel(workshopModel)) {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, null);
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
				}
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
			}
		}
		return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
	}


    /**
     * 根据工厂id获取车间信息
     * @param factoryId
     * @return
     */
    @RequestMapping(path = "findByFactoryId")
    @ResponseBody
	public RBuilderService.Response getWorkshopModelsByFactoryId(long factoryId){
        RBuilderService.Response response = rBuilderService.build(null,null);
        List<WorkshopModel> workshopModelsList = workshops.getWorkshopModelsByFactoryId(factoryId);
        if (workshopModelsList.size() > 0){
            return rBuilderService.update(response,HttpResponseMsg.HEAD_MSG_SUCCESS_DONE,workshopModelsList);
        }else {
            return rBuilderService.update(response,HttpResponseMsg.HEAD_MSG_ERROR_PARAM,null);
        }


    }






}
