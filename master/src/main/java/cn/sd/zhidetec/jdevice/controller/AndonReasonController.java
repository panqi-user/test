package cn.sd.zhidetec.jdevice.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.sd.zhidetec.jdevice.HttpResponseMsg;
import cn.sd.zhidetec.jdevice.dao.AndonReasonModelDao;
import cn.sd.zhidetec.jdevice.model.AndonReasonModel;
import cn.sd.zhidetec.jdevice.model.RoleModel;
import cn.sd.zhidetec.jdevice.model.UserModel;
import cn.sd.zhidetec.jdevice.service.RBuilderService;

@Controller
@RequestMapping("/andon")
public class AndonReasonController {

	@Autowired
	protected AndonReasonModelDao andonReasonModelDao;
	@Autowired
	protected RBuilderService rBuilderService;
	protected Logger logger = LoggerFactory.getLogger(getClass());
	
	/**
	 * 获取原因标签
	 */
	@RequestMapping(path = "/getReasonLabelList")
	@ResponseBody
	public RBuilderService.Response getReasonLabelList(HttpSession httpSession, //
			@RequestParam("workshopId") long workshopId, //
			@RequestParam("factoryId") long factoryId) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_User, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		// 获取原因标签
		if (workshopId <= 0) {
			return rBuilderService.build(HttpResponseMsg.HEAD_MSG_ERROR_PARAM, workshopId);
		} else {
			// 为空则重新进行初始化
			List<AndonReasonModel> list = andonReasonModelDao.getReasonModelsByWorkshopId(workshopId);
			return rBuilderService.build(HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, list);
		}
	}

	@RequestMapping(path = "/deleteAndonReasonModel")
	@ResponseBody
	public RBuilderService.Response deleteAndonReasonModel(HttpSession httpSession, //
			@RequestBody AndonReasonModel andonReasonModel) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_WorkshopAdmin, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		// 根据用户权限操作数据
		UserModel userModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_WorkshopAdmin) {
			if (userModel.getWorkshopId() == andonReasonModel.getWorkshopId()//
					&& userModel.getFactoryId() == andonReasonModel.getFactoryId()//
					&& userModel.getOrganizationId().equals(andonReasonModel.getOrganizationId())) {
				if (andonReasonModelDao.deleteReasonModelById(andonReasonModel.getId())) {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, null);
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
				}
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
			}
		}
		if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_FactoryAdmin) {
			if (userModel.getFactoryId() == andonReasonModel.getFactoryId()//
					&& userModel.getOrganizationId().equals(andonReasonModel.getOrganizationId())) {
				if (andonReasonModelDao.deleteReasonModelById(andonReasonModel.getId())) {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, null);
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
				}
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
			}
		}
		if (userModel.getRoleLevel() >= RoleModel.ROLE_LEVEL_OrganizationAdmin) {
			if (userModel.getOrganizationId().equals(andonReasonModel.getOrganizationId())) {
				if (andonReasonModelDao.deleteReasonModelById(andonReasonModel.getId())) {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, null);
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
				}
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
			}
		}
		return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
	}

	@RequestMapping(path = "/setAndonReasonModel")
	@ResponseBody
	public RBuilderService.Response setAndonReasonModel(HttpSession httpSession, //
			@RequestBody AndonReasonModel andonReasonModel) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_User, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		// 根据用户权限操作数据
		UserModel userModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		// 新建
		if (andonReasonModel.getId() <= 0) {
			andonReasonModel.setOrganizationId(userModel.getOrganizationId());
		}
		if (userModel.getRoleLevel() <= RoleModel.ROLE_LEVEL_WorkshopAdmin) {
			if (userModel.getWorkshopId() == andonReasonModel.getWorkshopId()//
					&& userModel.getFactoryId() == andonReasonModel.getFactoryId()//
					&& userModel.getOrganizationId().equals(andonReasonModel.getOrganizationId())) {
				if (andonReasonModelDao.setReasonModel(andonReasonModel)) {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, null);
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
				}
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
			}
		}
		if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_FactoryAdmin) {
			if (userModel.getFactoryId() == andonReasonModel.getFactoryId()//
					&& userModel.getOrganizationId().equals(andonReasonModel.getOrganizationId())) {
				if (andonReasonModelDao.setReasonModel(andonReasonModel)) {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, null);
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
				}
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
			}
		}
		if (userModel.getRoleLevel() >= RoleModel.ROLE_LEVEL_OrganizationAdmin) {
			if (userModel.getOrganizationId().equals(andonReasonModel.getOrganizationId())) {
				if (andonReasonModelDao.setReasonModel(andonReasonModel)) {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, null);
				} else {
					return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
				}
			} else {
				return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_PERMISSION, null);
			}
		}
		return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_FAIL_DONE, null);
	}

	/**
	 * 根据ID获取Andon消息原因
	 * 
	 * @param httpSession Session会话
	 * @param id          设备id
	 * @return 返回给前端的操作结果
	 */
	@RequestMapping(path = "/getAndonReasonModelById")
	@ResponseBody
	public RBuilderService.Response getAndonReasonModelById(HttpSession httpSession, @RequestParam("id") long id) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_User, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		// 根据用户权限返回数据
		UserModel userModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		AndonReasonModel andonReasonModel = andonReasonModelDao.getAndonReasonModelById(id);
		if (andonReasonModel != null) {
			if (userModel.getRoleLevel() <= RoleModel.ROLE_LEVEL_WorkshopAdmin//
					&& userModel.getWorkshopId() == andonReasonModel.getWorkshopId()) {
				return rBuilderService.update(response, //
						HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, //
						andonReasonModel);
			}
			if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_FactoryAdmin//
					&& userModel.getFactoryId() == andonReasonModel.getFactoryId()) {
				return rBuilderService.update(response, //
						HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, //
						andonReasonModel);
			}
			if (userModel.getRoleLevel() >= RoleModel.ROLE_LEVEL_OrganizationAdmin//
					&& userModel.getOrganizationId().equals(andonReasonModel.getOrganizationId())) {
				return rBuilderService.update(response, //
						HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, //
						andonReasonModel);
			}
		}
		return rBuilderService.update(response, //
				HttpResponseMsg.HEAD_MSG_NOT_FOUND_DATA, //
				null);
	}

	@RequestMapping(path = "/getAndonReasonModels")
	@ResponseBody
	public RBuilderService.Response getAndonReasonModels(HttpSession httpSession) {
		RBuilderService.Response response = rBuilderService.build(null, null);
		// 用户权限判断
		if (LoginController.hadPermission(httpSession, //
				RoleModel.ROLE_LEVEL_User, //
				rBuilderService, //
				response) == false) {
			return response;
		}
		// 根据用户权限返回数据
		UserModel userModel = (UserModel) httpSession.getAttribute(LoginController.SESSION_KEY_LOGIN_USER);
		if (userModel.getRoleLevel() <= RoleModel.ROLE_LEVEL_WorkshopAdmin) {
			return rBuilderService.update(response, //
					HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, //
					andonReasonModelDao.getReasonModelsByWorkshopId(userModel.getWorkshopId()));
		}
		if (userModel.getRoleLevel() == RoleModel.ROLE_LEVEL_FactoryAdmin) {
			return rBuilderService.update(response, //
					HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, //
					andonReasonModelDao.getReasonModelsByFactoryId(userModel.getFactoryId()));
		}
		if (userModel.getRoleLevel() >= RoleModel.ROLE_LEVEL_OrganizationAdmin) {
			return rBuilderService.update(response, //
					HttpResponseMsg.HEAD_MSG_SUCCESS_DONE, //
					andonReasonModelDao.getReasonModelsByOrganizationId(userModel.getOrganizationId()));
		}
		return rBuilderService.update(response, HttpResponseMsg.HEAD_MSG_NO_DATA, null);
	}


    /**
     * 根据车间id获取andon原因标签list
     * @param workshopId
     * @return
     */
    @RequestMapping(path = "/findReasionByWorkshopId")
    @ResponseBody
    public RBuilderService.Response findReasionByWorkShopId(long workshopId) {

        RBuilderService.Response response = rBuilderService.build(null, null);
        List<AndonReasonModel> reasionList = andonReasonModelDao.getReasonModelsByWorkshopId(workshopId);
        if (reasionList.size()!=0){

            return rBuilderService.update(response,HttpResponseMsg.HEAD_MSG_SUCCESS_DONE,reasionList);

        }else{
            return rBuilderService.update(response,HttpResponseMsg.HEAD_MSG_ERROR_PARAM,null);
        }


    }

	
}
